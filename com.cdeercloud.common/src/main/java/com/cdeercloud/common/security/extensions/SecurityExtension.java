package com.cdeercloud.common.security.extensions;

import java.util.Properties;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.definition.BeanDefinitionInitializer;
import com.cdeercloud.common.security.pwd.impl.CdeerHashEncoderImpl;
import com.cdeercloud.common.security.pwd.impl.CdeerPlainTextEncoderImpl;

public class SecurityExtension implements BeanDefinitionInitializer {
	private final static String RDBMS_REALM_BEAN_ID = "framework_security_hrmsRealm";
	private final static String RDBMS_CACHE_BEAN_ID = "framework_security_hrmsCache";
	private final static String HASHED_CREDENTIALS_MATCHER_BEAN_ID = "framework_security_hashedCredentialsMatcher";
	private final static String PASSWORD_ENCODER_BEAN_ID = "framework_security_passwordEncoder";

	@Override
	public void init(BeanDefinitionRegistry registry, Properties properties) {
		registerSecurityManager(registry, properties);
	}

	private void registerSecurityManager(BeanDefinitionRegistry registry, Properties properties) {
		RootBeanDefinition def = new RootBeanDefinition(DefaultWebSecurityManager.class);
		registry.registerBeanDefinition("framework_security_securityManager", def);

		String algorith = properties.getProperty("security.hashAlgorithmName");
		if (StringUtils.hasText(algorith)) {
			RootBeanDefinition matcherDef = new RootBeanDefinition(HashedCredentialsMatcher.class);
			matcherDef.getConstructorArgumentValues().addIndexedArgumentValue(0, "${security.hashAlgorithmName}");
			registry.registerBeanDefinition(HASHED_CREDENTIALS_MATCHER_BEAN_ID, matcherDef);

			RootBeanDefinition encoderDef = new RootBeanDefinition(CdeerHashEncoderImpl.class);
			encoderDef.getConstructorArgumentValues().addIndexedArgumentValue(0, "${security.hashAlgorithmName}");
			registry.registerBeanDefinition(PASSWORD_ENCODER_BEAN_ID, encoderDef);
		} else {
			RootBeanDefinition encoderDef = new RootBeanDefinition(CdeerPlainTextEncoderImpl.class);
			registry.registerBeanDefinition(PASSWORD_ENCODER_BEAN_ID, encoderDef);
		}
	}

}
