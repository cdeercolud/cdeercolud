package com.cdeercloud.common.security.pwd;

public interface CdeerHashEncoder {
	/**
	 * 用户密码加密。
	 * 
	 * @param pwd 密码
	 * @param salt 可以使用用户相关属性作为salt，比较loginId
	 * @return
	 */
	public String encode(String pwd, String salt);
}
