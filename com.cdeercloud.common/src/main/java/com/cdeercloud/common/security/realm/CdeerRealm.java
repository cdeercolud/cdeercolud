package com.cdeercloud.common.security.realm;

import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CdeerRealm extends AuthorizingRealm {
	final Logger logger = LoggerFactory.getLogger(CdeerRealm.class);
	// protected PermissionService permissionService;

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		logger.info("开始本地登录校验。");
		String username = (String) token.getPrincipal();

		CredentialsMatcher matcher = this.getCredentialsMatcher();

		return new SimpleAuthenticationInfo(username, "mk1q2w3e4r!", this.getName());

	}

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		Object primaryPrincipal = principals.getPrimaryPrincipal();
		if (primaryPrincipal == null) {
			return null;
		}

		@SuppressWarnings("unchecked")
		List<String> list = principals.asList();
		if (list.size() < 2) {
			throw new AuthenticationException();
		}
		String userId = list.get(1);
		// Collection<Permission> permissions = permissionService.findPermision(userId);
		// info.addObjectPermissions(permissions);
		return null;
	}

	@Override
	public boolean supports(AuthenticationToken token) {
		// TODO 在这是判断是内部用户，外部用户
		return super.supports(token);
	}

}
