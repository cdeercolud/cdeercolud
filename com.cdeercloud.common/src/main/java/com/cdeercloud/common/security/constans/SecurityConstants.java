package com.cdeercloud.common.security.constans;

/**
 * 
 * @author looyii
 *
 */
public class SecurityConstants {

	/**
	 * 当前用户Profile存放于Session的key
	 */
	public final static String SESSION_PROFILE_KEY = "session_user_key";

	public final static String REQUEST_TOKEN_KEY = "request_token";

	public final static String BEAN_ID_SESSION_DAO = "framework_security_sessionDAO";
}
