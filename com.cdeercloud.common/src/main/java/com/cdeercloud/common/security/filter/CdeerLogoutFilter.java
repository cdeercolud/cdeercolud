package com.cdeercloud.common.security.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cdeercloud.common.cache.CacheService;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.login.vo.Profile;
import com.cdeercloud.common.security.constans.TokenConstants;
import com.cdeercloud.common.util.JWTUtil;
import com.cdeercloud.common.util.JsonUtils;
import com.cdeercloud.common.util.PlatformUtil;

public class CdeerLogoutFilter extends LogoutFilter {

	@Autowired
	private CacheService cacheService;

	@Autowired
	private ITokenUtil tokenUtil;

	private String authHeaderName;

	@Override
	protected void issueRedirect(ServletRequest request, ServletResponse response, String redirectUrl) throws Exception {
		String token = this.tokenUtil.getToken();
		if (token != null) {
			// 返回登录平台
			String platform = PlatformUtil.getPlatform(WebUtils.toHttp(request).getHeader("user-agent"));
			Profile profile = JsonUtils.fromJson(JWTUtil.getProfile(token), Profile.class);
			if (profile != null) {
				String redisKey = String.format(TokenConstants.LOGIN_TOKEN, platform, profile.getId());
				boolean isExist = this.cacheService.exists(redisKey);
				if (isExist) {
					this.cacheService.delete(redisKey);
				}
			}
		}
		response.getWriter().write("success");
		response.flushBuffer();
	}

	public String getAuthHeaderName() {
		return authHeaderName;
	}

	public void setAuthHeaderName(String authHeaderName) {
		this.authHeaderName = authHeaderName;
	}

}
