package com.cdeercloud.common.security.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.util.WebUtils;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.util.CorsUtils;

public class CdeerOptionsFilter implements Filter {
	private Set<String> ips = new HashSet<>();
	private boolean corsProxy;
	private String authHeader;
	private String corsHeaders;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String method = WebUtils.toHttp(request).getMethod();
		if ("options".equals(method.toLowerCase())) {
			CorsUtils.addCorsSupport(WebUtils.toHttp(request), WebUtils.toHttp(response), this.ips, CorsUtils.isAnyAllow(ips), this.corsProxy);
			WebUtils.toHttp(response).addHeader("Access-Control-Allow-Headers", this.getAllAuthHeader());
			WebUtils.toHttp(response).addHeader("Access-Control-Allow-Methods", "POST,GET");
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
		//
	}

	private String getAllAuthHeader() {
		StringBuffer headers = new StringBuffer();
		if (StringUtils.hasText(this.authHeader)) {
			headers.append(this.authHeader);
		}
		if (StringUtils.hasText(this.corsHeaders)) {
			if (headers.length() > 0) {
				headers.append(",");
			}
			headers.append(this.corsHeaders);
		}
		return headers.toString();
	}

	public void setCorsIps(String corsIps) {
		if (StringUtils.hasText(corsIps)) {
			for (String ip : corsIps.split(",")) {
				this.ips.add(ip);
			}
		}
	}

	public void setCorsProxy(boolean corsProxy) {
		this.corsProxy = corsProxy;
	}

	public void setAuthHeader(String authHeader) {
		this.authHeader = authHeader;
	}

	public void setCorsHeaders(String corsHeaders) {
		this.corsHeaders = corsHeaders;
	}
}
