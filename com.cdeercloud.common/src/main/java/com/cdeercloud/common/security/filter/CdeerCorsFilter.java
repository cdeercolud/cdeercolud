package com.cdeercloud.common.security.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.util.WebUtils;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.util.CorsUtils;

/**
 * 过滤ip和rest请求，不符合要求的ip和http请求不通过
 * 
 * @author looyii
 *
 */
public class CdeerCorsFilter implements Filter {

	private Set<String> ips = new HashSet<String>();
	private boolean corsProxy;
	private String authHeader;
	private String corsHeaders;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		boolean anyAllow = CorsUtils.isAnyAllow(this.ips);
		CorsUtils.addCorsSupport(WebUtils.toHttp(request), WebUtils.toHttp(response), this.ips, anyAllow, this.corsProxy);
		// 允许跨域请求包含content-type头
		WebUtils.toHttp(response).addHeader("Access-Control-Allow-Headers", this.getAllAuthHeader());
		// 允许POST，GET请求
		WebUtils.toHttp(response).addHeader("Access-Control-Allow-Methods", "POST,GET");
		chain.doFilter(request, response);

	}

	private String getAllAuthHeader() {
		StringBuffer headers = new StringBuffer();
		if (StringUtils.hasText(this.authHeader)) {
			headers.append(this.authHeader);
		}
		if (StringUtils.hasText(this.corsHeaders)) {
			if (headers.length() > 0) {
				headers.append(",");
			}
			headers.append(this.corsHeaders);
		}
		return headers.toString();
	}

	public void setCorsIps(String corsIps) {
		if (StringUtils.hasText(corsIps)) {
			for (String ip : corsIps.split(",")) {
				this.ips.add(ip);
			}
		}
	}

	public void setCorsProxy(boolean corsProxy) {
		this.corsProxy = corsProxy;
	}

	/**
	 * @param authHeader the authHeader to set
	 */
	public void setAuthHeader(String authHeader) {
		this.authHeader = authHeader;
	}

	/**
	 * @param corsHeaders the corsHeaders to set
	 */
	public void setCorsHeaders(String corsHeaders) {
		this.corsHeaders = corsHeaders;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
