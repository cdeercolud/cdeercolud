package com.cdeercloud.common.security.pwd.impl;

import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Hash;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.crypto.hash.Sha256Hash;

import com.cdeercloud.common.security.pwd.CdeerHashEncoder;

public class CdeerHashEncoderImpl extends DefaultHashService implements CdeerHashEncoder {

	public CdeerHashEncoderImpl(String hashAlgorithmName) {
		super();
		this.setHashAlgorithmName(hashAlgorithmName);
	}

	@Override
	public String encode(String pwd, String salt) {
		HashRequest build = new HashRequest.Builder().setSource(pwd).setSalt(salt).build();
		Hash ch = this.computeHash(build);
		return ch.toHex();
	}

	public static void main(String[] args) {
		String encode = new CdeerHashEncoderImpl(Sha256Hash.ALGORITHM_NAME).encode("123456", "25c4b67a6ca211e8a5b446ad91d16481");
		System.out.println(encode);

	}

}
