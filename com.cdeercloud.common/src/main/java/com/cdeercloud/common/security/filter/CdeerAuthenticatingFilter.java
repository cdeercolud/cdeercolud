package com.cdeercloud.common.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cdeercloud.common.cache.CacheService;
import com.cdeercloud.common.login.LoginSecondsEnum;
import com.cdeercloud.common.login.service.ILoginService;
import com.cdeercloud.common.login.vo.Profile;
import com.cdeercloud.common.security.constans.TokenConstants;
import com.cdeercloud.common.util.JWTUtil;
import com.cdeercloud.common.util.JsonUtils;
import com.cdeercloud.common.util.PlatformUtil;

/**
 * 用户登录token生成
 * 
 * @author looyii
 *
 */

public class CdeerAuthenticatingFilter implements Filter {
	final Logger logger = LoggerFactory.getLogger(CdeerAuthenticatingFilter.class);
	public static final String DEFAULT_LOGINID_PARAM = "loginId";
	public static final String DEFAULT_PASSWORD_PARAM = "password";
	public static final String POST_METHOD = "POST";
	private String loginIdParam = DEFAULT_LOGINID_PARAM;
	private String passwordParam = DEFAULT_PASSWORD_PARAM;

	@Autowired
	private ILoginService loginService;
	@Autowired
	private CacheService cacheService;

	protected boolean isLoginSubmission(ServletRequest request, ServletResponse response) {
		boolean isHttp = request instanceof HttpServletRequest;
		return isHttp && WebUtils.toHttp(request).getMethod().equalsIgnoreCase(POST_METHOD);
	}

	public String getLoginIdParam() {
		return loginIdParam;
	}

	public void setLoginIdParam(String loginIdParam) {
		this.loginIdParam = loginIdParam;
	}

	public String getPasswordParam() {
		return passwordParam;
	}

	public void setPasswordParam(String passwordParam) {
		this.passwordParam = passwordParam;
	}

	protected String getLoginId(ServletRequest request) {
		return WebUtils.getCleanParam(request, getLoginIdParam());
	}

	protected String getPassword(ServletRequest request) {
		return WebUtils.getCleanParam(request, getPasswordParam());
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String loginId = this.getLoginId(request);
		logger.info(loginId + "：登录开始！");
		if (!isLoginSubmission(request, response)) {
			logger.info(loginId + "：登录失败！");
			WebUtils.toHttp(response).sendError(HttpServletResponse.SC_BAD_REQUEST);
		}

		String password = this.getPassword(request);
		// 返回登录平台
		String platform = PlatformUtil.getPlatform(WebUtils.toHttp(request).getHeader("user-agent"));
		// 获取登录信息
		Profile profile = this.loginService.findProfile(loginId, password);
		profile.setPlatform(platform);
		// 生成token
		String token = JWTUtil.sign(JsonUtils.toJson(profile));
		if (PlatformUtil._IOS.equals(platform) || PlatformUtil._ANDROID.equals(platform)) {
			this.otherToken(profile);
			// APP登录token保留时长
			this.cacheService.setex(String.format(TokenConstants.LOGIN_TOKEN, platform, profile.getId()), token, LoginSecondsEnum.APP_SECONDS.getKey());
		} else {
			// web登录token保留时长
			this.cacheService.setex(String.format(TokenConstants.LOGIN_TOKEN, platform, profile.getId()), token, LoginSecondsEnum.WEB_SECONDS.getKey());
		}
		response.getWriter().write(token);
		response.flushBuffer();
		logger.info(loginId + "：登录成功！");
		// chain.doFilter(request, response);

	}

	@Override
	public void destroy() {
		//
	}

	/**
	 * 删除另外移动设备token
	 * 
	 * @param profile
	 */
	private void otherToken(Profile profile) {
		profile.setPlatform(PlatformUtil._IOS);
		String token = JWTUtil.sign(JsonUtils.toJson(profile));
		if (JWTUtil.verify(token, JsonUtils.toJson(profile))) {
			this.cacheService.delete(token);
			// TODO 提醒另外一个手机退出登录

			return;
		}

		profile.setPlatform(PlatformUtil._ANDROID);
		token = JWTUtil.sign(JsonUtils.toJson(profile));
		if (JWTUtil.verify(token, JsonUtils.toJson(profile))) {
			this.cacheService.delete(token);
			// TODO 提醒另外一个手机退出登录

			return;
		}

	}

}
