package com.cdeercloud.common.security.filter;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.cache.CacheService;
import com.cdeercloud.common.core.config.ConfigHolder;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.login.LoginSecondsEnum;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.login.vo.Profile;
import com.cdeercloud.common.security.constans.TokenConstants;
import com.cdeercloud.common.util.JWTUtil;
import com.cdeercloud.common.util.JsonUtils;
import com.cdeercloud.common.util.PlatformUtil;

public class CdeerUserFilter extends AccessControlFilter {
	final Logger logger = LoggerFactory.getLogger(CdeerUserFilter.class);

	private List<String> publicUrlPatterns;

	private String authHeaderName;

	@Autowired
	private CacheService cacheService;

	@Autowired
	private ITokenUtil tokenUtil;

	@Resource
	private ConfigHolder holder;

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
		if (this.isLoginRequest(request, response)) {
			return true;
		}
		if (this.isPublicUrl(request)) {
			return true;
		} else {
			Subject subject = this.getSubject(request, response);
			return subject.getPrincipal() != null;
		}
	}

	private boolean isPublicUrl(ServletRequest request) {
		if ((this.publicUrlPatterns == null) || this.publicUrlPatterns.isEmpty()) {
			return false;
		}
		String requestUrl = this.getPathWithinApplication(request);
		for (String pattern : this.publicUrlPatterns) {
			if (this.pathsMatch(pattern, requestUrl)) {
				logger.info("public access url:[" + requestUrl + "] by pattern:[" + pattern + "]");
				return true;
			}
		}
		return false;
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		String token = tokenUtil.getToken();
		if (StringUtils.hasText(token)) {
			// 返回登录平台
			String platform = PlatformUtil.getPlatform(WebUtils.toHttp(request).getHeader("user-agent"));
			Profile profile = JsonUtils.fromJson(JWTUtil.getProfile(token), Profile.class);
			boolean isExist = this.cacheService.exists(String.format(TokenConstants.LOGIN_TOKEN, platform, profile.getId()));
			if (isExist) {
				// 更新redis的token过期时间
				if (PlatformUtil._IOS.equals(platform) || PlatformUtil._ANDROID.equals(platform)) {
					// APP登录token保留时长
					this.cacheService.setex(String.format(TokenConstants.LOGIN_TOKEN, platform, profile.getId()), token, LoginSecondsEnum.APP_SECONDS.getKey());
				} else {
					// web登录token保留时长
					this.cacheService.setex(String.format(TokenConstants.LOGIN_TOKEN, platform, profile.getId()), token, LoginSecondsEnum.WEB_SECONDS.getKey());
				}
				return true;
			} else {
				throw new CdeerException("session过期，请重新登录！");
			}

		}
		WebUtils.toHttp(response).sendError(HttpServletResponse.SC_FORBIDDEN);
		return false;
	}

	public void setPublicUrlPatterns(List<String> publicUrlPatterns) {
		this.publicUrlPatterns = publicUrlPatterns;
	}

	public String getAuthHeaderName() {
		return authHeaderName;
	}

	public void setAuthHeaderName(String authHeaderName) {
		this.authHeaderName = authHeaderName;
	}

	// public void setLoginService(ILoginService loginService) {
	// this.loginService = loginService;
	// }

}
