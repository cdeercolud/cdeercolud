package com.cdeercloud.common.security.constans;

/**
 * 登录token redis标识
 * 
 * @author looyii
 *
 */
public class TokenConstants {

	public static final String LOGIN_TOKEN = "TOKEN_%s_%s";
}
