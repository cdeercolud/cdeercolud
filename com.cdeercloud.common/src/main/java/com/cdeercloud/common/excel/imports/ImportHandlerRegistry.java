package com.cdeercloud.common.excel.imports;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.cdeercloud.common.core.exception.CdeerException;

@Component
public class ImportHandlerRegistry {
	private static final Map<String, ImportHandler<? extends Object>> MAP = new HashMap();

	protected void registry(String name, ImportHandler<? extends Object> handler) {
		if (MAP.containsKey(name)) {
			throw new CdeerException(name + "规则已存在。");
		}
		MAP.put(name, handler);
	}

	public ImportHandler<? extends Object> getHandler(String businessName) {
		return MAP.get(businessName);
	}
}
