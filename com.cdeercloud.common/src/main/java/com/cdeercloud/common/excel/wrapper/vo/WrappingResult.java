package com.cdeercloud.common.excel.wrapper.vo;

public class WrappingResult {

	private String code;
	private String message;
	private Object data;

	public final static String CODE_SUCCESS = "0";
	public final static String CODE_FAIL = "1";

	public WrappingResult(String code, String message, Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
