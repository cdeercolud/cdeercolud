package com.cdeercloud.common.excel.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import com.cdeercloud.common.excel.wrapper.ResultWrapper;

@Component
public class CdeerWebBeanPostProcessor implements BeanPostProcessor {

	@Value("${web.response.wrapper}")
	private boolean wrapper;

	@Resource(name = "${web.response.wrapper.bean}")
	private ResultWrapper successWrapper;

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof RequestMappingHandlerAdapter) {
			RequestMappingHandlerAdapter adapter = (RequestMappingHandlerAdapter) bean;
			List<HandlerMethodReturnValueHandler> handlers = adapter.getReturnValueHandlers();
			List<HandlerMethodReturnValueHandler> list = new ArrayList<HandlerMethodReturnValueHandler>();

			for (HandlerMethodReturnValueHandler h : handlers) {
				if (h.getClass() == RequestResponseBodyMethodProcessor.class) {
					RequestResponseBodyMethodProcessor rr = (RequestResponseBodyMethodProcessor) h;
					CdeerWebReturnValueHandler p = new CdeerWebReturnValueHandler(rr, this.wrapper, this.successWrapper);
					list.add(p);
				}
				list.add(h);
			}
			adapter.setReturnValueHandlers(list);
		}
		return bean;
	}

}
