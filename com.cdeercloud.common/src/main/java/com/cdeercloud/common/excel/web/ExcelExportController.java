package com.cdeercloud.common.excel.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.excel.export.dto.Column;
import com.cdeercloud.common.excel.export.dto.ExportConfig;
import com.cdeercloud.common.excel.export.impl.ExcelExportHandler;
import com.cdeercloud.common.web.CdeerController;

/**
 * 导出excel
 * 
 * @author looyii
 *
 */
@SuppressWarnings({ "unchecked" })
@RestController
public class ExcelExportController extends CdeerController {

	private final Logger logger = LoggerFactory.getLogger(ExcelExportController.class);

	@Autowired
	private ExcelExportHandler exportHandler;

	/**
	 * 选择导出 方法1
	 * 
	 * @param selection 选中的数据
	 *            列：[{"empEmployeeId":"a6db616c504811e8813a005056830549","employeeCode":"20180002","employeeName":"王丽丽","areaCompany":"0f0e69d0502e11e8813a005056830549","areaCompanyName":"长沙物管公司","idNumber":"430124198906077980","hireDate":1526400000000,"dimissionDate":1527127847000,"orgUnitId":"37f87cb18c6a40e4af7c7589ae6e5c00","orgUnitName":"新增组织511下级","posPositionId":"8a92a5cc634dbed801634ddae75f0005","posPositionName":"IT主管","positionLevelId":"dcd7a98f503a11e8813a005056830549","positionLevelName":"P3","dimissionType":"2","inBlackList":"Y","_id":3},{"empEmployeeId":"8a92a5cc633471460163349a17f30023","employeeCode":"00000029","employeeName":"12312313","areaCompany":"0f0e3e7a502e11e8813a005056830549","areaCompanyName":"物管板块","idNumber":"411122199503028091","hireDate":1525104000000,"dimissionDate":1525622400000,"orgUnitId":"0f0f5d52502e11e8813a005056830549","orgUnitName":"南宁半山湾案场","posPositionId":"47996507510811e8b20f005056a84efb","posPositionName":"保洁领班","positionLevelId":"dcd7c1d3503a11e8813a005056830549","positionLevelName":"2","dimissionType":"1","inBlackList":"N","_id":8},{"empEmployeeId":"8a92a5cc636d744201636e49bb920033","employeeCode":"00000010","employeeName":"张龙","areaCompany":"0f0e69d0502e11e8813a005056830549","areaCompanyName":"长沙物管公司","idNumber":"321323198312091615","hireDate":1525104000000,"dimissionDate":1526659200000,"orgUnitId":"0f0e6b56502e11e8813a005056830549","orgUnitName":"长沙物业案场服务部","posPositionId":"479afa7f510811e8b20f005056a84efb","posPositionName":"案场管理高级经理","positionLevelId":"dcd7c3c7503a11e8813a005056830549","positionLevelName":"6","dimissionType":"1","inBlackList":"N","_id":5},{"empEmployeeId":"8a92a5cc6385c7ec01638633d07b0011","employeeCode":"00000020","employeeName":"你好你好一","areaCompany":"orgUnitId00","areaCompanyName":"阳光城集团","idNumber":"320412198011301860","hireDate":1526918400000,"dimissionDate":1526965009000,"orgUnitId":"0f0e498c502e11e8813a005056830549","orgUnitName":"物管本部总经理","posPositionId":"47984a9d510811e8b20f005056a84efb","posPositionName":"物业管理本部总经理改名","positionLevelId":"dcd7c6b8503a11e8813a005056830549","positionLevelName":"12","dimissionType":null,"inBlackList":null,"_id":4},{"empEmployeeId":"8a92a5cc639133cb0163918622fc0043","employeeCode":"00000034","employeeName":"我要入职","areaCompany":"0f0e69d0502e11e8813a005056830549","areaCompanyName":"长沙物管公司","idNumber":"23424","hireDate":1527091200000,"dimissionDate":1527609600000,"orgUnitId":"0f0e8f6a502e11e8813a005056830549","orgUnitName":"长沙物业人力资源及行政部","posPositionId":"4799a147510811e8b20f005056a84efb","posPositionName":"人力资源及行政总监","positionLevelId":"dcd7c53d503a11e8813a005056830549","positionLevelName":"9","dimissionType":"2","inBlackList":"N","_id":1},{"empEmployeeId":"8a92a5cc634e928801634e9e79970003","employeeCode":"00000002","employeeName":"啊话","areaCompany":"0f0e69d0502e11e8813a005056830549","areaCompanyName":"长沙物管公司","idNumber":"123","hireDate":1526054400000,"dimissionDate":1526227200000,"orgUnitId":"fd1186e383a04427ac879b614ff86abb","orgUnitName":"测试组织51101改","posPositionId":"8a92a5cc634dbed801634dda3c6b0004","posPositionName":"IT经理","positionLevelId":"dcd7a98f503a11e8813a005056830549","positionLevelName":"P3","dimissionType":"1","inBlackList":"N","_id":6},{"empEmployeeId":"8a92a5cc6372ae050163772d63f000d1","employeeCode":"00000016","employeeName":"测测","areaCompany":"0f0e472a502e11e8813a005056830549","areaCompanyName":"物业管理本部","idNumber":"1223","hireDate":1527091200000,"dimissionDate":1527696000000,"orgUnitId":"0f0e498c502e11e8813a005056830549","orgUnitName":"物管本部总经理","posPositionId":"47984a9d510811e8b20f005056a84efb","posPositionName":"物业管理本部总经理改名","positionLevelId":"dcd7c6b8503a11e8813a005056830549","positionLevelName":"12","dimissionType":"1","inBlackList":"N","_id":0},{"empEmployeeId":"8a92a5cc6370f7e701637151d9d80052","employeeCode":"00000012","employeeName":"权花花","areaCompany":"0f0e69d0502e11e8813a005056830549","areaCompanyName":"长沙物管公司","idNumber":"110101198901130042","hireDate":1526659200000,"dimissionDate":1527177600000,"orgUnitId":"37f87cb18c6a40e4af7c7589ae6e5c00","orgUnitName":"新增组织511下级","posPositionId":"8a92a5cc634dbed801634ddae75f0005","posPositionName":"IT主管","positionLevelId":"dcd7a98f503a11e8813a005056830549","positionLevelName":"P3","dimissionType":"1","inBlackList":"N","_id":2},{"empEmployeeId":"8a92a5cc63347146016334a469b5002f","employeeCode":"00000030","employeeName":"马小玲","areaCompany":"0f0e69d0502e11e8813a005056830549","areaCompanyName":"长沙物管公司","idNumber":"110101198801200058","hireDate":1525104000000,"dimissionDate":1525190400000,"orgUnitId":"0f0f5d52502e11e8813a005056830549","orgUnitName":"南宁半山湾案场","posPositionId":"47996507510811e8b20f005056a84efb","posPositionName":"保洁领班","positionLevelId":"dcd7c1d3503a11e8813a005056830549","positionLevelName":"2","dimissionType":"2","inBlackList":"Y","_id":9},{"empEmployeeId":"8a92a5cc63365fa701633847db2a0009","employeeCode":"00000032","employeeName":"测试A","areaCompany":"041cd83392a7402e84634a2cada5b2b8","areaCompanyName":"测试组织12","idNumber":"110101198001010491","hireDate":1525104000000,"dimissionDate":1525795200000,"orgUnitId":"c19dd92f96714ecd960056ccac982979","orgUnitName":"测试部门","posPositionId":"8a92a5cc63365fa70163381c3da00004","posPositionName":"模板测试","positionLevelId":"dcd7b978503a11e8813a005056830549","positionLevelName":"M13","dimissionType":"1","inBlackList":"N","_id":7}]
	 *            cdeer_table_export_column_metas:
	 *            [{"label":"姓名","property":"employeeName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"工号","property":"employeeCode","align":"is-left","width":130,"level":1,"mapping":null},{"label":"身份证号码","property":"idNumber","align":"is-left","width":130,"level":1,"mapping":null},{"label":"入职时间","property":"hireDate","align":"is-left","width":130,"level":1,"mapping":{"sahara_date":"yyyy-MM-dd"}},{"label":"离职前公司","property":"areaCompanyName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前部门","property":"orgUnitName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前职位","property":"posPositionName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前职级","property":"positionLevelName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职时间","property":"dimissionDate","align":"is-left","width":130,"level":1,"mapping":{"sahara_date":"yyyy-MM-dd"}},{"label":"离职类型","property":"dimissionType","align":"is-left","width":130,"level":1,"mapping":{"1":"主动离职","2":"被动离职","3":"特殊离职"}},{"label":"是否已在黑名单","property":"inBlackList","align":"is-left","width":130,"level":1,"mapping":{"Y":"是","N":"否"}}]
	 * @param cdeer_table_export_column_metas 标题信息
	 *            列：
	 *            [{"label":"姓名","property":"employeeName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"工号","property":"employeeCode","align":"is-left","width":130,"level":1,"mapping":null},{"label":"身份证号码","property":"idNumber","align":"is-left","width":130,"level":1,"mapping":null},{"label":"入职时间","property":"hireDate","align":"is-left","width":130,"level":1,"mapping":{"sahara_date":"yyyy-MM-dd"}},{"label":"离职前公司","property":"areaCompanyName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前部门","property":"orgUnitName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前职位","property":"posPositionName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前职级","property":"positionLevelName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职时间","property":"dimissionDate","align":"is-left","width":130,"level":1,"mapping":{"sahara_date":"yyyy-MM-dd"}},{"label":"离职类型","property":"dimissionType","align":"is-left","width":130,"level":1,"mapping":{"1":"主动离职","2":"被动离职","3":"特殊离职"}},{"label":"是否已在黑名单","property":"inBlackList","align":"is-left","width":130,"level":1,"mapping":{"Y":"是","N":"否"}}]
	 * @param cdeer_table_export_config 配置信息
	 *            列：{"fileName":"离职信息","version":"xlsx","range":"all","sync":false}
	 * @param response
	 */
	@PostMapping("/common/exports")
	public void exports(@RequestParam(value = "selection") String selection, @RequestParam(value = "cdeer_table_export_column_metas") String cdeer_table_export_column_metas, @RequestParam(value = "cdeer_table_export_config") String cdeer_table_export_config, HttpServletResponse response, HttpServletRequest request) {
		Workbook wb = null;
		try {
			// 文件属性
			ExportConfig config = parseJson(cdeer_table_export_config, ExportConfig.class);
			// 文件名称
			String fileName = config.getFileName();
			// 版本号
			String version = config.getVersion();
			// 标题列表
			Column[] headerList = parseJson(cdeer_table_export_column_metas, Column[].class);
			List<Map<String, Object>> dataList = parseJson(selection, List.class);
			// TODO 将数据写入wb EXCEL
			response.reset();
			String downloadName = fileName + "." + version;
			downloadName = this.encodingString(request, downloadName);
			response.setContentType("application/vnd.ms-excel; charset=" + "UTF-8");
			response.setHeader("Content-Disposition", "attachment; filename=" + downloadName);
			wb.write(response.getOutputStream());
		} catch (RuntimeException e) {
			logger.error("导出过程发生RuntimeException", e);
			throw new CdeerException(e);
		} catch (IOException e) {
			logger.error("导出过程发生错误", e);
			throw new CdeerException("导出过程发生错误", e);
		} finally {
			if (wb != null) {
				try {
					wb.close();
				} catch (IOException e) {
					throw new CdeerException("关闭工作簿发生异常", e);
				}
			}
		}
	}

	/**
	 * 编码文件名
	 * 
	 * @param request
	 * @param downloadName
	 *            文件名
	 * @return
	 */
	private String encodingString(HttpServletRequest request, String downloadName) {
		try {
			String vers = request.getHeader("USER-AGENT");
			if (vers.indexOf("Firefox") > -1) {
				downloadName = new String(downloadName.getBytes("UTF-8"), "ISO-8859-1");
			} else {
				downloadName = URLEncoder.encode(downloadName, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("编码文件名发生异常", e);
		}
		return downloadName;
	}

	/**
	 * 方法二导出，
	 * 
	 * @param returnValue
	 * @param request
	 * @param response
	 */
	public void exports2(Object returnValue, ServletServerHttpRequest request, ServletServerHttpResponse response) {
		try {
			this.exportHandler.export(returnValue, request, response);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CdeerException("导出错误");
		}
	}
}
