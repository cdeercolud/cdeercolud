package com.cdeercloud.common.excel.config.interceptors;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LogTraceIdInterceptor extends HandlerInterceptorAdapter {

	private final static String LOG_TRACE_KEY = "cdeer-log-id";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		MDC.put(LOG_TRACE_KEY, UUID.randomUUID().toString());
		return super.preHandle(request, response, handler);
	}

}
