package com.cdeercloud.common.excel.export;

import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;

public abstract interface ExportHandler {

	public abstract void export(Object paramObject, ServletServerHttpRequest paramServletServerHttpRequest, ServletServerHttpResponse paramServletServerHttpResponse) throws Exception;
}
