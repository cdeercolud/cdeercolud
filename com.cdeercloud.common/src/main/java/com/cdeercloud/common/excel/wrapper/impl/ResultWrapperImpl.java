package com.cdeercloud.common.excel.wrapper.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;

import com.cdeercloud.common.excel.wrapper.ResultWrapper;
import com.cdeercloud.common.excel.wrapper.vo.WrappingResult;

@Component("cdeer_web_resultWrapper")
public class ResultWrapperImpl implements ResultWrapper {
	final Logger logger = LoggerFactory.getLogger(ResultWrapper.class);

	protected interface Converter<T extends Exception> {
		public String convert(T e);
	}

	protected static Map<Class<? extends Exception>, Converter<? extends Exception>> map = new HashMap<>();
	static {
		map.put(BindException.class, new Converter<BindException>() {
			@Override
			public String convert(BindException e) {
				List<ObjectError> errors = e.getAllErrors();
				if ((errors != null) && !errors.isEmpty()) {
					return errors.get(0).getDefaultMessage();
				}
				return null;
			};
		});
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object wrapException(Exception e) {
		logger.error("", e);
		Converter converter = map.get(e.getClass());
		String msg = null;
		if (converter != null) {
			msg = converter.convert(e);
		}
		if (msg == null) {
			msg = e.getMessage();
		}
		return new WrappingResult(WrappingResult.CODE_FAIL, msg, null);
	}

	@Override
	public Object wrapSuccess(Object returnValue) {
		return new WrappingResult(WrappingResult.CODE_SUCCESS, null, returnValue);
	}
}
