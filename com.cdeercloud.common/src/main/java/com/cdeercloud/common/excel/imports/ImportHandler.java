package com.cdeercloud.common.excel.imports;

import org.springframework.validation.BindingResult;

public abstract interface ImportHandler<T> {
	/**
	 * 业务名称
	 * 
	 * @return
	 */
	public abstract String businessName();

	/**
	 * 模板名称
	 * 
	 * @return
	 */
	public abstract String templateFileName();

	/**
	 * 导入数据开始行
	 * 
	 * @return
	 */
	public abstract int dataFirstRowNum();

	/**
	 * 导入数据验证
	 * 
	 * @param paramT
	 * @param paramBindingResult
	 * @return
	 */
	public abstract boolean validate(T paramT, BindingResult paramBindingResult);

	/**
	 * 保存数据
	 * 
	 * @param paramT
	 * @param paramImportContext
	 */
	public abstract void handle(T paramT, ImportContext paramImportContext);

	/**
	 * 下载模板，返回模板地址
	 * 
	 * @return
	 */
	public abstract String getDymFile();
}
