package com.cdeercloud.common.excel.export;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cdeercloud.common.util.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.excel.export.dto.Column;
import com.cdeercloud.common.excel.export.dto.ExportConfig;
import com.cdeercloud.common.util.JsonUtils;

/**
 * 
 * @author looyii
 *
 */
@SuppressWarnings("deprecation")
public class ExcelUtils<T> {
	private static final String DEFAULT_DATA_FORMAT = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 默认字体样式
	 */
	private final static String FONT_DEFAULT_NAME = "微软雅黑";

	/**
	 * 默认字体大小
	 */
	private final static short FONT_DEFAULT_SIZE = 10;

	/**
	 * 表头最大阈值
	 */
	private static final int HEADER_MAX_LEVEL = 10;

	/**
	 * 单元格最大数
	 */
	private static final int COLUMN_MAX_NUM = 500;

	Class<T> clazz;
	private static Map<String, HorizontalAlignment> ALIGN_MAPPING = new HashMap<String, HorizontalAlignment>();
	static {
		ALIGN_MAPPING.put("center", HorizontalAlignment.CENTER);
		ALIGN_MAPPING.put("left", HorizontalAlignment.LEFT);
		ALIGN_MAPPING.put("right", HorizontalAlignment.RIGHT);
	}

	public static Workbook createExcel(PagedResult<? extends Object> pr, Column[] cols, ExportConfig exportConfig) {
		Workbook wb = null;
		if ("xls".equals(exportConfig.getVersion())) {
			wb = new HSSFWorkbook();
		} else {
			wb = new XSSFWorkbook();
		}

		Sheet sheet = wb.createSheet("sheet1");
		int rowIndex = 0;
		Row row = sheet.createRow(rowIndex++);

		Map<String, CellStyle> headStyle = createStyles(wb);

		// 查询出排除列，包含列
		Map<String, String> excludeColumns = getExcludeColumn(exportConfig);
		Map<String, Map<String, ?>> includeColumns = getIncludeColumn(exportConfig);

		Map<Integer, String> funcMap = new HashMap<Integer, String>();
		Map<Integer, CellStyle> columnStylesMap = new HashMap<Integer, CellStyle>();

		Map<String, String> dealCustomerColumns = new HashMap<String, String>();

		List<Column> fCols = new ArrayList<>();
		for (Column column : cols) {
			if (StringUtils.isEmpty(column.getProperty())) {
				continue;
			}
			fCols.add(column);
		}
		cols = fCols.toArray(new Column[fCols.size()]);
		int headerCounter = 0;
		for (Column col : cols) {
			// 检查是否有excludeColumn
			if (excludeColumns.containsKey(col.getProperty())) {
				continue;
			}

			// 检查是否有includeColumn
			if (includeColumns.containsKey(col.getProperty())) {
				// continue;
				// TODO 定制列表头
				dealCustomerColumns.put(col.getProperty(), col.getProperty());
				headerCounter = createCustomerHeader(sheet, row, headStyle.get("header1"), includeColumns, funcMap, headerCounter, col.getProperty(), col.getLabel());
				continue;
			}

			Cell cell = row.createCell(headerCounter);
			cell.setCellValue(col.getLabel());

			cell.setCellStyle(headStyle.get("header1"));
			String width = col.getWidth();
			setColumnWidth(sheet, headerCounter, width);
			headerCounter++;
		}

		// 定制列表头
		for (String key : includeColumns.keySet()) {
			if (dealCustomerColumns.containsKey(key)) {
				continue;
			}
			headerCounter = createCustomerHeader(sheet, row, headStyle.get("header1"), includeColumns, funcMap, headerCounter, key, null);
		}

		List<? extends Object> datas = pr.getData();
		if (datas.isEmpty()) {
			return wb;
		}
		boolean isMap = Map.class.isAssignableFrom(datas.get(0).getClass());
		HashMap<String, Method> mm = new HashMap<String, Method>();
		HashMap<String, String> colMap = new HashMap<String, String>();
		try {
			for (Column column : cols) {
				colMap.put(column.getProperty(), JsonUtils.toJson(column));
				if (!isMap) {
					String prop = column.getProperty();
					PropertyDescriptor desc = new PropertyDescriptor(prop, datas.get(0).getClass());
					mm.put(prop, desc.getReadMethod());
				}
			}

			// 处理定制列
			if (!isMap) {
				for (String key : includeColumns.keySet()) {
					PropertyDescriptor desc = new PropertyDescriptor(key, datas.get(0).getClass());
					mm.put(key, desc.getReadMethod());
				}
			}
		} catch (IntrospectionException e) {
			throw new CdeerException("导出Excel时出现错误@读出属性出错", e);
		}

		int funcBeginRow = rowIndex + 1;
		for (int i = 0; i < datas.size(); i++) {
			Object object = datas.get(i);
			row = sheet.createRow(rowIndex++);
			int dataCounter = 0;
			for (Column col : cols) {
				// 检查是否有excludeColumn
				if (excludeColumns.containsKey(col.getProperty())) {
					continue;
				}

				// 检查是否有includeColumn
				if (includeColumns.containsKey(col.getProperty())) {
					// continue;
					// TODO 定制列数据
					dataCounter = createCustomerData(wb,  row, includeColumns, columnStylesMap, isMap, mm, object, dataCounter, col.getProperty());
					continue;
				}

				Cell cell = row.createCell(dataCounter);
				String align = col.getAlign();

				if (columnStylesMap.containsKey(dataCounter)) {
					cell.setCellStyle(columnStylesMap.get(dataCounter));
				} else {
					CellStyle cs = createDefaultCellStyle(wb, align, headStyle);
					cell.setCellStyle(cs);
					// 缓冲列样式
					columnStylesMap.put(dataCounter, cs);
				}

				String prop = col.getProperty();
				Object obj = null;
				if (isMap) {
					obj = ((Map<?, ?>) object).get(prop);
				} else {
					obj = ReflectionUtils.invokeMethod(mm.get(prop), object);
				}
				if (obj != null) {
					String value = obj.toString();
					Object display = null;
					Map<?, ?> cm = col.getMapping();
					if ((cm != null) && !cm.isEmpty()) {
						Object df = cm.get("cdeer_date");
						Object nf = cm.get("cdeer_number");
						if ((df != null) && (obj instanceof Date)) {
							display =DateUtil.format((Date) obj,df.toString());
						} else if (nf != null /* && obj instanceof Number */) {
							// TODO
							cell.getCellStyle().setDataFormat(HSSFDataFormat.getBuiltinFormat(nf.toString()));
							display = obj;
						} else {
							display = cm.get(obj);
						}
					}
					display = display == null ? "" : display;

					if (((cm != null) && !cm.isEmpty())) {
						cell.setCellValue(display.toString());
					} else {
						cell.setCellValue(value);
						// setCellValue(obj, cell, null);
					}

				} else {
					// null value
				}
				dataCounter++;
			}

			// 定制列数据
			for (String key : includeColumns.keySet()) {
				if (dealCustomerColumns.containsKey(key)) {
					continue;
				}
				dataCounter = createCustomerData(wb, row, includeColumns, columnStylesMap, isMap, mm, object, dataCounter, key);
			}
		}
		int funcEndRow = rowIndex;

		// create function row
		if (!funcMap.isEmpty()) {
			row = sheet.createRow(rowIndex++);
			for (Integer index : funcMap.keySet()) {
				Cell cell = row.createCell(index);
				String cellLable = CellReference.convertNumToColString(index);
				cell.setCellFormula(funcMap.get(index) + "(" + cellLable + funcBeginRow + ":" + cellLable + funcEndRow + ")");
				// cell.setCellValue(funcMap.get(index));
				cell.setCellStyle(columnStylesMap.get(index));

				HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
			}
		}

		return wb;
	}

	private static int createCustomerData(Workbook wb,  Row row, Map<String, Map<String, ?>> includeColumns, Map<Integer, CellStyle> columnStylesMap, boolean isMap, HashMap<String, Method> mm, Object object, int dataCounter, String key) throws NumberFormatException {
		Cell cell = row.createCell(dataCounter);

		Map<String, ?> dm = includeColumns.get(key);
		String align = (String) dm.get("align");

		if (columnStylesMap.containsKey(dataCounter)) {
			cell.setCellStyle(columnStylesMap.get(dataCounter));
		} else {
			CellStyle cs = createDefaultCellStyle(wb, align);
			cell.setCellStyle(cs);
			// 缓冲列样式
			columnStylesMap.put(dataCounter, cs);
		}

		Object value = null;
		if (isMap) {
			value = ((Map<?, ?>) object).get(key);
		} else {
			value = ReflectionUtils.invokeMethod(mm.get(key), object);
		}
		if (value != null) {
			String type = (String) dm.get("type");
			if (StringUtils.isEmpty(type)) {
				type = "string";
			}
			String format = (String) dm.get("format");
			if ("date".equals(type)) {
				format = format == null ? DEFAULT_DATA_FORMAT : format;
				cell.setCellValue(DateUtil.format((Date)value,format));
			} else if ("number".equals(type)) {
				if (format != null) {
					cell.getCellStyle().setDataFormat(HSSFDataFormat.getBuiltinFormat(format.toString()));
				}
				cell.setCellValue(Double.parseDouble(value.toString()));
			} else {
				cell.setCellValue(value.toString());
			}
		}
		dataCounter++;
		return dataCounter;
	}

	private static int createCustomerHeader(Sheet sheet, Row row, CellStyle headStyle, Map<String, Map<String, ?>> includeColumns, Map<Integer, String> funcMap, int headerCounter, String key, String rawTitle) throws NumberFormatException {
		Cell cell = row.createCell(headerCounter);
		Map<String, ?> dm = includeColumns.get(key);

		if (dm.containsKey("title")) {
			cell.setCellValue(dm.get("title").toString());
		} else {
			if (rawTitle != null) {
				cell.setCellValue(rawTitle);
			} else {
				cell.setCellValue("自定义属性");
			}
		}

		if (dm.containsKey("func")) {
			funcMap.put(headerCounter, dm.get("func").toString());
		}

		cell.setCellStyle(headStyle);

		String width = (String) dm.get("width");
		setColumnWidth(sheet, headerCounter, width);

		headerCounter++;
		return headerCounter;
	}

	private static CellStyle createDefaultCellStyle(Workbook wb, String align) {
		if (StringUtils.isEmpty(align)) {
			align = "center";
		}
		CellStyle style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setFontName("Courier New");
		style.setFont(font);
		// 兼容Element UI
		if ("is-center".equals(align)) {
			align = "center";
		}
		if (ALIGN_MAPPING.containsKey(align)) {
			style.setAlignment(ALIGN_MAPPING.get(align));
		}
		return style;
	}

	private static CellStyle createDefaultCellStyle(Workbook wb, String align, Map<String, CellStyle> headStyle) {
		if (StringUtils.isEmpty(align)) {
			align = "is-left";
		}
		return headStyle.get("data_" + align);
	}

	@SuppressWarnings("unused")
	private static void setCellValue(Object value, Cell cell, String format) {
		// 判断值的类型后进行强制类型转换
		if (value instanceof Number) {
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
		}

		if (value instanceof Integer) {
			int intValue = (Integer) value;
			cell.setCellValue(intValue);
		} else if (value instanceof Float) {
			float fValue = (Float) value;
			HSSFRichTextString data = new HSSFRichTextString(String.valueOf(fValue));
			cell.setCellValue(data);
		} else if (value instanceof Double) {
			double dValue = (Double) value;
			HSSFRichTextString data = new HSSFRichTextString(String.valueOf(dValue));
			cell.setCellValue(data);
		} else if (value instanceof Long) {
			long longValue = (Long) value;
			cell.setCellValue(longValue);
		}
		if (value instanceof Boolean) {
			boolean bValue = (Boolean) value;
			cell.setCellValue(bValue);
		} else if (value instanceof Date) {
			Date date = (Date) value;
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			cell.setCellValue(sdf.format(date));
		} else {
			// 其它数据类型都当作字符串简单处理
			cell.setCellValue(value.toString());
		}
	}

	private static void setColumnWidth(Sheet sheet, int headerCounter, String width) throws NumberFormatException {
		width = width == null ? "150" : width;
		if (width.matches("\\d+(px)?")) {
			if (width.endsWith("px")) {
				width = width.substring(0, width.length() - 2);
			}
			int w = Integer.parseInt(width) / 6;
			w = w > 254 ? 254 : w;
			sheet.setColumnWidth(headerCounter, w * 256);
		}
	}

	private static Map<String, String> getExcludeColumn(ExportConfig exportConfig) {
		Map<String, String> result = new HashMap<String, String>();
		Map<String, Map<String, ?>> customer = exportConfig.getCustomer();
		if ((customer != null) && !customer.isEmpty()) {
			for (String key : customer.keySet()) {
				Map<String, ?> property = customer.get(key);
				if (property.containsKey("include")) {
					boolean flag = (Boolean) property.get("include");
					if (!flag) {
						result.put(key, key);
					}
				}
			}
		}
		return result;
	}

	private static Map<String, Map<String, ?>> getIncludeColumn(ExportConfig exportConfig) {
		Map<String, Map<String, ?>> result = new HashMap<String, Map<String, ?>>();
		Map<String, Map<String, ?>> customer = exportConfig.getCustomer();
		if ((customer != null) && !customer.isEmpty()) {
			for (String key : customer.keySet()) {
				Map<String, ?> property = customer.get(key);
				if (property.containsKey("include")) {
					boolean flag = (Boolean) property.get("include");
					if (flag) {
						result.put(key, property);
					}
				} else {
					result.put(key, property);
				}
			}
		}
		return result;
	}

	public static HSSFWorkbook createExcel(PagedResult<? extends Object> pr, Column[] cols) throws Exception {
		List<? extends Object> data = pr.getData();
		if (data.isEmpty()) {
			HSSFWorkbook wb = new HSSFWorkbook();
			wb.createSheet("sheet1");
			return wb;
		}

		boolean isMap = Map.class.isAssignableFrom(data.get(0).getClass());
		if (isMap) {
			return createExcelFromMap(data, cols);
		} else {
			return createExcelFromObject(data, cols);
		}
	}

	@SuppressWarnings("unchecked")
	private static HSSFWorkbook createExcelFromMap(List<? extends Object> data, Column[] cols) {
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet1");
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		HSSFFont font = wb.createFont();
		// font.setColor(HSSFFont.COLOR_RED);
		// font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);

		// obj = ((Map<?,?>)object).get(prop);
		int rowIndex = 0;
		HSSFRow row = sheet.createRow(rowIndex++);

		// 根据cols是否为null确定是否导出全部字段 TODO 表头如何处理，目前是使用字段名称
		if ((cols == null) || (cols.length == 0)) {
			List<String> keyList = new ArrayList<String>();
			Map<String, ?> target = (Map<String, ?>) data.get(0);
			for (String key : target.keySet()) {
				keyList.add(key);
			}

			// create head
			int cellIndex = 0;
			for (String key : keyList) {
				HSSFCell cell = row.createCell(cellIndex++);
				cell.setCellStyle(style);
				cell.setCellValue(key);
			}

			for (Object obj : data) {
				row = sheet.createRow(rowIndex++);
				cellIndex = 0;
				Map<String, ?> objMap = (Map<String, ?>) obj;
				for (String key : keyList) {
					Object o = objMap.get(key);
					// create data
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellStyle(style);
					cell.setCellValue(o == null ? "" : o.toString());
				}
			}
		} else {
			List<String> heads = new ArrayList<String>();
			List<String> bodys = new ArrayList<String>();
			for (Column c : cols) {
				heads.add(c.getLabel());
				String property = c.getProperty();
				bodys.add(property);
			}

			// create head
			int cellIndex = 0;
			for (String h : heads) {
				HSSFCell cell = row.createCell(cellIndex++);
				cell.setCellStyle(style);
				cell.setCellValue(h);
			}

			// create data
			for (Object obj : data) {
				row = sheet.createRow(rowIndex++);
				cellIndex = 0;
				Map<String, ?> objMap = (Map<String, ?>) obj;
				for (String b : bodys) {
					Object o = objMap.get(b);
					// create data
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellStyle(style);
					cell.setCellValue(o == null ? "" : o.toString());
				}
			}
		}
		return wb;
	}

	private static HSSFWorkbook createExcelFromObject(List<? extends Object> data, Column[] cols) throws Exception {
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet1");
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		HSSFFont font = wb.createFont();
		// font.setColor(HSSFFont.COLOR_RED);
		// font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);

		int rowIndex = 0;
		HSSFRow row = sheet.createRow(rowIndex++);

		Field[] fields = null;
		Method[] methods = null;
		Map<String, Method> mm = new HashMap<String, Method>();

		List<String> heads = new ArrayList<String>();
		List<Method> bodys = new ArrayList<Method>();
		// 根据cols是否为null确定是否导出全部字段 TODO 表头如何处理，目前是使用字段名称
		if ((cols == null) || (cols.length == 0)) {
			Object target = data.get(0);
			fields = target.getClass().getDeclaredFields();
			methods = target.getClass().getDeclaredMethods();
			for (Method m : methods) {
				if (m.getName().startsWith("get")) {// 取方法名为get...的
					mm.put(m.getName(), m);
				}
			}
			// create head
			int cellIndex = 0;
			for (Field f : fields) {
				String fieldName = f.getName();
				HSSFCell cell = row.createCell(cellIndex++);
				cell.setCellStyle(style);
				cell.setCellValue(fieldName);
			}

			for (Object obj : data) {
				row = sheet.createRow(rowIndex++);
				cellIndex = 0;
				for (Field f : fields) {
					String fieldName = f.getName();
					String fieldMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
					Method m = mm.get(fieldMethodName);
					Object o = m.invoke(obj);// 执行
					// create data
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellStyle(style);
					cell.setCellValue(o == null ? "" : o.toString());
				}
			}
		} else {
			heads = new ArrayList<String>();
			bodys = new ArrayList<Method>();
			for (Column c : cols) {
				heads.add(c.getLabel());
				String property = c.getProperty();
				PropertyDescriptor desc = new PropertyDescriptor(property, data.get(0).getClass());
				bodys.add(desc.getReadMethod());
			}

			// create head
			int cellIndex = 0;
			for (String h : heads) {
				HSSFCell cell = row.createCell(cellIndex++);
				cell.setCellStyle(style);
				cell.setCellValue(h);
			}

			// create data
			for (Object obj : data) {
				row = sheet.createRow(rowIndex++);
				cellIndex = 0;
				for (Method m : bodys) {
					Object o = m.invoke(obj);// 执行
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellStyle(style);
					cell.setCellValue(o == null ? "" : o.toString());
				}
			}
		}

		return wb;
	}

	/**
	 * 多级表头导出
	 * 
	 * @param pr
	 * @param cols
	 * @param exportConfig
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Workbook multipleHeader(PagedResult<? extends Object> pr, Column[] cols, ExportConfig exportConfig) {
		Workbook wb = null;
		if ("xls".equals(exportConfig.getVersion())) {
			wb = new HSSFWorkbook();
		} else {
			wb = new XSSFWorkbook();
		}

		Sheet sheet = wb.createSheet("sheet1");
		SimpleDateFormat sdf = new SimpleDateFormat();
		Map<String, CellStyle> headStyle = createStyles(wb);

		Map<Integer, CellStyle> columnStylesMap = new HashMap<Integer, CellStyle>();

		List<Column> fCols = new ArrayList<>();
		for (Column column : cols) {
			if (StringUtils.isEmpty(column.getProperty())) {
				continue;
			}
			fCols.add(column);
		}
		cols = fCols.toArray(new Column[fCols.size()]);

		// 最大层级
		int maxLevel = getMaxLevel(cols, 1);
		setHeader(sheet, headStyle.get("header1"), maxLevel, cols, 0, 0);
		int rowIndex = maxLevel;
		Row row = sheet.createRow(rowIndex);
		int cellNum = 0;
		for (Column column : cols) {
			cellNum = setColumnWidth(sheet, column, cellNum, column.getWidth());
		}

		List<? extends Object> datas = pr.getData();
		if (datas.isEmpty()) {
			return wb;
		}
		boolean isMap = Map.class.isAssignableFrom(datas.get(0).getClass());
		// 方法集合
		HashMap<String, Method> mm = new HashMap<String, Method>();
		HashMap<String, String> colMap = new HashMap<String, String>();
		try {
			for (Column column : cols) {
				colMap.put(column.getProperty(), JsonUtils.toJson(column));
				if (!isMap) {
					String prop = column.getProperty();
					Column[] childrens = column.getChildren();
					if ((childrens != null) && (childrens.length > 0)) {
						for (Column children : childrens) {
							try {
								datas.get(0).getClass().getDeclaredField(children.getProperty());
							} catch (NoSuchFieldException | SecurityException e) {
								continue;
							}
							PropertyDescriptor desc = new PropertyDescriptor(children.getProperty(), datas.get(0).getClass());
							mm.put(children.getProperty(), desc.getReadMethod());
						}
					} else {
						PropertyDescriptor desc = new PropertyDescriptor(prop, datas.get(0).getClass());
						mm.put(prop, desc.getReadMethod());
					}
				}
			}

		} catch (IntrospectionException e) {
			throw new CdeerException("导出Excel时出现错误@读出属性出错", e);
		}

		for (int i = 0; i < datas.size(); i++) {
			Object object = datas.get(i);
			row = sheet.createRow(rowIndex++);
			int dataCounter = 0;
			for (Column col : cols) {
				String prop = col.getProperty();
				Object obj = null;
				if (isMap) {
					obj = ((Map<?, ?>) object).get(prop);
				} else {
					if (mm.get(prop) != null) {
						obj = ReflectionUtils.invokeMethod(mm.get(prop), object);
					}
				}

				Column[] childrens = col.getChildren();
				List<Object> list = new ArrayList<>();
				if ((childrens != null) && (childrens.length > 0)) {
					if (obj != null) {
						if (Map.class.isAssignableFrom(obj.getClass())) {
							Map<String, Object> map = (Map<String, Object>) obj;
							for (Column children : childrens) {
								list.add(map.get(children.getProperty()));
							}
						} else {
							list.add(obj);
						}
					} else {
						for (Column children : childrens) {
							Object display = null;
							Map<?, ?> cm = children.getMapping();
							if ((cm != null) && !cm.isEmpty()) {
								Object df = cm.get("cdeer_date");
								Object nf = cm.get("cdeer_number");
								// if ((df != null) && (obj instanceof Date)) {
								// sdf.applyPattern(df.toString());
								// display = sdf.format((Date) obj);
								// } else
								if (nf != null) {
									// TODO
									display = obj;
								} else {
									display = ReflectionUtils.invokeMethod(mm.get(children.getProperty()), object);
									display = cm.get(display);
								}
								display = display == null ? "" : display;
							} else {
								display = ReflectionUtils.invokeMethod(mm.get(children.getProperty()), object);
							}
							list.add(display);
						}
					}
				} else {
					list.add(obj);
				}
				for (Object val : list) {
					obj = val;
					Cell cell = row.createCell(dataCounter);
					String align = col.getAlign();
					if (columnStylesMap.containsKey(dataCounter)) {
						cell.setCellStyle(columnStylesMap.get(dataCounter));
					} else {
						CellStyle cs = createDefaultCellStyle(wb, align, headStyle);
						cell.setCellStyle(cs);
						// 缓冲列样式
						columnStylesMap.put(dataCounter, cs);
					}

					if (obj != null) {
						String value = obj.toString();
						Object display = null;
						Map<?, ?> cm = col.getMapping();
						if ((cm != null) && !cm.isEmpty()) {
							Object df = cm.get("cdeer_date");
							Object nf = cm.get("cdeer_number");
							if ((df != null) && (obj instanceof Date)) {
								sdf.applyPattern(df.toString());
								display = sdf.format((Date) obj);
							} else if (nf != null /* && obj instanceof Number */) {
								// TODO
								display = obj;
							} else {
								display = cm.get(obj);
							}
						}

						display = display == null ? "" : display;
						cell.getCellStyle().setDataFormat(wb.createDataFormat().getFormat(getCellFormatString(val)));
						if (((cm != null) && !cm.isEmpty())) {
							cell.setCellValue(display.toString());
						} else {
							cell.setCellValue(value);
							// setCellValue(obj, cell, null);
						}

					} else {
						// null value
					}
					dataCounter++;
				}
			}
		}

		return wb;
	}

	/**
	 * 递归设置标题
	 * 
	 * @param maxLevel 最大层级
	 * @param sheet 工作表对象
	 * @param columns 标题配置列表
	 * @param rownum 行号
	 * @param cellNum  列号
	 * @param headStyle
	 */
	private static void setHeader(Sheet sheet, CellStyle headStyle, int maxLevel, Column[] columns, int rownum, int cellNum) {
		if (cellNum > COLUMN_MAX_NUM) {
			return;
		}
		Row row = null;
		try {
			if (sheet.getRow(rownum) != null) {
				row = sheet.getRow(rownum++);
			} else {
				row = sheet.createRow(rownum++);
			}
			row.setHeightInPoints(16);
			int columnNum = cellNum;
			for (Column column : columns) {
				// 标题名称
				String name = column.getLabel();
				// 获取子表头
				Column[] childrens = column.getChildren();
				int lastCol = columnNum;
				if ((childrens != null) && (childrens.length > 0)) {
					setHeader(sheet, headStyle, maxLevel, childrens, rownum, lastCol);
					int size = getNextSize(childrens);
					lastCol = (columnNum + childrens.length + size) - 2;
					if (columnNum < lastCol) {
						CellRangeAddress cra = new CellRangeAddress(rownum - 1, rownum - 1, columnNum, lastCol);
						sheet.addMergedRegion(cra);
						setMergedCellStyle(cra, sheet);
					}
				} else {
					int firstRow = rownum - 1;
					int lastRow = maxLevel - 1;
					if (firstRow < lastRow) {
						CellRangeAddress cra = new CellRangeAddress(firstRow, lastRow, columnNum, columnNum);
						sheet.addMergedRegion(cra);
						setMergedCellStyle(cra, sheet);
					}
				}
				Cell cell = row.createCell(columnNum);
				cell.setCellStyle(headStyle);
				cell.setCellValue(name);
				for (int i = columnNum; i < (lastCol + 1); i++) {
					sheet.autoSizeColumn(i);
				}
				columnNum = lastCol + 1;
			}
		} catch (Exception e) {
			throw new CdeerException(e);
		}
	}

	/**
	 * 获取下下一级子元素数量
	 * 
	 * @param columns
	 *            下一级子元素
	 * @return
	 */
	private static int getNextSize(Column[] columns) {
		int size = 1;
		for (Column column : columns) {
			// 获取子表头
			Column[] childrens = column.getChildren();
			if ((childrens != null) && (childrens.length > 0)) {
				size = childrens.length;
			}
		}
		return size;
	}

	public static void setMergedCellStyle(CellRangeAddress cra, Sheet sheet) {
		// 使用RegionUtil类为合并后的单元格添加边框
		RegionUtil.setBorderBottom(1, cra, sheet); // 下边框
		RegionUtil.setBorderLeft(1, cra, sheet); // 左边框
		RegionUtil.setBorderRight(1, cra, sheet); // 有边框
		RegionUtil.setBorderTop(1, cra, sheet); // 上边框
	}

	/**
	 * 最大层级数
	 * 
	 * @param columns
	 * @param maxLevel
	 * @return
	 */
	private static int getMaxLevel(Column[] columns, int maxLevel) {
		if (maxLevel > HEADER_MAX_LEVEL) {
			return HEADER_MAX_LEVEL;
		}
		for (Column column : columns) {
			// 表头层级
			int level = column.getLevel();
			// 获取子表头
			Column[] childrens = column.getChildren();
			if ((childrens != null) && (childrens.length > 0)) {
				if (maxLevel < (level + 1)) {
					maxLevel++;
				}
				maxLevel = getMaxLevel(childrens, maxLevel);
			}
		}
		return maxLevel;
	}

	/**
	 * 设置单元格宽度
	 * 
	 * @param sheet
	 * @param column
	 *
	 */
	private static int setColumnWidth(Sheet sheet, Column column, int cellNum, String width) {
		width = width == null ? "150" : width;
		// 获取子表头
		Column[] childrens = column.getChildren();
		if ((cellNum < COLUMN_MAX_NUM) && (childrens != null) && (childrens.length > 0)) {
			for (Column children : childrens) {
				cellNum = setColumnWidth(sheet, children, cellNum, width);
			}
		} else {
			if (width.matches("\\d+(px)?")) {
				if (width.endsWith("px")) {
					width = width.substring(0, width.length() - 2);
				}
				int w = Integer.parseInt(width) / 6;
				w = w > 254 ? 254 : w;
				sheet.setColumnWidth(cellNum, w * 512);
			}
		}
		cellNum++;
		return cellNum;
	}

	/**
	 * 创建表格样式
	 * 
	 * @param wb 工作薄对象
	 * @return 样式列表
	 */
	public static Map<String, CellStyle> createStyles(Workbook wb) {
		Map<String, CellStyle> styles = new HashMap<String, CellStyle>();

		CellStyle style = wb.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		Font titleFont = wb.createFont();
		titleFont.setFontName(FONT_DEFAULT_NAME);
		titleFont.setFontHeightInPoints(FONT_DEFAULT_SIZE);
		titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(titleFont);
		styles.put("title", style);

		style = wb.createCellStyle();
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
		Font dataFont = wb.createFont();
		dataFont.setFontName(FONT_DEFAULT_NAME);
		dataFont.setFontHeightInPoints(FONT_DEFAULT_SIZE);
		style.setFont(dataFont);
		styles.put("data_", style);

		style = wb.createCellStyle();
		style.cloneStyleFrom(styles.get("data_"));
		style.setAlignment(HorizontalAlignment.LEFT);
		styles.put("data_is-left", style);

		style = wb.createCellStyle();
		style.cloneStyleFrom(styles.get("data_"));
		style.setAlignment(HorizontalAlignment.CENTER);
		styles.put("data_is-center", style);

		style = wb.createCellStyle();
		style.cloneStyleFrom(styles.get("data_"));
		style.setAlignment(HorizontalAlignment.RIGHT);
		styles.put("data_is-right", style);

		style = wb.createCellStyle();
		style.cloneStyleFrom(styles.get("data_"));
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		Font headerFont = wb.createFont();
		headerFont.setFontName(FONT_DEFAULT_NAME);
		headerFont.setFontHeightInPoints(FONT_DEFAULT_SIZE);
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.WHITE.getIndex());
		style.setFont(headerFont);
		styles.put("header", style);

		style = wb.createCellStyle();
		style.cloneStyleFrom(styles.get("data_"));
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		Font headerFont1 = wb.createFont();
		headerFont1.setFontName(FONT_DEFAULT_NAME);
		headerFont1.setFontHeightInPoints(FONT_DEFAULT_SIZE);
		headerFont1.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont1.setColor(IndexedColors.WHITE.getIndex());
		style.setFont(headerFont1);
		styles.put("header1", style);
		return styles;
	}

	/**
	 * 获取单元格格式
	 * 
	 * @param val
	 * @return
	 */
	private static String getCellFormatString(Object val) {
		String cellFormatString = "@";
		if (val instanceof Integer) {
			cellFormatString = "0";
		} else if (val instanceof Long) {
			cellFormatString = "0";
		} else if (val instanceof Double) {
			cellFormatString = "0.00";
		} else if (val instanceof Float) {
			cellFormatString = "0.00";
		} else if (val instanceof Date) {
			cellFormatString = "yyyy-MM-dd HH:mm";
		} else if (val instanceof BigDecimal) {
			cellFormatString = "0.00";
		}

		return cellFormatString;
	}

	public ExcelUtils(Class<T> clazz) {
		this.clazz = clazz;
	}
}