package com.cdeercloud.common.excel.config;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.cdeercloud.common.excel.wrapper.ResultWrapper;

@ControllerAdvice
public class CdeerWebExceptionHandler {

	@Resource(name = "${web.response.wrapper.bean}")
	private ResultWrapper exceptionWrapper;

	@Value("${web.response.wrapper}")
	private boolean wrapper;

	@ResponseBody
	@ExceptionHandler
	public Object defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		if (!this.wrapper) {
			throw e;
		}
		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
			throw e;
		}
		return this.exceptionWrapper.wrapException(e);
	}
}
