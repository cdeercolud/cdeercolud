package com.cdeercloud.common.excel.imports;

import javax.annotation.Resource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class ImportHandlerBeanPostProcessor implements BeanPostProcessor {
	@Resource
	private ImportHandlerRegistry handlerRegistry;

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if ((bean instanceof ImportHandler)) {
			ImportHandler<?> rule = (ImportHandler) bean;
			this.handlerRegistry.registry(rule.businessName(), rule);
		}
		return bean;
	}
}
