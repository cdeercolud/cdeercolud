package com.cdeercloud.common.excel.config;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.MethodParameter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.excel.export.ExcelUtils;
import com.cdeercloud.common.excel.export.dto.Column;
import com.cdeercloud.common.excel.export.dto.ExportConfig;
import com.cdeercloud.common.excel.wrapper.ResultWrapper;
import com.cdeercloud.common.excel.wrapper.anno.UnResultWrapper;
import com.cdeercloud.common.excel.wrapper.vo.WrappingResult;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CdeerWebReturnValueHandler implements HandlerMethodReturnValueHandler {
	private RequestResponseBodyMethodProcessor processor;
	private ResultWrapper successWrapper;
	private boolean wrapper;
	private static final String CDEER_TABLE_EXPORT_FLAG = "cdeer_table_export_flag";

	public CdeerWebReturnValueHandler(RequestResponseBodyMethodProcessor processor, boolean wrapper, ResultWrapper successWrapper) {
		this.processor = processor;
		this.wrapper = wrapper;
		this.successWrapper = successWrapper;
	}

	public void handleReturnValueWrap(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
		if (this.needWrapper(returnValue, returnType)) {
			Object wrap = this.successWrapper.wrapSuccess(returnValue);
			this.processor.handleReturnValue(wrap, returnType, mavContainer, webRequest);
		} else {
			this.processor.handleReturnValue(returnValue, returnType, mavContainer, webRequest);
		}
	}

	private boolean needWrapper(Object returnValue, MethodParameter returnType) {
		if (!this.wrapper) {
			return false;
		} else if (returnValue instanceof WrappingResult) {
			return false;
		} else if (returnType.getMethodAnnotation(UnResultWrapper.class) != null) {
			return false;
		}
		return true;
	}

	@Override
	public boolean supportsReturnType(MethodParameter returnType) {
		return this.processor.supportsReturnType(returnType);
	}

	@Override
	public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
		ServletServerHttpRequest inputMessage = this.createInputMessage(webRequest);
		ServletServerHttpResponse outputMessage = this.createOutputMessage(webRequest);
		HttpServletRequest request = inputMessage.getServletRequest();
		HttpServletResponse response = outputMessage.getServletResponse();
		String flag = request.getParameter(CDEER_TABLE_EXPORT_FLAG);
		if (flag == null) {
			this.handleReturnValueWrap(returnValue, returnType, mavContainer, webRequest);
		} else {
			mavContainer.setRequestHandled(true);
			response.setContentType("application/octet-stream");
			ObjectMapper objectMapper = new ObjectMapper();
			String exportConfigStr = request.getParameter("cdeer_table_export_config");
			ExportConfig exportConfig = objectMapper.readValue(exportConfigStr, ExportConfig.class);
			String fileName = exportConfig.getFileName();
			String version = exportConfig.getVersion();

			String downloadName = "";
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
				downloadName = URLEncoder.encode(fileName, "UTF-8");
				// 替换文件名中的空格
				downloadName = StringUtils.replace(downloadName, "+", "%20");
			} else {
				downloadName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
			}
			response.setHeader("Content-Disposition", "attachment; filename=" + downloadName + "." + version);
			if (returnValue instanceof PagedResult<?>) {
				PagedResult<?> pr = (PagedResult<?>) returnValue;
				// cdeer_table_export_column_metas数据 ：
				// [{"label":"姓名","property":"employeeName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"工号","property":"employeeCode","align":"is-left","width":130,"level":1,"mapping":null},{"label":"身份证号码","property":"idNumber","align":"is-left","width":130,"level":1,"mapping":null},{"label":"入职时间","property":"hireDate","align":"is-left","width":130,"level":1,"mapping":{"cdeer_date":"yyyy-MM-dd"}},{"label":"离职前公司","property":"areaCompanyName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前部门","property":"orgUnitName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前职位","property":"posPositionName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职前职级","property":"positionLevelName","align":"is-left","width":130,"level":1,"mapping":null},{"label":"离职时间","property":"dimissionDate","align":"is-left","width":130,"level":1,"mapping":{"cdeer_date":"yyyy-MM-dd"}},{"label":"离职类型","property":"dimissionType","align":"is-left","width":130,"level":1,"mapping":{"1":"主动离职","2":"被动离职","3":"特殊离职"}},{"label":"是否已在黑名单","property":"inBlackList","align":"is-left","width":130,"level":1,"mapping":{"Y":"是","N":"否"}}]
				String meta = request.getParameter("cdeer_table_export_column_metas");

				Column[] cols = objectMapper.readValue(meta, Column[].class);
				Workbook wb = null;
				if ("multiple".equals(flag)) {
					wb = ExcelUtils.multipleHeader(pr, cols, exportConfig);
				} else {
					wb = ExcelUtils.createExcel(pr, cols, exportConfig);
				}
				if (exportConfig.getSync()) {
					wb.write(outputMessage.getBody());
					outputMessage.getBody().flush();
				}
			}
		}

	}

	protected ServletServerHttpRequest createInputMessage(NativeWebRequest webRequest) {
		HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
		return new ServletServerHttpRequest(servletRequest);
	}

	protected ServletServerHttpResponse createOutputMessage(NativeWebRequest webRequest) {
		HttpServletResponse response = webRequest.getNativeResponse(HttpServletResponse.class);
		return new ServletServerHttpResponse(response);
	}
}
