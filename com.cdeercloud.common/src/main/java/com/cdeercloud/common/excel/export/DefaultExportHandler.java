package com.cdeercloud.common.excel.export;

import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Service;

@Service("default_export_handler")
public class DefaultExportHandler implements ExportHandler {
	@Override
	public void export(Object returnValue, ServletServerHttpRequest inputMessage, ServletServerHttpResponse outputMessage) throws Exception {
	}
}
