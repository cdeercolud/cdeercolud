package com.cdeercloud.common.excel.export.dto;

import java.util.Map;

/**
 * 表格列类
 * 
 * @author looyii
 *
 */
public class Column {
	private String label;
	private String property;
	private boolean sortable;

	private String abbr;
	private String align;
	private String axis;
	private String background;
	private String bgcolor;
	private String bordercolor;
	private String cssChar;
	private String charoff;
	private String cssClass;
	private String dir;
	private String headers;
	private String height;
	private String lang;
	private String nowrap;
	private String onclick;
	private String ondbclick;
	private String onhelp;
	private String onkeydown;
	private String onkeypress;
	private String onkeyup;
	private String onmousedown;
	private String onmousemove;
	private String onmouseout;
	private String onmouseover;
	private String onmouseup;
	private String scope;
	private String cssStyle;
	private String valign;
	private String width;
	private boolean showCellTitle;

	private Map<?, ?> mapping;

	private String cellRender;

	private Column[] children;
	private int level;

	public Column() {
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * 获取property。
	 * 
	 * @return String
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * 设置property。
	 * 
	 * @param property String
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * 设置sortable。
	 * 
	 * @param sortable boolean
	 */
	public void setSortable(boolean sortable) {
		this.sortable = sortable;
	}

	/**
	 * 判断是否排序。
	 * 
	 * @return boolean
	 */
	public boolean isSortable() {
		return sortable;
	}

	/**
	 * 获取abbr。
	 * 
	 * @return String
	 */
	public String getAbbr() {
		return abbr;
	}

	/**
	 * 设置abbr。
	 * 
	 * @param abbr String
	 */
	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	/**
	 * 获取align。
	 * 
	 * @return String
	 */
	public String getAlign() {
		return align;
	}

	/**
	 * 设置align。
	 * 
	 * @param align String
	 */
	public void setAlign(String align) {
		this.align = align;
	}

	/**
	 * 获取axis。
	 * 
	 * @return String
	 */
	public String getAxis() {
		return axis;
	}

	/**
	 * 设置axis。
	 * 
	 * @param axis String
	 */
	public void setAxis(String axis) {
		this.axis = axis;
	}

	/**
	 * 获取background。
	 * 
	 * @return String
	 */
	public String getBackground() {
		return background;
	}

	/**
	 * 设置background。
	 * 
	 * @param background String
	 */
	public void setBackground(String background) {
		this.background = background;
	}

	/**
	 * 获取bgcolor。
	 * 
	 * @return String
	 */
	public String getBgcolor() {
		return bgcolor;
	}

	/**
	 * 设置bgcolor。
	 * 
	 * @param bgcolor String
	 */
	public void setBgcolor(String bgcolor) {
		this.bgcolor = bgcolor;
	}

	/**
	 * 获取bordercolor。
	 * 
	 * @return String
	 */
	public String getBordercolor() {
		return bordercolor;
	}

	/**
	 * 设置bordercolor。
	 * 
	 * @param bordercolor String
	 */
	public void setBordercolor(String bordercolor) {
		this.bordercolor = bordercolor;
	}

	/**
	 * 获取cssChar。
	 * 
	 * @return String
	 */
	public String getCssChar() {
		return cssChar;
	}

	/**
	 * 设置cssChar。
	 * 
	 * @param cssChar String
	 */
	public void setCssChar(String cssChar) {
		this.cssChar = cssChar;
	}

	/**
	 * 获取charoff。
	 * 
	 * @return String
	 */
	public String getCharoff() {
		return charoff;
	}

	/**
	 * 设置charoff。
	 * 
	 * @param charoff String
	 */
	public void setCharoff(String charoff) {
		this.charoff = charoff;
	}

	/**
	 * 获取cssClass。
	 * 
	 * @return String
	 */
	public String getCssClass() {
		return cssClass;
	}

	/**
	 * 设置cssClass。
	 * 
	 * @param cssClass String
	 */
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	/**
	 * 获取dir。
	 * 
	 * @return String
	 */
	public String getDir() {
		return dir;
	}

	/**
	 * 设置dir。
	 * 
	 * @param dir String
	 */
	public void setDir(String dir) {
		this.dir = dir;
	}

	/**
	 * 获取headers。
	 * 
	 * @return String
	 */
	public String getHeaders() {
		return headers;
	}

	/**
	 * 设置headers。
	 * 
	 * @param headers String
	 */
	public void setHeaders(String headers) {
		this.headers = headers;
	}

	/**
	 * 获取height。
	 * 
	 * @return String
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * 设置height。
	 * 
	 * @param height String
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * 获取lang。
	 * 
	 * @return String
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * 设置lang。
	 * 
	 * @param lang String
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * 获取nowrap。
	 * 
	 * @return String
	 */
	public String getNowrap() {
		return nowrap;
	}

	/**
	 * 设置nowrap。
	 * 
	 * @param nowrap String
	 */
	public void setNowrap(String nowrap) {
		this.nowrap = nowrap;
	}

	/**
	 * 获取onclick。
	 * 
	 * @return String
	 */
	public String getOnclick() {
		return onclick;
	}

	/**
	 * 设置onclick。
	 * 
	 * @param onclick String
	 */
	public void setOnclick(String onclick) {
		this.onclick = onclick;
	}

	/**
	 * 获取ondbclick。
	 * 
	 * @return String
	 */
	public String getOndbclick() {
		return ondbclick;
	}

	/**
	 * 设置ondbclick。
	 * 
	 * @param ondbclick String
	 */
	public void setOndbclick(String ondbclick) {
		this.ondbclick = ondbclick;
	}

	/**
	 * 获取onhelp。
	 * 
	 * @return String
	 */
	public String getOnhelp() {
		return onhelp;
	}

	/**
	 * 设置onhelp。
	 * 
	 * @param onhelp String
	 */
	public void setOnhelp(String onhelp) {
		this.onhelp = onhelp;
	}

	/**
	 * 获取onkeydown。
	 * 
	 * @return String
	 */
	public String getOnkeydown() {
		return onkeydown;
	}

	/**
	 * 设置onkeydown。
	 * 
	 * @param onkeydown String
	 */
	public void setOnkeydown(String onkeydown) {
		this.onkeydown = onkeydown;
	}

	/**
	 * 获取onkeypress。
	 * 
	 * @return String
	 */
	public String getOnkeypress() {
		return onkeypress;
	}

	/**
	 * 设置onkeypress。
	 * 
	 * @param onkeypress String
	 */
	public void setOnkeypress(String onkeypress) {
		this.onkeypress = onkeypress;
	}

	/**
	 * 获取onkeyup。
	 * 
	 * @return String
	 */
	public String getOnkeyup() {
		return onkeyup;
	}

	/**
	 * 设置onkeyup。
	 * 
	 * @param onkeyup String
	 */
	public void setOnkeyup(String onkeyup) {
		this.onkeyup = onkeyup;
	}

	/**
	 * 获取onmousedown。
	 * 
	 * @return String
	 */
	public String getOnmousedown() {
		return onmousedown;
	}

	/**
	 * 设置onmousedown。
	 * 
	 * @param onmousedown String
	 */
	public void setOnmousedown(String onmousedown) {
		this.onmousedown = onmousedown;
	}

	/**
	 * 获取onmousemove。
	 * 
	 * @return String
	 */
	public String getOnmousemove() {
		return onmousemove;
	}

	/**
	 * 设置onmousemove。
	 * 
	 * @param onmousemove String
	 */
	public void setOnmousemove(String onmousemove) {
		this.onmousemove = onmousemove;
	}

	/**
	 * 获取onmouseout。
	 * 
	 * @return String
	 */
	public String getOnmouseout() {
		return onmouseout;
	}

	/**
	 * 设置onmouseout。
	 * 
	 * @param onmouseout String
	 */
	public void setOnmouseout(String onmouseout) {
		this.onmouseout = onmouseout;
	}

	/**
	 * 获取onmouseover。
	 * 
	 * @return String
	 */
	public String getOnmouseover() {
		return onmouseover;
	}

	/**
	 * 设置onmouseout。
	 * 
	 * @param onmouseover String
	 */
	public void setOnmouseover(String onmouseover) {
		this.onmouseover = onmouseover;
	}

	/**
	 * 获取onmouseup。
	 * 
	 * @return String
	 */
	public String getOnmouseup() {
		return onmouseup;
	}

	/**
	 * 设置onmouseup。
	 * 
	 * @param onmouseup String
	 */
	public void setOnmouseup(String onmouseup) {
		this.onmouseup = onmouseup;
	}

	/**
	 * 获取scope。
	 * 
	 * @return String
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * 设置scope。
	 * 
	 * @param scope String
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * 获取cssStyle。
	 * 
	 * @return String
	 */
	public String getCssStyle() {
		return cssStyle;
	}

	/**
	 * 设置cssStyle。
	 * 
	 * @param cssStyle String
	 */
	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}

	/**
	 * 获取valign。
	 * 
	 * @return String
	 */
	public String getValign() {
		return valign;
	}

	/**
	 * 设置valign。
	 * 
	 * @param valign String
	 */
	public void setValign(String valign) {
		this.valign = valign;
	}

	/**
	 * 获取width。
	 * 
	 * @return String
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * 设置width。
	 * 
	 * @param width String
	 */
	public void setWidth(String width) {
		this.width = width;
	}

	/**
	 * 获取mapping。
	 * 
	 * @return Map<?, ?>
	 */
	public Map<?, ?> getMapping() {
		return mapping;
	}

	/**
	 * 设置mapping。
	 * 
	 * @param mapping mapping
	 */
	public void setMapping(Map<?, ?> mapping) {
		this.mapping = mapping;
	}

	/**
	 * 获取showCellTitle。
	 * 
	 * @return boolean
	 */
	public boolean isShowCellTitle() {
		return showCellTitle;
	}

	/**
	 * 设置showCellTitle。
	 * 
	 * @param showCellTitle boolean
	 */
	public void setShowCellTitle(boolean showCellTitle) {
		this.showCellTitle = showCellTitle;
	}

	/**
	 * 获取cellRender。
	 * 
	 * @return String
	 */
	public String getCellRender() {
		return cellRender;
	}

	/**
	 * 设置cellRender。
	 * 
	 * @param cellRender String
	 */
	public void setCellRender(String cellRender) {
		this.cellRender = cellRender;
	}

	/**
	 * 获取children。
	 * 
	 * @return Column
	 */
	public Column[] getChildren() {
		return children;
	}

	/**
	 * 设置children。
	 * 
	 * @param children Column
	 */
	public void setChildren(Column[] children) {
		this.children = children;
	}

	/**
	 * 获取level。
	 * 
	 * @return int
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * 设置level。
	 * 
	 * @param level int
	 */
	public void setLevel(int level) {
		this.level = level;
	}

}
