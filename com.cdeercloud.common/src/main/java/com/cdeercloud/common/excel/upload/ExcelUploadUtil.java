package com.cdeercloud.common.excel.upload;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cdeercloud.common.util.LogUtil;

/**
 * excel导入返回表格数据
 * 
 * @author looyii
 *
 */
public class ExcelUploadUtil {
	private static Logger log = Logger.getLogger(ExcelUploadUtil.class);

	/**
	 * 读取Excel文件
	 * 
	 * @param file
	 * @return
	 */
	public static List<ExcelData> readExcel(InputStream is, String fileName) {
		List<ExcelData> datas = null;
		if (StringUtils.isBlank(fileName) || (is == null)) {
			return datas;
		}
		String extension = FilenameUtils.getExtension(fileName);
		extension = StringUtils.isNotBlank(extension) ? extension.toLowerCase() : extension;
		try {
			if ("xls".equals(extension)) {
				datas = readExcel2003(is);
			} else if ("xlsx".equals(extension)) {
				datas = readExcel2007(is);
			}
		} catch (Exception e) {
			LogUtil.exception(log, e);
		}
		return datas;
	}

	/**
	 * 读取Excel2007
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static List<ExcelData> readExcel2007(InputStream is) throws IOException {
		List<ExcelData> datas = new ArrayList<ExcelData>();
		XSSFWorkbook workbook = new XSSFWorkbook(is);
		try {
			// 判断Sheet的总数是否大于0
			if (workbook.getNumberOfSheets() > 0) {
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
					XSSFSheet sheet = workbook.getSheetAt(i);
					if (sheet != null) {
						List<List<String>> rows = new ArrayList<List<String>>();
						ExcelData data = new ExcelData();
						data.setTitle(sheet.getSheetName());
						data.setDatas(rows);
						datas.add(data);
						// 读取Row
						int rowNum = sheet.getPhysicalNumberOfRows();
						if (rowNum > 0) {
							for (int j = 0; j < rowNum; j++) {
								XSSFRow xssfRow = sheet.getRow(j);
								if ((xssfRow != null) && (xssfRow.getLastCellNum() > 0)) {
									List<String> row = new ArrayList<String>();
									rows.add(row);
									if (xssfRow.getLastCellNum() > data.getColumns()) {
										data.setColumns(xssfRow.getLastCellNum());
									}
									for (int k = 0; k < xssfRow.getLastCellNum(); k++) {
										XSSFCell cell = xssfRow.getCell(k);
										if (cell != null) {
											cell.setCellType(HSSFCell.CELL_TYPE_STRING);
										}
										row.add((cell != null) && StringUtils.isNotBlank(cell.getStringCellValue()) ? cell.getStringCellValue() : "");
									}
								}
							}
						}
					}
				}
			}
		} finally {
			workbook.close();
		}
		return datas;
	}

	/**
	 * 读取Excel2003
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static List<ExcelData> readExcel2003(InputStream is) throws IOException {
		List<ExcelData> datas = new ArrayList<ExcelData>();
		HSSFWorkbook workbook = new HSSFWorkbook(is);
		try {
			// 判断Sheet的总数是否大于0
			if (workbook.getNumberOfSheets() > 0) {
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
					HSSFSheet sheet = workbook.getSheetAt(i);
					if (sheet != null) {
						List<List<String>> rows = new ArrayList<List<String>>();
						ExcelData data = new ExcelData();
						data.setTitle(sheet.getSheetName());
						data.setDatas(rows);
						datas.add(data);
						// 读取Row
						int rowNum = sheet.getPhysicalNumberOfRows();
						if (rowNum > 0) {
							for (int j = 0; j < rowNum; j++) {
								HSSFRow hssfRow = sheet.getRow(j);
								if ((hssfRow != null) && (hssfRow.getLastCellNum() > 0)) {
									List<String> row = new ArrayList<String>();
									rows.add(row);
									if (hssfRow.getLastCellNum() > data.getColumns()) {
										data.setColumns(hssfRow.getLastCellNum());
									}
									for (int k = 0; k < hssfRow.getLastCellNum(); k++) {
										HSSFCell cell = hssfRow.getCell(k);
										if (cell != null) {
											cell.setCellType(HSSFCell.CELL_TYPE_STRING);
										}
										row.add((cell != null) && StringUtils.isNotBlank(cell.getStringCellValue()) ? cell.getStringCellValue() : "");
									}
								}
							}
						}
					}
				}
			}
		} finally {
			workbook.close();
		}
		return datas;
	}
}
