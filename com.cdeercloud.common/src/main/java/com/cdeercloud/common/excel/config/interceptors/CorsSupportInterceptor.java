package com.cdeercloud.common.excel.config.interceptors;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cdeercloud.common.util.CorsUtils;

public class CorsSupportInterceptor extends HandlerInterceptorAdapter {

	final Logger logger = LoggerFactory.getLogger(CorsSupportInterceptor.class);
	private Set<String> ips;
	private boolean anyAllow;
	private boolean isProxy;

	public CorsSupportInterceptor(Set<String> ips, boolean isProxy) {
		this.ips = ips;
		this.anyAllow = CorsUtils.isAnyAllow(this.ips);
		this.isProxy = isProxy;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		CorsUtils.addCorsSupport(request, response, this.ips, this.anyAllow, this.isProxy);
		return super.preHandle(request, response, handler);
	}

}
