package com.cdeercloud.common.excel.export.dto;

import java.util.Map;

public class ExportConfig {
	private String fileName;
	private String version;
	private String range;
	private boolean sync;
	private String execTime;

	private Map<String, Map<String, ?>> customer;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public boolean getSync() {
		return sync;
	}

	public void setSync(boolean sync) {
		this.sync = sync;
	}

	public String getExecTime() {
		return execTime;
	}

	public void setExecTime(String execTime) {
		this.execTime = execTime;
	}

	public Map<String, Map<String, ?>> getCustomer() {
		return customer;
	}

	public void setCustomer(Map<String, Map<String, ?>> customer) {
		this.customer = customer;
	}

}
