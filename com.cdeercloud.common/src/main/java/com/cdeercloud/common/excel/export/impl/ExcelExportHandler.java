package com.cdeercloud.common.excel.export.impl;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.excel.export.ExportHandler;
import com.cdeercloud.common.excel.export.dto.Column;
import com.cdeercloud.common.excel.export.dto.ExportConfig;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * TODO 导出实现类
 * 
 * @author looyii
 *
 */
@Component("excelExportHandler")
public class ExcelExportHandler extends MybatisServiceImpl implements ExportHandler {

	private final static Logger logger = LoggerFactory.getLogger(ExcelExportHandler.class);

	@Override
	public void export(Object returnValue, ServletServerHttpRequest inputMessage, ServletServerHttpResponse outputMessage) throws Exception {
		HttpServletRequest request = inputMessage.getServletRequest();
		HttpServletResponse response = outputMessage.getServletResponse();
		ObjectMapper objectMapper = new ObjectMapper();
		String exportConfigStr = request.getParameter("cdeer_table_export_config");
		ExportConfig exportConfig = objectMapper.readValue(exportConfigStr, ExportConfig.class);
		String fileName = exportConfig.getFileName();
		String version = exportConfig.getVersion();
		// 导出模式，single 单行标题， multiple 多行标题
		String flag = request.getParameter("cdeer_table_export_flag");
		String downloadName = "";
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
			downloadName = URLEncoder.encode(fileName, "UTF-8");
			// 替换文件名中的空格
			downloadName = StringUtils.replace(downloadName, "+", "%20");
		} else {
			downloadName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
		}

		if (returnValue instanceof PagedResult<?>) {
			PagedResult<?> pr = (PagedResult<?>) returnValue;
			String meta = request.getParameter("sahara_table_export_column_metas");
			Column[] cols = objectMapper.readValue(meta, Column[].class);
			Workbook wb = null;
			boolean bool = true;
			try {
				if ("multiple".equals(flag)) {
					// TODO 返回多表头的excel
					// wb = this.multipleHeader(pr, cols, exportConfig);
				} else {
					// TODO 返回单表头的excel
					// wb = this.createExcel(pr, cols, exportConfig);
				}
			} catch (Exception e) {
				bool = false;
				logger.error("导出生成excel异常：", e);
			}

			if (exportConfig.getSync()) {
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", "attachment; filename=" + downloadName + "." + version);
				if (exportConfig.getSync()) {
					wb.write(outputMessage.getBody());
					outputMessage.getBody().flush();
				}
			} else {
				// 异步下载 ，考虑写入相关表，通过任务提示
			}
		}
	}

}
