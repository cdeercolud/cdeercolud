package com.cdeercloud.common.excel.web;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.excel.imports.ImportHandler;
import com.cdeercloud.common.excel.imports.ImportHandlerRegistry;
import com.cdeercloud.common.web.CdeerController;

//TODO  待实现， 通过全局格式实现
@SuppressWarnings({ "unchecked" })
@RestController
public class ExcelImportController extends CdeerController {

	@Resource
	private ImportHandlerRegistry registry;

	// TODO 下载模板方法

	// TODO 下载错误数据

	/**
	 * 导入excel接口
	 * 
	 * @param business 业务名称（全系统唯一）
	 * @param file 文件对象
	 * @param context 自定义参数
	 * @param response
	 * @return
	 */
	@SuppressWarnings({ "unused" })
	@PostMapping("/common/imports/{business}")
	public Map<String, String> imports(@PathVariable String business, MultipartFile file, @RequestParam(required = false) String params) {
		if (registry == null) {
			throw new CdeerException("registry " + registry + " 没有注入成功.");
		}
		ImportHandler<Object> handler = (ImportHandler<Object>) registry.getHandler(business);
		if (handler == null) {
			throw new CdeerException("未找到业务功能 " + business + " 对应的导入处理逻辑.");
		}
		// TODO 第几行数据开始
		handler.dataFirstRowNum();

		// TODO 验证行数据
		// handler.validate(paramT, paramBindingResult);

		// TODO 验证成功保存数据
		// handler.handle(paramT, paramImportContext);

		Map map = new HashMap<>();
		map.put("id", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("total", 0);// 导入总行数
		map.put("fail", 0);// 错误行数
		map.put("success", "0");// 成功行数
		return map;
	}

}
