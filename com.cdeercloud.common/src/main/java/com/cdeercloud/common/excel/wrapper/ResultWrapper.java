package com.cdeercloud.common.excel.wrapper;

public interface ResultWrapper {

	public Object wrapException(Exception e);

	public Object wrapSuccess(Object data);
}
