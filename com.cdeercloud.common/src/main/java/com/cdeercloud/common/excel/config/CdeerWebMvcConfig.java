package com.cdeercloud.common.excel.config;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.cdeercloud.common.excel.config.interceptors.CorsSupportInterceptor;
import com.cdeercloud.common.excel.config.interceptors.LogTraceIdInterceptor;

@Configuration
public class CdeerWebMvcConfig extends WebMvcConfigurerAdapter {

	@Value("${web.cors.ips}")
	private String corsIps;
	@Value("${web.cors.proxy}")
	private boolean corsProxy;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		HashSet<String> ipsSet = new HashSet<>();
		if (StringUtils.hasText(this.corsIps)) {
			for (String ip : corsIps.split(",")) {
				ipsSet.add(ip);
			}
		}
		registry.addInterceptor(new CorsSupportInterceptor(ipsSet, this.corsProxy));
		registry.addInterceptor(new LogTraceIdInterceptor());
	}

}
