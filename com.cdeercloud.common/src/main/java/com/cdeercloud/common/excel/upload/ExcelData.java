package com.cdeercloud.common.excel.upload;

import java.io.Serializable;
import java.util.List;

public class ExcelData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8170047809403648247L;
	private String title; // 标题
	private List<List<String>> datas; // 数据
	private int columns;// 列数

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<List<String>> getDatas() {
		return datas;
	}

	public void setDatas(List<List<String>> datas) {
		this.datas = datas;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	} 

}
