package com.cdeercloud.common.excel.imports;

import java.util.Map;

import org.springframework.validation.BindingResult;

public class ImportContext {
	private Map<String, Object> params;
	private int sheetNum = -1;
	private int total;
	private int rowNum;
	private int code;
	public Map<String, Object> result;
	private Object next;
	private BindingResult br;

	public Map<String, Object> getParams() {
		return this.params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public int getSheetNum() {
		return this.sheetNum;
	}

	public void setSheetNum(int sheetNum) {
		this.sheetNum = sheetNum;
	}

	public int getTotal() {
		return this.total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCode() {
		return this.code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getRowNum() {
		return this.rowNum;
	}

	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}

	public BindingResult getBr() {
		return this.br;
	}

	public void setBr(BindingResult br) {
		this.br = br;
	}

	public Object getNext() {
		return this.next;
	}

	public void setNext(Object next) {
		this.next = next;
	}
}
