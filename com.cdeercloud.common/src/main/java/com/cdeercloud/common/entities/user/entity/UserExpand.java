package com.cdeercloud.common.entities.user.entity;

import java.io.Serializable;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 
 * @author looyii
 *
 */
@ApiModel("用户扩展表")
@CdeerEntity
@CdeerTable(name = "t_user_expand")
@JsonInclude(value = Include.NON_NULL)
public class UserExpand implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4933756222085488182L;

	@ApiModelProperty(value = "主键ID")
	private String id;

	@ApiModelProperty(value = "用户ID User.id")
	private String userId;

	@ApiModelProperty(value = "* 年龄", required = true)
	private Integer age;

	@ApiModelProperty(value = "是否住宿（Y：是、N：否）")
	private String isStay;

	@ApiModelProperty(value = "身份证")
	private String idcard;

	@ApiModelProperty(value = "* 生日", required = true)
	private String birthDay;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "* 民族", required = true)
	private String national;

	@ApiModelProperty(value = "学籍号")
	private String studentCode;

	@ApiModelProperty(value = "学号")
	private String studentNo;

	@ApiModelProperty(value = "地址")
	private String address;

	@ApiModelProperty(value = "微信")
	private String wechat;

	@ApiModelProperty(value = "QQ")
	private String qq;

	@ApiModelProperty(value = "业余爱好")
	private String hobby;

	@ApiModelProperty(value = "政治面貌")
	private String politicalOutLook;

	@ApiModelProperty(value = "婚姻生育")
	private String maritalFertility;

	@ApiModelProperty(value = "信息完成百分比")
	private String rate;

	@ApiModelProperty(value = "密码")
	private String password;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getIsStay() {
		return isStay;
	}

	public void setIsStay(String isStay) {
		this.isStay = isStay;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNational() {
		return national;
	}

	public void setNational(String national) {
		this.national = national;
	}

	public String getStudentCode() {
		return studentCode;
	}

	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	public String getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getPoliticalOutLook() {
		return politicalOutLook;
	}

	public void setPoliticalOutLook(String politicalOutLook) {
		this.politicalOutLook = politicalOutLook;
	}

	public String getMaritalFertility() {
		return maritalFertility;
	}

	public void setMaritalFertility(String maritalFertility) {
		this.maritalFertility = maritalFertility;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
