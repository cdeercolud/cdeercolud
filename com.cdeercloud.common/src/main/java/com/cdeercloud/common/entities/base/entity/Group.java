package com.cdeercloud.common.entities.base.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 
 * @author looyii
 *
 */
@ApiModel(value = "群组表", description = "教育局、学校空间、班级、普通群组")
@CdeerEntity
@CdeerTable(name = "t_group")
@JsonInclude(value = Include.NON_NULL)
public class Group implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6887510117849442048L;

	@ApiModelProperty(value = "群组主键id")
	private String id;

	@ApiModelProperty(value = "群组编号")
	private String groupCode;

	@ApiModelProperty(value = "* 群组名称", required = true)
	@NotBlank(message = "群组名称不能为空")
	private String name;

	@ApiModelProperty(value = "群组拼音")
	private String pinyin;

	@ApiModelProperty(value = "＊ 1：教育局、2：学校、3：校区、4、班级、5：普通小组", required = true)
	private String type;

	@ApiModelProperty(value = "群头像")
	private String avatar;

	@ApiModelProperty(value = "群组父CODE")
	private String parentCode;

	@ApiModelProperty(value = "上下级关系")
	private String levelPath;

	@ApiModelProperty(value = "用户数量")
	private String userCount;

	@ApiModelProperty(value = "排序方式")
	private String sort;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

	@ApiModelProperty(value = "前缀：拼音第一个字母")
	private String prefix;

	@ApiModelProperty(value = "群组扩展")
	private GroupExpand groupExpand;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getLevelPath() {
		return levelPath;
	}

	public void setLevelPath(String levelPath) {
		this.levelPath = levelPath;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public GroupExpand getGroupExpand() {
		return groupExpand;
	}

	public void setGroupExpand(GroupExpand groupExpand) {
		this.groupExpand = groupExpand;
	}

}
