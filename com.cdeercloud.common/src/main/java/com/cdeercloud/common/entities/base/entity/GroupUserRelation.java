package com.cdeercloud.common.entities.base.entity;

import java.io.Serializable;
import java.util.Date;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 
 * @author looyii
 *
 */
@ApiModel(value = "群组用户关系表", description = "用户所在群组")
@CdeerEntity
@CdeerTable(name = "t_group_user_relation")
@JsonInclude(value = Include.NON_NULL)
public class GroupUserRelation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5326943673618632829L;

	@ApiModelProperty(value = "主键ID")
	private String id;

	@ApiModelProperty(value = "* 角色：1、管理员、2：老师、3：家长、4：学生、5：普通用户", required = true)
	private String type;

	@ApiModelProperty(value = "* 用户所在群组", required = true)
	private String groupCode;

	@ApiModelProperty(value = "* 用户ID User.id", required = true)
	private String userId;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

	@ApiModelProperty(value = "职务")
	private String job;

	@ApiModelProperty(value = "班级岗位")
	private String classJob;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getClassJob() {
		return classJob;
	}

	public void setClassJob(String classJob) {
		this.classJob = classJob;
	}

}
