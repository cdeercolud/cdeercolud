package com.cdeercloud.common.entities.base.entity;

import java.io.Serializable;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author looyii
 *
 */
@ApiModel(value = "群组扩展表", description = "群组扩展表，通过groupCode关联")
@CdeerEntity
@CdeerTable(name = "t_group_expand")
@JsonInclude(value = Include.NON_NULL)
public class GroupExpand implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4922865945423461153L;

	@ApiModelProperty(value = "群组扩展表主键ID")
	private String id;

	@ApiModelProperty(value = "群组编码 Group.groupCode")
	private String groupCode;

	@ApiModelProperty(value = "* 学校id School.id", required = true)
	private String schoolId;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "是否允许群聊")
	private String isChat;

	@ApiModelProperty(value = "是否允许邀请")
	private String isInvite;

	@ApiModelProperty(value = "是否有通讯录")
	private String isAddressList;

	@ApiModelProperty(value = "班级类型 1：选修班、2：行政班")
	private String classType;

	@ApiModelProperty(value = "二维码")
	private String qrcode;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsChat() {
		return isChat;
	}

	public void setIsChat(String isChat) {
		this.isChat = isChat;
	}

	public String getIsInvite() {
		return isInvite;
	}

	public void setIsInvite(String isInvite) {
		this.isInvite = isInvite;
	}

	public String getIsAddressList() {
		return isAddressList;
	}

	public void setIsAddressList(String isAddressList) {
		this.isAddressList = isAddressList;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

}
