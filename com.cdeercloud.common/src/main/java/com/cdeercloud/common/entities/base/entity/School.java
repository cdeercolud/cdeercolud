package com.cdeercloud.common.entities.base.entity;

import java.io.Serializable;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author looyii
 *
 */
@ApiModel(value = "学校表", description = "学校表")
@CdeerEntity
@CdeerTable(name = "t_school")
@JsonInclude(value = Include.NON_NULL)
public class School implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5293757111962729611L;

	@ApiModelProperty(value = "主键ID")
	private String id;

	@ApiModelProperty(value = "* 学校名", required = true)
	private String name;

	@ApiModelProperty(value = "* 学校全名", required = true)
	private String fullName;

	@ApiModelProperty(value = "* 所在城市", required = true)
	private String cityId;

	@ApiModelProperty(value = "* 学校类型：幼儿园、小学、初中、高中、大学、培训机构、教育局", required = true)
	private String schoolType;

	@ApiModelProperty(value = "开通方式 1:后台开通，2:用户自主开通")
	private String openType;
	// 学校地址
	@ApiModelProperty(value = "* 学校地址", required = true)
	private String address;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "创建时间")
	private String createTime;

	@ApiModelProperty(value = "修改时间")
	private String modifyTime;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getSchoolType() {
		return schoolType;
	}

	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}

	public String getOpenType() {
		return openType;
	}

	public void setOpenType(String openType) {
		this.openType = openType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

}
