package com.cdeercloud.common.entities.common.entity;

import java.io.Serializable;
import java.util.Date;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "文件记录表", description = "文件记录表")
@CdeerEntity
@CdeerTable(name = "t_attachment")
@JsonInclude(value = Include.NON_NULL)
public class Attachment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4238521160306989408L;

	@ApiModelProperty(value = "主键Id")
	private String id;
	// 多媒体类型(PHOTO：图片；AUDIO：声音；VIDEO：视屏；FILE：文件；LIVE：视频通话)
	// private String type;

	@ApiModelProperty(value = "文件路径")
	private String filePath;

	@ApiModelProperty(value = "文件名")
	private String fileName;

	@ApiModelProperty(value = "文件大小")
	private Integer size;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "扩展名")
	private String extName;

	@ApiModelProperty(value = "附加数据，键值对{\"contentType\",\"\"}")
	private String nameValuePair;

	@ApiModelProperty(value = "MD5")
	private String md5;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExtName() {
		return extName;
	}

	public void setExtName(String extName) {
		this.extName = extName;
	}

	public String getNameValuePair() {
		return nameValuePair;
	}

	public void setNameValuePair(String nameValuePair) {
		this.nameValuePair = nameValuePair;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

}
