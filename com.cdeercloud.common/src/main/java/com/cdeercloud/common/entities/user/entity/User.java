package com.cdeercloud.common.entities.user.entity;

import java.io.Serializable;
import java.util.Date;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 
 * @author looyii
 *
 */
@ApiModel(value = "用户表", description = "用户表")
@CdeerEntity
@CdeerTable(name = "t_user")
@JsonInclude(value = Include.NON_NULL)
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7985213890584087761L;

	@ApiModelProperty(value = "用户主键")
	private String id;

	@ApiModelProperty(value = "* 名字", required = true)
	private String name;

	@ApiModelProperty(value = "名字拼音")
	private String pinyin;

	@ApiModelProperty(value = "前缀：拼音第一个字母")
	private String prefix;

	@ApiModelProperty(value = "手机")
	private String mobile;

	@ApiModelProperty(value = "头像")
	private String avatar;

	@ApiModelProperty(value = "状态")
	private String status;
	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

	@ApiModelProperty(value = "登录id")
	private String loginId;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

}
