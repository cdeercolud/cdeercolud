package com.cdeercloud.common.entities.common.entity;

import java.io.Serializable;
import java.util.Date;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "数据字典主表",description = "数据字典主表")
@CdeerEntity
@CdeerTable(name = "t_sys_dict")
@JsonInclude(value = Include.NON_NULL)
public class SysDict implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4639619225809171963L;

	@ApiModelProperty(value = "字典id")
	private String dictId;

	@ApiModelProperty(value = "* 字典编码", required = true)
	private String dictCode;

	@ApiModelProperty(value = "* 字典名称", required = true)
	private String dictName;

	@ApiModelProperty(value = "字典描述")
	private String description;

	@ApiModelProperty(value = "* 所属语言,字典LANGUAGE", required = true)
	private String language;

	@ApiModelProperty(value = "创建人")
	private String createBy;

	@ApiModelProperty(value = "创建时间")
	private String createTime;

	@ApiModelProperty(value = "最后更新人")
	private Date modifyBy;

	@ApiModelProperty(value = "最后更新日期")
	private Date modifyTime;

	@ApiModelProperty(value = "是否默认（1：是，0：否），默认不能修改， 查询过滤")
	private String isDefault;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) {
		this.dictId = dictId;
	}

	public String getDictCode() {
		return dictCode;
	}

	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Date getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Date modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

}
