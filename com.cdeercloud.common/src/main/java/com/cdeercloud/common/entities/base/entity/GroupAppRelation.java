package com.cdeercloud.common.entities.base.entity;

import java.io.Serializable;
import java.util.Date;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 
 * @author looyii
 *
 */
@ApiModel(value = "群组应用关系表", description = "群组应用关系表")
@CdeerEntity
@CdeerTable(name = "t_group_app_relation")
@JsonInclude(value = Include.NON_NULL)
public class GroupAppRelation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6426747978627481086L;
	@ApiModelProperty(value = "主键ID")
	private String id;
	// 应用类型
	@ApiModelProperty(value = "* 应用ID APP.id", required = true)
	private String appId;

	@ApiModelProperty(value = "* 使用范围（和群组类型一至） 1：教育局、2：学校、3：校区、4、班级、5：普通小组", required = true)
	private String range;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

	@ApiModelProperty(value = "父ID")
	private String parentId;

	@ApiModelProperty(value = "应用在群里的操作权限 1:新增（新增消息）、2、查看（查看消息列表）")
	private String handleType;

	@ApiModelProperty(value = "* 终端（1：web、2：IOS、3：Android,4:屏幕，5：手环，6：图书柜，7：VR，8:V卡）", required = true)
	private String terminal;

	@ApiModelProperty(value = "* 群组编码 Group.groupCode", required = true)
	private String groupCode;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getHandleType() {
		return handleType;
	}

	public void setHandleType(String handleType) {
		this.handleType = handleType;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

}
