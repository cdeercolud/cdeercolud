package com.cdeercloud.common.entities.common.entity;

import java.io.Serializable;
import java.util.Date;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "地区表",description = "地区表")
@CdeerEntity
@CdeerTable(name = "t_sys_area")
@JsonInclude(value = Include.NON_NULL)
public class SysArea implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5181609788752404635L;


	@ApiModelProperty(value = "地区id")
	private String areaId;

    @ApiModelProperty(value = "* 地区名称", required = true)
	private String areaName;

    @ApiModelProperty(value = "* 地区编码", required = true)
	private String areaCode;

    @ApiModelProperty(value = "父地区id")
	private String parentAreaId;

    @ApiModelProperty(value = "地区类型(1国家,2省,3城市,4县乡,5镇)")
	private String areaType;

    @ApiModelProperty(value = "国家区号")
	private String nationCode;

    @ApiModelProperty(value = "城市区号")
	private String cityCode;

    @ApiModelProperty(value = "邮政编码")
	private String postCode;

    @ApiModelProperty(value = "是否可用(Y可用,N禁用)")
	private String isActive;

    @ApiModelProperty(value = "描述")
	private String areaDescription;

    @ApiModelProperty(value = "树节点全路径id，用地区id拼接，以下划线分隔")
	private String path;

    @ApiModelProperty(value = "创建人")
	private String createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "最后更新人")
	private String modifyBy;

    @ApiModelProperty(value = "最后更新日期")
	private Date modifyTime;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getParentAreaId() {
		return parentAreaId;
	}

	public void setParentAreaId(String parentAreaId) {
		this.parentAreaId = parentAreaId;
	}

	public String getAreaType() {
		return areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public String getNationCode() {
		return nationCode;
	}

	public void setNationCode(String nationCode) {
		this.nationCode = nationCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getAreaDescription() {
		return areaDescription;
	}

	public void setAreaDescription(String areaDescription) {
		this.areaDescription = areaDescription;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

}
