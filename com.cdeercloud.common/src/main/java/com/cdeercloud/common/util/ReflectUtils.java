package com.cdeercloud.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdeercloud.common.core.exception.CdeerException;

public class ReflectUtils {
	static final Logger logger = LoggerFactory.getLogger(ReflectUtils.class);

	public static void setFieldValue(Object object, String fieldName, Object value) {
		Field field = getDeclaredField(object, fieldName);
		if (field == null) {
			String info = object.getClass().getName() + "找不到字段" + fieldName;
			throw new IllegalArgumentException(info);
		}
		makeAccessible(field);
		try {
			field.set(object, value);
		} catch (IllegalAccessException e) {
			//
		}
	}

	public static Object getFieldValue(Object object, String fieldName) {
		Field field = getDeclaredField(object, fieldName);
		if (field == null) {
			String info = object.getClass().getName() + "找不到字段" + fieldName;
			throw new IllegalArgumentException(info);
		}
		makeAccessible(field);
		Object result = null;
		try {
			result = field.get(object);
		} catch (IllegalAccessException e) {
			//
		}
		return result;
	}

	private static Field getDeclaredField(Object object, String filedName) {
		for (Class<?> superCls = object.getClass(); superCls != Object.class; superCls = superCls.getSuperclass()) {
			try {
				return superCls.getDeclaredField(filedName);
			} catch (NoSuchFieldException e) {
				//
			}
		}
		return null;
	}

	private static void makeAccessible(Field field) {
		if (!Modifier.isPublic(field.getModifiers())) {
			field.setAccessible(true);
		}
	}

	public static Object invokeMethod(Object object, String methodName) throws InvocationTargetException {
		return invokeMethod(object, methodName, new Class[0], new Object[0]);
	}

	public static Object invokeMethod(Object object, String methodName, Class<?>[] parameterTypes, Object[] parameters) throws InvocationTargetException {
		Method method = getDeclaredMethod(object, methodName, parameterTypes);
		if (method == null) {
			String info = object.getClass().getName() + "找不到方法" + methodName;
			throw new IllegalArgumentException(info);
		}
		method.setAccessible(true);
		try {
			return method.invoke(object, parameters);
		} catch (IllegalAccessException e) {
			logger.warn("", e);
		}

		return null;
	}

	public static String methodToProperty(String name) {
		if (name.startsWith("is")) {
			name = name.substring(2);
		} else if (name.startsWith("get") || name.startsWith("set")) {
			name = name.substring(3);
		} else {
			throw new CdeerException("Error parsing property name '" + name + "'.  Didn't start with 'is', 'get' or 'set'.");
		}

		if ((name.length() == 1) || ((name.length() > 1) && !Character.isUpperCase(name.charAt(1)))) {
			name = name.substring(0, 1).toLowerCase(Locale.ENGLISH) + name.substring(1);
		}

		return name;
	}

	public static boolean isProperty(String name) {
		return name.startsWith("get") || name.startsWith("set") || name.startsWith("is");
	}

	public static boolean isGetter(String name) {
		return name.startsWith("get") || name.startsWith("is");
	}

	public static boolean isSetter(String name) {
		return name.startsWith("set");
	}

	private static Method getDeclaredMethod(Object object, String methodName, Class<?>[] parameterTypes) {
		for (Class<?> superCls = object.getClass(); superCls != Object.class; superCls = superCls.getSuperclass()) {
			try {
				return superCls.getDeclaredMethod(methodName, parameterTypes);
			} catch (NoSuchMethodException e) {
				//
			}
		}
		return null;
	}
}
