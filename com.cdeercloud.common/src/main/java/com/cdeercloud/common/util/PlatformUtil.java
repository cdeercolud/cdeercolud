package com.cdeercloud.common.util;

/**
 * 获取客户端来源
 * 
 * @author looyii
 *
 */
public class PlatformUtil {
	public static final String _IOS = "IOS";
	public static final String _ANDROID = "Android";
	public static final String _WED = "WED";

	/** java获取客户端 */
	public static String getPlatform(String userAgent) {
		// 客户端类型常量
		if (userAgent.contains("iPhone") || userAgent.contains("iPod") || userAgent.contains("iPad")) {
			return _IOS;
		} else if (userAgent.contains("Android") || userAgent.contains("Linux")) {
			return _ANDROID;
		} else {
			return _WED;
		}
	}
}
