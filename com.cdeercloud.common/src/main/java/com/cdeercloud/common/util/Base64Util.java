package com.cdeercloud.common.util;

import org.springframework.util.Base64Utils;

import com.cdeercloud.common.core.exception.CdeerException;

public class Base64Util {

	public static byte[] base64ToFile(String base64Data) throws Exception {
		String dataPrix = "";
		String data = "";
		if ((base64Data == null) || "".equals(base64Data)) {
			throw new Exception("上传失败，上传图片数据为空");
		} else {
			String[] d = base64Data.split("base64,");
			if ((d != null) && (d.length == 2)) {
				dataPrix = d[0];
				data = d[1];
			} else {
				throw new Exception("上传失败，数据不合法");
			}
		}
		String suffix = "";
		if ("data:image/jpeg;".equalsIgnoreCase(dataPrix)) {// data:image/jpeg;base64,base64编码的jpeg图片数据
			suffix = ".jpg";
		} else if ("data:image/x-icon;".equalsIgnoreCase(dataPrix)) {// data:image/x-icon;base64,base64编码的icon图片数据
			suffix = ".ico";
		} else if ("data:image/gif;".equalsIgnoreCase(dataPrix)) {// data:image/gif;base64,base64编码的gif图片数据
			suffix = ".gif";
		} else if ("data:image/png;".equalsIgnoreCase(dataPrix)) {// data:image/png;base64,base64编码的png图片数据
			suffix = ".png";
		} else if ("data:image/jpg;".equalsIgnoreCase(dataPrix)) {// data:image/png;base64,base64编码的png图片数据
			suffix = ".jpg";
		} else {
			// throw new HrmsException("上传图片格式不合法");
		}
		return Base64Utils.decodeFromString(data);
	}

	public static String getSuffix(String base64Data) throws CdeerException {
		String dataPrix = "";
		String suffix = "";
		if ("data:image/jpeg;".equalsIgnoreCase(dataPrix)) {// data:image/jpeg;base64,base64编码的jpeg图片数据
			suffix = "jpg";
		} else if ("data:image/x-icon;".equalsIgnoreCase(dataPrix)) {// data:image/x-icon;base64,base64编码的icon图片数据
			suffix = "ico";
		} else if ("data:image/gif;".equalsIgnoreCase(dataPrix)) {// data:image/gif;base64,base64编码的gif图片数据
			suffix = "gif";
		} else if ("data:image/png;".equalsIgnoreCase(dataPrix)) {// data:image/png;base64,base64编码的png图片数据
			suffix = "png";
		} else if ("data:image/jpg;".equalsIgnoreCase(dataPrix)) {// data:image/png;base64,base64编码的png图片数据
			suffix = "jpg";
		} else {
			suffix = "jpg";
		}
		return suffix;
	}
}
