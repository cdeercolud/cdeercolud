package com.cdeercloud.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.cdeercloud.common.core.exception.CdeerException;

/**
 * 
 * @author looyii
 *
 */
public class DownloadUtil {
	private DownloadUtil() {
	}

	public static void downloadFile(HttpServletResponse response, byte[] fileInByte, String downloadName) {
		try {
			response.setContentType("multipart/form-data");
			response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(downloadName, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new CdeerException("下载文件名编码时出现错误.", e);
		}
		try {
			ServletOutputStream out = response.getOutputStream();
			out.write(fileInByte);
			out.flush();
		} catch (IOException e) {
			throw new CdeerException("read temp error....", e);
		}
	}
}
