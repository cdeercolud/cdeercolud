package com.cdeercloud.common.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @Auther: looyii
 * @Date: 2018/7/20 17:52
 * @Description: 获取request,response的相关数据
 */
public class HttpServletUtil {

    /**
     * 获取request对象
     * @return
     */
    public static HttpServletRequest getRequest(){
        ServletRequestAttributes servletRequestAttributes = getServletRequestAttributes();
        if(servletRequestAttributes != null){
            return servletRequestAttributes.getRequest();
        }
        return null;
    }

    /**
     * 获取response对象
     * @return
     */
    public static HttpServletResponse getResponse(){

        ServletRequestAttributes servletRequestAttributes = getServletRequestAttributes();
        if(servletRequestAttributes != null){
            return servletRequestAttributes.getResponse();
        }
        return null;

    }

    private static  ServletRequestAttributes getServletRequestAttributes() {

        ServletRequestAttributes sra = null;
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
             sra = (ServletRequestAttributes) requestAttributes;
        }
        return sra;

    }

}
