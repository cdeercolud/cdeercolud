package com.cdeercloud.common.util;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public final class DateUtil {
	private static Logger log = Logger.getLogger(DateUtil.class);
	public static final String YYYY_MM_DD_HHMMSS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String YYYY_MM_DD_HHMMSS = "yyyy-MM-dd HH:mm:ss";
	public static final String YYYY_MM_DD_HHMM = "yyyy-MM-dd HH:mm";
	public static final String YYYY_MM_DD_HH = "yyyy-MM-dd HH";
	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String YYYYMMDD_DOT = "yyyy.MM.dd";
	public static final String YYYY_W = "yyyy-w";
	public static final String YYYY_MM = "yyyy-MM";
	public static final String YYYY = "yyyy";
	public static final String MMM = "MM月";
	public static final String MM = "MM";
	public static final String MM_DD = "MM-dd";
	public static final String MM_SS = "mm:ss";
	public static final String YY_MM = "yy-MM";
	public static final String YYYY_MM_ZH_CN = "yyyy年MM月";
	public static final String YYYY_MM_DD_HHMMSS_ZH_CN = "yyyy年MM月dd日  HH:mm:ss";
	public static final String YYYYMMDD = "yyyyMMdd";
	public static final String YYYY_MM_DD_ZH_CN = "yyyy年MM月dd日";
	public static final String MM_DD_ZH_CN = "MM月dd日";
	public static final String MM_DOT_DD = "MM.dd";
	public static final String MM_DD_HHMM = "MM-dd HH:mm";
	public static final String MM_DD_HHMM_ZH_CN = "MM月dd日 HH:mm";
	public static final String HHMM = "HH:mm";
	public static final String[] DAY_NAMES = new String[] { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };

	public final static String LOOK_TIME_VALUE = "%s'";
	public final static String LOOK_TIME_VALUE_MILL = "%s''";

	/**
	 * 上学期开始日期
	 */
	public final static String FIRST_SEMESTER_START_DAY = "02-01";
	/**
	 * 上学期结束日期
	 */
	public final static String FIRST_SEMESTER_END_DAY = "08-31";
	/**
	 * 下学期开始日期
	 */
	public final static String SECOND_SEMESTER_START_DAY = "09-01";
	/**
	 * 下学期结束日期
	 */
	public final static String SECOND_SEMESTER_END_DAY = "01-31";

	/**
	 * 年度开始月份日期
	 */
	public final static String YEAR_START = "-09-01";
	/**
	 * 年度结束月份日期
	 */
	public final static String YEAR_END = "-08-31";

	private static Map<String, String> typeMap = null;

	static {
		typeMap = new HashMap<String, String>();
		if (typeMap.size() == 0) {
			typeMap.put("1", "周一");
			typeMap.put("2", "周二");
			typeMap.put("3", "周三");
			typeMap.put("4", "周四");
			typeMap.put("5", "周五");
			typeMap.put("6", "周六");
			typeMap.put("7", "周日");
		}
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static Date getNow() {
		return new Date();
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static String getNowString(String pattern) {
		return format(new Date(), pattern);
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static String getNowString() {
		return format(new Date(), "yyyyMMddHHmmss");
	}

	/**
	 * 获取指定格式字符串
	 * 
	 * @param date
	 *            日期
	 * @param pattern
	 *            日期格式
	 * @return
	 */
	public static String format(Date date, String pattern) {
		if (date != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.CHINA);
			return sdf.format(date);
		}
		return null;
	}

	/**
	 * 获取日期字符串（YYYY_MM_DD_HHMMSS）
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String formatMillDateTime(Date date) {
		return format(date, YYYY_MM_DD_HHMMSS_SSS);
	}

	/**
	 * 获取日期字符串（YYYY_MM_DD_HHMMSS）
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String formatDateTime(Date date) {
		return format(date, YYYY_MM_DD_HHMMSS);
	}

	/**
	 * 获取日期字符串（yyyyMMdd）
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String formatDate(Date date) {
		return format(date, "yyyyMMdd");
	}

	/**
	 * 获取时间字符串（yyyyMMddHHmmss）
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String formatTime(Date date) {
		return format(date, "yyyyMMddHHmmss");
	}

	/**
	 * 获取当前时间字符串（yyyyMMddHHmmss）
	 * 
	 * @return
	 */
	public static String formatNowTime() {
		return formatNowTime("yyyyMMddHHmmss");
	}

	/**
	 * 获取当前时间字符串
	 * 
	 * @param pattern
	 * @return
	 */
	public static String formatNowTime(String pattern) {
		return format(getNow(), pattern);
	}

	/**
	 * 时间字符串转换成时间
	 * 
	 * @param date
	 *            日期字符串
	 * @return
	 */
	public static Date parseDate(String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			return sdf.parse(date);
		} catch (Exception e) {
			LogUtil.exception(log, e);
		}
		return null;
	}

	/**
	 * 指定格式 时间字符串转换成时间
	 * 
	 * @param date
	 *            日期字符
	 * @param pattern
	 *            日期格式字符
	 * @return
	 */
	public static Date parseDate(String date, String pattern) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.parse(date);
		} catch (Exception e) {
			LogUtil.exception(log, e);
		}
		return null;
	}

	/**
	 * 指定格式 时间字符串转换成时间
	 * 
	 * @param date
	 *            日期字符串
	 * @param pattern
	 *            日期格式字符串
	 * @return
	 */
	public static Date parse(String date, String pattern) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.parse(date);
		} catch (Exception e) {
			LogUtil.exception(log, e);
		}
		return null;
	}

	/**
	 * 比较两个日期大小，one==two返回0，one>two返回-1，two>one返回1
	 * 
	 * @param one
	 * @param two
	 * @return int
	 */
	public static int compareGapInSeconds(Date one, Date two) {
		return (two).compareTo(one);
	}

	/**
	 * 多少天以后
	 * 
	 * @param date
	 * @param afterDays
	 * @return
	 */
	public static Date getAfterDate(Date date, int afterDays) {

		long time = date.getTime();

		long ibeforeDays = (long) afterDays * 1000 * 60 * 60 * 24;

		return new Date(time + ibeforeDays);
	}

	/**
	 * 多少分以前
	 * 
	 * @param date
	 * @param min
	 * @return
	 */
	public static Date getBeforeMin(Date date, int min) {

		long time = date.getTime();

		long duration = (long) min * 1000 * 60;

		return new Date(time - duration);
	}

	/**
	 * 多少时以前
	 * 
	 * @param date
	 * @param hour
	 * @return
	 */
	public static Date getBeforeHour(Date date, int hour) {

		long time = date.getTime();

		long duration = (long) hour * 1000 * 60 * 60;
		return new Date(time - duration);
	}

	/**
	 * 多少天以前
	 * 
	 * @param date
	 * @param beforeDays
	 * @return
	 */
	public static Date getBeforeDate(Date date, int beforeDays) {

		long time = date.getTime();

		long ibeforeDays = (long) beforeDays * 1000 * 60 * 60 * 24;

		return new Date(time - ibeforeDays);
	}

	/**
	 * 多周以前
	 * 
	 * @param date
	 * @param beforeWeeks
	 * @return
	 */
	public static Date getBeforeWeek(Date date, int beforeWeeks) {
		return DateUtil.getBeforeDate(date, beforeWeeks * 7);
	}

	/**
	 * 多少月以前
	 * 
	 * @param date
	 * @param beforeMonth
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Date getBeforeMonth(Date date, int beforeMonth) {
		int month = date.getMonth();
		int year = date.getYear();
		int m = month - beforeMonth;
		if (m > 0) {
			date.setMonth(m);
		} else {
			while (m < 0) {
				year--;
				m = 12 + m;
			}
			date.setYear(year);
			date.setMonth(m);
		}

		return date;
	}

	/**
	 * 获取某时间的前一月
	 */
	public static Date getBackMonth(Date date) {
		Calendar canlendar = Calendar.getInstance();
		canlendar.setTime(date);
		canlendar.add(Calendar.MONTH, -1);
		return canlendar.getTime();
	}

	public static Date getBackMonthBeginDay(Date date) {
		Calendar canlendar = Calendar.getInstance();
		canlendar.setTime(date);
		canlendar.add(Calendar.MONTH, -1);
		canlendar.set(Calendar.DAY_OF_MONTH, 1);
		canlendar.set(Calendar.HOUR_OF_DAY, 0);
		canlendar.set(Calendar.MINUTE, 0);
		canlendar.set(Calendar.SECOND, 0);
		return canlendar.getTime();
	}

	public static Date getBackMonthEndDay(Date date) {
		Calendar canlendar = Calendar.getInstance();
		canlendar.setTime(date);
		canlendar.add(Calendar.MONTH, -1);
		canlendar.set(Calendar.DAY_OF_MONTH, canlendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		canlendar.set(Calendar.HOUR_OF_DAY, 23);
		canlendar.set(Calendar.MINUTE, 59);
		canlendar.set(Calendar.SECOND, 59);
		canlendar.set(Calendar.MILLISECOND, 999);
		return canlendar.getTime();
	}

	/**
	 * 获取某时间的前一天
	 */
	public static Date getBackDate(Date date) {
		Calendar canlendar = Calendar.getInstance();
		canlendar.setTime(date);
		canlendar.add(Calendar.DATE, -1);
		return canlendar.getTime();
	}

	/**
	 * 获取当前日期多少天之前或者多少天之后日期
	 * 
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date getDay(Date date, int day) {
		Calendar canlendar = Calendar.getInstance();
		canlendar.setTime(date);
		canlendar.add(Calendar.DATE, day);
		return canlendar.getTime();
	}

	public static int getWeekOfYear(Date date) {
		Calendar c = new GregorianCalendar();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setTime(date);
		return c.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 相差多少天
	 * 
	 * @param t1,t2
	 * @return
	 */
	public static int diffDay(Date t1, Date t2) {
		if ((t1 != null) && (t2 != null)) {
			long l = t1.getTime() - t2.getTime();
			if (l > 0) {
				long hour = l / (60 * 60 * 1000);
				if (hour > 24) {
					if ((hour % 24) == 0) {
						return (int) (hour / 24);
					} else {
						return (int) ((hour / 24) + 1);
					}
				} else {
					return 1;
				}
			} else {
				return 0;
			}
		}
		return 0;
	}

	/**
	 * 获取GMT时间
	 * 
	 * @param date
	 * @return
	 */
	public static final String toGMT(Date date) {
		Locale aLocale = Locale.US;
		DateFormat fmt = new SimpleDateFormat("EEE,d MMM yyyy hh:mm:ss z", new DateFormatSymbols(aLocale));
		fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		return fmt.format(date);
	}

	public static Calendar getFirstDayOfWeek() {
		Calendar monday = Calendar.getInstance();
		return getADayOfWeek(monday, Calendar.MONDAY);
	}

	public static Date getFirstDayOfWeek(Date day) {
		Calendar monday = Calendar.getInstance();
		monday.setTime(day);
		return getADayOfWeek(monday, Calendar.MONDAY).getTime();
	}

	public static Calendar getFirstDayOfWeek(Calendar day) {
		Calendar monday = (Calendar) day.clone();
		return getADayOfWeek(monday, Calendar.MONDAY);
	}

	public static Calendar getLastDayOfWeek() {
		Calendar sunday = Calendar.getInstance();
		return getADayOfWeek(sunday, Calendar.SUNDAY);
	}

	public static Date getLastDayOfWeek(Date day) {
		Calendar c = Calendar.getInstance();
		c.setTime(day);
		return getADayOfWeek(c, Calendar.SUNDAY).getTime();
	}

	public static Calendar getLastDayOfWeek(Calendar day) {
		Calendar sunday = (Calendar) day.clone();
		return getADayOfWeek(sunday, Calendar.SUNDAY);
	}

	private static Calendar getADayOfWeek(Calendar day, int dayOfWeek) {
		int week = day.get(Calendar.DAY_OF_WEEK);
		if (week == dayOfWeek) {
			return day;
		}
		int diffDay = dayOfWeek - week;
		if (week == Calendar.SUNDAY) {
			diffDay -= 7;
		} else if (dayOfWeek == Calendar.SUNDAY) {
			diffDay += 7;
		}
		day.add(Calendar.DATE, diffDay);
		return day;
	}

	public static String getStringTime(Long ts) {
		if ((ts != null) && (ts.longValue() > 0)) {
			Date date = new Date(ts);
			return format(date, "yyyy-MM-dd HH:mm:ss.SSS");
		}
		return null;
	}

	public static Date getDateTime(Long ts) {
		if ((ts != null) && (ts.longValue() > 0)) {
			return new Date(ts);
		}
		return null;
	}

	public static Date getMonthBeginDay(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();

	}

	public static Date getMonthEndDay(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		return c.getTime();

	}

	public static Date getWeekBeginDay(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.setFirstDayOfWeek(Calendar.MONDAY);// 将每周第一天设为星期一，默认是星期天
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	public static Date getWeekEndDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.setFirstDayOfWeek(Calendar.MONDAY);// 将每周第一天设为星期一，默认是星期天
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);// 日子设为星期天
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		return c.getTime();
	}

	public static Date getDayBeginTime(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();

	}

	public static Date getDayEndTime(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		return c.getTime();
	}

	public static String getDayBeginTime(String dateString) {
		Date date = DateUtil.parse(dateString, DateUtil.YYYY_MM_DD);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return DateUtil.format(c.getTime(), DateUtil.YYYY_MM_DD_HHMMSS_SSS);

	}

	public static String getDayEndTime(String dateString) {
		Date date = DateUtil.parse(dateString, DateUtil.YYYY_MM_DD);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		return DateUtil.format(c.getTime(), DateUtil.YYYY_MM_DD_HHMMSS_SSS);
	}

	public static Date getHourBeginTime(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();

	}

	public static Date getHourEndTime(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		return c.getTime();
	}

	public static String getDayName(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return DAY_NAMES[c.get(Calendar.DAY_OF_WEEK) - 1];
	}

	/**
	 * 获取周名称
	 * 
	 * @param type
	 * @return
	 */
	public static String getWeekName(String type) {
		return typeMap.get(type);
	}

	public static String getDateString(long time) {
		StringBuffer timeContent = new StringBuffer();
		long t = time / 1000;
		long tmpTime = t % 60;
		if (tmpTime == 0) {
			timeContent.append(String.format(LOOK_TIME_VALUE, t / 60));
		} else {
			// 如果大于一分钟
			if ((t / 60) > 0) {
				timeContent.append(String.format(LOOK_TIME_VALUE, t / 60));
			}
			timeContent.append(String.format(LOOK_TIME_VALUE_MILL, tmpTime));
		}
		return timeContent.toString();
	}

	/**
	 * 获取上周一
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastWeekMonday(Date date) {
		Date a = DateUtil.addDays(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(a);
		cal.add(Calendar.WEEK_OF_YEAR, -1);// 一周
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();
	}

	/**
	 * 获取上周日
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastWeekSunday(Date date) {
		Date a = DateUtil.addDays(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(a);
		cal.set(Calendar.DAY_OF_WEEK, 1);
		return cal.getTime();
	}

	public static Date addDays(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_WEEK, -1);
		return cal.getTime();
	}

	/**
	 * 获取上一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastDay(Date date) {
		Date a = DateUtil.addDays(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(a);
		cal.add(Calendar.DATE, -0);
		return cal.getTime();
	}

	/**
	 * 根据日期获取当前多少年
	 * 
	 * @param dateString
	 * @return
	 */
	public static int getCurrentDateDiffByDate(String dateString) {
		if (StringUtils.isBlank(dateString)) {
			return 0;
		}
		Date date = DateUtil.parseDate(dateString);
		Calendar c1 = Calendar.getInstance();
		c1.setTime(new Date());
		Calendar c2 = Calendar.getInstance();
		c2.setTime(date);

		return (c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR)) < 0 ? 0 : c1.get(java.util.Calendar.YEAR) - c2.get(java.util.Calendar.YEAR);
	}

	/**
	 * 获取星期几
	 * 
	 * @param date
	 * @return
	 */
	public static int getWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int week = c.get(Calendar.DAY_OF_WEEK);
		return week;
	}

	/**
	 * 获取某天前一个月的第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastMonthFirstDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
		return c.getTime();
	}

	/**
	 * 获取某天前一个月的最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastMonthEndDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}

	/**
	 * 判断另个日期相差天数
	 * 
	 * @param fDate
	 * @param oDate
	 * @return
	 */
	public static int daysOfTwo(Date fDate, Date oDate) {
		int days = (int) ((oDate.getTime() - fDate.getTime()) / (1000 * 3600 * 24));
		return days;
	}

	/**
	 * 根据每周工作多少天，获取两个日期之间的所有工作日的字符串格式
	 *
	 * @param start
	 *            开始日期(包含)
	 * @param end
	 *            结束日期(包含)
	 * @param workDays
	 *            每周工作几天(默认周一开始)
	 * @return 格式yyyy-MM-dd
	 */
	public static List<String> getDaysBetween(Date start, Date end, int workDays) {
		List<String> days = new ArrayList<String>();
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		if (yearFormat.format(start).equals(yearFormat.format(end))) {
			days.addAll(DateUtil.getBetweenTwoDatesDays(start, end));
		} else {
			Date yearLast = DateUtil.getYearLast(start);
			days.addAll(DateUtil.getBetweenTwoDatesDays(start, yearLast));
			Date yearFirst = DateUtil.getYearFirst(end);
			days.addAll(DateUtil.getBetweenTwoDatesDays(yearFirst, end));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			for (int r = days.size() - 1; r >= 0; r--) {
				// 排除非工作日
				int day = 0;
				day = sdf.parse(days.get(r)).getDay();
				if ((day == 0) || (day > workDays)) {
					days.remove(r);
				}
			}
		} catch (ParseException e) {
			LogUtil.info(log, e);
		}
		return days;
	}

	/*
	 * 获取两个日期之间的所有日期
	 */
	public static List<String> getBetweenTwoDatesDays(Date startDate, Date endDate) {
		int days = daysOfTwo(startDate, endDate) + 1;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		List<String> dates = new ArrayList<String>();
		for (int i = 0; i < days; i++) {
			dates.add(DateUtil.format(calendar.getTime(), YYYY_MM_DD));
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	/**
	 * 本年度开始时间，9月1日至8月31日
	 * 
	 * @return
	 */
	public static Date getThisYearStartDate() {
		Date now = DateUtil.getNow();
		String year = DateUtil.format(now, DateUtil.YYYY);
		Date date = DateUtil.parse(year + YEAR_START, DateUtil.YYYY_MM_DD);
		if (now.before(date)) {
			year = String.valueOf((Integer.parseInt(year) - 1));
		}

		return DateUtil.parse(year + YEAR_START, DateUtil.YYYY_MM_DD);
	}

	/**
	 * 本年度结束时间，9月1日至8月31日
	 * 
	 * @return
	 */
	public static Date getThisYearEndDate() {
		Date now = DateUtil.getNow();
		String year = DateUtil.format(now, DateUtil.YYYY);
		Date date = DateUtil.parse(year + YEAR_END, DateUtil.YYYY_MM_DD);
		if (now.after(date)) {
			year = String.valueOf((Integer.parseInt(year) + 1));
		}

		return DateUtil.parse(year + YEAR_END, DateUtil.YYYY_MM_DD);
	}

	/**
	 * 获取年度名称,如果是1月1日-8月31日，年份上减一
	 * 
	 * @param date
	 * @return
	 */
	public static String getYearName(Date date) {
		String year = DateUtil.format(date, DateUtil.YYYY);
		Date d = DateUtil.parse(year + YEAR_START, DateUtil.YYYY_MM_DD);
		if ((d.after(getYearFirst(date)) && d.before(DateUtil.parse(year + YEAR_END, DateUtil.YYYY_MM_DD)))) {
			year = String.valueOf((Integer.parseInt(year) - 1));
		}
		return year + "年度";
	}

	/**
	 * 获取当年的第一天
	 * 
	 * @return
	 */
	public static Date getCurrYearFirst() {
		return getYearFirst(DateUtil.getNow());
	}

	/**
	 * 获取当年的最后一天
	 * 
	 * @return
	 */
	public static Date getCurrYearLast() {
		return getYearLast(DateUtil.getNow());
	}

	/**
	 * 获取某年第一天日期
	 * 
	 * @param date
	 *            年份
	 * @return Date
	 */
	public static Date getYearFirst(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		calendar.clear();
		calendar.set(Calendar.YEAR, year);
		Date currYearFirst = calendar.getTime();
		return currYearFirst;
	}

	/**
	 * 获取某年最后一天日期
	 * 
	 * @param date
	 *            年份
	 * @return Date
	 */
	public static Date getYearLast(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		calendar.clear();
		calendar.set(Calendar.YEAR, year);
		calendar.roll(Calendar.DAY_OF_YEAR, -1);
		Date currYearLast = calendar.getTime();
		return currYearLast;
	}

	/**
	 * 判断是否本年第一天
	 * 
	 * @param now
	 * @return
	 */
	public static boolean isYearFirstDay(Date now) {
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		return (1 == c.get(Calendar.DAY_OF_MONTH)) && (0 == c.get(Calendar.MONTH));
	}

	/**
	 * 判断是否本年第一天
	 * 
	 * @param now
	 * @return
	 */
	public static boolean isYearLasttDay(Date now) {
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		return (1 == c.get(Calendar.DAY_OF_MONTH)) && (0 == c.get(Calendar.MONTH));
	}

	/**
	 * 判断是不是本月的第一天
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isMonthFirstDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return 1 == c.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 判断指定日期在指定日期所在月份是否为最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isMonthEndDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int specifyDayIndex = calendar.get(Calendar.DAY_OF_MONTH);

		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		int endDayIndex = calendar.get(Calendar.DAY_OF_MONTH);

		return specifyDayIndex == endDayIndex;
	}

	/**
	 * 判断是否同一天
	 * 
	 * @param day1
	 * @param day2
	 * @return
	 */
	public static boolean isSameDay(Date day1, Date day2) {
		if ((day1 == null) || (day2 == null)) {
			return false;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(day1);
		c1.set(Calendar.HOUR_OF_DAY, 0);
		c1.set(Calendar.MINUTE, 0);
		c1.set(Calendar.SECOND, 0);
		c1.set(Calendar.MILLISECOND, 0);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(day2);
		c2.set(Calendar.HOUR_OF_DAY, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.SECOND, 0);
		c2.set(Calendar.MILLISECOND, 0);
		return c1.getTime().getTime() == c2.getTime().getTime();
	}

	/**
	 * 判断是否学期第一天
	 * 
	 * @param now
	 * @return
	 */
	public static boolean isSemesterFirstDay(Date now) {
		String date = DateUtil.format(now, MM_DD);
		return date.equals(FIRST_SEMESTER_START_DAY) || date.equals(SECOND_SEMESTER_START_DAY);
	}

	/**
	 * 判断是否学期最后一天
	 * 
	 * @param now
	 * @return
	 */
	public static boolean isSemesterEndDay(Date now) {
		String date = DateUtil.format(now, MM_DD);
		return date.equals(FIRST_SEMESTER_END_DAY) || date.equals(SECOND_SEMESTER_END_DAY);
	}

	/**
	 * 获取本学期第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getSemesterBeginDay(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		if ((month >= 1) && (month <= 7)) {// 第二学期
			c.set(year, 1, 1, 0, 0, 0);
		} else {// 第一学期
			c.set(year - 1, 8, 1, 0, 0, 0);
		}
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	/**
	 * 获取本学期最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getSemesterEndDay(Date date) {
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		if ((month >= 1) && (month <= 7)) {// 第二学期
			c.set(year, 7, 31, 0, 0, 0);
		} else {// 第一学期
			c.set(year, 0, 31, 0, 0, 0);
		}
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	/**
	 * 判断是否本周第一天
	 * 
	 * @param now
	 * @return
	 */
	public static boolean isWeekFirstDay(Date now) {
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
	}

	/**
	 * 判断是否本周最后一天
	 * 
	 * @param now
	 * @return
	 */
	public static boolean isWeekLastDay(Date now) {
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}

	public static void main(String[] args) throws ParseException, java.text.ParseException {
		// Date t1 = parse("2016-09-20 18:07:06.671", YYYY_MM_DD_HHMMSS_SSS);
		// Date t2 = parse("2016-09-20 16:07:06.671", YYYY_MM_DD_HHMMSS_SSS);
		// System.out.println(diffDay(t1, t2));
		// System.out.println(DateUtil.format(getWeekBeginDay(new Date()),
		// DateUtil.YYYY_MM_DD_HHMMSS_SSS));
		// System.out.println(DateUtil.format(getWeekEndDay(new Date()),
		// DateUtil.YYYY_MM_DD_HHMMSS_SSS));
		// System.out.println(DateUtil.format(getFirstDayOfWeek(new Date()),
		// YYYY_MM_DD));
		// System.out.println(DateUtil.format(getLastWeekMonday(new Date()),
		// YYYY_MM_DD));
		// System.out.println(DateUtil.format(getLastWeekSunday(new Date()),
		// YYYY_MM_DD));
		// System.out.println(DateUtil.format(DateUtil.parse("2016-12",
		// YYYY_MM), "MM"));
		// System.out.println(DateUtil.format(DateUtil.getWeekEndDay(new
		// Date()), YYYY_MM_DD_HHMMSS_SSS));
		// System.out.println(DateUtil.format(new Date(1471682602531L),
		// DateUtil.YYYY_MM_DD_HHMMSS_SSS));
		// System.out.println(DateUtil.format(new Date(1471682664471L),
		// DateUtil.YYYY_MM_DD_HHMMSS_SSS));
		// System.out.println(DateUtil.format(DateUtil.parse("2015-10-23 17:40:02.511 +0800",
		// "yyyy-MM-dd"), "yyyy-MM-dd HH:mm:ss"));
		// System.out.println(DateUtil.getFirstDayOfWeek().getTime().getTime());
		// System.out.println(getWeekEndDay(new Date()));
		// System.out.println(getWeek(new Date()));
		// System.out.println(format(new Date(1505182709461L), "yyyy-MM-dd HH:mm:ss.SSS"));
		/*
		 * 
		 * 
		 * for (int i = 0; i <= 10; i++) { Calendar c = Calendar.getInstance();
		 * c.add(Calendar.DAY_OF_MONTH, i);
		 * System.out.println(DateUtil.getWeekOfYear(c.getTime())); }
		 */
		// Calendar c = Calendar.getInstance();
		// c.setTime(new Date());
		// System.out.println(c.get(Calendar.DAY_OF_WEEK));
		//
		// System.out.println(getDayName(new Date()));

		// Date startDate = DateUtil.parse("1606150900", "yyMMddHHmm");
		// long distance = DateUtil.parse("2016-04-19 16:02:17.475",
		// "yyyy-MM-dd HH:mm:ss").getTime();
		// if (distance > 0) {
		//
		// System.out.println(distance);
		// }

		Date now = DateUtil.getAfterDate(DateUtil.getNow(), 100);
		System.out.println(DateUtil.format(DateUtil.getSemesterBeginDay(now), "yyyyMMdd"));
		System.out.println(DateUtil.format(DateUtil.getSemesterEndDay(now), "yyyyMMdd"));

	}
}
