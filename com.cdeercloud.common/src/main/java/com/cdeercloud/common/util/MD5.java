package com.cdeercloud.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

/**
 * Created by wangdao on 2014/12/14.
 */
public class MD5 {

	public static String md5(String s) {
		try {
			return md5(s.getBytes("ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String md5(byte[] byteArray) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");

			byte[] md5Bytes = md5.digest(byteArray);

			StringBuffer hexValue = new StringBuffer();

			for (byte md5Byte : md5Bytes) {
				int val = (md5Byte) & 0xff;
				if (val < 16) {
					hexValue.append("0");
				}
				hexValue.append(Integer.toHexString(val));
			}

			return hexValue.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) {
		System.out.println(md5("1"));
	}
}
