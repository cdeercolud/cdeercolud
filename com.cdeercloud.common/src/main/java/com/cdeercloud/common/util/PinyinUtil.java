package com.cdeercloud.common.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class PinyinUtil {
	private static Logger log = Logger.getLogger(PinyinUtil.class);

	public static String pinyin(char c) {
		String[] pinyins = PinyinHelper.toHanyuPinyinStringArray(c);
		if (pinyins == null) {
			return null;
		}
		return pinyins[0];
	}

	// 使用PinYin4j.jar将汉字转换为拼音
	public static String chineseToPinyin(String chinese) {
		if (StringUtils.isBlank(chinese)) {
			return chinese;
		}
		StringBuffer sb = new StringBuffer();
		char[] chars = chinese.toCharArray();
		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		format.setVCharType(HanyuPinyinVCharType.WITH_V);
		for (char c : chars) {
			if (c > 128) {
				try {
					String strs[] = PinyinHelper.toHanyuPinyinStringArray(c, format);
					if ((strs != null) && (strs.length > 0)) {
						sb.append(strs[0]);
					} else {
						sb.append(c);
					}
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					LogUtil.exception(log, e);
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public static String convertNumberToHanzi(int i) {
		if (i == 1) {
			return "一";
		}
		if (i == 2) {
			return "二";
		}
		if (i == 3) {
			return "三";
		}
		if (i == 4) {
			return "四";
		}
		if (i == 5) {
			return "五";
		}
		if (i == 6) {
			return "六";
		}

		if (i == 7) {
			return "七";
		}
		if (i == 8) {
			return "八";
		}
		if (i == 9) {
			return "九";
		}
		if (i == 0) {
			return "零";
		}

		return null;
	}

	public static String convertHanziToNumber(String str) {
		if (str.contains("一")) {
			str = str.replaceAll("一", "1");
		}
		if (str.contains("二")) {
			str = str.replaceAll("二", "2");
		}
		if (str.contains("三")) {
			str = str.replaceAll("三", "3");
		}
		if (str.contains("四")) {
			str = str.replaceAll("四", "4");
		}
		if (str.contains("五")) {
			str = str.replaceAll("五", "5");
		}
		if (str.contains("六")) {
			str = str.replaceAll("六", "6");
		}
		if (str.contains("七")) {
			str = str.replaceAll("七", "7");
		}
		if (str.contains("八")) {
			str = str.replaceAll("八", "8");
		}
		if (str.contains("九")) {
			str = str.replaceAll("九", "9");
		}

		if (str.contains("零")) {
			str = str.replaceAll("零", "0");
		}

		return str;
	}

	/**
	 * 中文转unicode
	 * 
	 * @param str
	 * @return
	 */
	public static String chinaToUnicode(String str) {
		String result = "";
		for (int i = 0; i < str.length(); i++) {
			int chr1 = str.charAt(i);
			if ((chr1 >= 19968) && (chr1 <= 171941)) {// 汉字范围 \u4e00-\u9fa5 (中文)
				result += "\\u" + Integer.toHexString(chr1);
			} else {
				result += str.charAt(i);
			}
		}
		return result;
	}
}

