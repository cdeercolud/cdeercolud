package com.cdeercloud.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.commons.lang3.StringUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdeercloud.common.core.exception.CdeerException;

public class ZipUtils {

	static final Logger logger = LoggerFactory.getLogger(ZipUtils.class);

	private static String unzipScorm(String srcPath, String destPath) throws IOException {
		// 上传的文件
		File srcFile = new File(srcPath);
		// 目标目录
		File destFile = new File(destPath);
		// 不存在创建
		if (!destFile.isDirectory() && !destFile.mkdir()) {
			throw new IOException("create disrectory error src:" + destPath);
		}
		try {
			// 解压
			unZip(srcPath, destPath);
		} catch (Exception e) {
			logger.error(String.format("uzip file failure %n srcFile.getPath():%s  %n  srcFile.getPath():%s ", new Object[] { srcFile.getPath(), destPath }));
			throw new IOException(e);
		}
		return destPath;
	}

	public static boolean isScorm(File file) {
		// 获得zip信息
		ZipFile zipFile = null;
		boolean flag = false;
		try {
			zipFile = new ZipFile(file);
			ZipEntry entry = zipFile.getEntry("imsmanifest.xml");
			flag = entry == null ? false : true;
		} catch (IOException e) {
			logger.error("获取zip格式文件异常", e);
			throw new CdeerException("获取zip格式文件异常");
		} finally {
			if (null != zipFile) {
				try {
					zipFile.close();
				} catch (IOException e) {
					logger.error("关闭zipFile流异常", e);
					throw new CdeerException("网络异常");
				}
			}
		}
		return flag;
	}

	public static void exitDirs(String path) {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	public static void unZip(String zipFileName, String path) {
		exitDirs(path);
		ZipFile zipFile = null;
		try {
			zipFile = new ZipFile(zipFileName, "UTF-8");
			Enumeration<?> e = zipFile.getEntries();
			ZipEntry zipEntry = null;
			while (e.hasMoreElements()) {
				zipEntry = (ZipEntry) e.nextElement();
				if (zipEntry.isDirectory()) {
					String name = zipEntry.getName();
					name = name.substring(0, name.length() - 1);
					new File(path + name).mkdir();
				} else {
					String fileName = zipEntry.getName().replaceAll("\\\\", "/");
					String subPath = null;
					if (fileName.indexOf("/") != -1) {
						subPath = fileName.substring(0, fileName.lastIndexOf("/"));
					}
					if (StringUtils.isNotBlank(subPath)) {
						exitDirs(path + subPath);
					}
					if (fileName.indexOf(".") != -1) {
						File file = new File(path + fileName);
						InputStream in = null;
						FileOutputStream out = null;
						try {
							file.createNewFile();
							in = zipFile.getInputStream(zipEntry);
							out = new FileOutputStream(file);
							byte[] by = new byte[1024];
							int c;
							while ((c = in.read(by)) != -1) {
								out.write(by, 0, c);
							}
						} catch (IOException e1) {
							logger.error(String.format("read file exception %n path:%s  %n  subPath:%s  %n fileName:%s", new Object[] { path, subPath, fileName }));
							throw new RuntimeException(e1);
						} finally {
							if (null != out) {
								try {
									out.close();
								} catch (IOException e1) {
									logger.error("FileOutputStream Close Exception:" + e);
								}
							}
							if (null != in) {
								try {
									in.close();
								} catch (IOException e1) {
									logger.error("InputStream Close Exception:" + e);
								}
							}
						}
					}
				}
			}
		} catch (IOException e) {
			logger.error(String.format("unZip file failure %n zipFileName:%s %n path:%s ", new Object[] { zipFileName, path }));
			throw new RuntimeException(e);
		} finally {
			if (null != zipFile) {
				try {
					zipFile.close();
				} catch (IOException e) {
					logger.error("关闭zipFile流异常", e);
					throw new CdeerException("网络异常");
				}
			}
		}
	}
}
