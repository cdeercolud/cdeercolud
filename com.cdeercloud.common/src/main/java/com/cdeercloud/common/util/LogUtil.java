package com.cdeercloud.common.util;

import org.apache.log4j.Logger;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

public final class LogUtil {

	private static final String NULL = "";

	public static void warn(Logger log, String message) {
		log.warn(message);
	}

	public static void error(Logger log, String message) {
		log.error(message);
	}

	public static void debug(Logger log, Object message) {
		if (log.isDebugEnabled()) {
			log.debug(message);
		}
	}

	public static void debug(Logger log, String message, Object... objects) {
		if (log.isDebugEnabled()) {
			FormattingTuple ft = MessageFormatter.arrayFormat(message, objects);
			log.debug(ft.getMessage(), ft.getThrowable());
		}
	}

	public static void info(Logger log, Object message) {
		if (log.isInfoEnabled()) {
			log.info(message);
		}
	}

	public static void info(Logger log, String message, Object... objects) {
		if (log.isInfoEnabled()) {
			StringBuffer str = new StringBuffer();
			if (message != null) {
				str.append(message).append(", ");
			}
			if ((objects != null) && (objects.length > 0)) {
				int length = objects.length;
				for (int i = 0; i < length; i++) {
					Object obj = objects[i];
					str.append(obj == null ? NULL : obj);
					if (i != (length - 1)) {
						str.append("|");
					}
				}
			}
			log.info(str.toString());
		}
	}

	public static void warn(Logger log, Object message) {
		log.warn(message);
	}

	public static void exception(Logger log, Exception e) {
		e.printStackTrace();
		log.error("", e);
	}
}
