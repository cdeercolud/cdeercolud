package com.cdeercloud.common.util;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class CorsUtils {
		final static Logger logger = LoggerFactory.getLogger(CorsUtils.class);
		//表明它允许"http://foo.org"发起跨域请求
		private static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
		public static void addCorsSupport(HttpServletRequest request, HttpServletResponse response,Set<String> ips,boolean anyAllow,boolean isProxy) {
			if (ips != null && !ips.isEmpty()) {
				String value = response.getHeader(ACCESS_CONTROL_ALLOW_ORIGIN);
				if(!StringUtils.hasText(value)){
					if (anyAllow) {
						response.addHeader(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
					} else {
						String remoteAddr = getRemoteAddr(request,isProxy);
						if (ips.contains(remoteAddr)) {
							response.addHeader(ACCESS_CONTROL_ALLOW_ORIGIN, remoteAddr);
						} else {
							logger.warn("客户端IP未注册，不予支持跨域CORS");
						}
					}
				}
			}
		}
		public static boolean isAnyAllow(Set<String> ips){
			return ips!=null && ips.contains("*");
		}
		
		private static String getRemoteAddr(HttpServletRequest request,boolean isProxy) {
			return isProxy ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr();
		}
}

