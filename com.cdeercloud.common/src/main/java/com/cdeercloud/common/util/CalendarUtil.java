package com.cdeercloud.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <b>描述</b>: 日历转换工具类：阴历和阳历日期互换(阴历日期范围19000101~20491229)<br>
 * 
 * @author liu 2015-1-5
 */
public class CalendarUtil {
	// 计算阴历日期参照1900年到2049年
	private final static int[] LUNAR_INFO = { 0x04bd8, 0x04ae0, 0x0a570, 0x054d5, 0x0d260, 0x0d950, 0x16554, 0x056a0, 0x09ad0, 0x055d2, 0x04ae0, 0x0a5b6, 0x0a4d0, 0x0d250, 0x1d255, 0x0b540, 0x0d6a0, 0x0ada2, 0x095b0, 0x14977, 0x04970, 0x0a4b0, 0x0b4b5, 0x06a50, 0x06d40, 0x1ab54, 0x02b60, 0x09570, 0x052f2, 0x04970,
			0x06566, 0x0d4a0, 0x0ea50, 0x06e95, 0x05ad0, 0x02b60, 0x186e3, 0x092e0, 0x1c8d7, 0x0c950, 0x0d4a0, 0x1d8a6, 0x0b550, 0x056a0, 0x1a5b4, 0x025d0, 0x092d0, 0x0d2b2, 0x0a950, 0x0b557, 0x06ca0, 0x0b550, 0x15355, 0x04da0, 0x0a5d0, 0x14573, 0x052d0, 0x0a9a8, 0x0e950, 0x06aa0, 0x0aea6, 0x0ab50, 0x04b60, 0x0aae4,
			0x0a570, 0x05260, 0x0f263, 0x0d950, 0x05b57, 0x056a0, 0x096d0, 0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250, 0x0d558, 0x0b540, 0x0b5a0, 0x195a6, 0x095b0, 0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40, 0x0af46, 0x0ab60, 0x09570, 0x04af5, 0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06b58, 0x055c0, 0x0ab60,
			0x096d5, 0x092e0, 0x0c960, 0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0, 0x0abb7, 0x025d0, 0x092d0, 0x0cab5, 0x0a950, 0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0, 0x0a5b0, 0x15176, 0x052b0, 0x0a930, 0x07954, 0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 0x0a4e0, 0x0d260, 0x0ea65, 0x0d530, 0x05aa0, 0x076a3,
			0x096d0, 0x04bd7, 0x04ad0, 0x0a4d0, 0x1d0b6, 0x0d250, 0x0d520, 0x0dd45, 0x0b5a0, 0x056d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 0x0aa50, 0x1b255, 0x06d20, 0x0ada0 };

	// 允许输入的最小年份
	private final static int MIN_YEAR = 1900;
	// 允许输入的最大年份
	private final static int MAX_YEAR = 2049;
	// 当年是否有闰月
	private static boolean isLeapYear;
	// 阳历日期计算起点
	private final static String START_DATE = "1900-01-30";

	/**
	 * 春季
	 */
	public static final String SPRING_SEASON = "春季";
	public static final String SPRING_TYPE = "1";

	/**
	 * 秋季
	 */
	public static final String FALL_SEASON = "秋季";
	public static final String FALL_TYPE = "2";

	/**
	 * 年
	 */
	public static final String YEAR = "年";

	/**
	 * 春季第一天
	 */
	public static final String SPRING_FIRSTDAY = "-01-16";

	/**
	 * 春季最后一天
	 */
	public static final String SPRING_LASTDAY = "-08-31";

	/**
	 * 秋季第一天
	 */
	public static final String FALL_FIRSTDAY = "-09-01";

	/**
	 * 秋季最后一天
	 */
	public static final String FALL_LASTDAY = "-01-15";

	/**
	 * 年第一天
	 */
	public static final String YEAR_FIRSTDAY = "-01-01";

	/**
	 * 计算阴历 {@code year}年闰哪个月 1-12 , 没闰传回 0
	 * 
	 * @param year 阴历年
	 * @return (int)月份
	 * @author liu 2015-1-5
	 */
	private static int getLeapMonth(int year) {
		return LUNAR_INFO[year - 1900] & 0xf;
	}

	/**
	 * 计算阴历{@code year}年闰月多少天
	 * 
	 * @param year 阴历年
	 * @return (int)天数
	 * @author liu 2015-1-5
	 */
	private static int getLeapMonthDays(int year) {
		if (getLeapMonth(year) != 0) {
			if ((LUNAR_INFO[year - 1900] & 0xf0000) == 0) {
				return 29;
			} else {
				return 30;
			}
		} else {
			return 0;
		}

	}

	/**
	 * 计算阴历{@code lunarYeay}年{@code month}月的天数
	 * 
	 * @param lunarYeay 阴历年
	 * @param month 阴历月
	 * @return (int)该月天数
	 * @throws Exception
	 * @author liu 2015-1-5
	 */
	private static int getMonthDays(int lunarYeay, int month) throws Exception {
		if ((month > 31) || (month < 0)) {
			throw (new Exception("月份有错！"));
		}
		// 0X0FFFF[0000 {1111 1111 1111} 1111]中间12位代表12个月，1为大月，0为小月
		int bit = 1 << (16 - month);
		if (((LUNAR_INFO[lunarYeay - 1900] & 0x0FFFF) & bit) == 0) {
			return 29;
		} else {
			return 30;
		}
	}

	/**
	 * 计算阴历{@code year}年的总天数
	 * 
	 * @param year 阴历年
	 * @return (int)总天数
	 * @author liu 2015-1-5
	 */
	private static int getYearDays(int year) {
		int sum = 29 * 12;
		for (int i = 0x8000; i >= 0x8; i >>= 1) {
			if ((LUNAR_INFO[year - 1900] & 0xfff0 & i) != 0) {
				sum++;
			}
		}
		return sum + getLeapMonthDays(year);
	}

	/**
	 * 计算两个阳历日期相差的天数。
	 * 
	 * @param startDate 开始时间
	 * @param endDate 截至时间
	 * @return (int)天数
	 * @author liu 2015-1-5
	 */
	private static int daysBetween(Date startDate, Date endDate) {
		long betweenDays = (endDate.getTime() - startDate.getTime()) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(betweenDays));
	}

	/**
	 * 阴历转换为阳历
	 * 
	 * @param lunarDate 阴历日期,格式YYYY_MM_DD
	 * @param leapMonthFlag 是否为闰月
	 * @return 阳历日期,格式：YYYY_MM_DD
	 * @throws Exception
	 * @author liu 2015-1-5
	 */
	public static String lunarToSolar(String lunarDate, boolean leapMonthFlag) throws Exception {
		int lunarYear = Integer.parseInt(lunarDate.substring(0, 4));
		int lunarMonth = Integer.parseInt(lunarDate.substring(5, 7));
		int lunarDay = Integer.parseInt(lunarDate.substring(8, 10));

		// checkLunarDate(lunarYear, lunarMonth, lunarDay, leapMonthFlag);
		int offset = 0;

		for (int i = MIN_YEAR; i < lunarYear; i++) {
			int yearDaysCount = getYearDays(i); // 求阴历某年天数
			offset += yearDaysCount;
		}
		// 计算该年闰几月
		int leapMonth = getLeapMonth(lunarYear);

		if (leapMonthFlag & (leapMonth != lunarMonth)) {
			// throw (new Exception("您输入的闰月标志有误！"));
		}

		// 当年没有闰月或月份早于闰月或和闰月同名的月份
		if ((leapMonth == 0) || (lunarMonth < leapMonth) || ((lunarMonth == leapMonth) && !leapMonthFlag)) {
			for (int i = 1; i < lunarMonth; i++) {
				int tempMonthDaysCount = getMonthDays(lunarYear, i);
				offset += tempMonthDaysCount;
			}

			// 检查日期是否大于最大天
			if (lunarDay > getMonthDays(lunarYear, lunarMonth)) {
				throw (new Exception("不合法的农历日期！"));
			}
			offset += lunarDay; // 加上当月的天数
		} else {// 当年有闰月，且月份晚于或等于闰月
			for (int i = 1; i < lunarMonth; i++) {
				int tempMonthDaysCount = getMonthDays(lunarYear, i);
				offset += tempMonthDaysCount;
			}
			if (lunarMonth > leapMonth) {
				int temp = getLeapMonthDays(lunarYear); // 计算闰月天数
				offset += temp; // 加上闰月天数

				if (lunarDay > getMonthDays(lunarYear, lunarMonth)) {
					throw (new Exception("不合法的农历日期！"));
				}
				offset += lunarDay;
			} else { // 如果需要计算的是闰月，则应首先加上与闰月对应的普通月的天数
				// 计算月为闰月
				int temp = getMonthDays(lunarYear, lunarMonth); // 计算非闰月天数
				offset += temp;

				if (lunarDay > getLeapMonthDays(lunarYear)) {
					throw (new Exception("不合法的农历日期！"));
				}
				offset += lunarDay;
			}
		}

		SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.YYYY_MM_DD);
		Date myDate = null;
		myDate = formatter.parse(START_DATE);
		Calendar c = Calendar.getInstance();
		c.setTime(myDate);
		c.add(Calendar.DATE, offset);
		myDate = c.getTime();

		return formatter.format(myDate);
	}

	/**
	 * 阳历日期转换为阴历日期
	 * 
	 * @param solarDate 阳历日期,格式YYYY_MM_DD
	 * @return 阴历日期
	 * @throws Exception
	 * @author liu 2015-1-5
	 */
	public static String solarToLunar(String solarDate) throws Exception {
		int i;
		int temp = 0;
		int lunarYear;
		int lunarMonth; // 农历月份
		int lunarDay; // 农历当月第几天

		SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.YYYY_MM_DD);
		Date myDate = null;
		Date startDate = null;
		try {
			myDate = formatter.parse(solarDate);
			startDate = formatter.parse(START_DATE);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		int offset = daysBetween(startDate, myDate);

		for (i = MIN_YEAR; i <= MAX_YEAR; i++) {
			temp = getYearDays(i); // 求当年农历年天数
			if ((offset - temp) < 1) {
				break;
			} else {
				offset -= temp;
			}
		}
		lunarYear = i;

		int leapMonth = getLeapMonth(lunarYear);// 计算该年闰哪个月
		// 设定当年是否有闰月
		if (leapMonth > 0) {
			isLeapYear = true;
		} else {
			isLeapYear = false;
		}

		for (i = 1; i <= 12; i++) {
			if ((i == (leapMonth + 1)) && isLeapYear) {
				temp = getLeapMonthDays(lunarYear);
				isLeapYear = false;
				i--;
			} else {
				temp = getMonthDays(lunarYear, i);
			}
			offset -= temp;
			if (offset <= 0) {
				break;
			}
		}

		offset += temp;
		lunarMonth = i;
		lunarDay = offset;

		String month = lunarMonth + "";
		String d = lunarDay + "";
		if (month.length() == 1) {
			month = "0" + lunarMonth;
		}
		if (d.length() == 1) {
			d = "0" + d;
		}
		// return "阴历：" + lunarYear + "年" + (leapMonthFlag & (lunarMonth == leapMonth) ? "闰" : "") + lunarMonth + "月" + lunarDay + "日";
		return lunarYear + "-" + month + "-" + d + "";
	}

	/**
	 * 获取季度开始时间
	 * 
	 * @param date
	 * @return YYYY_MM_DD
	 */
	public static String getSeasonStartTime(Date date) {
		try {
			String season = CalendarUtil.getSeasonType(date);// 判断这天是什么季节
			String year = DateUtil.format(date, DateUtil.YYYY);
			if (CalendarUtil.FALL_TYPE.equals(season)) {// 秋季
				year = getLunarToSolarYear(date);
				return year + CalendarUtil.FALL_FIRSTDAY;
			} else {
				return CalendarUtil.getSpringFirstDay(year);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取季度的结束时间
	 * 
	 * @param date
	 * @return YYYY_MM_DD
	 */
	public static String getSeasonEndTime(Date date) {
		String season = CalendarUtil.getSeasonType(date);// 判断这天是什么季节
		String year = DateUtil.format(date, DateUtil.YYYY);
		if (CalendarUtil.FALL_TYPE.equals(season)) {
			return CalendarUtil.getFallLastEndDay(year);
		} else {
			return year + CalendarUtil.SPRING_LASTDAY;
		}
	}

	/**
	 * 获取季度的开始时间
	 * 
	 * @param year
	 * @param seasonType
	 * @return YYYY_MM_DD
	 */
	public static String getSeasonStartTime(String year, String seasonType) {
		if (CalendarUtil.FALL_TYPE.equals(seasonType)) {
			return year + FALL_FIRSTDAY;
		} else {
			return year + SPRING_FIRSTDAY;
		}
	}

	/**
	 * 获取季度的结束时间
	 * 
	 * @param date
	 * @return YYYY_MM_DD
	 */
	public static String getSeasonEndTime(String year, String seasonType) {
		if (CalendarUtil.FALL_TYPE.equals(seasonType)) {
			return getFallLastDay(year);
		} else {
			return year + CalendarUtil.SPRING_LASTDAY;
		}
	}

	/**
	 * 获取秋季的最后一天（下一年的正月十五） YYYY_MM_DD
	 * 
	 * @return
	 */
	public static String getFallLastDay(String year) {
		try {
			Date date = new Date();
			// 1月1日
			Date startDate = DateUtil.parse(year + YEAR_FIRSTDAY, DateUtil.YYYY_MM_DD);
			// 正月十五转阳历
			Date endDate = DateUtil.parse(lunarToSolar(year + FALL_LASTDAY, false), DateUtil.YYYY_MM_DD);
			// 如果是在1月1日——正月十五，减一年
			if (date.after(startDate) && date.before(endDate)) {
				return lunarToSolar(year + FALL_LASTDAY, false);
			} else {
				return lunarToSolar((Integer.parseInt(year) + 1) + FALL_LASTDAY, false);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取秋季的最后一天（下一年的正月十五） YYYY_MM_DD
	 * 
	 * @return
	 */
	private static String getFallLastEndDay(String year) {
		try {
			return lunarToSolar(year + FALL_LASTDAY, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取春节的第一天（正月16） YYYY_MM_DD
	 * 
	 * @return
	 */
	public static String getSpringFirstDay(String year) {
		try {
			return lunarToSolar(year + SPRING_FIRSTDAY, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 返回当天属于什么季节 1春节， 2秋季
	 * 
	 * @return
	 */
	public static String getSeasonType(Date date) {
		try {
			String year = DateUtil.format(date, DateUtil.YYYY);
			Date day = DateUtil.parse(year + SPRING_LASTDAY, DateUtil.YYYY_MM_DD);
			// 当天的农历
			Date lunarThisDay = DateUtil.parse(CalendarUtil.solarToLunar(DateUtil.format(date, DateUtil.YYYY_MM_DD)), DateUtil.YYYY_MM_DD);
			// 农历正月十六
			Date lunarDay = DateUtil.parse(year + SPRING_FIRSTDAY, DateUtil.YYYY_MM_DD);
			if ((date.compareTo(day) < 0) && (lunarThisDay.compareTo(lunarDay) >= 0)) {
				return SPRING_TYPE;// 春（农历正月十六至阳历8月31号）
			} else {
				return FALL_TYPE;// 秋（阳历9月1号至农历正月十五）
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SPRING_TYPE;
	}

	/**
	 * 返回当天属于什么季节 1春节， 2秋季
	 * 
	 * @return
	 */
	public static String getSeasonType(String seasonName) {
		try {
			if (SPRING_SEASON.equals(seasonName)) {
				return SPRING_TYPE;// 春（农历正月十六至阳历8月31号）
			} else {
				return FALL_TYPE;// 秋（阳历9月1号至农历正月十五）
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SPRING_TYPE;
	}

	/**
	 * * 获取季节名称
	 * 
	 * @param date
	 * @return
	 */
	public static String getSeasonName(Date date) {
		return getSeasonNameByType(getSeasonType(date));
	}

	/**
	 * * 获取季节名称
	 * 
	 * @param date
	 * @return
	 */
	public static String getSeasonNameByType(String type) {
		return SPRING_TYPE.equals(type) ? SPRING_SEASON : FALL_SEASON;
	}

	/**
	 * 根据时间获取名称
	 * 
	 * @param date
	 * @param util
	 * @return yyyy年春季
	 */
	public static String getSeasonTitle(Date date) {
		try {
			String season = CalendarUtil.getSeasonType(date);// 判断这天是什么季节
			String year = DateUtil.format(date, DateUtil.YYYY);
			if (season.equals(FALL_TYPE)) {
				year = getLunarToSolarYear(date);
				return year + YEAR + FALL_SEASON;
			} else {
				return year + YEAR + SPRING_SEASON;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 根据年和季节获取名称
	 * 
	 * @param date
	 * @return yyyy年春季
	 */
	public static String getSeasonTitle(String year, String season) {
		if (season.equals(FALL_TYPE)) {
			return year + YEAR + FALL_SEASON;
		} else {
			return year + YEAR + SPRING_SEASON;
		}
	}

	/**
	 * 获取本学期季节
	 * 
	 * @return
	 */
	public static String getThisSeason() {
		String season = CalendarUtil.getSeasonType(new Date());// 判断这天是什么季节
		return season;
	}

	/**
	 * 获取上学期季节
	 * 
	 * @return
	 */
	public static String getLastSeason() {
		String season = CalendarUtil.getSeasonType(new Date());// 判断这天是什么季节
		if (season.equals(FALL_TYPE)) {
			return SPRING_TYPE;
		} else {
			return FALL_TYPE;
		}
	}

	/**
	 * 获取年
	 * 
	 * @param season
	 * @param now
	 * @return YYYY
	 */
	public static String getYear(Date date) {
		String season = CalendarUtil.getSeasonType(date);// 判断这天是什么季节
		String year = "";
		try {
			if (season.equals(FALL_TYPE)) {
				year = getLunarToSolarYear(date);
			} else {
				year = DateUtil.format(date, DateUtil.YYYY);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return year;
	}

	/**
	 * 获取上个季度的年
	 * 
	 * @return YYYY_MM_DD
	 */
	public static String getLastSeasonYear() {
		Date now = DateUtil.getNow();
		String year = getYear(now);
		String season = getSeasonType(now);
		if (SPRING_TYPE.equals(season)) {// 春季，获取去年的秋季第一天
			return (Integer.parseInt(year) - 1) + "";
		} else {// 秋季，获取春季的第一天
			return year;
		}
	}

	/**
	 * 获取上个季度的开始时间
	 * 
	 * @return YYYY_MM_DD
	 */
	public static String getLastSeasonStartData() {
		Date now = DateUtil.getNow();
		String year = getYear(now);
		String season = getSeasonType(now);
		if (SPRING_TYPE.equals(season)) {// 春季，获取去年的秋季第一天
			return (Integer.parseInt(year) - 1) + FALL_FIRSTDAY;
		} else {// 秋季，获取春季的第一天
			return getSpringFirstDay(year);
		}
	}

	/**
	 * 获取上个季度的结束时间时间
	 * 
	 * @return YYYY_MM_DD
	 */
	public static String getLastSeasonEndData() {
		Date now = DateUtil.getNow();
		String year = getYear(now);
		String season = getSeasonType(now);
		if (SPRING_TYPE.equals(season)) {
			return getFallLastEndDay(year);
		} else {// 秋季，获取
			return year + SPRING_LASTDAY;
		}
	}

	/**
	 * 阴历转农历后
	 * 
	 * @param year
	 * @return yyyy
	 */
	private static String getLunarToSolarYear(Date date) {
		String year = DateUtil.format(date, DateUtil.YYYY);
		try {
			// 1月1日
			Date startDate = DateUtil.parse(year + YEAR_FIRSTDAY, DateUtil.YYYY_MM_DD);
			// 正月十五转阳历
			Date endDate = DateUtil.getDayEndTime(DateUtil.parse(lunarToSolar(year + FALL_LASTDAY, false), DateUtil.YYYY_MM_DD));
			// 如果是在1月1日——正月十五，减一年
			if (date.after(startDate) && (date.compareTo(endDate) <= 0)) {
				year = (Integer.parseInt(year) - 1) + "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return year;
	}

	/**
	 * 返回格式（01月01日，2017年01月，01月01日-01月-07日）
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static String getDateString(Date startDate, Date endDate) {
		String sDate = DateUtil.format(startDate, DateUtil.YYYYMMDD);
		String eDate = DateUtil.format(endDate, DateUtil.YYYYMMDD);
		if (sDate.equals(eDate)) {
			return DateUtil.format(startDate, DateUtil.MM_DD_ZH_CN);
		}
		String sBeginDay = CalendarUtil.getSeasonStartTime(startDate).replace("-", "");
		String sEndDay = CalendarUtil.getSeasonEndTime(startDate).replace("-", "");
		if (sBeginDay.equals(sDate) && sEndDay.equals(eDate)) {
			return CalendarUtil.getSeasonTitle(startDate);
		}
		String mBeginDay = DateUtil.format(DateUtil.getMonthBeginDay(startDate), DateUtil.YYYYMMDD);
		String mEndDay = DateUtil.format(DateUtil.getMonthEndDay(endDate), DateUtil.YYYYMMDD);
		if (mBeginDay.equals(sDate) && mEndDay.equals(eDate)) {
			return DateUtil.format(startDate, DateUtil.YYYY_MM_ZH_CN);
		}
		return DateUtil.format(startDate, DateUtil.MM_DD_ZH_CN).concat("-").concat(DateUtil.format(endDate, DateUtil.MM_DD_ZH_CN));
	}

	public static void main(String[] args) {
		try {
			System.out.println(getSeasonEndTime(new Date()));// 获取季度的结束时间
			System.out.println(getSeasonEndTime("2016", "1"));// 获取季度的结束时间
			System.out.println(getFallLastDay("2016"));// 获取季度的结束时间
			System.out.println(getSeasonType(new Date()));
			System.out.println(getSeasonTitle(new Date()));
			System.out.println(lunarToSolar("2014-01-15", false));
			Date now = new Date();
			Date thisDay = DateUtil.parse(DateUtil.format(now, DateUtil.YYYY_MM_DD), DateUtil.YYYY_MM_DD);
			Date day = DateUtil.parse("2016-08-31", DateUtil.YYYY_MM_DD);
			// 当天的农历
			Date lunarThisDay = DateUtil.parse(CalendarUtil.solarToLunar(DateUtil.format(now, DateUtil.YYYY_MM_DD)), DateUtil.YYYY_MM_DD);
			// 农历正月十六
			Date lunarDay = DateUtil.parse("2016-01-16", DateUtil.YYYY_MM_DD);
			if ((thisDay.compareTo(day) < 0) && (lunarThisDay.compareTo(lunarDay) >= 0)) {
				System.out.println("春");
			} else {
				System.out.println("秋");
			}
			System.out.println(CalendarUtil.solarToLunar("2016-02-26"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
