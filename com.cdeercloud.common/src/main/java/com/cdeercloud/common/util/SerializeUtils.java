package com.cdeercloud.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdeercloud.common.core.exception.CdeerException;

public class SerializeUtils {
	final static Logger logger = LoggerFactory.getLogger(SerializeUtils.class);

	public static byte[] serialize(Object object) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(out);
			oos.writeObject(object);
			return out.toByteArray();
		} catch (IOException e) {
			String msg = "序列化时发生错误";
			logger.error(msg, e);
			throw new CdeerException(msg, e);
		}
	}

	public static Object deserialize(byte[] datas) {
		String msg = "反序列化时发生错误";
		try {
			ByteArrayInputStream in = new ByteArrayInputStream(datas);
			ObjectInputStream oos = new ObjectInputStream(in);
			return oos.readObject();
		} catch (ClassNotFoundException e) {
			logger.error(msg, e);
			throw new CdeerException(msg, e);
		} catch (IOException e) {
			logger.error(msg, e);
			throw new CdeerException(msg, e);
		}
	}
}
