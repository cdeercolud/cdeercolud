package com.cdeercloud.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.coobird.thumbnailator.Thumbnails;

/**
 * 图像处理工具类
 * Created by kevin on 2016/9/1.
 */
@Component
public class ImageUtil {

	/**
	 * 几种常见的图片格式
	 */
	public static String IMAGE_TYPE_GIF = "gif";// 图形交换格式
	public static String IMAGE_TYPE_JPG = "jpg";// 联合照片专家组
	public static String IMAGE_TYPE_JPEG = "jpeg";// 联合照片专家组
	public static String IMAGE_TYPE_BMP = "bmp";// 英文Bitmap（位图）的简写，它是Windows操作系统中的标准图像文件格式
	public static String IMAGE_TYPE_PNG = "png";// 可移植网络图形
	public static String IMAGE_TYPE_PSD = "psd";// Photoshop的专用格式

	private static String FILE_SERVER;
	private static String FILE_BASE_URI;

	@Value("${file.server.baseurl}")
	public void setFileHost(String baseurl) {
		FILE_SERVER = baseurl;
	}

	@Value("${file.server.download-uri}")
	public void setBaseUri(String uri) {
		FILE_BASE_URI = uri;
	}

	public static String getFilePath(String id) {
		return !StringUtils.isEmpty(id) ? FILE_SERVER + FILE_BASE_URI + "/" + id : "";
	}

	/**
	 * 获取图片缩略图
	 *
	 * @param imgStream
	 * @param width
	 * @param height
	 * @return
	 * @throws IOException
	 */
	public static InputStream getThumbnail(InputStream imgStream, int width, int height) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Thumbnails.of(imgStream).size(width, height).toOutputStream(os);
		ByteArrayInputStream bis = new ByteArrayInputStream(os.toByteArray());
		return bis;
	}

	public static byte[] getThumbnailBytes(InputStream imgStream, int width, int height) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Thumbnails.of(imgStream).size(width, height).toOutputStream(os);
		return os.toByteArray();
	}

	public static InputStream getThumbnail(File file, int width, int height) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Thumbnails.of(file).size(width, height).toOutputStream(os);
		ByteArrayInputStream bis = new ByteArrayInputStream(os.toByteArray());
		return bis;
	}

}
