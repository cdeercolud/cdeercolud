package com.cdeercloud.common.file.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cdeercloud.common.file.manager.FileManager;
import com.cdeercloud.common.file.vo.DfsNameValuePair;
import com.cdeercloud.common.file.vo.FileVo;
import com.cdeercloud.common.web.CdeerController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(description = "文件上传", tags = {"文件上传 FileController"})
@RestController
public class FileController extends CdeerController {

	final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Resource
	private FileManager fileManager;

	@PostMapping("/upload")
	@ApiOperation(value = "上传文件", notes = "上传文件")
	public ResponseEntity<String> upload(@ApiParam(value = "文件") MultipartFile file) {

		try {
			String fileName = file.getOriginalFilename();
			String extName = fileName.substring(fileName.lastIndexOf(".") + 1);
			String id = this.fileManager.upload(file.getBytes(), fileName, extName, new DfsNameValuePair("contentType", file.getContentType()));
			ResponseEntity<String> response = new ResponseEntity<>(id, HttpStatus.OK);
			return response;
		} catch (IOException e) {
			ResponseEntity<String> response = new ResponseEntity<>("网络异常，请稍后再试", HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}

	}

	@PostMapping("/upload/image")
	@ApiOperation(value = "上传图片", notes = "上传图片")
	public ResponseEntity<String> uploadImage(@ApiParam(value = "文件") MultipartFile file) {

		try {
			// 判断是否图片
			byte[] fileData = file.getBytes();
			if (!isPicture(fileData)) {
				ResponseEntity<String> response = new ResponseEntity<>("不是有效的图片格式", HttpStatus.INTERNAL_SERVER_ERROR);
				return response;
			}
			// 上传到文件服务器
			String fileName = file.getOriginalFilename();
			String extName = fileName.substring(fileName.lastIndexOf(".") + 1);
			String id = this.fileManager.upload(fileData, fileName, extName, new DfsNameValuePair("contentType", file.getContentType()));
			ResponseEntity<String> response = new ResponseEntity<>(id, HttpStatus.OK);
			return response;
		} catch (IOException e) {
			ResponseEntity<String> response = new ResponseEntity<>("网络异常，请稍后再试", HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}

	}

	private boolean isPicture(byte[] file) {

		try {
			ByteArrayInputStream in = new ByteArrayInputStream(file);
			ImageIO.read(in);
			return true;
		} catch (IOException e) {
			return false;
		}

	}

	@GetMapping("/download/{fileId}")
	@ApiOperation(value = "下载文件", notes = "下载文件")
	public ResponseEntity download(@ApiParam(value = "文件id") @PathVariable("fileId") String fileId) {

		try {
			FileVo file = this.fileManager.getFile(fileId);
			if (file == null) {
				return new ResponseEntity<>("文件不存在", HttpStatus.NOT_FOUND);
			}
			HttpHeaders headers = new HttpHeaders();
			Gson gson = new Gson();
			List<DfsNameValuePair> dfsNameValuePairs = gson.fromJson(file.getNameValuePair(), new TypeToken<List<DfsNameValuePair>>() {
			}.getType());
			if ((dfsNameValuePairs != null) && (dfsNameValuePairs.size() > 0)) {
				headers.setContentType(MediaType.parseMediaType(dfsNameValuePairs.get(0).getValue()));
			} else {
				headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			}
			headers.setContentDispositionFormData("attachment", file.getFileName());
			return new ResponseEntity<>(file.getContent(), headers, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@DeleteMapping("/files/{fileIds}")
	@ApiOperation(value = "删除文件", notes = "删除文件")
	public void delete(@ApiParam(value = "文件id[]") @PathVariable("fileIds") String[] fileIds) {

		this.fileManager.delete(fileIds);

	}
}
