package com.cdeercloud.common.file.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cdeercloud.common.file.manager.FileManager;
import com.cdeercloud.common.util.Base64Util;
import com.cdeercloud.common.util.ImageUtil;
import com.cdeercloud.common.web.CdeerController;

@RestController
public class CustomFileController extends CdeerController {

	final Logger logger = LoggerFactory.getLogger(CustomFileController.class);

	@Resource
	private FileManager fileManager;

	@Value("${file.server.baseurl}")
	private String fileServer;

	@Value("${boot.application.contextPath}")
	private String contextPath;

	@Value("${file.server.download-uri}")
	private String downloadUri;

	@Value("${file.image.thumbnail.width}")
	private String thumbnailWidth;

	@Value("${file.image.thumbnail.height}")
	private String thumbnailHeight;

	/**
	 * wangEditor上传文件接口
	 * 
	 * @param file 上传的文件 * 备注：因为wangEditor上传图片的接口需要特定的返回值才能预览，所以才需要此接口
	 * 
	 * @return
	 */
	@PostMapping("/upload/for/wangeditor")
	public ResponseEntity uploadForWangeditor(MultipartFile file) {
		// 文件类型限制
		String[] allowedType = { "image/bmp", "image/gif", "image/jpeg", "image/png" };
		Map<String, Object> map = new HashMap<>();
		boolean allowed = Arrays.asList(allowedType).contains(file.getContentType());
		if (!allowed) {
			ResponseEntity<String> response = new ResponseEntity<>("error|不支持的文件类型", HttpStatus.OK);
			return response;
		}
		try {
			String fileName = file.getOriginalFilename();
			String extName = fileName.substring(fileName.lastIndexOf(".") + 1);
			// 上传
			String id = this.fileManager.upload(file.getBytes(), fileName, extName, null);

			// 拼装wangEditor需要的数据格式
			String url = this.fileServer + this.downloadUri + "/" + id;
			map.put("url", url);
			// ResponseEntity<String> response = new ResponseEntity<>(url, HttpStatus.OK);
			ResponseEntity<Map<String, Object>> response = new ResponseEntity<>(map, HttpStatus.OK);
			return response;
		} catch (IOException e) {
			ResponseEntity<String> response = new ResponseEntity<>("error|网络异常，请稍后再试", HttpStatus.OK);
			return response;
		}
	}

	@PostMapping("/upload/base64")
	public ResponseEntity bash64ToFile(@RequestParam(name = "file") String base64Data, HttpServletRequest request) throws Exception {
		byte[] fileBytes = Base64Util.base64ToFile(base64Data);
		String extName = Base64Util.getSuffix(base64Data);
		String fileName = UUID.randomUUID().toString().replace("-", "");
		this.logger.info("=========:" + fileName + "====" + extName);
		try {
			// 上传原图
			String id = this.fileManager.upload(fileBytes, fileName + "." + extName, extName, null);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(fileBytes);
			// 上传缩略图
			byte[] thumbnailBytes = ImageUtil.getThumbnailBytes(byteArrayInputStream, Integer.parseInt(this.thumbnailWidth), Integer.parseInt(this.thumbnailHeight));
			String thumbnailId = this.fileManager.upload(thumbnailBytes, fileName + "." + extName, extName, null);

			// 返回图片Id
			Map data = new HashMap();
			data.put("id", id);
			data.put("thumbnailId", thumbnailId);
			ResponseEntity<Map> response = new ResponseEntity<>(data, HttpStatus.OK);
			return response;
		} catch (IOException e) {
			ResponseEntity<String> response = new ResponseEntity<>("网络异常，请稍后再试", HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}
	}

	/* 上传图片返回原图Id以及缩略图Id */
	@PostMapping("/upload/image/convert")
	public ResponseEntity uploadImageAndConvert(MultipartFile file) {
		String[] allowedType = { "image/bmp", "image/gif", "image/jpeg", "image/jpg", "image/png", "application/octet-stream" };
		boolean allowed = Arrays.asList(allowedType).contains(file.getContentType());
		if (!allowed) {
			ResponseEntity<String> response = new ResponseEntity<>("error|不支持的文件类型", HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}
		try {
			String fileName = file.getOriginalFilename();
			String extName = fileName.substring(fileName.lastIndexOf(".") + 1);
			// 上传原图
			String id = this.fileManager.upload(file.getBytes(), fileName, extName, null);

			// 上传缩略图
			byte[] thumbnailBytes = ImageUtil.getThumbnailBytes(file.getInputStream(), Integer.parseInt(this.thumbnailWidth), Integer.parseInt(this.thumbnailHeight));
			String thumbnailId = this.fileManager.upload(thumbnailBytes, "缩略图-" + fileName, extName, null);

			// 返回图片Id
			Map data = new HashMap();
			data.put("id", id);
			data.put("thumbnailId", thumbnailId);
			ResponseEntity<Map> response = new ResponseEntity<>(data, HttpStatus.OK);
			return response;
		} catch (IOException e) {
			ResponseEntity<String> response = new ResponseEntity<>("网络异常，请稍后再试", HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}
	}

}
