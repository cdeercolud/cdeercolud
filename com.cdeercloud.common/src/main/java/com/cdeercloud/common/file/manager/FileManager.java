package com.cdeercloud.common.file.manager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.entities.common.entity.Attachment;
import com.cdeercloud.common.file.service.DFSManager;
import com.cdeercloud.common.file.service.IAttachmentService;
import com.cdeercloud.common.file.vo.DfsNameValuePair;
import com.cdeercloud.common.file.vo.FileVo;
import com.cdeercloud.common.util.DateUtil;
import com.cdeercloud.common.util.MD5;
import com.google.gson.Gson;

/**
 * <pre>
 * 分布式文件管理 glusterfs实现
 * 描述：文件存储路径
 * </pre>
 *
 * @author wangzw13 zaiwei1.wang@meicloud.com
 * @version 1.00.00
 *          <p>
 * 
 *          <pre>
 *                   修改记录
 *                      修改后版本:     修改人：  修改日期:     修改内容:
 *          </pre>
 */
@Component
public class FileManager implements DFSManager {
	final Logger logger = LoggerFactory.getLogger(FileManager.class);
	@Value("${dfs.glusterfs.client.basedir}")
	public String baseDir;

	@Resource
	IAttachmentService attachmentService;

	@Override
	public boolean delete(String id) {
		// 先查记录，然后删文件，再删记录
		Attachment attachment = this.attachmentService.get(id);
		String writeFileName = attachment.getMd5() + "." + attachment.getExtName();
		File file = getFile(attachment.getFilePath(), writeFileName);
		if (file.exists()) {
			if (!file.delete()) {
				throw new CdeerException("delete file error");
			}
		}
		this.attachmentService.delete(new String[] { id });
		return true;
	}

	public void delete(String[] ids) {
		for (String id : ids) {
			Attachment attachment = this.attachmentService.get(id);
			File file = getFile(attachment.getFilePath(), attachment.getMd5());
			if (file.exists()) {
				if (!file.delete()) {
					throw new CdeerException("delete file error");
				}
			}
		}
		this.attachmentService.delete(ids);
	}

	@Override
	public boolean exist(String id) {
		// 先查记录，记录存在则根据路径查找文件，如果存在则返回true
		Attachment attachment = this.attachmentService.get(id);
		if (attachment != null) {
			String writeFileName = attachment.getMd5() + "." + attachment.getExtName();
			File file = getFile(attachment.getFilePath(), writeFileName);
			if (file.exists()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String fetchFileName(String id) {
		// 查找记录，从记录中读取文件名称
		Attachment attachment = this.attachmentService.get(id);
		if (attachment == null) {
			throw new CdeerException("文件不存在");
		}
		return attachment.getFileName();
	}

	/**
	 * 获取文件
	 *
	 * @param id
	 * @return FileVo 包含文件的内容、名称、类型、大小等信息
	 */
	public FileVo getFile(String id) {
		Attachment attachment = this.attachmentService.get(id);
		if (attachment == null) {
			return null;
		}
		FileVo fileVo = new FileVo();
		BeanUtils.copyProperties(attachment, fileVo);
		String writeFileName = attachment.getMd5() + "." + attachment.getExtName();
		File file = getFile(attachment.getFilePath(), writeFileName);
		if (!file.exists()) {
			return null;
		}
		try {
			fileVo.setContent(FileUtils.readFileToByteArray(file));
			fileVo.setFileName(writeFileName);
			return fileVo;
		} catch (IOException e) {
			throw new CdeerException("读取文件出现异常", e);
		}
	}

	@Override
	public String upload(byte[] data, String fileName, String fileExtName, DfsNameValuePair... nvp) {
		// 检查是否存在
		String md5 = MD5.md5(data);
		Attachment theSameAttachment = this.attachmentService.findByMd5(md5);
		String id = "";
		if (theSameAttachment != null) {
			// 如果是已存在的文件则新建一条数据库记录,指向同一个文件
			Attachment attachment = new Attachment();
			BeanUtils.copyProperties(theSameAttachment, attachment);
			attachment.setFileName(fileName);
			attachment.setExtName(fileExtName);
			Gson gson = new Gson();
			attachment.setNameValuePair(nvp == null ? "" : gson.toJson(nvp));
			attachment.setId(null);
			id = this.attachmentService.add(attachment);
		} else {
			// 如果是不存在的文件则新建一条数据库记录,同时保存该文件到文件系统
			Attachment attachment = new Attachment();
			attachment.setFileName(fileName);
			attachment.setExtName(fileExtName);
			Gson gson = new Gson();
			attachment.setNameValuePair(nvp == null ? "" : gson.toJson(nvp));
			attachment.setFilePath(getPath());
			attachment.setSize(data.length);
			attachment.setMd5(md5);

			// 保存文件到文件系统 同时写数据库记录
			String writeFileName = attachment.getMd5() + "." + attachment.getExtName();

			File file = getFile(attachment.getFilePath(), writeFileName);
			try {
				FileUtils.writeByteArrayToFile(file, data);
			} catch (IOException e) {
				logger.error("写文件出现IO异常 :" + this.baseDir + attachment.getFilePath(), e);
				throw new CdeerException("写文件出现IO异常 :" + this.baseDir + attachment.getFilePath(), e);
			}

			id = this.attachmentService.add(attachment);
		}
		return id;
	}

	public File getScormFile(String filePath, String fileName) {
		return new File(this.baseDir + "/scorm/" + filePath + fileName);
	}

	public File getFile(String filePath, String fileName) {
		if (null == fileName) {
			return new File(this.baseDir + filePath);
		}
		return new File(this.baseDir + filePath + fileName);
	}

	/**
	 * 根据当前日期生成文件保存路径
	 *
	 * @return 格式：/yyyy/MM/dd/
	 */
	public String getPath() {
		String date = DateUtil.format(DateUtil.getNow(), DateUtil.YYYY_MM_DD);
		String[] temp = date.split("-");
		String year = temp[0];
		String month = temp[1];
		String day = temp[2];
		return "/" + year + "/" + month + "/" + day + "/";
	}

	@Override
	public String upload(File file, String fileName, String fileExtName, DfsNameValuePair... nvp) {
		try {
			byte[] datas = this.toByte(file);
			return this.upload(datas, fileName, fileExtName, nvp);
		} catch (CdeerException e) {
			throw e;
		} catch (Exception e) {
			throw new CdeerException("upload error", e);
		}
	}

	@Override
	public String upload(InputStream in, String fileName, String fileExtName, DfsNameValuePair... nvp) {
		try {
			byte[] datas = this.toByte(in);
			return this.upload(datas, fileName, fileExtName, nvp);
		} catch (CdeerException e) {
			throw e;
		} catch (Exception e) {
			throw new CdeerException("upload error", e);
		}
	}

	public byte[] toByte(File file) throws FileNotFoundException, IOException {
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			return this.readBytes(in);
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public byte[] toByte(InputStream in) throws FileNotFoundException, IOException {
		try {
			return this.readBytes(in);
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	private byte[] readBytes(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[512];
		int len = -1;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		byte[] fileBuff = out.toByteArray();
		return fileBuff;
	}
}
