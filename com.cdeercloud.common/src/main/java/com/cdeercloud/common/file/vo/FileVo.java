package com.cdeercloud.common.file.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "文件VO", description = "文件VO")
public class FileVo {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "文件名")
	private String fileName;

	@ApiModelProperty(value = "文件路径")
	private String filePath;

	@ApiModelProperty(value = "文件大小")
	private int size;

	@ApiModelProperty(value = "扩展名")
	private String extName;

	@ApiModelProperty(value = "附加数据，键值对")
	private String nameValuePair;

	@ApiModelProperty(value = "文件二进制数据")
	private byte[] content;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getExtName() {
		return extName;
	}

	public void setExtName(String extName) {
		this.extName = extName;
	}

	public String getNameValuePair() {
		return nameValuePair;
	}

	public void setNameValuePair(String nameValuePair) {
		this.nameValuePair = nameValuePair;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

}