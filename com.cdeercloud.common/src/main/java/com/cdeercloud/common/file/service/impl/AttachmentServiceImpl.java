package com.cdeercloud.common.file.service.impl;

import org.springframework.stereotype.Service;

import com.cdeercloud.common.entities.common.entity.Attachment;
import com.cdeercloud.common.file.service.IAttachmentService;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;

@Service
public class AttachmentServiceImpl extends MybatisServiceImpl implements IAttachmentService {

	@EntityStatement
	private IEntityStatement<Attachment> attachmentDao;

	@Override
	public String add(Attachment attachment) {
		this.attachmentDao.create(attachment);
		return attachment.getId();
	}

	@Override
	public void delete(String[] ids) {
		getSqlSessionTemplate().selectList("id", ids);
		getSqlSessionTemplate().delete("deleteByIds", ids);
	}

	@Override
	public void update(Attachment attachment) {
		this.attachmentDao.update(attachment);
	}

	@Override
	public Attachment get(String id) {
		return this.attachmentDao.find(id);
	}

	@Override
	public Attachment findByMd5(String md5) {
		return this.attachmentDao.findFirstBy("md5", md5);
	}

}
