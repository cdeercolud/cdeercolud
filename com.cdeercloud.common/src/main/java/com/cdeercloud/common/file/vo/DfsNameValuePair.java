package com.cdeercloud.common.file.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author looyii
 *
 */
@ApiModel(value = "文件对应的contentType头信息", description = "文件对应的contentType头信息")
public class DfsNameValuePair {

	@ApiModelProperty(value = "请求头信息contentType")
	private String name;

	@ApiModelProperty(value = "请求头信息contentType 对应的值")
	private String value;

	public DfsNameValuePair(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
