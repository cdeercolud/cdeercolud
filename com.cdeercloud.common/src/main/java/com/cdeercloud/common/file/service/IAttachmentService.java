package com.cdeercloud.common.file.service;

import com.cdeercloud.common.entities.common.entity.Attachment;

public interface IAttachmentService {

	String add(Attachment attachment);

	void delete(String[] ids);

	void update(Attachment attachment);

	Attachment get(String id);

	Attachment findByMd5(String md5);
}
