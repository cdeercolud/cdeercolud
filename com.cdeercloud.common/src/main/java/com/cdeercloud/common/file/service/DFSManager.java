package com.cdeercloud.common.file.service;

import java.io.File;
import java.io.InputStream;

import com.cdeercloud.common.file.vo.DfsNameValuePair;

/**
 * 
 * @author looyii
 *
 */
public interface DFSManager {
	public String upload(byte[] data, String fileName, String fileExtName, DfsNameValuePair... nvp);

	public String upload(File file, String fileName, String fileExtName, DfsNameValuePair... nvp);

	public String upload(InputStream in, String fileName, String fileExtName, DfsNameValuePair... nvp);

	boolean exist(String id);

	boolean delete(String id);

	public String fetchFileName(String id);
}
