package com.cdeercloud.common.scheduler.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 触发器配置表
 * 
 * @author looyii
 *
 */
@CdeerEntity
@CdeerTable(name = "t_sys_scheduler_trigger")
@JsonInclude(value = Include.NON_NULL)
public class SysSchedulerTrigger implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8817108086095454182L;
	private String id;
	@NotEmpty(message = "触发器名称不能为空！")
	@Length(max = 21, message = "触发器名称不能超过21个字符！")
	private String name;
	private String description;
	@NotEmpty(message = "开始时间不能为空！")
	private Date startTime;
	private Date endTime;
	private String isForever;
	private String cronExpression;
	@NotEmpty(message = "触发器类型不能为空！")
	private String type;
	@NotEmpty(message = "重复间隔不能为空！")
	// @Range(min=1,message="重复间隔不能小于1！")
	private Long triggerInterval;
	private String intervalType;
	private String intervalOption;
	private Long orderNum;
	private String createBy;
	private Date createTime;
	private String modifyBy;
	private Date modifyTime;
	private String attribute1;
	private String attribute2;
	private String attribute3;
	private String attribute4;
	private String attribute5;
	// 有效状态（1：有效，d：删除）
	private String validStatus;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public java.util.Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getIsForever() {
		return isForever;
	}

	public void setIsForever(String isForever) {
		this.isForever = isForever;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTriggerInterval() {
		return triggerInterval;
	}

	public void setTriggerInterval(Long triggerInterval) {
		this.triggerInterval = triggerInterval;
	}

	public String getIntervalType() {
		return intervalType;
	}

	public void setIntervalType(String intervalType) {
		this.intervalType = intervalType;
	}

	public String getIntervalOption() {
		return intervalOption;
	}

	public void setIntervalOption(String intervalOption) {
		this.intervalOption = intervalOption;
	}

	public Long getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}

	public String getAttribute4() {
		return attribute4;
	}

	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}

	public String getAttribute5() {
		return attribute5;
	}

	public void setAttribute5(String attribute5) {
		this.attribute5 = attribute5;
	}

	public String getValidStatus() {
		return validStatus;
	}

	public void setValidStatus(String validStatus) {
		this.validStatus = validStatus;
	}

}