package com.cdeercloud.common.scheduler.service;

import java.util.List;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.scheduler.entity.SysSchedulerTrigger;

public interface ITriggerService {

	/**
	 * 查询任务关联的触发器列表
	 * 
	 * @param triggerIds
	 * @return
	 */
	public List<SysSchedulerTrigger> findSchedulerTriggerByJob(List<Object> triggerIds);

	/**
	 * 任务绑定触发器
	 * 
	 * @param ids
	 * @return
	 */
	public List<SysSchedulerTrigger> bindTrigger(List<Object> ids);

	/**
	 * 触发器列表
	 * 
	 * @param schedulerTrigger
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public PagedResult<SysSchedulerTrigger> queryTriggers(SysSchedulerTrigger schedulerTrigger, int pageIndex, int pageSize);

	/**
	 * 触发器详情
	 * 
	 * @param id
	 * @return
	 */
	public SysSchedulerTrigger findById(String id);

	/**
	 * 新增/修改稿触发器
	 * 
	 * @param sysSchedulerTrigger
	 * @return
	 */
	public SysSchedulerTrigger saveOrUpdate(SysSchedulerTrigger sysSchedulerTrigger);

	/**
	 * 删除触发器
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deleteBatchByIds(String[] ids);

}
