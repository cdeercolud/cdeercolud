package com.cdeercloud.common.scheduler.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.scheduler.entity.SysSchedulerJob;
import com.cdeercloud.common.scheduler.entity.SysSchedulerTrigger;
import com.cdeercloud.common.scheduler.service.IJobService;
import com.cdeercloud.common.web.CdeerController;

@RestController
@RequestMapping("scheduler/job")
public class JobController extends CdeerController {

	@Resource
	private IJobService jobService;

	/**
	 * 新增或修改任务
	 * 
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping("save")
	public SysSchedulerJob save(SysSchedulerJob form) {
		return this.jobService.saveOrUpdate(form);
	}

	/**
	 * 查询任务列表
	 * 
	 * @param sysSchedulerJobDto
	 * @return
	 */
	@RequestMapping("query")
	public PagedResult<SysSchedulerJob> queryJob(SysSchedulerJob sysSchedulerJob) {
		return this.jobService.queryJobs(sysSchedulerJob, this.pageIndex(), this.pageSize());
	}

	/**
	 * 暂停任务调度
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("pause")
	public boolean pauseJob(String ids) {
		return this.jobService.pauseJob(ids);
	}

	/**
	 * 恢复任务调度
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("resume")
	public boolean resumeJob(String ids) {
		return this.jobService.resumeJob(ids);
	}

	/**
	 * 手动执行任务调度
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("execute")
	public boolean executeJob(String id) {
		return this.jobService.executeJobById(id);
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("findJob")
	public SysSchedulerJob showEdit(String id) {
		return this.jobService.findById(id);
	}

	/**
	 * 关联的触发器列表
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("showTrigger")
	public List<SysSchedulerTrigger> showBindTrigger(String id) {
		return this.jobService.showBindTrigger(id);
	}

	/**
	 * 任务解绑触发器
	 * 
	 * @param jobId
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("unbindTrigger")
	public boolean unbindTriger(String jobId, List<String> ids) {
		return this.jobService.deleteSchedulerTriggerForJob(jobId, ids);
	}

	/**
	 * 任务绑定触发器
	 * 
	 * @param jobId
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("bindTrigger")
	public boolean bindTrigger(String jobId, List<Object> ids) {
		return this.jobService.bindTrigger(jobId, ids);

	}

	/**
	 * 删除任务
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("batchDel")
	public boolean deleteBatch(@RequestParam("ids[]") String[] ids) {
		return jobService.deleteBatchByIds(ids);
	}

}
