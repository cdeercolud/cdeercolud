package com.cdeercloud.common.scheduler.service;

import java.util.List;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.scheduler.entity.SysSchedulerJob;
import com.cdeercloud.common.scheduler.entity.SysSchedulerTrigger;

public interface IJobService {

	/**
	 * 新增或修改任务
	 * 
	 * @param job
	 * @return
	 */
	public SysSchedulerJob saveOrUpdate(SysSchedulerJob job);

	/**
	 * 查询任务
	 * 
	 * @param sysSchedulerJob
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public PagedResult<SysSchedulerJob> queryJobs(SysSchedulerJob sysSchedulerJob, int pageIndex, int pageSize);

	/**
	 * 暂停任务调度
	 * 
	 * @param ids
	 * @return
	 */
	public boolean pauseJob(String ids);

	/**
	 * 恢复任务调度
	 * 
	 * @param ids
	 * @return
	 */
	public boolean resumeJob(String ids);

	/**
	 * 手动执行任务调度
	 * 
	 * @param id
	 * @return
	 */
	public boolean executeJobById(String id);

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public SysSchedulerJob findById(String id);

	/**
	 * 关联的触发器列表
	 * 
	 * @param id
	 * @return
	 */
	public List<SysSchedulerTrigger> showBindTrigger(String id);

	/**
	 * 删除任务触发器绑定
	 * 
	 * @param jobId
	 * @param ids
	 * @return
	 */
	public boolean deleteSchedulerTriggerForJob(String jobId, List<String> ids);

	/**
	 * 任务绑定触发器
	 * 
	 * @param jobId
	 * @param ids
	 * @return
	 */
	public boolean bindTrigger(String jobId, List<Object> ids);

	/**
	 * 删除任务
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deleteBatchByIds(String[] ids);

}
