package com.cdeercloud.common.scheduler.constant;

/**
 * 常量类，定义一些quartz任务调度使用到的常量。
 * 
 * @author looyii
 *
 */
public class SchedulerConstant {

	public static final String QUARTZ_JOB_STORE_TYPE_RAM = "ram";
	public static final String QUARTZ_JOB_STORE_TYPE_JDBC = "jdbc";
	public static final String QUARTZ_JOB_STORE_TYPE_TERRACOTTA = "terracotta";
	public static final String QUARTZ_TRANSACTION_TYPE_ALONE = "alone";
	public static final String QUARTZ_TRANSACTION_TYPE_CMT = "cmt";

	public static final String QUARTZ_MISFIRE_INSTRUCTION_FLAG = "_misfireInstruction";

	public static final int INTEGER_VALUE_0 = 0;
	public static final int INTEGER_VALUE_2048 = 2048;

	/** 任务启用状态 **/
	public static final String JOB_STATUS_NORMAL = "normal";
	/** 任务暂停状态 **/
	public static final String JOB_STATUS_PAUSE = "pause";

	public static final String JOB_TYPE_NORMAL = "normal";

	public static final String TRIGGER_TYPE_RUN_ONCE = "run_once";
	public static final String TRIGGER_TYPE_SIMPLE_REPEAT = "simple_repeat";
	public static final String TRIGGER_TYPE_CRON_REPEAT = "cron_repeat";
	public static final String TRIGGER_TYPE_CRON_EXPRESSION = "cron_expression";

	public static final String STRING_Y = "Y";
	public static final String STRING_N = "N";

	public static final String TRIGGER_EXECUTE_TYPE_HAND = "hand";
	public static final String TRIGGER_EXECUTE_TYPE_AUTO = "auto";

	public static final String GROUP1 = "group1";

}
