package com.cdeercloud.common.scheduler.service.impl;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.orm.vo.ParamMap;
import com.cdeercloud.common.scheduler.constant.SchedulerConstant;
import com.cdeercloud.common.scheduler.entity.SysSchedulerTrigger;
import com.cdeercloud.common.scheduler.service.ITriggerService;

@Component
public class TriggerServiceImpl extends MybatisServiceImpl implements ITriggerService {

	private static final Logger logger = LoggerFactory.getLogger(TriggerServiceImpl.class);

	@EntityStatement
	private IEntityStatement<SysSchedulerTrigger> sysSchedulerTriggerDao;

	@Resource
	private Scheduler scheduler;

	@Override
	public List<SysSchedulerTrigger> findSchedulerTriggerByJob(List<Object> triggerIds) {
		return this.sysSchedulerTriggerDao.findByIdentities(triggerIds);
	}

	@Override
	public List<SysSchedulerTrigger> bindTrigger(List<Object> ids) {
		return this.sysSchedulerTriggerDao.findByIdentities(ids);

	}

	@Override
	public PagedResult<SysSchedulerTrigger> queryTriggers(SysSchedulerTrigger schedulerTrigger, int pageIndex, int pageSize) {
		ParamMap pm = new ParamMap();
		pm.include("name", schedulerTrigger.getName());
		pm.include("type", schedulerTrigger.getType());
		pm.include("createBy", schedulerTrigger.getCreateBy());
		pm.eq("validStatus", CommonStatusEnum.NORMAL.getValue());
		return this.sysSchedulerTriggerDao.pagedQuery(pm, pageIndex, pageSize);
	}

	@Override
	public SysSchedulerTrigger findById(String id) {
		return this.sysSchedulerTriggerDao.find(id);
	}

	@Override
	public SysSchedulerTrigger saveOrUpdate(SysSchedulerTrigger sysSchedulerTrigger) {
		// 检查trigger的字段
		if (sysSchedulerTrigger == null) {
			logger.error("保存的触发器为空！");
			throw new CdeerException("保存的触发器为空！");
		}
		checkTriggerFieldValue(sysSchedulerTrigger);

		if (StringUtils.isEmpty(sysSchedulerTrigger.getId())) {
			sysSchedulerTrigger.setValidStatus(CommonStatusEnum.NORMAL.getValue());
			this.sysSchedulerTriggerDao.create(sysSchedulerTrigger);
			return sysSchedulerTrigger;
		} else {
			SysSchedulerTrigger dbtrigger = this.sysSchedulerTriggerDao.find(sysSchedulerTrigger.getId());
			dbtrigger.setName(sysSchedulerTrigger.getName());
			dbtrigger.setDescription(sysSchedulerTrigger.getDescription());
			dbtrigger.setCronExpression(sysSchedulerTrigger.getCronExpression());
			dbtrigger.setTriggerInterval(sysSchedulerTrigger.getTriggerInterval());
			dbtrigger.setIntervalType(sysSchedulerTrigger.getIntervalType());
			dbtrigger.setIntervalOption(sysSchedulerTrigger.getIntervalOption());
			dbtrigger.setType(sysSchedulerTrigger.getType());
			dbtrigger.setStartTime(sysSchedulerTrigger.getStartTime());
			dbtrigger.setEndTime(sysSchedulerTrigger.getEndTime());
			dbtrigger.setIsForever(sysSchedulerTrigger.getIsForever());
			this.sysSchedulerTriggerDao.update(dbtrigger);

			// DOTO 更新绑定quartz的调度任务
			try {
				Set<TriggerKey> triggerKeySet = this.scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(dbtrigger.getId()));
				for (TriggerKey triggerKey : triggerKeySet) {
					Trigger tg = this.scheduler.getTrigger(triggerKey);
					// Trigger newtg = SchedulerContext.buildTrigger(trigger)
					// .getTriggerBuilder().forJob(tg.getJobKey()).build();
					// scheduler.rescheduleJob(tg.getKey(), newtg);
				}
			} catch (SchedulerException e) {
				logger.error(e.getMessage(), e);
			}
			return dbtrigger;
		}
	}

	private void checkTriggerFieldValue(SysSchedulerTrigger trigger) {
		String type = trigger.getType();
		if (SchedulerConstant.TRIGGER_TYPE_RUN_ONCE.equals(type)) {
			trigger.setIsForever(SchedulerConstant.STRING_N);
			trigger.setTriggerInterval(null);
			trigger.setCronExpression(null);
		} else if (SchedulerConstant.TRIGGER_TYPE_SIMPLE_REPEAT.equals(type)) {
			trigger.setCronExpression(null);
			if ((trigger.getTriggerInterval() == null) || (trigger.getTriggerInterval() == 0)) {
				throw new CdeerException("重复间隔不能为空或者0！");
			}
			if (SchedulerConstant.STRING_Y.equals(trigger.getIsForever())) {
				trigger.setEndTime(null);
			} else {
				if (trigger.getEndTime() == null) {
					throw new CdeerException("结束时间不能为空！");
				}
			}
		} else {
			trigger.setTriggerInterval(null);
			if (SchedulerConstant.STRING_Y.equals(trigger.getIsForever())) {
				trigger.setEndTime(null);
			} else {
				if (trigger.getEndTime() == null) {
					throw new CdeerException("结束时间不能为空！");
				}
			}
			if (trigger.getCronExpression() == null) {
				throw new CdeerException("Cron表达式不能为空！");
			}
		}
	}

	@Override
	public boolean deleteBatchByIds(String[] ids) {
		if (ids != null) {
			for (String id : ids) {
				SysSchedulerTrigger trigger = findById(id);
				trigger.setValidStatus(CommonStatusEnum.DELETE_D.getValue());
				this.sysSchedulerTriggerDao.update(trigger);

				// TODO 更新绑定quartz的调度任务
				try {
					Set<TriggerKey> triggerKeySet = this.scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(trigger.getId()));
					for (TriggerKey triggerKey : triggerKeySet) {
						this.scheduler.unscheduleJob(triggerKey);
					}
				} catch (SchedulerException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return true;
	}

}
