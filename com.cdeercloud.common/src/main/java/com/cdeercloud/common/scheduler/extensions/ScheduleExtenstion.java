package com.cdeercloud.common.scheduler.extensions;

import java.util.Properties;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import com.cdeercloud.common.core.definition.BeanDefinitionInitializer;

public class ScheduleExtenstion implements BeanDefinitionInitializer {
	@Override
	public void init(BeanDefinitionRegistry registry, Properties properties) {
		String property = properties.getProperty("schedule.active");
		if ("true".equals(property)) {
			String jfBeanName = "framework_schedule_springBeanJobFactory";
			RootBeanDefinition jfDef = new RootBeanDefinition(SpringBeanJobFactory.class);
			jfDef.setScope(BeanDefinition.SCOPE_SINGLETON);
			registry.registerBeanDefinition(jfBeanName, jfDef);

			RootBeanDefinition def = new RootBeanDefinition(SchedulerFactoryBean.class);
			def.setScope(BeanDefinition.SCOPE_SINGLETON);
			MutablePropertyValues pvs = def.getPropertyValues();
			pvs.addPropertyValue("dataSource", new RuntimeBeanReference("framework_orm_dbcp_dataSource"));
			pvs.addPropertyValue("jobFactory", new RuntimeBeanReference(jfBeanName));
			registry.registerBeanDefinition("framework_schedule_schedulerFactoryBean", def);
		}
	}

}
