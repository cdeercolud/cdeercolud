package com.cdeercloud.common.scheduler.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.core.util.ContextUtils;

/**
 * 执行job服务
 * 
 * @author looyii
 *
 */
public class SchedulerJob implements Job {

	private static final Logger logger = LoggerFactory.getLogger(SchedulerJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDetail jobDetail = context.getJobDetail();
		JobDataMap jobDataMap = jobDetail.getJobDataMap();
		String rpcServer = jobDataMap.getString("rpcServer");
		String rpcPath = jobDataMap.getString("rpcPath");
		if ("spring-bean".equals(rpcPath)) {
			Object bean = ContextUtils.getBean(rpcServer);
			if (bean instanceof Job) {
				Job job = (Job) bean;
				job.execute(context);
			} else {
				logger.error("未实现Job接口.");
			}
		} else {
			// TODO 可通过rpc执行其他服务器bean
			throw new CdeerException("手动执行任务失败， 没有注入相关的bean");
		}

	}

}
