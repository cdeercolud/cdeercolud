package com.cdeercloud.common.scheduler.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.vo.ParamMap;
import com.cdeercloud.common.scheduler.constant.SchedulerConstant;
import com.cdeercloud.common.scheduler.entity.SysSchedulerJob;
import com.cdeercloud.common.scheduler.entity.SysSchedulerTrigger;
import com.cdeercloud.common.scheduler.job.SchedulerJob;
import com.cdeercloud.common.scheduler.service.IJobService;
import com.cdeercloud.common.scheduler.service.ITriggerService;
import com.cdeercloud.common.util.DateUtil;

@Component
public class JobServiceImpl implements IJobService {

	private static final Logger logger = LoggerFactory.getLogger(JobServiceImpl.class);

	@EntityStatement
	private IEntityStatement<SysSchedulerJob> sysSchedulerJobDao;

	@Autowired
	private ITriggerService triggerService;

	// @Resource
	private Scheduler scheduler;

	@Override
	public SysSchedulerJob saveOrUpdate(SysSchedulerJob job) {
		if (StringUtils.isEmpty(job.getId())) {
			job.setId(null);
			job.setStatus(SchedulerConstant.JOB_STATUS_NORMAL);
			job.setType(SchedulerConstant.JOB_TYPE_NORMAL);
			job.setValidStatus(CommonStatusEnum.NORMAL.getValue());
			this.sysSchedulerJobDao.create(job);
			return job;
		} else {
			SysSchedulerJob dbjob = this.sysSchedulerJobDao.find(job.getId());
			dbjob.setName(job.getName());
			// dbjob.setType(job.getType());
			dbjob.setStatus(job.getStatus());
			dbjob.setBeanId(job.getBeanId());
			dbjob.setDescription(job.getDescription());
			dbjob.setExecutor(job.getExecutor());
			dbjob.setDisplayName(job.getDisplayName());
			this.sysSchedulerJobDao.update(dbjob);
			return dbjob;
		}
	}

	@Override
	public PagedResult<SysSchedulerJob> queryJobs(SysSchedulerJob sysSchedulerJob, int pageIndex, int pageSize) {
		ParamMap param = new ParamMap();
		param.include("name", sysSchedulerJob.getName());
		param.include("status", sysSchedulerJob.getStatus());
		param.include("createdBy", sysSchedulerJob.getCreateBy());
		param.eq("validStatus", CommonStatusEnum.NORMAL.getValue());
		param.range("creationDate", DateUtil.getDayBeginTime(sysSchedulerJob.getQueryBeginTime()), DateUtil.getDayEndTime(sysSchedulerJob.getQueryendTime()));
		return this.sysSchedulerJobDao.pagedQuery(param, pageIndex, pageSize);
	}

	@Override
	public boolean pauseJob(String ids) {
		if (StringUtils.isEmpty(ids)) {
			return false;
		}
		List<SysSchedulerJob> jobs = new ArrayList<SysSchedulerJob>();
		String[] st = ids.split(",");
		for (String element : st) {
			SysSchedulerJob job = this.sysSchedulerJobDao.find(element);
			jobs.add(job);
		}

		try {
			for (SysSchedulerJob schedulerJob : jobs) {
				this.scheduler.pauseJob(new JobKey(schedulerJob.getId(), SchedulerConstant.GROUP1));
				schedulerJob.setStatus(SchedulerConstant.JOB_STATUS_PAUSE);
				this.saveOrUpdate(schedulerJob);
			}
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}

	@Override
	public boolean resumeJob(String ids) {
		if (StringUtils.isEmpty(ids)) {
			return false;
		}
		List<SysSchedulerJob> jobs = new ArrayList<SysSchedulerJob>();
		String[] st = ids.split(",");
		for (String element : st) {
			SysSchedulerJob job = this.sysSchedulerJobDao.find(element);
			jobs.add(job);
		}

		try {
			for (SysSchedulerJob schedulerJob : jobs) {
				this.scheduler.resumeJob(new JobKey(schedulerJob.getId(), SchedulerConstant.GROUP1));
				schedulerJob.setStatus(SchedulerConstant.JOB_STATUS_NORMAL);
				this.saveOrUpdate(schedulerJob);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}

	@Override
	public boolean executeJobById(String id) {
		SysSchedulerJob sysSchedulerJob = this.sysSchedulerJobDao.find(id);
		if (sysSchedulerJob == null) {
			throw new CdeerException("未查询到任务，请刷新页面再操作！");
		}

		if (SchedulerConstant.JOB_STATUS_PAUSE.equals(sysSchedulerJob.getStatus())) {
			throw new CdeerException("暂停状态的任务不能执行！");
		}
		try {
			JobDataMap data = new JobDataMap();
			data.put("executeType", SchedulerConstant.TRIGGER_EXECUTE_TYPE_HAND);
			JobKey jobKey = new JobKey(sysSchedulerJob.getId(), SchedulerConstant.GROUP1);
			boolean flag = scheduler.checkExists(jobKey);
			if (flag) {
				scheduler.triggerJob(jobKey, data);
			} else {
				JobBuilder builder = JobBuilder.newJob(SchedulerJob.class);
				builder.withIdentity(sysSchedulerJob.getId(), SchedulerConstant.GROUP1);
				builder.usingJobData("rpcServer", sysSchedulerJob.getExecutor());
				builder.usingJobData("rpcPath", sysSchedulerJob.getBeanId());
				JobDetail job = builder.build();

				TriggerBuilder<Trigger> tBuilder = TriggerBuilder.newTrigger();
				tBuilder.startNow();
				Trigger trigger = tBuilder.build();
				try {
					this.scheduler.scheduleJob(job, trigger);
				} catch (SchedulerException e) {
					logger.error("", e);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new CdeerException("手动执行任务失败：" + e.getMessage(), e);
		}
		return true;
	}

	@Override
	public SysSchedulerJob findById(String id) {
		return this.sysSchedulerJobDao.find(id);
	}

	@Override
	public List<SysSchedulerTrigger> showBindTrigger(String id) {
		try {
			List<? extends Trigger> triggers = this.scheduler.getTriggersOfJob(new JobKey(id, SchedulerConstant.GROUP1));
			if (CollectionUtils.isEmpty(triggers)) {
				return null;
			}
			List<Object> triggerIds = new ArrayList<>();
			for (Trigger trigger : triggers) {
				triggerIds.add(trigger.getKey().getName());
			}
			return this.triggerService.findSchedulerTriggerByJob(triggerIds);
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public boolean deleteSchedulerTriggerForJob(String jobId, List<String> ids) {
		try {
			if (ids != null) {
				List<? extends Trigger> jobTriggers = this.scheduler.getTriggersOfJob(new JobKey(jobId, SchedulerConstant.GROUP1));
				for (Trigger trigger : jobTriggers) {
					String group = trigger.getKey().getName();
					if (ids.contains(group)) {
						scheduler.unscheduleJob(trigger.getKey());
					}
				}
				return true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}

	@Override
	public boolean bindTrigger(String jobId, List<Object> ids) {
		List<SysSchedulerTrigger> triggers = this.triggerService.bindTrigger(ids);
		this.createSchedulerTriggerForJob(jobId, triggers);
		return true;
	}

	private List<SysSchedulerTrigger> createSchedulerTriggerForJob(String jobId, List<SysSchedulerTrigger> triggers) {
		try {
			// scheduleUtils.down();
			SysSchedulerJob schedulerJob = this.sysSchedulerJobDao.find(jobId);
			// 增加检查暂停的任务不能再添加新的触发器
			if (SchedulerConstant.JOB_STATUS_PAUSE.equals(schedulerJob.getStatus())) {
				throw new CdeerException("暂停状态的任务不能再关联触发器！");
			}

			// TODO 检查不能添加重复的触发器

			for (SysSchedulerTrigger schedulerTrigger : triggers) {
				JobBuilder builder = JobBuilder.newJob(SchedulerJob.class);
				builder.withIdentity(schedulerJob.getId(), SchedulerConstant.GROUP1);
				builder.usingJobData("rpcServer", schedulerJob.getExecutor());
				builder.usingJobData("rpcPath", schedulerJob.getBeanId());
				JobDetail job = builder.build();

				TriggerBuilder<Trigger> tBuilder = TriggerBuilder.newTrigger();
				tBuilder.withIdentity(schedulerTrigger.getId(), SchedulerConstant.GROUP1);

				tBuilder.withSchedule(CronScheduleBuilder.cronSchedule(schedulerTrigger.getCronExpression()));
				Trigger trigger = tBuilder.build();
				this.scheduler.scheduleJob(job, trigger);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new CdeerException(e.getMessage());
		}
		return triggers;
	}

	@Override
	public boolean deleteBatchByIds(String[] ids) {
		try {
			if ((ids != null) && (ids.length > 0)) {
				List<SysSchedulerJob> jobs = new ArrayList<SysSchedulerJob>();
				for (String id : ids) {
					SysSchedulerJob job = findById(id);
					jobs.add(job);
					job.setValidStatus(CommonStatusEnum.DELETE_D.getValue());
					this.sysSchedulerJobDao.update(job);
				}
				// 删除已经安排的quartz调度任务
				List<JobKey> list = new ArrayList<JobKey>();
				for (SysSchedulerJob schedulerJob : jobs) {
					list.add(new JobKey(schedulerJob.getId(), SchedulerConstant.GROUP1));
				}
				this.scheduler.deleteJobs(list);
			}
		} catch (SchedulerException e) {
			logger.error("", e);
		}
		return true;
	}
}
