package com.cdeercloud.common.scheduler.web;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.scheduler.entity.SysSchedulerTrigger;
import com.cdeercloud.common.scheduler.service.ITriggerService;
import com.cdeercloud.common.web.CdeerController;

@RestController
@RequestMapping("scheduler/trigger")
public class TriggerController extends CdeerController {

	@Resource
	private ITriggerService triggerService;

	/**
	 * 触发器列表
	 * 
	 * @param schedulerTrigger
	 * @return
	 */
	@ResponseBody
	@RequestMapping("query")
	public PagedResult<SysSchedulerTrigger> queryTrigger(SysSchedulerTrigger schedulerTrigger) {
		return this.triggerService.queryTriggers(schedulerTrigger, this.pageIndex(), this.pageSize());
	}

	@RequestMapping("showEdit")
	public SysSchedulerTrigger showEdit(String id) {
		return this.triggerService.findById(id);
	}

	/**
	 * 触发器验证
	 * 
	 * @param cron
	 * @return
	 */
	@ResponseBody
	@RequestMapping("validateCron")
	public boolean validateCron(String cron) {
		boolean flag = org.quartz.CronExpression.isValidExpression(cron);
		return flag;
	}

	/**
	 * 新增/修改触发器
	 * 
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping("save")
	public SysSchedulerTrigger save(SysSchedulerTrigger sysSchedulerTrigger) {
		return this.triggerService.saveOrUpdate(sysSchedulerTrigger);
	}

	/**
	 * 删除触发器
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping("batchDel")
	public boolean deleteBatch(@RequestParam("ids[]") String[] ids) {
		return this.triggerService.deleteBatchByIds(ids);
	}

}
