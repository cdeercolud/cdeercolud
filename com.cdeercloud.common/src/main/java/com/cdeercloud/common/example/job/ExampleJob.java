package com.cdeercloud.common.example.job;

import java.util.concurrent.atomic.AtomicBoolean;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("exampleJob")
public class ExampleJob implements Job {

	private static Logger logger = LoggerFactory.getLogger(ExampleJob.class);

	private static AtomicBoolean islock = new AtomicBoolean(false);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		if (!islock.get()) {
			islock.set(true);
			try {
				// DOTO 执行
			} catch (Exception e) {
				logger.error("Msg Job Failure", e);
			} finally {
				islock.set(false);
			}
		}
	}

}
