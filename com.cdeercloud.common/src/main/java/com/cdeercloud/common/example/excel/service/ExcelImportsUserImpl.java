package com.cdeercloud.common.example.excel.service;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.alibaba.fastjson.JSONObject;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.example.excel.dto.ExcelDto;
import com.cdeercloud.common.excel.imports.ImportContext;
import com.cdeercloud.common.excel.imports.ImportHandler;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;

/**
 * TODO 导入示例实现类
 * 
 * @author looyii
 *
 */
@Component
public class ExcelImportsUserImpl extends MybatisServiceImpl implements ImportHandler<ExcelDto> {

	private final static Logger log = LoggerFactory.getLogger(ExcelImportsUserImpl.class);

	@Value("${web.imports.fails.dir:./}")
	private String filePath;
	private String fileExt = ".xlsx";

	@Override
	public String businessName() {
		return "user_import";
	}

	@Override
	public int dataFirstRowNum() {
		return 1;
	}

	@Override
	public String templateFileName() {
		JSONObject obj = new JSONObject();
		obj.put("dir", filePath + "用户导入模板" + this.fileExt);
		return obj.toJSONString();
	}

	@Override
	public boolean validate(ExcelDto t, BindingResult br) {
		if (StringUtils.isBlank(t.getCode()) || StringUtils.isBlank(t.getName())) {
			br.addError(new FieldError(t.getClass().getName(), "", "必填字段不能为空"));
		}
		if ((t.getBirth() == null) || (t.getBirth() == null)) {
			br.addError(new FieldError(t.getClass().getName(), "", "日期出错"));
		}

		if (br.hasErrors()) {
			return false;
		}
		// 校验流程
		return true;
	}

	@Override
	public void handle(ExcelDto t, ImportContext context) {
		// 保存数据
	}

	@Override
	public String getDymFile() {
		// 下载模板
		String templateFile = "222";
		return templateFile;
	}

	private void closeStream(OutputStream stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
				log.debug("文件流关闭异常!");
				throw new CdeerException("文件流关闭异常!");
			}
		}
	}

}
