package com.cdeercloud.common.example.excel.web;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.example.excel.dto.ExcelDto;
import com.cdeercloud.common.excel.export.ExcelUtils;
import com.cdeercloud.common.excel.export.dto.Column;
import com.cdeercloud.common.util.DownloadUtil;

/**
 * 导出示例
 * 
 * @author looyii
 *
 */
@RestController
@RequestMapping("download/excel/example")
public class ExcelExportExampleController {

	/**
	 * excel导出示例
	 * 
	 * @param response
	 */
	@GetMapping("export")
	public void listEmployeesExport(HttpServletResponse response) {
		List<ExcelDto> lists = new ArrayList<ExcelDto>();
		ExcelDto dto = new ExcelDto();
		dto.setCode("1001");
		dto.setName("张三");
		lists.add(dto);
		dto = new ExcelDto();
		dto.setCode("1002");
		dto.setName("李四");
		lists.add(dto);
		dto = new ExcelDto();
		dto.setCode("1003");
		dto.setName("王五");
		lists.add(dto);
		dto = new ExcelDto();
		dto.setCode("1004");
		dto.setName("赵六");
		lists.add(dto);
		PagedResult<ExcelDto> pageResult = new PagedResult<>(lists, 10, 1, 10);

		Column colA = new Column();
		colA.setLabel("员工号");
		colA.setProperty("code");
		Column colB = new Column();
		colB.setLabel("姓名");
		colB.setProperty("name");
		Column[] cols = { colA, colB };
		HSSFWorkbook workbook = null;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			workbook = ExcelUtils.createExcel(pageResult, cols);
			workbook.write(output);
		} catch (Exception e) {
			throw new CdeerException(e.getMessage());
		}
		DownloadUtil.downloadFile(response, output.toByteArray(), "excel导出.xls");
	}
}
