package com.cdeercloud.common.enums;

/**
 * 应用类型
 *
 * @author looyii
 *
 */
public enum AppEnum {
	//分享
	SHARE("1001", "share", "分享"),
	//作业
	WORK("1002", "homework", "作业"),
	//投票
	VOTE("1003", "vote", "投票"),
	//问卷
	QUESTIONNAIRE("1004", "questionnaire", "问卷");

	private String appType;
	private String appBean;
	private String appName;

	private AppEnum(String appType, String appBean, String appName) {
		this.appType = appType;
		this.appBean = appBean;
		this.appName = appName;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppBean() {
		return appBean;
	}

	public void setAppBean(String appBean) {
		this.appBean = appBean;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

}
