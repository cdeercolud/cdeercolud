package com.cdeercloud.common.enums;

public class SchoolOpenType {

	/**
	 * 学校由系统开通
	 */
	public static final String OPEN_BY_SYSTEM = "1";

	/**
	 * 学校由用户开通
	 */
	public static final String OPEN_BY_USER = "2";

	public static String getOpenTypeName(String openType) {
		if (SchoolOpenType.OPEN_BY_SYSTEM.equals(openType)) {
			return "系统开通";
		}
		if (SchoolOpenType.OPEN_BY_USER.equals(openType)) {
			return "用户开通";
		}
		return "";
	}

}
