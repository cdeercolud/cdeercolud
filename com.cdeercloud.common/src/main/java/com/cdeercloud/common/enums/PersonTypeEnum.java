package com.cdeercloud.common.enums;

/**
 * 用户相关类型
 * 
 * @author looyii
 *
 */
public enum PersonTypeEnum {

	// (type) 创建人
	CREATE("create"),
	// (type)相关人
	TO("to"),
	// (type)执行人
	EXECUTOR("executor"),
	// (personType) 用户
	USER("u"),
	// (personType) 群组
	GROUP("g");

	private String value;

	private PersonTypeEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
