package com.cdeercloud.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public final class SchoolType {

	public static final String SCHOOL = "school";
	public static final String COMPANY = "company";

	public static final String KINDERGARTEN_CODE = "1";
	public static final String KINDERGARTEN_NAME = "幼儿园";

	public static final String PRIMARYSCHOOL_CODE = "10";
	public static final String PRIMARYSCHOOL_NAME = "小学";

	public static final String JUNIORMIDDLESCHOOL_CODE = "100";
	public static final String JUNIORMIDDLESCHOOL_NAME = "初中";

	public static final String SENIORHIGHSCHOOL_CODE = "1000";
	public static final String SENIORHIGHSCHOOL_NAME = "高中";

	public static final String UNIVERSITY_CODE = "10000";
	public static final String UNIVERSITY_NAME = "大学";

	public static final String EDUCATIONDEPARTMENT_CODE = "100000";
	public static final String EDUCATIONDEPARTMENT_NAME = "教育局";

	public static final String TRAININGINSTITUTIONS_CODE = "1000000";
	public static final String TRAININGINSTITUTIONS_NAME = "培训机构";

	public static final String COMPANY_CODE = "10000000";
	public static final String COMPANY_NAME = "企业";

	public static final String CDC_CODE = "100000000";
	public static final String CDC_NAME = "疾控中心";

	public static final String RESOURCE_PLATFORM_CODE = "200000000";// 第三方资源平台
	public static final String RESOURCE_PLATFORM_NAME = "资源平台";

	public static final String OPEN_TYPE_SYSTEM = "1";
	public static final String OPEN_TYPE_USER = "2";

	// 标识上级组织
	public static final String ORG_TYPE_SUPER = "1";// 学校之上的组织（如：教育集团、教育局）

	/**
	 * 存放类型、名称
	 */
	public static final Map<String, String> SCHOOL_TYPE_MAP = new HashMap<String, String>();

	static {
		SCHOOL_TYPE_MAP.put(KINDERGARTEN_CODE, KINDERGARTEN_NAME);
		SCHOOL_TYPE_MAP.put(PRIMARYSCHOOL_CODE, PRIMARYSCHOOL_NAME);
		SCHOOL_TYPE_MAP.put(JUNIORMIDDLESCHOOL_CODE, JUNIORMIDDLESCHOOL_NAME);
		SCHOOL_TYPE_MAP.put(SENIORHIGHSCHOOL_CODE, SENIORHIGHSCHOOL_NAME);
		SCHOOL_TYPE_MAP.put(UNIVERSITY_CODE, UNIVERSITY_NAME);
		SCHOOL_TYPE_MAP.put(EDUCATIONDEPARTMENT_CODE, EDUCATIONDEPARTMENT_NAME);
		SCHOOL_TYPE_MAP.put(TRAININGINSTITUTIONS_CODE, TRAININGINSTITUTIONS_NAME);
		SCHOOL_TYPE_MAP.put(COMPANY_CODE, COMPANY_NAME);
		SCHOOL_TYPE_MAP.put(CDC_CODE, CDC_NAME);
		SCHOOL_TYPE_MAP.put(RESOURCE_PLATFORM_NAME, RESOURCE_PLATFORM_CODE);
	}

	/**
	 * 是否为学校组织
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isSchoolSuperOrg(String type) {
		if (StringUtils.isNotBlank(type) && (type.length() > 5) && !COMPANY_CODE.equals(type)) {
			return true;
		}
		return false;
	}

	/**
	 * 获取单一类型学校类型名称
	 * 
	 * @param schoolType
	 * @return
	 */
	public static String getSingalSchoolTypeName(String schoolType) {
		return SCHOOL_TYPE_MAP.get(schoolType);
	}

	/**
	 * 是否为幼儿园
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isKindergarten(String type) {
		if ((type.length() > 0) && (type.charAt(0) == '1')) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为小学
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isPrimarySchool(String type) {
		if ((type.length() > 1) && (type.charAt(1) == '1')) {
			return true;
		}
		return false;
	}

	/**
	 * 是否是初中
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isJuniorMiddleSchool(String type) {
		if ((type.length() > 2) && (type.charAt(2) == '1')) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为高中
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isSeniorHighSchool(String type) {
		if ((type.length() > 3) && (type.charAt(3) == '1')) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为大学
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isUniversity(String type) {
		if ((type.length() > 4) && (type.charAt(4) == '1')) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为教育局
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isBureauEducation(String type) {
		if (EDUCATIONDEPARTMENT_CODE.equals(type)) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为学校
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isSchool(String type) {
		if (StringUtils.isNotBlank(type)) {
			if ((type.length() > 0) && (type.charAt(type.length() - 1) == '1')) {
				return true;
			} else if ((type.length() > 1) && (type.charAt(type.length() - 2) == '1')) {
				return true;
			} else if ((type.length() > 2) && (type.charAt(type.length() - 3) == '1')) {
				return true;
			} else if ((type.length() > 3) && (type.charAt(type.length() - 4) == '1')) {
				return true;
			} else if ((type.length() > 4) && (type.charAt(type.length() - 5) == '1')) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 是否为公司
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isCompany(String type) {
		if (COMPANY_CODE.equals(type)) {
			return true;
		}
		return false;
	}

	public static String getSchoolTypeName(String type) {

		StringBuffer retstr = new StringBuffer();
		if (type != null) {
			if ((type.length() > 0) && (type.charAt(type.length() - 1) == '1')) {
				retstr.append(KINDERGARTEN_NAME);
			}
			if ((type.length() > 1) && (type.charAt(type.length() - 2) == '1')) {
				retstr.append(PRIMARYSCHOOL_NAME);
			}
			if ((type.length() > 2) && (type.charAt(type.length() - 3) == '1')) {
				retstr.append(JUNIORMIDDLESCHOOL_NAME);
			}

			if ((type.length() > 3) && (type.charAt(type.length() - 4) == '1')) {
				retstr.append(SENIORHIGHSCHOOL_NAME);
			}

			if ((type.length() > 4) && (type.charAt(type.length() - 5) == '1')) {
				retstr.append(UNIVERSITY_NAME);
			}

			if ((type.length() > 5) && (type.charAt(type.length() - 6) == '1')) {
				retstr.append(EDUCATIONDEPARTMENT_NAME);
			}

			if ((type.length() > 6) && (type.charAt(type.length() - 7) == '1')) {
				retstr.append(TRAININGINSTITUTIONS_NAME);
			}

			if ((type.length() > 7) && (type.charAt(type.length() - 8) == '1')) {
				retstr.append(COMPANY_NAME);
			}
			return StringUtils.isNotBlank(retstr.toString()) ? retstr.toString() : "";
		}
		return "";
	}

	public static Map<String, String> convertTypeToMap(String type) {
		// 验证学校类型是否正确
		if (!type.matches("1[0|1]{0,}")) {
			return null;
		}
		String[] typeNames = new String[] { COMPANY_NAME, TRAININGINSTITUTIONS_NAME, EDUCATIONDEPARTMENT_NAME, UNIVERSITY_NAME, SENIORHIGHSCHOOL_NAME, JUNIORMIDDLESCHOOL_NAME, PRIMARYSCHOOL_NAME, KINDERGARTEN_NAME };
		// String[] types = new String[] {COMPANY_CODE,TRAININGINSTITUTIONS_CODE,EDUCATIONDEPARTMENT_CODE,UNIVERSITY_CODE,SENIORHIGHSCHOOL_CODE,JUNIORMIDDLESCHOOL_CODE,PRIMARYSCHOOL_CODE,KINDERGARTEN_CODE};

		Map<String, String> typeMap = new HashMap<String, String>();
		String temp = type;
		if (type.length() > typeNames.length) {
			temp = type.substring(type.length() - typeNames.length);
		} else if (type.length() < typeNames.length) {
			for (int i = 0; i < (typeNames.length - type.length()); i++) {
				temp = "0" + temp;
			}
		}
		int multiple = 1;
		for (int i = temp.length() - 1; i >= 0; i--) {
			if (temp.charAt(i) == '1') {
				typeMap.put(typeNames[i], multiple + "");
			}
			multiple *= 10;
		}
		return typeMap;

	}

	public static String getSchoolTypeByName(String name) {

		int retType = 0;
		if (name != null) {
			if (name.contains(KINDERGARTEN_NAME)) {
				retType = retType + 1;

			}
			if (name.contains(PRIMARYSCHOOL_NAME)) {
				retType = retType + 10;
			}

			if (name.contains(JUNIORMIDDLESCHOOL_NAME)) {
				retType = retType + 100;
			}

			if (name.contains(SENIORHIGHSCHOOL_NAME)) {
				retType = retType + 1000;
			}

			if (name.contains(UNIVERSITY_NAME)) {
				retType = retType + 10000;
			}
			if (name.contains(EDUCATIONDEPARTMENT_NAME)) {
				retType = retType + 100000;
			}

			if (name.contains(TRAININGINSTITUTIONS_NAME)) {
				retType = retType + 1000000;
			}

			if (name.contains(COMPANY_NAME)) {
				retType = retType + 10000000;
			}

			if (name.contains(CDC_NAME)) {
				retType = retType + 100000000;
			}

			return retType == 0 ? "" : String.valueOf(retType);
		}
		return null;
	}

	/**
	 * 根据学校类型查询学校类别
	 * 
	 * @param schoolType
	 * @return
	 */
	public static List<String> parse(String schoolType) {
		if (StringUtils.isEmpty(schoolType)) {
			return null;
		}

		List<String> types = new ArrayList<String>();
		int type = 0;
		try {
			type = Integer.parseInt(schoolType);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}

		switch (type) {
		case 10:
			types.add(PRIMARYSCHOOL_CODE);
			break;
		case 100:
			types.add(JUNIORMIDDLESCHOOL_CODE);
			break;
		case 1000:
			types.add(SENIORHIGHSCHOOL_CODE);
			break;
		case 110:
			types.add(PRIMARYSCHOOL_CODE);
			types.add(JUNIORMIDDLESCHOOL_CODE);
			break;
		case 1010:
			types.add(PRIMARYSCHOOL_CODE);
			types.add(SENIORHIGHSCHOOL_CODE);
			break;
		case 1100:
			types.add(JUNIORMIDDLESCHOOL_CODE);
			types.add(SENIORHIGHSCHOOL_CODE);
			break;
		case 1110:
			types.add(PRIMARYSCHOOL_CODE);
			types.add(JUNIORMIDDLESCHOOL_CODE);
			types.add(SENIORHIGHSCHOOL_CODE);
			break;
		case 10000:
			types.add(UNIVERSITY_CODE);
			break;
			default:
				break;
		}
		return types.size() == 0 ? null : types;
	}

	public static void main(String[] args) {
		System.out.println("小学".matches("^幼儿园|小学|初中|高中|大学|教育局|企业$"));

		System.out.println("01222".matches("1[0|1]{0,}"));

		System.out.println(convertTypeToMap("100001111111101"));
	}
}
