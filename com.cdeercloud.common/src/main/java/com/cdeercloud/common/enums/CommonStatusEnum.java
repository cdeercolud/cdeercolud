package com.cdeercloud.common.enums;

/**
 * 公共状态表
 * 
 * @author looyii
 *
 */
public enum CommonStatusEnum {
	// 有效
	NORMAL("1"),
	// 锁定
	LOCK("2"),
	// 等待审批
	WAIT_APPROVE("3"),
	// 审批通过
	APPROVE("4"),
	// 审批不通过
	UNAPPROVE("5"),
	// 关闭
	CLOSE("6"),
	// 申请关闭
	APPLY_CLOSE("7"),
	// 审批已执行
	APPROVE_EXECUTED("8"),
	// 撤销
	CENCEL("9"),
	// 隐藏
	HIDE("h"),
	// 删除
	DELETE_D("d"),
	// 不激活
	UN_ACTIVATED("u"),
	// 不更新
	NOT_UPDATE("n"),
	// 毕业状态
	GRADUATING("g");

	private String value;

	private CommonStatusEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
