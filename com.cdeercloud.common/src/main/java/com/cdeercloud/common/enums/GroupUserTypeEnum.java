package com.cdeercloud.common.enums;

/**
 * 群组用户角色
 *
 * @Auther: looyii
 * @Date: 2018/7/21 23:00
 * @Description:
 */
public enum GroupUserTypeEnum {
    // 群创建者
    CREATOR("1"),
    // 群组管理员
    MANAGER("2"),
    // 老师
    ORDINARY("3"),
    //家长
    PARENT("4"),
    //学生
    STUDENT("5");

    private String value;

    private GroupUserTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
