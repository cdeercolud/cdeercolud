package com.cdeercloud.common.cache.template;

import redis.clients.jedis.Protocol.Command;

public class CacheCommandResult<T> {
	private String key;
	private Command command;
	private T result;

	public CacheCommandResult() {
		
	}

	public CacheCommandResult(String key, Command command, T result) {
		super();
		this.key = key;
		this.command = command;
		this.result = result;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Command getCommand() {
		return command;
	}

	public void setCommand(Command command) {
		this.command = command;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}
}
