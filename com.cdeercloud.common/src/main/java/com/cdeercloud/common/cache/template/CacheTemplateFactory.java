package com.cdeercloud.common.cache.template;

import com.cdeercloud.common.cache.template.impl.CacheClusterTemplate;
import com.cdeercloud.common.cache.template.impl.CachePoolTemplate;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.util.Pool;

/**
 * 模板静态模板工厂
 * @author looyii
 *
 */
public class CacheTemplateFactory {

	/**
	 * 单机连接池模板
	 * @author bindi.lin@meicloud.com
	 * @return
	 */
	public static CacheTemplate cachePoolTemplate(Pool<Jedis> pool) {
		return new CachePoolTemplate(pool);
	}
	
	/**
	 * 集群多主模板
	 * @author bindi.lin@meicloud.com
	 * @return
	 */
	public static CacheTemplate cacheClusterTemplate(JedisCluster cluster) {
		return new CacheClusterTemplate(cluster);
	}
}
