package com.cdeercloud.common.cache.template.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdeercloud.common.cache.template.CacheCallBack;
import com.cdeercloud.common.cache.template.CacheCommandResult;
import com.cdeercloud.common.cache.template.CacheTemplate;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.util.JsonUtils;

import redis.clients.jedis.Jedis;
import redis.clients.util.Pool;

public class CachePoolTemplate implements CacheTemplate{
	private static final Logger logger = LoggerFactory.getLogger(CachePoolTemplate.class);

	private Pool<Jedis> pool;
	
	/**
	 * 修改为适用于哨兵连接池形式。
	 * @param pool
	 */
	public CachePoolTemplate(Pool<Jedis> pool) {
		this.pool = pool;
	}

	@Override
	public <T> T execute(CacheCallBack<T> callback) {
		Jedis jedis = null;
		CacheCommandResult<T> result = null;
		boolean isSuccess = false;
		long startTime = System.currentTimeMillis();
		try {
			jedis = this.pool.getResource();
			result = callback.handle(jedis);
			isSuccess = true;
			return result.getResult();
		} catch (Exception e) {
			isSuccess = false;
			logger.error("Redis Exception:{}", e);
			throw new CdeerException(e);
		} finally {
			if (null != jedis) {
				jedis.close();
			}
			String template = "HCache execute success:{},command:{},key:{},value:{},cost:{}ms";
			logger.debug(template, isSuccess, result.getCommand(), result.getKey(), JsonUtils.toJson(result),
					System.currentTimeMillis() - startTime);
		}
	}
}
