package com.cdeercloud.common.cache.template;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

public interface CacheCallBack<T> {
	CacheCommandResult<T> handle(Jedis jedis);

	CacheCommandResult<T> handle(JedisCluster jedisCluster);
}
