package com.cdeercloud.common.cache.template;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

public class DefaultCacheCallBack<T> implements CacheCallBack<T> {

	@Override
	public CacheCommandResult<T> handle(Jedis jedis) {
		return null;
	}

	@Override
	public CacheCommandResult<T> handle(JedisCluster jedisCluster) {
		return null;
	}

}
