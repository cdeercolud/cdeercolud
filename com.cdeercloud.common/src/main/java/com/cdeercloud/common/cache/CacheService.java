package com.cdeercloud.common.cache;

import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.BinaryClient.LIST_POSITION;

public interface CacheService {

	public String set(String key, String value);

	public String set(byte[] key, byte[] value);

	public String get(String key);

	public byte[] get(byte[] key);

	public Long hset(String key, String field, String value);

	public String hget(String key, String field);

	public Long hdel(String key, String field);

	public Map<String, String> hgetAll(String key);

	public Long delete(String key);

	public String set(Class<?> clazz, Object obj, String key);

	public List<String> hgetMany(String key, String... fields);

	public String hsetMany(String key, Map<String, String> hash);

	public boolean exists(String key);

	public Long setnx(String key, String value);

	public String setex(String key, String value, int seconds);

	public Long setex(Class<?> clazz, Object obj, String key, int seconds);

	public Long expire(String key, int seconds);

	public Long ttl(String key);

	public String set(Map<String, String> redisMap, String key);

	public <T> T get(String key, Class<T> clazz);

	public String lindex(String key, int index);

	public Long linsert(String key, LIST_POSITION where, String pivot, String value);

	public Long llen(String key);

	public String lpop(String key);

	public Long lpush(String key, String... values);

	public Long lpushx(String key, String... values);

	public Long lrem(String key, long count, String value);

	public String lset(String key, long count, String value);

	public List<String> lrange(String key, long start, long end);

	public String rpop(String key);

	public Long rpush(String key, String... values);

	public Long rpushx(String key, String... values);

	public Long sadd(String key, String... members);

	public Boolean sismember(String key, String member);

	public Set<String> smembers(String key);

	public Long srem(String key, String... members);
}
