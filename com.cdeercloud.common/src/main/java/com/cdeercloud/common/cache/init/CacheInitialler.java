package com.cdeercloud.common.cache.init;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.cache.CacheRedisEnum;
import com.cdeercloud.common.cache.CacheService;
import com.cdeercloud.common.cache.impl.CacheServiceClusterImpl;
import com.cdeercloud.common.cache.impl.CacheServicePoolImpl;
import com.cdeercloud.common.cache.template.CacheTemplateFactory;
import com.cdeercloud.common.core.exception.CdeerException;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.Protocol;
import redis.clients.util.Pool;

/**
 * 初始化redis配置
 * 
 * @author looyii
 *
 */
@Configuration
public class CacheInitialler {
	@Value("${cache.redis.type}")
	private String type;

	@Value("${cache.redis.host}")
	private String host;

	@Value("${cache.redis.port}")
	private int port;

	@Value("${cache.redis.maxTotal}")
	private int maxTotal;

	@Value("${cache.redis.maxIdle}")
	private int maxIdle;

	@Value("${cache.redis.minIdle}")
	private int minIdle;

	@Value("${cache.redis.maxWaitMillis}")
	private long maxWaitMillis;

	@Value("${cache.redis.timeout}")
	private int timeout;

	@Value("${cache.redis.testOnBorrow}")
	private boolean testOnBorrow;

	@Value("${cache.redis.testWhileIdle}")
	private boolean testWhileIdle;

	@Value("${cache.redis.testOnCreate}")
	private boolean testOnCreate;

	@Value("${cache.redis.auth:#{null}}")
	private String auth;

	@Value("${cache.redis.database}")
	private int database;

	@Value("${cache.redis.sentinel.master:#{null}}")
	private String sentinelMaster;

	@Value("${cache.redis.sentinel.nodes:#{null}}")
	private String sentinelNodes;

	@Value("${cache.redis.cluster.nodes:#{null}}")
	private String clusterNodes;

	@Value("${cache.redis.cluster.maxAttempts}")
	private int maxAttempts;

	@Bean(name = "cacheService")
	public CacheService smartCacheBean() {
		CacheRedisEnum redisType = CacheRedisEnum.getTypeByCode(type);
		if (CacheRedisEnum.POOL == redisType) {
			return new CacheServicePoolImpl(CacheTemplateFactory.cachePoolTemplate(initPool()));
		} else if (CacheRedisEnum.CLUSTER == redisType) {
			return new CacheServiceClusterImpl(CacheTemplateFactory.cacheClusterTemplate(initCluster()));
		} else {
			throw new CdeerException("不支持Redis类型，请配置cache.redis.type");
		}
	}

	/**
	 * 初始化Jedis配置
	 * 兼容：集群和非集群
	 * 
	 * @author bindi.lin@meicloud.com
	 * @return
	 */
	private GenericObjectPoolConfig initJedisConf() {
		GenericObjectPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(maxTotal);
		config.setMaxIdle(maxIdle);
		config.setMinIdle(minIdle);
		config.setMaxWaitMillis(maxWaitMillis);
		config.setTestOnCreate(testOnCreate);
		config.setTestWhileIdle(testWhileIdle);
		config.setTestOnBorrow(testOnBorrow);
		return config;
	}

	/**
	 * 初始化连接池。
	 * 
	 * @author lijiawei
	 * @return
	 */
	private Pool<Jedis> initPool() {
		if (StringUtils.isEmpty(auth) || "null".equals(auth)) {
			auth = null;
		}

		Pool<Jedis> jedisPool = null;
		if (StringUtils.hasText(this.sentinelMaster) && StringUtils.hasText(this.sentinelNodes)) {
			// 初始化Redis哨兵连接池
			// 把Redis哨兵连接地址以逗号拆分
			Set<String> sentinels = StringUtils.commaDelimitedListToSet(this.sentinelNodes);
			jedisPool = new JedisSentinelPool(this.sentinelMaster, sentinels, this.initJedisConf(), this.timeout, this.auth, database);
		} else {
			// 初始化Redis连接池
			jedisPool = new JedisPool(this.initJedisConf(), this.host, this.port, this.timeout, this.auth, database);
		}

		return jedisPool;
	}

	/**
	 * 初始化连接池。
	 * 
	 * @author lijiawei
	 * @return
	 */
	private JedisCluster initCluster() {
		if (StringUtils.isEmpty(auth) || "null".equals(auth)) {
			auth = null;
		}

		String[] serverArray = this.clusterNodes.split(",");
		if (!StringUtils.hasText(this.clusterNodes) || (0 == serverArray.length)) {
			throw new CdeerException("请配置cache.redis.cluster.nodes");
		}

		if (this.database > 0) {
			throw new CdeerException("Redis集群不支持多数据库");
		}

		Set<HostAndPort> nodes = new HashSet<>();
		for (String ipPort : serverArray) {
			String[] ipPortPair = ipPort.split(":");
			nodes.add(new HostAndPort(ipPortPair[0].trim(), Integer.valueOf(ipPortPair[1].trim())));
		}
		// 集群下不能设置DB
		JedisCluster jedisCluster = new JedisCluster(nodes, this.timeout, Protocol.DEFAULT_TIMEOUT, this.maxAttempts, this.auth, initJedisConf());

		return jedisCluster;
	}

}
