package com.cdeercloud.common.cache.annotation;

/**
 * 
 * @author looyii
 *
 */
public @interface CacheProperty {

	/**
	 * 被标注的属性是否需要缓存
	 * 
	 * @return 
	 */
	public boolean exclude() default false;

	/**
	 * cache=true,keyField=false时有效，设置属性缓存的key
	 * 
	 * @return
	 */
	public String key() default "";
}
