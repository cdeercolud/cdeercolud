package com.cdeercloud.common.cache.impl;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.cache.CacheService;
import com.cdeercloud.common.cache.annotation.CacheProperty;
import com.cdeercloud.common.cache.template.CacheCommandResult;
import com.cdeercloud.common.cache.template.CacheTemplate;
import com.cdeercloud.common.cache.template.DefaultCacheCallBack;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.util.ReflectUtils;

import redis.clients.jedis.BinaryClient.LIST_POSITION;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.Protocol.Command;

/**
 * Redis集群实现
 * 
 * @author looyii
 *
 */
public class CacheServiceClusterImpl implements CacheService {

	private Logger logger = LoggerFactory.getLogger(CacheServiceClusterImpl.class);

	private CacheTemplate cacheTemplate;

	public CacheServiceClusterImpl(CacheTemplate cacheTemplate) {
		this.cacheTemplate = cacheTemplate;
	}

	@Override
	public String set(String key, String value) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.SET, jedisCluster.set(key, value));
			}
		});
	}

	@Override
	public String set(byte[] key, byte[] value) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(new String(key), Command.SET, jedisCluster.set(key, value));
			}
		});
	}

	@Override
	public String get(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.GET, jedisCluster.get(key));
			}
		});
	}

	@Override
	public byte[] get(byte[] key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<byte[]>() {
			@Override
			public CacheCommandResult<byte[]> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<byte[]>(new String(key), Command.GET, jedisCluster.get(key));
			}
		});
	}

	@Override
	public Long hset(String key, String field, String value) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.HSET, jedisCluster.hset(key, field, value));
			}
		});

	}

	@Override
	public String hget(String key, String field) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.HGET, jedisCluster.hget(key, field));
			}
		});

	}

	@Override
	public Long hdel(String key, String field) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.HGET, jedisCluster.hdel(key, field));
			}
		});

	}

	@Override
	public Map<String, String> hgetAll(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Map<String, String>>() {
			@Override
			public CacheCommandResult<Map<String, String>> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Map<String, String>>(key, Command.HGETALL, jedisCluster.hgetAll(key));
			}
		});
	}

	@Override
	public Long delete(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.DEL, jedisCluster.del(key));
			}
		});

	}

	@Override
	public String set(Class<?> clazz, Object obj, String key) {
		Map<String, String> redisMap = getFieldValueMap(clazz, obj);

		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.HMSET, jedisCluster.hmset(key, redisMap));
			}
		});

	}

	@Override
	public List<String> hgetMany(String key, String... fields) {

		return cacheTemplate.execute(new DefaultCacheCallBack<List<String>>() {
			@Override
			public CacheCommandResult<List<String>> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<List<String>>(key, Command.HMGET, jedisCluster.hmget(key, fields));
			}
		});

	}

	@Override
	public String hsetMany(String key, Map<String, String> hash) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.HMSET, jedisCluster.hmset(key, hash));
			}
		});

	}

	@Override
	public boolean exists(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Boolean>() {
			@Override
			public CacheCommandResult<Boolean> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Boolean>(key, Command.EXISTS, jedisCluster.exists(key));
			}
		});
	}

	@Override
	public Long setnx(String key, String value) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.SETNX, jedisCluster.setnx(key, value));
			}
		});

	}

	@Override
	public String setex(String key, String value, int seconds) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.SETEX, jedisCluster.setex(key, seconds, value));
			}
		});
	}

	@Override
	public Long setex(Class<?> clazz, Object obj, String key, int seconds) {
		Map<String, String> redisMap = getFieldValueMap(clazz, obj);

		cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.HMSET, jedisCluster.hmset(key, redisMap));
			}
		});

		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.EXPIRE, jedisCluster.expire(key, seconds));
			}
		});

	}

	@Override
	@Async
	public Long expire(String key, int seconds) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.EXPIRE, jedisCluster.expire(key, seconds));
			}
		});

	}

	@Override
	public Long ttl(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.TTL, jedisCluster.ttl(key));
			}
		});
	}

	@Override
	public String set(Map<String, String> redisMap, String key) {
		if ((redisMap != null) && (redisMap.size() > 0)) {
			return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
				@Override
				public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
					return new CacheCommandResult<String>(key, Command.HMSET, jedisCluster.hmset(key, redisMap));
				}
			});

		} else {
			throw new CdeerException("the saving Map is null !");
		}
	}

	@Override
	public <T> T get(String key, Class<T> clazz) {
		Map<String, String> map = this.hgetAll(key);
		if ((null != map) && !map.isEmpty()) {
			try {
				return populate(map, clazz);
			} catch (InstantiationException | IllegalAccessException e) {
				logger.error("数据转换发生异常：" + e);
				throw new CdeerException("数据转换发生异常", e);
			}
		}
		return null;
	}

	@Override
	public String lindex(String key, int index) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.LINDEX, jedisCluster.lindex(key, index));
			}
		});

	}

	@Override
	public Long linsert(String key, LIST_POSITION where, String pivot, String value) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.LINSERT, jedisCluster.linsert(key, where, pivot, value));
			}
		});

	}

	@Override
	public Long llen(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.LLEN, jedisCluster.llen(key));
			}
		});

	}

	@Override
	public String lpop(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.LPOP, jedisCluster.lpop(key));
			}
		});

	}

	@Override
	public Long lpush(String key, String... values) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.LPUSH, jedisCluster.lpush(key, values));
			}
		});

	}

	@Override
	public Long lpushx(String key, String... values) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.LPUSHX, jedisCluster.lpushx(key, values));
			}
		});

	}

	@Override
	public Long lrem(String key, long count, String value) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.LREM, jedisCluster.lrem(key, count, value));
			}
		});

	}

	@Override
	public String lset(String key, long count, String value) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.LSET, jedisCluster.lset(key, count, value));
			}
		});
	}

	@Override
	public List<String> lrange(String key, long start, long end) {
		return cacheTemplate.execute(new DefaultCacheCallBack<List<String>>() {
			@Override
			public CacheCommandResult<List<String>> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<List<String>>(key, Command.LSET, jedisCluster.lrange(key, start, end));
			}
		});
	}

	@Override
	public String rpop(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<String>() {
			@Override
			public CacheCommandResult<String> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<String>(key, Command.RPOP, jedisCluster.rpop(key));
			}
		});
	}

	@Override
	public Long rpush(String key, String... values) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.RPUSH, jedisCluster.rpush(key, values));
			}
		});
	}

	@Override
	public Long rpushx(String key, String... values) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.RPUSHX, jedisCluster.rpushx(key, values));
			}
		});
	}

	@Override
	public Long sadd(String key, String... members) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.SADD, jedisCluster.sadd(key, members));
			}
		});

	}

	@Override
	public Boolean sismember(String key, String member) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Boolean>() {
			@Override
			public CacheCommandResult<Boolean> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Boolean>(key, Command.SISMEMBER, jedisCluster.sismember(key, member));
			}
		});

	}

	@Override
	public Set<String> smembers(String key) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Set<String>>() {
			@Override
			public CacheCommandResult<Set<String>> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Set<String>>(key, Command.SMEMBERS, jedisCluster.smembers(key));
			}
		});

	}

	@Override
	public Long srem(String key, String... members) {
		return cacheTemplate.execute(new DefaultCacheCallBack<Long>() {
			@Override
			public CacheCommandResult<Long> handle(JedisCluster jedisCluster) {
				return new CacheCommandResult<Long>(key, Command.SREM, jedisCluster.srem(key, members));
			}
		});

	}

	private <T> T populate(Map<String, String> map, Class<T> clazz) throws InstantiationException, IllegalAccessException {
		T obj = clazz.newInstance();
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(clazz, Object.class);
			MethodDescriptor[] mds = beanInfo.getMethodDescriptors();
			for (MethodDescriptor md : mds) {
				String name = md.getName();
				if (!ReflectUtils.isSetter(name)) {
					continue;
				}
				Method method = md.getMethod();
				CacheProperty anno = AnnotationUtils.findAnnotation(method, CacheProperty.class);
				String annoKey = null;
				if (anno != null) {
					if (anno.exclude()) {
						continue;
					}
					if (StringUtils.hasText(anno.key())) {
						annoKey = anno.key();
					}
				}

				String key = annoKey != null ? annoKey : ReflectUtils.methodToProperty(name);
				if (!map.containsKey(key)) {
					continue;
				}
				Class<?> pClazz = method.getParameterTypes()[0];
				// TODO converter
				String value = map.get(key);
				if (StringUtils.hasText(value)) {
					method.invoke(obj, StringConverter.string2Object(value, pClazz));
				}
			}
		} catch (IllegalArgumentException e) {
			logger.error("", e);
			throw new CdeerException("", e);
		} catch (InvocationTargetException e) {
			logger.error("", e);
			throw new CdeerException("", e);
		} catch (IntrospectionException e) {
			logger.error("", e);
			throw new CdeerException("", e);
		}

		return obj;
	}

	private Map<String, String> getFieldValueMap(Class<?> clazz, Object obj) {
		Map<String, String> redisMap = new HashMap<String, String>();
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(clazz, Object.class);
			MethodDescriptor[] mds = beanInfo.getMethodDescriptors();
			for (MethodDescriptor md : mds) {
				String name = md.getName();
				if (!ReflectUtils.isGetter(name)) {
					continue;
				}
				Method method = md.getMethod();
				Class<?> rt = method.getReturnType();
				if (!rt.isPrimitive() && !rt.getName().startsWith("java")) {
					continue;
				}
				if (Collection.class.isAssignableFrom(rt) || Map.class.isAssignableFrom(rt)) {
					continue;
				}
				if (name.startsWith("$") || "serialVersionUID".equals(name) || "class".equals(name)) {
					continue;
				}
				CacheProperty anno = AnnotationUtils.findAnnotation(method, CacheProperty.class);
				String annoKey = null;
				if (anno != null) {
					if (anno.exclude()) {
						continue;
					}
					if (StringUtils.hasText(anno.key())) {
						annoKey = anno.key();
					}
				}

				String key = annoKey != null ? annoKey : ReflectUtils.methodToProperty(name);
				Object result = ReflectUtils.invokeMethod(obj, name);
				if (result != null) {
					// TODO
					redisMap.put(key, StringConverter.object2String(result));
				}
			}
		} catch (InvocationTargetException e) {
			logger.error("", e);
		} catch (IntrospectionException e) {
			logger.error("", e);
		}
		return redisMap;
	}

}
