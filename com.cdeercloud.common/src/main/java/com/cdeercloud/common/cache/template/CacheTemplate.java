package com.cdeercloud.common.cache.template;

public interface CacheTemplate {
	/**
	 * 执行命令
	 * @author bindi.lin@meicloud.com
	 * @param callback
	 * @return
	 */
	public <T> T execute(CacheCallBack<T> callback);
}
