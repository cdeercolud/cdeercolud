package com.cdeercloud.common.cache.template.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdeercloud.common.cache.template.CacheCallBack;
import com.cdeercloud.common.cache.template.CacheCommandResult;
import com.cdeercloud.common.cache.template.CacheTemplate;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.util.JsonUtils;

import redis.clients.jedis.JedisCluster;

public class CacheClusterTemplate implements CacheTemplate {

	private static final Logger logger = LoggerFactory.getLogger(CacheClusterTemplate.class);

	private JedisCluster cluster;

	public CacheClusterTemplate(JedisCluster cluster) {
		this.cluster = cluster;
	}

	@Override
	public <T> T execute(CacheCallBack<T> callback) {
		CacheCommandResult<T> result = null;
		boolean isSuccess = false;
		long startTime = System.currentTimeMillis();
		try {
			result = callback.handle(cluster);
			isSuccess = true;
			return result.getResult();
		} catch (Exception e) {
			isSuccess = false;
			logger.error("Redis Exception:{}", e);
			throw new CdeerException(e);
		} finally {
			String template = "HCache execute success:{},command:{},key:{},value:{},cost:{}ms";
			logger.debug(template, isSuccess, result.getCommand(), result.getKey(), JsonUtils.toJson(result),
					System.currentTimeMillis() - startTime);
		}
	}
}
