package com.cdeercloud.common.cache.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.cdeercloud.common.core.exception.CdeerException;

public class StringConverter {
	private static abstract class AbstraceConverter{
		public Object convert(String str){
			try {
				return this.doConvert(str);
			} catch (Throwable e) {
				throw new CdeerException("类型转换时发生错误。",e);
			}
		}
		public abstract Object doConvert(String str) throws Throwable;
	}
	private static AbstraceConverter cvInt = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			return Integer.parseInt(str);
		}
	};
	private static AbstraceConverter cvLong = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			return Long.parseLong(str);
		}
	};
	private static AbstraceConverter cvFloat = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			return Float.parseFloat(str);
		}
	};
	private static AbstraceConverter cvDouble = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			return Double.parseDouble(str);
		}
	};
	private static AbstraceConverter cvShort = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			return Short.parseShort(str);
		}
	};
	private static AbstraceConverter cvByte = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			return Byte.parseByte(str);
		}
	};
	private static AbstraceConverter cvString = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			return str;
		}
	};
	private static AbstraceConverter cvDate = new AbstraceConverter() {
		@Override
		public Object doConvert(String str) throws Throwable {
			if(str.contains(":")){
				return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
			}else if(str.contains("-")){
				return new SimpleDateFormat("yyyy-MM-dd").parse(str);
			}else if(str.matches("\\d+")){
				return new Date(Long.parseLong(str));
			}
			return null;
		}
	};
	
	
	private static Map<Class<?>,AbstraceConverter> converters = new HashMap<>();
	static{
		converters.put(int.class, cvInt);
		converters.put(Integer.class, cvInt);

		converters.put(long.class, cvLong);
		converters.put(Long.class, cvLong);

		converters.put(short.class, cvShort);
		converters.put(Short.class, cvShort);

		converters.put(byte.class, cvByte);
		converters.put(Byte.class, cvByte);

		converters.put(float.class, cvFloat);
		converters.put(Float.class, cvFloat);
		
		converters.put(double.class, cvDouble);
		converters.put(Double.class, cvDouble);
		
		converters.put(String.class, cvString);
		converters.put(Date.class, cvDate);
	}
	
	public static String object2String(Object obj) {
		return String.valueOf(obj);
	}

	public static Object string2Object(String str, Class<?> type) {
		AbstraceConverter converter = converters.get(type);
		if(converter == null){
			throw new CdeerException("无法解析类型："+type.getName());
		}
		return converter.convert(str);
	}
}
