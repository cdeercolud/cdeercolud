package com.cdeercloud.common.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * Redis缓存类型
 * @author looyii
 *
 */
public enum CacheRedisEnum {
	//连接池缓存
	POOL("pool", "连接池缓存"),
	//连接池缓存
	CLUSTER("cluster", "集群缓存");
	
	private static Map<String, CacheRedisEnum> cacheRedisTypeMap = new HashMap<>();
	
	static {
		for(CacheRedisEnum type : CacheRedisEnum.values()) {
			cacheRedisTypeMap.put(type.code, type);
		}
	}
	
	private String code;
	private String desc;
	
	private CacheRedisEnum(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	/**
	 * 根据code获取type
	 * @author bindi.lin@meicloud.com
	 * @param code
	 * @return
	 */
	public static CacheRedisEnum getTypeByCode(String code) {
		return cacheRedisTypeMap.get(code);
	}
	
}
