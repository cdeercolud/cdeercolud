package com.cdeercloud.common.core.definition;

import java.util.Properties;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;

public interface BeanDefinitionInitializer {
	void init(BeanDefinitionRegistry var1, Properties var2);
}
