package com.cdeercloud.common.core.vo;

public class PageInfo {
	private int next;
	private int prev;
	private int totalPage;// 总页数
	private int pageIndex;// 开始条数
	private int pageSize;// 每页条数

	public PageInfo(int pageIndex, int pageSize, long total) {
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
		this.totalPage = (int) (total / pageSize);
		if ((total % pageSize) != 0) {
			totalPage++;
		}
		this.next = pageIndex >= this.totalPage ? pageIndex : pageIndex + 1;
		this.prev = pageIndex < 2 ? 1 : pageIndex - 1;
	}

	public int getNext() {
		return this.next;
	}

	public int getPrev() {
		return this.prev;
	}

	public int getFirst() {
		return 1;
	}

	public int getLast() {
		return this.totalPage;
	}

	public int getTotalPage() {
		return this.totalPage;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}
}
