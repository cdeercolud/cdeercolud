package com.cdeercloud.common.core.definition;

import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.config.YamlProcessor;
import org.springframework.core.io.Resource;

public class CdeerYamlProcessor extends YamlProcessor {
	public CdeerYamlProcessor(Resource... resources) {
		this.setResources(resources);
	}

	public Properties createProperties() {
		final Properties result = new Properties();
		process(new MatchCallback() {
			@Override
			public void process(Properties properties, Map<String, Object> map) {
				result.putAll(properties);
			}
		});
		return result;
	}

}