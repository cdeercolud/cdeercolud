package com.cdeercloud.common.core.logger;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * Kafka Servers初始化
 * 
 * @author looyii
 *
 */
public class KafkaServersPropertyDefiner extends PropertyDefinerBase {

	private static String value = "";

	public static void init(String value) {
		KafkaServersPropertyDefiner.value = value;
	}

	@Override
	public String getPropertyValue() {
		return value;
	}

}
