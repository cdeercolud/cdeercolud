package com.cdeercloud.common.core.vo;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cdeercloud.common.core.exception.CdeerException;

public class PagedResult<T> {
	public final static PagedResult<Object> EMPTY;
	static{
		EMPTY = new PagedResult<Object>(Collections.emptyList(), 0, 1, 10) {
			@Override
			public void setData(List<Object> data) {
				throw new CdeerException("不允许调用EMPTY的setData()方法!");
			}
			@Override
			public void setTotal(long total) {
				throw new CdeerException("不允许调用EMPTY的setTotal()方法!");
			}
		};
	}
	private List<T> data;
	private long total;
	private PageInfo info;
	private Map<String,Object> addtions;

	public PagedResult(List<T> data, long total, int pageIndex, int pageSize) {
		this.data = data;
		this.total = total;
		this.info = new PageInfo(pageIndex, pageSize, total);
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public PageInfo getInfo() {
		return this.info;
	}
	public void putAddition(String key,Object value){
		if(this.addtions==null){
			this.addtions = new HashMap<String, Object>();
		}
		this.addtions.put(key, value);
	}
	public Map<String,Object> getAdditions(){
		if(this.addtions!=null){
			return Collections.unmodifiableMap(this.addtions);
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public final static <T> PagedResult<T> empty(){
		return (PagedResult<T>)EMPTY;
	}
}
