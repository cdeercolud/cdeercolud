package com.cdeercloud.common.core.definition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;

class BeanDefinitionInitScanner {
	static final Logger logger = LoggerFactory.getLogger(BeanDefinitionInitScanner.class);

	private static final String ENTITY_LOCATION_PATTERN = "classpath*:com/**/extensions/*.class";
	private static final ResourcePatternResolver RESOLVER = new PathMatchingResourcePatternResolver();
	private static final MetadataReaderFactory READER_FACTORY = new SimpleMetadataReaderFactory();

	public static List<Class<?>> scanBeanDefinitionInitClasses() {
		List<MetadataReader> readers = scanEntity(ENTITY_LOCATION_PATTERN);
		return scanClass(readers, BeanDefinitionInitializer.class);
	}

	private static List<MetadataReader> scanEntity(String pattern) {
		List<MetadataReader> list = new ArrayList<MetadataReader>();
		try {
			Resource[] resources = RESOLVER.getResources(ENTITY_LOCATION_PATTERN);
			for (Resource res : resources) {
				MetadataReader meta = READER_FACTORY.getMetadataReader(res);
				list.add(meta);
			}
		} catch (IOException e) {
			logger.error("读取类文件时发生I/O错误。", e);
		}
		return list;
	}

	private static List<Class<?>> scanClass(List<MetadataReader> metas, Class<?> superClazz) {
		List<Class<?>> ret = new ArrayList<Class<?>>();
		for (MetadataReader reader : metas) {
			Class<?> clazz = null;
			String name = reader.getClassMetadata().getClassName();
			try {
				clazz = Class.forName(name);
			} catch (ClassNotFoundException e) {
				logger.error("类加载不成功，找不到类文件。", e);
				continue;
			}
			if (superClazz != null) {
				if (!superClazz.isAssignableFrom(clazz) || (clazz == superClazz)) {
					continue;
				}
			}
			ret.add(clazz);
		}
		return ret;
	}
}
