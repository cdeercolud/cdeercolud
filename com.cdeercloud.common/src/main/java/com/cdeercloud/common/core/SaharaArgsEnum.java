package com.cdeercloud.common.core;

/**
 * 启动参数
 * 
 * @author looyii
 *
 */
public enum SaharaArgsEnum {

	//
	CONFIG_FILE("sahara.config.file", true, "Sahara配置文件", null),
	//
	K8S_NAMESPACE("k8s.namespace", false, "K8s部署命名空间", "defaultNameSpace"),
	//
	K8S_PODNAME("k8s.pod.name", false, "K8sPod名称", "defaultPod");

	/** 启动参数值 */
	private String key;
	/** 是否必填 */
	private boolean required;
	/** 描述 */
	private String desc;
	/** 默认值 */
	private String defVal;

	private SaharaArgsEnum(String key, boolean required, String desc, String defVal) {
		this.key = key;
		this.required = required;
		this.desc = desc;
		this.defVal = defVal;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getDefVal() {
		return defVal;
	}

	public void setDefVal(String defVal) {
		this.defVal = defVal;
	}

	public String keyArg() {
		return "--" + this.key;
	}

	public String valArg(String arg) {
		return arg.substring(this.keyArg().length() + 1);
	}

}
