package com.cdeercloud.common.core.definition;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.SaharaArgsEnum;
import com.cdeercloud.common.core.config.ConfigHolder;
import com.cdeercloud.common.core.exception.CdeerException;

/**
 * 
 * @author looyii
 *
 */
public class CdeerBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
	private static final String DEFAULT_CONFIG_YML = "classpath*:/META-INF/conf/default-config.yml";
	static final Logger logger = LoggerFactory.getLogger(CdeerBeanDefinitionRegistryPostProcessor.class);
	private ApplicationContext applicationContext;
	private SimpleCommandLinePropertySource propertySource;
	private boolean junitTest = false;

	public CdeerBeanDefinitionRegistryPostProcessor(ApplicationContext applicationContext) {
		this(applicationContext, false);
	}

	public CdeerBeanDefinitionRegistryPostProcessor(ApplicationContext applicationContext, SimpleCommandLinePropertySource propertySource) {
		this(applicationContext, propertySource, false);
	}

	public CdeerBeanDefinitionRegistryPostProcessor(ApplicationContext applicationContext, boolean junitTest) {
		this(applicationContext, null, junitTest);
	}

	private CdeerBeanDefinitionRegistryPostProcessor(ApplicationContext applicationContext, SimpleCommandLinePropertySource propertySource, boolean junitTest) {
		this.applicationContext = applicationContext;
		this.propertySource = propertySource;
		this.junitTest = junitTest;
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		List<Class<?>> clazzes = BeanDefinitionInitScanner.scanBeanDefinitionInitClasses();
		List<String> paths = this.preparedPaths();

		RootBeanDefinition factory = new RootBeanDefinition(YamlPropertiesFactoryBean.class);
		factory.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		factory.getPropertyValues().addPropertyValue("resources", paths);
		registry.registerBeanDefinition("framework_core_yaml", factory);

		RootBeanDefinition holder = new RootBeanDefinition(ConfigHolder.class);
		holder.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		RuntimeBeanReference factoryRef = new RuntimeBeanReference("framework_core_yaml");
		holder.getConstructorArgumentValues().addIndexedArgumentValue(0, factoryRef);
		registry.registerBeanDefinition("framework_core_configHolder", holder);

		List<Resource> resources = this.preparedResources(paths);
		CdeerYamlProcessor processor = new CdeerYamlProcessor(resources.toArray(new Resource[resources.size()]));
		Properties properties = processor.createProperties();
		try {
			for (Class<?> clz : clazzes) {
				BeanDefinitionInitializer def = (BeanDefinitionInitializer) clz.newInstance();
				def.init(registry, properties);
			}
		} catch (InstantiationException e) {
			logger.error("", e);
			throw new CdeerException("初始化BeanDefinitionInit发生异常。", e);
		} catch (IllegalAccessException e) {
			logger.error("", e);
			throw new CdeerException("初始化BeanDefinitionInit发生异常。", e);
		}
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

	}

	private List<Resource> preparedResources(List<String> paths) {
		List<Resource> resources = new ArrayList<Resource>();
		try {
			for (String path : paths) {
				Resource[] tmp = this.applicationContext.getResources(path);
				Collections.addAll(resources, tmp);
			}
		} catch (IOException e) {
			logger.error("", e);
		}
		return resources;
	}

	private List<String> preparedPaths() {
		List<String> paths = new ArrayList<String>();
		paths.add(DEFAULT_CONFIG_YML);

		List<String> yamlFiles = this.listYamlFiles(junitTest);
		Collections.addAll(paths, yamlFiles.toArray(new String[yamlFiles.size()]));
		return paths;
	}

	private List<String> listYamlFiles(boolean junit) {
		String[] profiles = this.applicationContext.getEnvironment().getActiveProfiles();
		List<String> list = new ArrayList<String>();
		boolean webApp = false;
		if (webApp) {
			// TODO 需要处理在WEB环境下的情况
		} else {
			if (junit) {
				if ((profiles == null) || (profiles.length == 0)) {
					list.add("classpath:sahara.yml");
				} else {
					for (String profile : profiles) {
						list.add("classpath:sahara-" + profile + ".yml");
					}
				}
			} else {
				String property = propertySource.getProperty(SaharaArgsEnum.CONFIG_FILE.getKey());
				if (!StringUtils.hasText(property)) {
					throw new CdeerException("未定配置文件(" + SaharaArgsEnum.CONFIG_FILE.keyArg() + ")");
				}
				File file = new File(property);
				if (!file.exists()) {
					throw new CdeerException("配置文件不存在：" + property);
				}
				list.add(file.toURI().toString());
				/*
				 * CodeSource codeSource = this.getClass().getProtectionDomain().getCodeSource();
				 * try {
				 * File file = new File(codeSource.getLocation().toURI().getPath());
				 * File dir = file.getParentFile();
				 * for (String profile : profiles) {
				 * File configFile = new File(dir, "smart-" + profile + ".yml");
				 * list.add(configFile.toURI().toString());
				 * }
				 * } catch (URISyntaxException e) {
				 * logger.error("",e);
				 * }
				 */
			}

		}
		return list;
	}
}