package com.cdeercloud.common.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;

import com.cdeercloud.common.core.exception.CdeerException;

public class ContextUtils {
    static final Logger logger = LoggerFactory.getLogger(ContextUtils.class);
    private static ApplicationContext context;

    public ContextUtils() {
    }

    protected static void init(ApplicationContext context) {
        context = context;
    }

    public static ApplicationContext getContext() {
        return context;
    }

    public static Object getBean(String beanId) {
        if (context == null) {
            throw new CdeerException("Spring 容器未初始化完成。");
        } else {
            return context.getBean(beanId);
        }
    }

    public static <T> List<T> getBeanByType(Class<T> type) {
        List<T> ret = new ArrayList();
        if (context != null && type != null) {
            Map<String, ? extends T> beans = BeanFactoryUtils.beansOfTypeIncludingAncestors(context, type, true, true);
            if (beans.size() == 0) {
                logger.info("No bean of type " + type.getName() + " found.");
            }

            return new ArrayList(beans.values());
        } else {
            return ret;
        }
    }
}

