package com.cdeercloud.common.core.logger;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * KafkaTopic初始化
 * 
 * @author looyii
 *
 */
public class KafkaTopicPropertyDefiner extends PropertyDefinerBase {

	private static String value = "";

	public static void init(String value) {
		KafkaTopicPropertyDefiner.value = value;
	}

	@Override
	public String getPropertyValue() {
		return value;
	}

}
