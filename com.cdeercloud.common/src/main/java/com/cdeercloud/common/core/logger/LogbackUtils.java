package com.cdeercloud.common.core.logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.yaml.snakeyaml.Yaml;

import com.cdeercloud.common.core.SaharaArgsEnum;
import com.cdeercloud.common.core.exception.CdeerException;

/**
 * 命名初始化
 * 
 * @author looyii
 *
 */
public class LogbackUtils {
	@SuppressWarnings("unchecked")
	public static void initLogbackPropertyDefiner(String[] args) {
		String appName = null;
		String dir = null;
		String level = null;
		String kafkaTopic = null;
		String kafkaServers = null;

		for (String arg : args) {
			if (arg.startsWith(SaharaArgsEnum.CONFIG_FILE.keyArg())) {
				String fileName = SaharaArgsEnum.CONFIG_FILE.valArg(arg);
				File file = new File(fileName);
				if (!file.exists()) {
					break;
				}
				Yaml yaml = new Yaml();
				FileInputStream in = null;
				try {
					in = new FileInputStream(file);
					Properties p = yaml.loadAs(in, Properties.class);
					Object obj = p.get("core");
					if ((obj != null) && (obj instanceof Map)) {
						Map<String, Object> map = (Map<String, Object>) obj;
						appName = (String) map.get("logger.logback.app.name");
						dir = (String) map.get("logger.logback.file.home");
						level = (String) map.get("logger.logback.root.level");
						kafkaTopic = (String) map.get("logger.logback.kafka.topic");
						kafkaServers = (String) map.get("logger.logback.kafka.servers");
					}
				} catch (FileNotFoundException e) {
					throw new CdeerException(e);
				} finally {
					try {
						in.close();
					} catch (IOException e) {

					}
				}
				AppNamePropertyDefiner.init(appName);
				FileDirPropertyDefiner.initTopic(dir);
				RootLevelPropertyDefiner.initLevel(level);
				KafkaTopicPropertyDefiner.init(kafkaTopic);
				KafkaServersPropertyDefiner.init(kafkaServers);
			}

			if (arg.startsWith(SaharaArgsEnum.K8S_NAMESPACE.keyArg())) {
				K8sNameSpacePropertyDefiner.init(SaharaArgsEnum.K8S_NAMESPACE.valArg(arg));
			}

			if (arg.startsWith(SaharaArgsEnum.K8S_PODNAME.keyArg())) {
				K8sPodNamePropertyDefiner.init(SaharaArgsEnum.K8S_PODNAME.valArg(arg));
			}

		}
	}

}
