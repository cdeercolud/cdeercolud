package com.cdeercloud.common.core.logger;

import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.exception.CdeerException;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * LogBack 目录参数初始化
 * 
 * @author looyii
 *
 */
public class FileDirPropertyDefiner extends PropertyDefinerBase {
	private static String dir;

	public static void initTopic(String dir) {
		FileDirPropertyDefiner.dir = dir;
	}

	@Override
	public String getPropertyValue() {
		if (!StringUtils.hasText(dir)) {
			throw new CdeerException("logback for file 未指明 dir");
		}
		return dir;
	}
}