package com.cdeercloud.common.core.logger;

import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.exception.CdeerException;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * LogBack Level 参数初始化
 * 
 * @author looyii
 *
 */
public class RootLevelPropertyDefiner extends PropertyDefinerBase {
	private static String level;

	public static void initLevel(String level) {
		RootLevelPropertyDefiner.level = level;
	}

	@Override
	public String getPropertyValue() {
		if (!StringUtils.hasText(level)) {
			throw new CdeerException("logback for file 未指明 level");
		}
		return level;
	}
}
