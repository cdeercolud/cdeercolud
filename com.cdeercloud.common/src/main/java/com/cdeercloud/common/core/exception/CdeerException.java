package com.cdeercloud.common.core.exception;

/**
 * 异常基类
 * 
 * @author looyii
 *
 */
public class CdeerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3631980985148878682L;

	/**
	 * 默认的构造方法。
	 */
	public CdeerException() {
	}

	/**
	 * 自定义的构造方法。
	 * 
	 * @param cause Throwable
	 */
	public CdeerException(Throwable cause) {
		super(cause);
	}

	/**
	 * 自定义的构造方法。
	 * 
	 * @param message String
	 */
	public CdeerException(String message) {
		super(message);
	}

	/**
	 * 自定义的构造方法。
	 * 
	 * @param message String
	 * @param cause Throwable
	 */
	public CdeerException(String message, Throwable cause) {
		super(message, cause);
	}

}
