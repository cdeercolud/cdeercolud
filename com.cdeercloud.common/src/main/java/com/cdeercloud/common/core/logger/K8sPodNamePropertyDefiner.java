package com.cdeercloud.common.core.logger;

import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.SaharaArgsEnum;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * k8sPod名称参数初始化
 * 
 * @author looyii
 *
 */
public class K8sPodNamePropertyDefiner extends PropertyDefinerBase {
	private static String k8sPodName;

	public static void init(String value) {
		K8sPodNamePropertyDefiner.k8sPodName = value;
	}

	@Override
	public String getPropertyValue() {
		if (!StringUtils.hasText(k8sPodName)) {
			k8sPodName = System.getenv(SaharaArgsEnum.K8S_PODNAME.toString());
			if (!StringUtils.hasText(k8sPodName)) {
				k8sPodName = SaharaArgsEnum.K8S_PODNAME.getDefVal();
			}
		}
		return k8sPodName;
	}

}
