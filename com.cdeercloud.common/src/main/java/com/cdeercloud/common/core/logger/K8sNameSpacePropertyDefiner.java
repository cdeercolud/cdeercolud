package com.cdeercloud.common.core.logger;

import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.SaharaArgsEnum;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * k8s命名空间参数初始化
 * 
 * @author looyii
 *
 */
public class K8sNameSpacePropertyDefiner extends PropertyDefinerBase {
	private static String k8sNamespace;

	public static void init(String value) {
		K8sNameSpacePropertyDefiner.k8sNamespace = value;
	}

	@Override
	public String getPropertyValue() {
		if (!StringUtils.hasText(k8sNamespace)) {
			k8sNamespace = System.getenv(SaharaArgsEnum.K8S_NAMESPACE.toString());
			if (!StringUtils.hasText(k8sNamespace)) {
				k8sNamespace = SaharaArgsEnum.K8S_NAMESPACE.getDefVal();
			}
		}
		return k8sNamespace;
	}

}
