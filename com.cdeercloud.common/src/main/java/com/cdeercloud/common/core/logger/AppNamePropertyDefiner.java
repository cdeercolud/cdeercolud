package com.cdeercloud.common.core.logger;

import ch.qos.logback.core.PropertyDefinerBase;

public class AppNamePropertyDefiner extends PropertyDefinerBase {

	private static String value = "";

	public static void init(String value) {
		AppNamePropertyDefiner.value = value;
	}

	@Override
	public String getPropertyValue() {
		return value;
	}

}
