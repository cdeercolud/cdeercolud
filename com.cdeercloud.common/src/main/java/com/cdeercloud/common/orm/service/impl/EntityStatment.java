package com.cdeercloud.common.orm.service.impl;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.orm.mybatis.util.MUtils;
import com.cdeercloud.common.orm.mybatis.vo.InModel;
import com.cdeercloud.common.orm.mybatis.vo.TableModel;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.vo.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实体类处理器。
 * 
 * @author looyii
 *
 * @param <T>
 */
@SuppressWarnings("unchecked")
public class EntityStatment<T> implements IEntityStatement<T>, InitializingBean {
	private static final Log log = LogFactory.getLog(EntityStatment.class);
	private AbstractSqlSessionTemplateWrapper wrapper;
	private Class<T> entityClass;
	private BeanInfo entityBeanInfo;
	private TableModel tableModel;

	public EntityStatment() {

	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.tableModel = MUtils.getTableModel(this.entityClass.getName());
		try {
			this.entityBeanInfo = Introspector.getBeanInfo(this.entityClass, Object.class);
		} catch (IntrospectionException e) {
			log.error("解析实体类时发生错误", e);
		}
	}

	@Override
	public int create(T t) {
		return this.wrapper.createByStatement("create", t);
	}

	@Override
	public int update(T t) {
		return this.update(t, true);
	}

	@Override
	public int update(T t, boolean updateNullProperty) {
		String statement = updateNullProperty ? "updateWithNull" : "updateNotNull";
		return this.wrapper.updateByStatement(statement, t);
	}

	@Override
	public int createOrUpdate(T t) {
		if (this.isEmpty(this.getEntityIdentity(t))) {
			return this.create(t);
		} else {
			return this.update(t);
		}
	}

	@Override
	public T find(Serializable id) {
		String statementId = this.getEntityClass().getName() + ".find";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("resultColunms", "t.*");
		return (T) this.wrapper.getSqlSessionTemplate().selectOne(statementId, map);
	}

	@Override
	public int delete(T entity) {
		return this.delete(this.getEntityIdentity(entity));
	}

	@Override
	public int delete(Serializable id) {
		return this.wrapper.deleteByStatement("delete", id);
	}

	@Override
	public int deleteBy(ParamMap pm) {
		if ((pm == null) || pm.isEmpty()) {
			// TODO 开启全局配置方式决定约束行为
			throw new IllegalArgumentException("条件删除时至少需要提供一个条件");
		}
		Map<String, Object> map = this.prepareParameter(pm);
		return this.wrapper.deleteByStatement("deleteByMap", map);
	}

	@Override
	public int deleteBy(String property, Object value) {
		ParamMap pm = new ParamMap();
		pm.add(property, value);
		return this.deleteBy(pm);
	}

	@Override
	public int deleteAll(List<T> list) {
		if ((list == null) || list.isEmpty()) {
			return 0;
		}
		List<Serializable> ids = new ArrayList<Serializable>();
		for (T t : list) {
			Serializable entityIdentity = this.getEntityIdentity(t);
			ids.add(entityIdentity);
		}
		return this.deleteByIdentities(ids);
	}

	// TODO 处理超过1000条数据的情况
	@Override
	public int deleteByIdentities(List<? extends Serializable> identities) {
		String property = this.tableModel.getPk().getProperty();
		return this.deleteBy(property, identities);
	}

	@Override
	public List<T> findBy(String property, Object value, Order... orders) {
		return this.findBy(property, value, -1, -1, orders);
	}

	@Override
	public List<T> findBy(String property, Object value, int pageIndex, int pageSize, Order... orders) {
		ParamMap pm = new ParamMap();
		pm.add(property, value);
		return this.findBy(pm, pageIndex, pageSize, orders);
	}

	@Override
	public List<T> findBy(ParamMap pm, Order... orders) {
		return this.findBy(pm, null, orders);
	}

    @Override
    public List<T> findBy(ParamMap pm, Results rcs, Order... orders) {
        return this.findBy(pm, -1, -1, rcs, orders);
    }

	public List<T> findBy(ParamMap pm, int pageIndex, int pageSize, Results rcs, Order... orders) {
		Map<String, Object> map = this.prepareParameter(pm, rcs, orders);
		String id = this.getEntityClass().getName() + ".selectByMap";
		return this.wrapper.findByStatement(id, map, pageIndex, pageSize);
	}

	@Override
	public List<T> findBy(ParamMap pm, int pageIndex, int pageSize, Order... orders) {
		return this.findBy(pm, pageIndex, pageSize, null, orders);
	}

	@Override
	public List<T> findByIdentities(List<Object> identities, Order... orders) {
		String pkProperty = this.tableModel.getPk().getProperty();
		return this.findBy(pkProperty, identities, orders);
	}

	@Override
	public T findFirstBy(String name, Object value, Order... orders) {
		List<T> list = this.findBy(name, value, orders);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public T findFirstBy(ParamMap pm, Order... orders) {
		List<T> list = this.findBy(pm, orders);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<T> list(Order... orders) {
		return this.findBy(new ParamMap(), orders);
	}

	@Override
	public List<T> list(Results rcs, Order... orders) {
		return this.findBy(new ParamMap(), rcs, orders);
	}

	@Override
	public List<T> list(int pageIndex, int pageSize, Order... orders) {
		return this.findBy(new ParamMap(), pageIndex, pageSize, orders);
	}

	@Override
	public int count(ParamMap pm) {
		Map<String, Object> map = this.prepareParameter(pm);
		return this.wrapper.countExt("selectByMap", map);
	}

	@Override
	public int count() {
		return this.count(new ParamMap());
	}

	@Override
	public int count(String property, Object value) {
		ParamMap pm = new ParamMap();
		pm.add(property, value);
		return this.count(pm);
	}

	@Override
	public void batchCreate(List<?> list) {
		this.batchCreate(list, 1000);
	}

	@Override
	public void batchCreate(final List<?> list, final int frequency) {
		this.wrapper.batchCreateByStatement("create", list, frequency);
	}

	@Override
	public void batchUpdate(List<?> list) {
		this.batchUpdate(list, 1000);
	}

	@Override
	public void batchUpdate(List<?> list, int frequency) {
		this.wrapper.batchUpdateByStatement("updateWithNull", list, frequency);
	}

	@Override
	public void batchUpdate(List<?> list, int frequency, boolean updateNullProperty) {
		String statement = updateNullProperty ? "updateWithNull" : "updateNotNull";
		this.wrapper.batchUpdateByStatement(statement, list, frequency);
	}

	@Override
	public PagedResult<T> pagedQuery(ParamMap pm, int pageIndex, int pageSize, Order... orders) {
		Map<String, Object> parameter = this.prepareParameter(pm, orders);
		return this.wrapper.pagedQueryByStatement("selectByMap", parameter, pageIndex, pageSize);
	}

	// API(check exist)----------------------------------------------
	@Override
	public boolean checkExist(String prop, Object value, String filterIdentify) {
		ParamMap pm = new ParamMap();
		pm.eqIgnoreCase(prop, value);
		if (filterIdentify != null) {
			pm.ne(this.tableModel.getPk().getProperty(), filterIdentify);
		}
		return this.checkExist(pm);
	}

	@Override
	public boolean checkExist(String prop, Object value) {
		return this.checkExist(prop, value, null);
	}

	@Override
	public boolean checkExist(ParamMap pm) {
		return this.count(pm) > 0;
	}

	// utils-----------------------------------------------------

	public RowBounds getRowBounds(int pageIndex, int pageSize) {
		if (pageSize == -1) {
			return RowBounds.DEFAULT;
		}
		if (pageIndex <= 0) {
			pageIndex = 1;
		}
		int offset = (pageIndex - 1) * pageSize;
		return new RowBounds(offset, pageSize);
	}

	private Map<String, Object> prepareParameter(ParamMap pm, Order... orders) {
		return this.prepareParameter(pm, null, orders);
	}

	private Map<String, Object> prepareParameter(ParamMap pm, Results rcs, Order... orders) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<InModel> ins = new ArrayList<InModel>();
		List<Param> params = new ArrayList<Param>();
		Map<String, String> pcm = MUtils.getPCMapping(this.entityClass.getName());
		List<Param> parames = pm.list();
		for (Param p : parames) {
			if (p.isLogicalCalculus()) {
				params.add(p);
				continue;
			}
			Operator op = p.getOperator();
			String property = p.getProperty();
			String column = pcm.get(property);
			if (!StringUtils.hasText(column)) {
				throw new IllegalArgumentException("未能找到与(" + property + ")属性相对应的列");
			}
			p.setColumn(column);
			if ((op == Operator.IN) || (op == Operator.NOT_IN)) {
				boolean notIn = (op == Operator.NOT_IN);
				ins.add(new InModel(column, p.getValue(), notIn));
			} else {
				params.add(p);
			}
		}
		map.put("params", params);
		if (!ins.isEmpty()) {
			map.put("ins", ins);
		}

		if ((orders != null) && (orders.length > 0)) {
			for (Order order : orders) {
				if (!StringUtils.hasText(order.getColumn())) {
					String column = pcm.get(order.getProperty());
					order.setColumn(column);
				}
			}
			map.put("orderbys", orders);
		}

		String resultColunm = "t.*";
		if (rcs != null) {
			List<String> columns = rcs.getProperties();
			StringBuffer sb = new StringBuffer();
			for (String c : columns) {
				String col = pcm.get(c);
				sb.append("t.").append(col).append(",");
			}
			sb.deleteCharAt(sb.length() - 1);
			resultColunm = sb.toString();
		}
		map.put("resultColunms", resultColunm);
		return map;
	}

	private Serializable getEntityIdentity(T t) {
		String property = this.tableModel.getPk().getProperty();
		try {
			Map<String, PropertyDescriptor> pds = this.parsePropertyDescriptors(t);
			return (Serializable) pds.get(property).getReadMethod().invoke(t);
		} catch (IllegalArgumentException e) {
			log.error("非法参数", e);
		} catch (IntrospectionException e) {
			log.error("", e);
		} catch (IllegalAccessException e) {
			log.error("非法访问", e);
		} catch (InvocationTargetException e) {
			log.error("调用目标发生错误", e);
		}
		return null;
	}

	private boolean isEmpty(Serializable id) {
		return (id == null) || "".equals(id.toString().trim());
	}

	private HashMap<String, PropertyDescriptor> parsePropertyDescriptors(T object) throws IntrospectionException {
		PropertyDescriptor[] pds = entityBeanInfo.getPropertyDescriptors();
		HashMap<String, PropertyDescriptor> pdMap = new HashMap<String, PropertyDescriptor>();
		for (PropertyDescriptor pd : pds) {
			pdMap.put(pd.getName(), pd);
		}
		return pdMap;
	}

	protected SqlSessionTemplate getSqlSessionTemplate() {
		return this.wrapper.getSqlSessionTemplate();
	}

	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.wrapper = new AbstractSqlSessionTemplateWrapper(sqlSessionTemplate) {
			@Override
			protected String getStatementId(String key) {
				// TODO 添加检测是否实体类环节
				String statement = null;
				if ((key != null) && (key.indexOf(".") > -1)) {
					statement = key;
				} else {
					String clsName = EntityStatment.this.getEntityClass().getName();
					statement = clsName + "." + key;
				}
				this.checkStatementExist(statement);
				return statement;
			}
		};
	}

	// -----------------------------------------------------------------

	public Class<T> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
}
