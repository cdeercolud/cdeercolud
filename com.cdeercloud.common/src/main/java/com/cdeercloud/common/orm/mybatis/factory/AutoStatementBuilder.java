package com.cdeercloud.common.orm.mybatis.factory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.builder.BuilderException;
import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.apache.ibatis.builder.xml.XMLStatementBuilder;
import org.apache.ibatis.executor.keygen.Jdbc3KeyGenerator;
import org.apache.ibatis.executor.keygen.KeyGenerator;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.executor.keygen.SelectKeyGenerator;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.scripting.xmltags.ChooseSqlNode;
import org.apache.ibatis.scripting.xmltags.DynamicSqlSource;
import org.apache.ibatis.scripting.xmltags.ForEachSqlNode;
import org.apache.ibatis.scripting.xmltags.IfSqlNode;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SetSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;
import org.apache.ibatis.scripting.xmltags.WhereSqlNode;
import org.apache.ibatis.scripting.xmltags.XMLScriptBuilder;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.JdbcType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import com.cdeercloud.common.orm.Constants;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.mybatis.vo.ColumnModel;
import com.cdeercloud.common.orm.mybatis.vo.TableModel;
import com.cdeercloud.common.orm.mybatis.vo.TransientField;
import com.cdeercloud.common.orm.strategy.NamingStrategy;
import com.cdeercloud.common.orm.strategy.impl.UUIDHexGenerator;

/**
 * 
 * @author looyii
 *
 */
public class AutoStatementBuilder {
	static final Logger logger = LoggerFactory.getLogger(AutoStatementBuilder.class);
	private Configuration config;
	private List<TableModel> tables;

	private MapperBuilderAssistant builderAssistant;
	private static Map<Class<?>, String> jdbcTypeMap = new HashMap<Class<?>, String>();
	static {
		jdbcTypeMap.put(String.class, "VARCHAR");
		jdbcTypeMap.put(java.util.Date.class, "TIMESTAMP");
		jdbcTypeMap.put(java.sql.Date.class, "DATE");
		jdbcTypeMap.put(java.sql.Timestamp.class, "TIMESTAMP");
		jdbcTypeMap.put(java.sql.Time.class, "TIME");
		jdbcTypeMap.put(Number.class, "NUMERIC");
		jdbcTypeMap.put(int.class, "NUMERIC");
		jdbcTypeMap.put(Integer.class, "NUMERIC");
		jdbcTypeMap.put(long.class, "NUMERIC");
		jdbcTypeMap.put(Long.class, "NUMERIC");
		jdbcTypeMap.put(float.class, "NUMERIC");
		jdbcTypeMap.put(Float.class, "NUMERIC");
		jdbcTypeMap.put(double.class, "NUMERIC");
		jdbcTypeMap.put(Double.class, "NUMERIC");
		jdbcTypeMap.put(short.class, "NUMERIC");
		jdbcTypeMap.put(Short.class, "NUMERIC");
		jdbcTypeMap.put(BigDecimal.class, "NUMERIC");
	}

	public AutoStatementBuilder(Configuration config, List<TableModel> tables) {
		this.config = config;
		this.tables = tables;
	}

	public void built() {
		builderAssistant = new MapperBuilderAssistant(config, "mdp-auto-builder");
		Collection<XMLStatementBuilder> incompletes = config.getIncompleteStatements();
		Collection<XMLStatementBuilder> dest = new ArrayList<XMLStatementBuilder>();
		for (XMLStatementBuilder builder : incompletes) {
			dest.add(builder);
		}
		config.getIncompleteStatements().clear();

		for (TableModel table : this.tables) {
			List<ResultMapping> rms = new ArrayList<ResultMapping>();
			List<ColumnModel> cols = table.getAllCols();
			for (ColumnModel col : cols) {
				String property = col.getProperty();
				String column = col.getColumn();
				Class<?> javaType = col.getJavaType();
				ResultMapping.Builder builder = new ResultMapping.Builder(config, property, column, javaType);
				rms.add(builder.build());
			}
			List<TransientField> transientFields = table.getTransientFields();
			for (TransientField tf : transientFields) {
				Class<?> javaType = tf.getFieldType();
				String property = tf.getFieldName();
				String column = NamingStrategy.DEFAULT.propertyToColumnName(property);
				ResultMapping.Builder builder = new ResultMapping.Builder(config, property, column, javaType);
				rms.add(builder.build());
			}
			Class<?> clazz = table.getCls();
			ResultMap.Builder b = new ResultMap.Builder(config, this.getResultMapName(clazz), clazz, rms);
			ResultMap rm = b.build();
			config.addResultMap(rm);
		}
		for (XMLStatementBuilder builder : dest) {
			builder.parseStatementNode();
		}
		this.preparedSqlFragment(config);
		List<SqlNode> nodes = findIncludeNode(config, Constants.COMMON_SELECT_CONDITION);
		IfSqlNode inNode = getInNode(config);
		nodes.add(inNode);
		MixedSqlNode includeNode = new MixedSqlNode(nodes);

		this.preparedSqlFragmentNoAlias(config);
		List<SqlNode> nodes2 = findIncludeNode(config, Constants.COMMON_SELECT_CONDITION_NOALIAS);
		IfSqlNode inNode2 = getInNode(config);
		nodes2.add(inNode2);
		MixedSqlNode includeNode2 = new MixedSqlNode(nodes2);

		for (TableModel table : tables) {
			Class<?> cls = table.getCls();
			ResultMap rm = config.getResultMap(this.getResultMapName(cls));
			List<ResultMap> rms = new ArrayList<ResultMap>();
			rms.add(rm);

			this.preparedFindStatement(config, table, cls, rms);
			this.preparedInsertStatement(config, table, cls);
			this.preparedUpdateStatement(config, table, cls, false);
			this.preparedUpdateStatement(config, table, cls, true);
			this.preparedDeleteStatement(config, table, cls);
			this.preparedQueryStatement(config, table, cls, rms, includeNode, false);
			this.preparedDeleteByStatement(config, table, cls, rms, includeNode2);
		}
	}

	@SuppressWarnings("unchecked")
	protected List<SqlNode> findIncludeNode(Configuration config, String sqlId) {
		XNode node = config.getSqlFragments().get(sqlId);
		XMLScriptBuilder tmp = new XMLScriptBuilder(config, null, null);
		Method m = ReflectionUtils.findMethod(tmp.getClass(), "parseDynamicTags", XNode.class);
		m.setAccessible(true);
		Object ret = null;
		try {
			ret = m.invoke(tmp, node);
		} catch (IllegalAccessException e) {
			logger.error("", e);
		} catch (IllegalArgumentException e) {
			logger.error("", e);
		} catch (InvocationTargetException e) {
			logger.error("", e);
		}
		return (List<SqlNode>) ret;
	}

	protected IfSqlNode getInNode(Configuration config) {
		ArrayList<SqlNode> nn = new ArrayList<SqlNode>();
		nn.add(new TextSqlNode("${item.property} ${item.inFlag} "));
		nn.add(new ForEachSqlNode(config, new TextSqlNode("#{itemx}"), "item.values", "indexx", "itemx", "(", ")", ","));
		MixedSqlNode m = new MixedSqlNode(nn);
		ForEachSqlNode fe1 = new ForEachSqlNode(config, m, "ins", "index", "item", " and ", "", " and ");

		IfSqlNode inNode = new IfSqlNode(fe1, "ins!=null");
		return inNode;
	}

	private void preparedFindStatement(Configuration config, TableModel table, Class<?> cls, List<ResultMap> rms) {
		String sql = "select ${resultColunms} from " + table.getTableName() + " t where t." + table.getPk().getColumn() + "= #{id}";
		TextSqlNode sqlNode = new TextSqlNode(sql);
		SqlSource sqlSource = new DynamicSqlSource(config, sqlNode);
		String id = cls.getName() + ".find";
		MappedStatement.Builder builder = new MappedStatement.Builder(config, id, sqlSource, SqlCommandType.SELECT);
		builder.resultMaps(rms);
		MappedStatement ms = builder.build();
		config.addMappedStatement(ms);
	}

	private void preparedInsertStatement(Configuration config, TableModel table, Class<?> cls) {
		String skid = null;
		if (CdeerGenerationType.SEQUENCE == table.getStrategy()) {
			String id = "create" + SelectKeyGenerator.SELECT_KEY_SUFFIX;
			String resultType = "long";
			Class<?> resultTypeClass = resolveClass(resultType);
			StatementType statementType = StatementType.valueOf(StatementType.PREPARED.toString());

			String keyProperty = table.getPk().getProperty();
			String parameterType = null;// cls.getName();
			String databaseId = null;
			boolean executeBefore = true;
			Class<?> parameterTypeClass = resolveClass(parameterType);

			boolean useCache = false;
			KeyGenerator keyGenerator = new NoKeyGenerator();
			Integer fetchSize = null;
			Integer timeout = null;
			boolean flushCache = false;
			String parameterMap = null;
			String resultMap = null;
			ResultSetType resultSetTypeEnum = null;

			List<SqlNode> contents = new ArrayList<SqlNode>();
			contents.add(new TextSqlNode("SELECT " + table.getGenerator() + ".nextVal from dual"));
			MixedSqlNode rootSqlNode = new MixedSqlNode(contents);
			SqlSource sqlSource = new DynamicSqlSource(config, rootSqlNode);
			SqlCommandType sqlCommandType = SqlCommandType.SELECT;
			builderAssistant = new MapperBuilderAssistant(config, "mdp-auto-builder");
			builderAssistant.setCurrentNamespace(cls.getName());
			Class<?> langClass = config.getLanguageRegistry().getDefaultDriverClass();
			LanguageDriver lang = config.getLanguageRegistry().getDriver(langClass);
			builderAssistant.addMappedStatement(id, sqlSource, statementType, sqlCommandType, fetchSize, timeout, parameterMap, parameterTypeClass, resultMap, resultTypeClass, resultSetTypeEnum, flushCache, useCache, false, keyGenerator, keyProperty, null, databaseId, lang);
			id = builderAssistant.applyCurrentNamespace(id, false);
			skid = id;
			MappedStatement keyStatement = config.getMappedStatement(id, false);
			config.addKeyGenerator(id, new SelectKeyGenerator(keyStatement, executeBefore));
		}

		StringBuffer sb = new StringBuffer();
		sb.append("insert into ");
		sb.append(table.getTableName());
		sb.append(" ( ");
		List<ColumnModel> allCols = table.getAllCols();
		for (ColumnModel col : allCols) {
			sb.append(col.getColumn()).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(" ) ");
		sb.append(" values( ");
		for (ColumnModel col : allCols) {
			String jdbcType = this.findJdbcType(col.getJdbcType(), col.getJavaType());
			sb.append("#{").append(col.getProperty()).append(jdbcType).append("},");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(" ) ");
		TextSqlNode sqlNode = new TextSqlNode(sb.toString());

		SqlSource sqlSource = new DynamicSqlSource(config, sqlNode);
		String id = cls.getName() + ".create";
		MappedStatement.Builder builder = new MappedStatement.Builder(config, id, sqlSource, SqlCommandType.INSERT);
		if (CdeerGenerationType.SEQUENCE == table.getStrategy()) {
			builder.keyGenerator(config.getKeyGenerator(skid));
		} else if (CdeerGenerationType.UUID == table.getStrategy()) {
			builder.keyGenerator(new UUIDHexGenerator(table.getPk().getProperty()));
		} else if (CdeerGenerationType.AUTO == table.getStrategy()) {
			builder.keyGenerator(new Jdbc3KeyGenerator());
			builder.keyProperty(table.getPk().getProperty());
		}
		MappedStatement ms = builder.build();
		config.addMappedStatement(ms);
	}

	private void preparedUpdateStatement(Configuration config, TableModel table, Class<?> cls, boolean onlyUpdateNotNullField) {
		SqlNode sqlNode = null;
		List<SqlNode> nodes = new ArrayList<SqlNode>();
		nodes.add(new TextSqlNode("update " + table.getTableName() + " t"));
		List<SqlNode> setList = new ArrayList<SqlNode>();
		// TODO 需要处理空字符串问题
		if (onlyUpdateNotNullField) {
			for (ColumnModel col : table.getCols()) {
				String prop = col.getProperty();
				String test = prop + "!=null ";
				TextSqlNode contents = new TextSqlNode("t." + col.getColumn() + "=#{" + prop + "},");
				setList.add(new IfSqlNode(contents, test));
			}
		} else {
			for (ColumnModel col : table.getCols()) {
				String prop = col.getProperty();
				String test = prop + "!=null ";
				TextSqlNode contents = new TextSqlNode("t." + col.getColumn() + "=#{" + col.getProperty() + "},");
				IfSqlNode whenNode = new IfSqlNode(contents, test);
				String test2 = prop + "==null ";
				TextSqlNode contents2 = new TextSqlNode("t." + col.getColumn() + "=null,");
				IfSqlNode otherwiseNode = new IfSqlNode(contents2, test2);
				List<SqlNode> whens = new ArrayList<SqlNode>();
				whens.add(whenNode);
				setList.add(new ChooseSqlNode(whens, otherwiseNode));
			}
		}

		MixedSqlNode ifNodes = new MixedSqlNode(setList);
		nodes.add(new SetSqlNode(config, ifNodes));
		nodes.add(new TextSqlNode("where t." + table.getPk().getColumn() + "=#{" + table.getPk().getProperty() + "}"));
		sqlNode = new MixedSqlNode(nodes);
		DynamicSqlSource sqlSource = new DynamicSqlSource(config, sqlNode);
		String id = cls.getName() + (onlyUpdateNotNullField ? ".updateNotNull" : ".updateWithNull");
		MappedStatement.Builder builder = new MappedStatement.Builder(config, id, sqlSource, SqlCommandType.UPDATE);
		MappedStatement ms = builder.build();
		config.addMappedStatement(ms);
	}

	private void preparedDeleteStatement(Configuration config, TableModel table, Class<?> cls) {
		String sql = "delete from " + table.getTableName() + "  where " + table.getPk().getColumn() + "= #{id}";
		TextSqlNode sqlNode = new TextSqlNode(sql);
		SqlSource sqlSource = new DynamicSqlSource(config, sqlNode);
		String id = cls.getName() + ".delete";
		MappedStatement.Builder builder = new MappedStatement.Builder(config, id, sqlSource, SqlCommandType.DELETE);
		MappedStatement ms = builder.build();
		config.addMappedStatement(ms);
	}

	private void preparedDeleteByStatement(Configuration config, TableModel table, Class<?> cls, List<ResultMap> rms, MixedSqlNode includeNode) {
		List<SqlNode> nodes = new ArrayList<SqlNode>();
		TextSqlNode sqlNode = new TextSqlNode("delete  from " + table.getTableName() + " ");
		WhereSqlNode whereNode = new WhereSqlNode(config, includeNode);
		nodes.add(sqlNode);
		nodes.add(whereNode);
		MixedSqlNode mixed = new MixedSqlNode(nodes);
		SqlSource sqlSource = new DynamicSqlSource(config, mixed);
		String id = cls.getName() + ".deleteByMap";
		MappedStatement.Builder builder = new MappedStatement.Builder(config, id, sqlSource, SqlCommandType.DELETE);
		MappedStatement ms = builder.build();
		config.addMappedStatement(ms);
	}

	protected String findJdbcType(JdbcType jdbcType, Class<?> cls) {
		String str = null;
		if (jdbcType != null) {
			str = jdbcType.name();
		} else {
			str = jdbcTypeMap.get(cls);
		}
		if (str != null) {
			return ",jdbcType=" + str;
		}
		return "";
	}

	private void preparedQueryStatement(Configuration config, TableModel table, Class<?> cls, List<ResultMap> rms, MixedSqlNode includeNode, boolean count) {
		String rs = count ? "count(*)" : "${resultColunms}";
		TextSqlNode sqlNode = new TextSqlNode("select " + rs + " from " + table.getTableName() + " t ");

		WhereSqlNode whereNode = new WhereSqlNode(config, includeNode);

		TextSqlNode orderNode = new TextSqlNode(" t.${item.column} ${item.order} ");

		List<SqlNode> contents = new ArrayList<SqlNode>();
		contents.add(sqlNode);
		contents.add(whereNode);
		if (!count) {
			ForEachSqlNode forEachNode = new ForEachSqlNode(config, orderNode, "orderbys", "index", "item", "ORDER BY", "", ",");
			IfSqlNode ifSqlNode = new IfSqlNode(forEachNode, "orderbys!=null");
			contents.add(ifSqlNode);
		}

		MixedSqlNode mixed = new MixedSqlNode(contents);
		SqlSource sqlSource = new DynamicSqlSource(config, mixed);
		String id = cls.getName() + (count ? ".countByMap" : ".selectByMap");
		MappedStatement.Builder builder = new MappedStatement.Builder(config, id, sqlSource, SqlCommandType.SELECT);
		if (!count) {
			builder.resultMaps(rms);
		} else {
			ResultMap.Builder inlineResultMapBuilder = new ResultMap.Builder(config, builder.id() + "-Inline", Long.class, new ArrayList<ResultMapping>());
			ArrayList<ResultMap> rms2 = new ArrayList<ResultMap>();

			rms2.add(inlineResultMapBuilder.build());
			builder.resultMaps(rms2);
		}
		MappedStatement ms = builder.build();
		config.addMappedStatement(ms);
	}

	protected void preparedSqlFragment(Configuration config) {
		StringBuffer sb = new StringBuffer();
		sb.append("<sql id=\"").append(Constants.COMMON_SELECT_CONDITION).append("\">");
		sb.append("<foreach item=\"item\" collection=\"params\">");
		sb.append(" ${item.logic} ${item.columnx} ${item.operator} <if test=\"item.valuex!=null\">#{item.valuex}</if>");
		sb.append("</foreach>");
		sb.append("</sql>");
		XPathParser parser = new XPathParser(sb.toString());
		XNode node = parser.evalNode("/sql");
		config.getSqlFragments().put(Constants.COMMON_SELECT_CONDITION, node);
	}

	protected void preparedSqlFragmentNoAlias(Configuration config) {
		StringBuffer sb = new StringBuffer();
		sb.append("<sql id=\"").append(Constants.COMMON_SELECT_CONDITION_NOALIAS).append("\">");
		sb.append("<foreach item=\"item\" collection=\"params\">");
		sb.append(" ${item.logic} ${item.columnNoAlias} ${item.operator} <if test=\"item.valuex!=null\">#{item.valuex}</if>");
		sb.append("</foreach>");
		sb.append("</sql>");
		XPathParser parser = new XPathParser(sb.toString());
		XNode node = parser.evalNode("/sql");
		config.getSqlFragments().put(Constants.COMMON_SELECT_CONDITION_NOALIAS, node);
	}

	private String getResultMapName(Class<?> clazz) {
		return clazz.getSimpleName() + ".EntityResultMap";
	}

	protected Class<?> resolveClass(String alias) {
		if (alias == null) {
			return null;
		}
		try {
			return config.getTypeAliasRegistry().resolveAlias(alias);
		} catch (Exception e) {
			throw new BuilderException("Error resolving class . Cause: " + e, e);
		}
	}

}
