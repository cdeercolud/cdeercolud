package com.cdeercloud.common.orm.service;

import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.orm.vo.Order;
import com.cdeercloud.common.orm.vo.ParamMap;
import com.cdeercloud.common.orm.vo.Results;

import java.io.Serializable;
import java.util.List;

public interface IEntityStatement<T> {
	public T find(Serializable id);

	public int create(T t);

	public int update(T t);

    /**
     * @param t
     * @param updateNullProperty true 修改为空的， false 修改不为空的属性
     * @return
     */
    public int update(T t, boolean updateNullProperty);

	public int createOrUpdate(T t);

	@Deprecated
	public int delete(T entity);

	@Deprecated
	public int delete(Serializable id);

	@Deprecated
	public int deleteBy(ParamMap pm);

	@Deprecated
	public int deleteBy(String property, Object value);

	@Deprecated
	public int deleteAll(List<T> list);

	@Deprecated
	public int deleteByIdentities(List<? extends Serializable> identities);

	/**
	 * 
	 * @param property 字段属性
	 * @param value 字段值
	 * @param orders 排序
	 * @return
	 */
	public List<T> findBy(String property, Object value, Order... orders);

	/**
	 * 
	 * @param property 字段属性
	 * @param value 字段值
	 * @param pageIndex 开始条数
	 * @param pageSize 查询条数
	 * @param orders 排序
	 * @return
	 */
	public List<T> findBy(String property, Object value, int pageIndex, int pageSize, Order... orders);

	/**
	 * 查询
	 * 
	 * @param pm 参数
	 * @param orders 排序
	 * @return
	 */
	public List<T> findBy(ParamMap pm, Order... orders);

	/**
	 * 查询指定记录
	 * 
	 * @param pm 参数
	 * @param pageIndex 开始条数
	 * @param pageSize 查询条数
	 * @param orders 排序
	 * @return
	 */
	public List<T> findBy(ParamMap pm, int pageIndex, int pageSize, Order... orders);

	/**
	 * 通过ids查询
	 * 
	 * @param identities ids
	 * @param orders 排序
	 * @return
	 */
	public List<T> findByIdentities(List<Object> identities, Order... orders);

	/**
     * 查询第一天记录
     *
     * @param property 字段属性
	 * @param value 字段值
	 * @param orders 排序
	 * @return
	 */
	public T findFirstBy(String property, Object value, Order... orders);

	/**
	 * 查询第一条记录
	 * 
	 * @param pm 参数
	 * @param orders 排序
	 * @return
	 */
	public T findFirstBy(ParamMap pm, Order... orders);

	/**
	 * 查询
	 * 
	 * @param orders
	 * @return
	 */
	public List<T> list(Order... orders);

	/**
	 * 查询指定字段记录
	 * 
	 * @param rcs 指定的查询字段
	 * @param orders 排序
	 * @return
	 */
	public List<T> list(Results rcs, Order... orders);

    /**
     * 查询指定字段记录
     *
     * @param pm     查询条件
     * @param rcs    指定的查询字段
     * @param orders 排序
     * @return
     */
    public List<T> findBy(ParamMap pm, Results rcs, Order... orders);

	/**
	 * 查询制定记录
	 * 
	 * @param pageIndex 开始条数
	 * @param pageSize 查询条数
	 * @param orders 排序
	 * @return
	 */
	public List<T> list(int pageIndex, int pageSize, Order... orders);

	/**
	 * 查询条数
	 * 
	 * @return
	 */
	public int count();

	/**
	 * 查询条数
	 * 
	 * @param pm
	 * @return
	 */
	public int count(ParamMap pm);

	/**
	 * 查询条数
	 * 
	 * @param property 字段属性
	 * @param value
	 * @return
	 */
	public int count(String property, Object value);

	/**
	 * 批量创建
	 * 
	 * @param list 创建对象
	 */
	public void batchCreate(List<?> list);

	/**
	 * 批量创建
	 * 
	 * @param list 创建对象
	 * @param frequency 每次创建多少条
	 */
	public void batchCreate(List<?> list, int frequency);

	/**
	 * 批量更新
	 * 
	 * @param list 更新对象
	 */
	public void batchUpdate(List<?> list);

	/**
	 * 批量更新
	 * 
	 * @param list 更新对象
	 * @param frequency 每次更新多少条
	 */
	public void batchUpdate(List<?> list, int frequency);

	/**
	 * 批量更新
	 * 
	 * @param list 更新对象
	 * @param frequency 每次更新多少条
	 * @param updateNullProperty 是否更新为空参数 true：是，false:否
	 */
	public void batchUpdate(List<?> list, int frequency, boolean updateNullProperty);

	/**
	 * 分页查询
	 * 
	 * @param pm 参数
	 * @param pageIndex 开始条数
	 * @param pageSize 查多少条
	 * @param orders 排序
	 * @return
	 */
	public PagedResult<T> pagedQuery(ParamMap pm, int pageIndex, int pageSize, Order... orders);

	/**
	 * 检查是否存在
	 * 
	 * @param property 字段属性
	 * @param value 字段值
	 * @param filterIdentify 字段!=某个值
	 * @return
	 */
	public boolean checkExist(String property, Object value, String filterIdentify);

	/**
	 * 检查是否存在
	 * 
	 * @param property 字段属性
	 * @param value 字段值
	 * @return
	 */
	public boolean checkExist(String property, Object value);

	/**
	 * 检查是否存在
	 * 
	 * @param pm 参数
	 * @return
	 */
	public boolean checkExist(ParamMap pm);

}
