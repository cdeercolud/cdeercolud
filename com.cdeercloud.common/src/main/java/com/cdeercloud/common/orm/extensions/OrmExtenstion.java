package com.cdeercloud.common.orm.extensions;

import java.util.Properties;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import com.cdeercloud.common.core.config.ConfigHolder;
import com.cdeercloud.common.core.definition.BeanDefinitionInitializer;

public class OrmExtenstion implements BeanDefinitionInitializer {
	@Override
	public void init(BeanDefinitionRegistry registry, Properties properties) {
		new OrmBeanFactoryPostProcessor(new ConfigHolder(properties)).postProcessBeanDefinitionRegistry(registry);
	}
}