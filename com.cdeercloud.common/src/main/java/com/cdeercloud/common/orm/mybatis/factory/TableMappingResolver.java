package com.cdeercloud.common.orm.mybatis.factory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.reflection.property.PropertyNamer;
import org.apache.ibatis.type.JdbcType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.orm.Constants;
import com.cdeercloud.common.orm.annotation.CdeerColumn;
import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.cdeercloud.common.orm.annotation.CdeerTransient;
import com.cdeercloud.common.orm.mybatis.util.MUtils;
import com.cdeercloud.common.orm.mybatis.util.ScanUtils;
import com.cdeercloud.common.orm.mybatis.vo.ColumnModel;
import com.cdeercloud.common.orm.mybatis.vo.TableModel;
import com.cdeercloud.common.orm.mybatis.vo.TransientField;
import com.cdeercloud.common.orm.strategy.NamingStrategy;

/**
 * 
 * @author looyii
 *
 */
public class TableMappingResolver implements InitializingBean {

	private Map<String, List<TableModel>> tmMap = new HashMap<String, List<TableModel>>();

	@Override
	public void afterPropertiesSet() throws Exception {
		List<TableModel> list = this.resolveMapping();
		for (TableModel tm : list) {
			String ds = tm.getDataSourceName();
			if (StringUtils.hasText(ds)) {
				List<TableModel> tms = tmMap.get(ds);
				if (tms == null) {
					tms = new ArrayList<TableModel>();
					tmMap.put(ds, tms);
				}
				tms.add(tm);
			}
		}
	}

	public List<TableModel> getTableModels(String dataSourceName) {
		List<TableModel> list = this.tmMap.get(dataSourceName);
		if (list != null) {
			return Collections.unmodifiableList(list);
		}
		return Collections.emptyList();
	}

	private List<TableModel> resolveMapping() {
		List<Class<?>> clses = ScanUtils.scanEntityClasses(CdeerEntity.class);
		List<TableModel> ret = new ArrayList<TableModel>();
		for (Class<?> cls : clses) {
			List<Method> ms = getGetterMethods(cls);

			CdeerTable annoTable = cls.getAnnotation(CdeerTable.class);
			String tableName = null;
			String dataSourceName = Constants.DEFAULT_SMART_DATASOURCE_NAME;
			if (annoTable != null) {
				tableName = annoTable.name();
				String ds = annoTable.dataSource();
				if (StringUtils.hasText(ds)) {
					dataSourceName = ds;
				}
			} else {
				tableName = NamingStrategy.DEFAULT.classToTableName(cls.getName());
			}

			TableModel table = new TableModel(tableName, cls);
			table.setDataSourceName(dataSourceName);
			for (Method method : ms) {
				Class<?> rt = method.getReturnType();
				if (!rt.isPrimitive() && !rt.getName().startsWith("java")) {
					continue;
				}
				if (Collection.class.isAssignableFrom(rt) || Map.class.isAssignableFrom(rt)) {
					continue;
				}
				String name = PropertyNamer.methodToProperty(method.getName());
				if (name.startsWith("$") || "serialVersionUID".equals(name) || "class".equals(name)) {
					continue;
				}
				CdeerTransient transientAnno = AnnotationUtils.findAnnotation(method, CdeerTransient.class);
				if (transientAnno != null) {
					table.addTransienField(new TransientField(name, rt));
					continue;
				}

				CdeerColumn annoColumn = AnnotationUtils.findAnnotation(method, CdeerColumn.class);
				CdeerId annoId = AnnotationUtils.findAnnotation(method, CdeerId.class);
				String columnName = null;
				JdbcType jdbcType = null;
				ColumnModel cm = null;
				boolean isPk = (annoId != null);
				if (annoColumn != null) {
					if (!"".equals(annoColumn.name())) {
						columnName = annoColumn.name();
					}
					if (JdbcType.UNDEFINED != annoColumn.jdbcType()) {
						jdbcType = annoColumn.jdbcType();
					}
				}
				if (isPk) {
					CdeerGeneratedValue gv = AnnotationUtils.findAnnotation(method, CdeerGeneratedValue.class);
					if (gv != null) {
						table.setStrategy(gv.strategy());
						table.setGenerator(gv.generator());
					}
				}
				if (columnName != null) {
					cm = new ColumnModel(name, columnName.toUpperCase(), rt, isPk);
					cm.setJdbcType(jdbcType);
				} else {
					String colName = NamingStrategy.DEFAULT.propertyToColumnName(name);
					cm = new ColumnModel(name, colName.toUpperCase(), rt, isPk);
				}
				if (jdbcType != null) {
					cm.setJdbcType(jdbcType);
				}
				table.addColumn(cm);
			}
			if (table.getPk() == null) {
				throw new CdeerException(cls.getName() + "类未设置主键字段(@MdpId)");
			}
			ret.add(table);
		}

		MUtils.init(ret);
		return ret;
	}

	protected static List<Method> getGetterMethods(Class<?> cls) {
		List<Method> ms = new ArrayList<Method>();
		Method[] methods = cls.getMethods();
		for (Method method : methods) {
			String name = method.getName();
			if (name.startsWith("get") && (name.length() > 3)) {
				if (method.getParameterTypes().length == 0) {
					ms.add(method);
				}
			} else if (name.startsWith("is") && (name.length() > 2)) {
				if (method.getParameterTypes().length == 0) {
					ms.add(method);
				}
			}
		}
		return ms;
	}
}
