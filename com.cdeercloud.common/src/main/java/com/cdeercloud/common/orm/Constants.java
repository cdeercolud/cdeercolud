package com.cdeercloud.common.orm;

public class Constants {
	public static final String IS_SMART_COUNT_STATEMENT = "is_smart_count_statement";
	public static final String SMART_AUTO_COUNT_ALIAS = "smart_auto_count";
	public static final String COMMON_SELECT_CONDITION = "smart_common_select_condition";
	public static final String COMMON_SELECT_CONDITION_NOALIAS = "smart_common_select_condition_noalias";
	public static final String DEFAULT_SMART_DATASOURCE_NAME = "default_smart_datasource_name";

	public Constants() {
	}
}
