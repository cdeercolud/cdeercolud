package com.cdeercloud.common.orm.annotation;

public enum CdeerGenerationType {
	//UUID
	UUID,
	//SEQUENCE
	SEQUENCE,
	// AUTO
	AUTO,
	//ASSIGN
	ASSIGN
}
