package com.cdeercloud.common.orm.mybatis.vo;

import org.apache.ibatis.type.JdbcType;

/**
 * 数据库列模型。
 * 
 * @author looyii
 *
 */
public class ColumnModel {
	private boolean pk;
	private String property;
	private String column;
	private Class<?> javaType;
	private JdbcType jdbcType;

	/**
	 * 自定义的构造方法。
	 * 
	 * @param property String
	 * @param column String
	 * @param javaType Class<?>
	 */
	public ColumnModel(String property, String column, Class<?> javaType) {
		this.property = property;
		this.column = column;
		this.javaType = javaType;
	}

	public ColumnModel(String property, String column, Class<?> javaType, boolean isPk) {
		this.property = property;
		this.column = column;
		this.javaType = javaType;
		this.pk = isPk;
	}

	/**
	 * 获取property。
	 * 
	 * @return String
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * 设置property。
	 * 
	 * @param property String
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * 获取column。
	 * 
	 * @return String
	 */
	public String getColumn() {
		return column;
	}

	/**
	 * 设置column。
	 * 
	 * @param column String
	 */
	public void setColumn(String column) {
		this.column = column;
	}

	/**
	 * 获取javaType。
	 * 
	 * @return Class<?>
	 */
	public Class<?> getJavaType() {
		return javaType;
	}

	/**
	 * 设置 javaType。
	 * 
	 * @param javaType Class<?>
	 */
	public void setJavaType(Class<?> javaType) {
		this.javaType = javaType;
	}

	/**
	 * 获取jdbcType。
	 * 
	 * @return JdbcType
	 */
	public JdbcType getJdbcType() {
		return jdbcType;
	}

	/**
	 * 设置JdbcType。
	 * 
	 * @param jdbcType JdbcType
	 */
	public void setJdbcType(JdbcType jdbcType) {
		this.jdbcType = jdbcType;
	}

	/**
	 * 获取pk。
	 * 
	 * @return boolean
	 */
	public boolean isPk() {
		return pk;
	}

	/**
	 * 设置pk。
	 * 
	 * @param pk boolean
	 */
	public void setPk(boolean pk) {
		this.pk = pk;
	}
}
