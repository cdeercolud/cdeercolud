package com.cdeercloud.common.orm.util;

import org.springframework.beans.factory.BeanFactory;

import com.cdeercloud.common.orm.service.IEntityStatement;

/**
 * Orm 辅助类。
 * 
 * @author looyii
 *
 */
public class OrmUtils {

	@SuppressWarnings("unchecked")
	public static <T> IEntityStatement<T> getEntityStatement(BeanFactory beanFactory, Class<T> clazz) {
		String name = clazz.getSimpleName().toLowerCase() + "_dao";
		return (IEntityStatement<T>) beanFactory.getBean(name);
	}
}
