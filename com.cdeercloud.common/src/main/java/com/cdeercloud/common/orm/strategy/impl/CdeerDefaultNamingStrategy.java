package com.cdeercloud.common.orm.strategy.impl;

import java.io.Serializable;

import com.cdeercloud.common.orm.strategy.NamingStrategy;

public class CdeerDefaultNamingStrategy implements NamingStrategy, Serializable {
	private static final long serialVersionUID = 3195508752924928349L;

	@Override
	public String classToTableName(String className) {
		String unqualify = unqualify(className);
		return addUnderscores(unqualify);
	}

	@Override
	public String propertyToColumnName(String propertyName) {
		return addUnderscores(unqualify(propertyName));
	}

	protected static String addUnderscores(String name) {
		StringBuffer buf = new StringBuffer(name.replace('.', '_'));
		for (int i = 1; i < (buf.length() - 1); i++) {
			if (Character.isLowerCase(buf.charAt(i - 1)) && Character.isUpperCase(buf.charAt(i)) && Character.isLowerCase(buf.charAt(i + 1))) {
				buf.insert(i++, '_');
			}
		}
		return buf.toString().toUpperCase();
	}

	protected static String unqualify(String qualifiedName) {
		int loc = qualifiedName.lastIndexOf(".");
		return (loc < 0) ? qualifiedName : qualifiedName.substring(qualifiedName.lastIndexOf(".") + 1);
	}

	@Override
	public String foreignKeyColumnName(String propertyName) {
		return this.propertyToColumnName(propertyName) + "_id";
	}

}
