package com.cdeercloud.common.orm.schema;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * Smart Spring XML命名空间处理器。
 * 
 * @author looyii
 *
 */
public class CdeerNamespaceHandler extends NamespaceHandlerSupport {
	@Override
	public void init() {
		registerBeanDefinitionParser("datasource", new DsBeanDefinitionParser());
	}

}
