package com.cdeercloud.common.orm.schema;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.cdeercloud.common.orm.mybatis.factory.CdeerSqlSessionFactoryBean;
import com.cdeercloud.common.orm.mybatis.util.ScanUtils;
import com.cdeercloud.common.orm.service.impl.EntityStatment;

public class DsBeanDefinitionParser implements BeanDefinitionParser {
	private static final Log log = LogFactory.getLog(DsBeanDefinitionParser.class);
	private static final ResourcePatternResolver RESOLVER = new PathMatchingResourcePatternResolver();

	@Override
	public BeanDefinition parse(Element element, ParserContext parserContext) {
		initBeanPostProcessor(element, parserContext);
		initMultiDatasource(element, parserContext);
		initSqlSessionTemplate(element, parserContext);
		return null;
	}

	private void initSqlSessionTemplate(Element element, ParserContext parserContext) {
		Object source = parserContext.extractSource(element);
		BeanDefinitionRegistry registry = parserContext.getRegistry();
		List<Class<?>> clses = ScanUtils.scanEntityClasses(CdeerEntity.class);
		for (Class<?> clz : clses) {
			RootBeanDefinition def = new RootBeanDefinition(EntityStatment.class);
			def.setScope(BeanDefinition.SCOPE_SINGLETON);
			def.setSource(source);
			String templateBeanName = "framework_orm_sqlSessionTemplate";
			CdeerTable st = AnnotationUtils.findAnnotation(clz, CdeerTable.class);
			if (st != null) {
				String ds = st.dataSource();
				if (StringUtils.hasText(ds)) {
					templateBeanName = "framework_core_sqlSessionTemplate_" + ds;
				}
			}
			RuntimeBeanReference sqlSessionTemplateRef = new RuntimeBeanReference(templateBeanName);
			def.getPropertyValues().addPropertyValue("sqlSessionTemplate", sqlSessionTemplateRef);
			def.getPropertyValues().addPropertyValue("entityClass", clz);
			String beanName = clz.getSimpleName().toLowerCase() + "_dao";
			registry.registerBeanDefinition(beanName, def);
		}
	}

	private void initBeanPostProcessor(Element element, ParserContext parserContext) {
		Object source = parserContext.extractSource(element);
		RootBeanDefinition processor = new RootBeanDefinition(DaoAnnotationBeanPostProcessor.class);
		processor.setSource(source);
		processor.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		parserContext.getReaderContext().registerWithGeneratedName(processor);
	}

	private void initMultiDatasource(Element element, ParserContext parserContext) {
		Object source = parserContext.extractSource(element);
		Map<String, Properties> map = new HashMap<String, Properties>();
		try {
			Resource[] reses = RESOLVER.getResources("classpath*:META-INF/conf/datasource/*.properties");
			for (Resource res : reses) {
				Properties prop = new Properties();
				InputStream inputStream = null;
				try {
					inputStream = res.getInputStream();
					prop.load(inputStream);
				} catch (FileNotFoundException e) {
					log.error("", e);
				} catch (IOException e) {
					log.error("", e);
				} finally {
					try {
						inputStream.close();
					} catch (IOException e) {
						log.info("", e);
					}
				}
				String name = res.getFilename();
				map.put(name.substring(0, name.length() - 11), prop);
			}
		} catch (IOException e) {
			log.error("", e);
		}

		for (Map.Entry<String, Properties> ent : map.entrySet()) {
			BeanDefinitionRegistry registry = parserContext.getRegistry();

			String dsBeanName = "smart_datasource_" + ent.getKey();
			Properties prop = ent.getValue();
			RootBeanDefinition ds = new RootBeanDefinition(BasicDataSource.class);
			ds.setSource(source);
			ds.setScope(BeanDefinition.SCOPE_SINGLETON);
			ds.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			ds.setDestroyMethodName("close");
			MutablePropertyValues dsMpv = ds.getPropertyValues();
			dsMpv.addPropertyValue("driverClassName", prop.get("jdbc.driverClassName"));
			dsMpv.addPropertyValue("url", prop.get("jdbc.url"));
			dsMpv.addPropertyValue("username", prop.get("jdbc.username"));
			dsMpv.addPropertyValue("password", prop.get("jdbc.password"));

			fillProp(dsMpv, prop, "validationQuery", "jdbc.validationQuery");
			fillProp(dsMpv, prop, "initialSize", "jdbc.initialSize");
			fillProp(dsMpv, prop, "maxTotal", "jdbc.maxActive");
			fillProp(dsMpv, prop, "minIdle", "jdbc.minIdle");
			fillProp(dsMpv, prop, "maxIdle", "jdbc.maxIdle");

			registry.registerBeanDefinition(dsBeanName, ds);

			RuntimeBeanReference tmr = new RuntimeBeanReference("framework_orm_tableMappingResolver");
			String factoryBeanName = "framework_core_sqlSessionFactoryBean_" + ent.getKey();
			RootBeanDefinition factory = new RootBeanDefinition(CdeerSqlSessionFactoryBean.class);
			factory.setSource(source);
			factory.setScope(BeanDefinition.SCOPE_SINGLETON);
			factory.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			MutablePropertyValues fMpv = factory.getPropertyValues();
			fMpv.addPropertyValue("resolver", tmr);
			fMpv.addPropertyValue("dataSourceName", ent.getKey());
			fMpv.addPropertyValue("configLocation", "${smart.mybatis.configLocation}");
			fMpv.addPropertyValue("mapperLocations", "classpath*:/META-INF/conf/mybatis/**/*-mapper.xml");
			RuntimeBeanReference dsRef = new RuntimeBeanReference(dsBeanName);
			fMpv.addPropertyValue("dataSource", dsRef);
			registry.registerBeanDefinition(factoryBeanName, factory);

			String templateBeanName = "framework_core_sqlSessionTemplate_" + ent.getKey();
			RootBeanDefinition template = new RootBeanDefinition(SqlSessionTemplate.class);
			template.setSource(source);
			template.setScope(BeanDefinition.SCOPE_SINGLETON);
			template.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			RuntimeBeanReference factoryRef = new RuntimeBeanReference(factoryBeanName);
			template.getConstructorArgumentValues().addIndexedArgumentValue(0, factoryRef);
			registry.registerBeanDefinition(templateBeanName, template);
		}
	}

	private void fillProp(MutablePropertyValues pvs, Properties prop, String field, String pKey) {
		Object value = prop.get(field);
		if (value == null) {
			value = "${" + pKey + "}";
		}
		pvs.addPropertyValue(field, value);
	}
}
