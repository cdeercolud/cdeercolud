package com.cdeercloud.common.orm.interceptor;

import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;

import com.cdeercloud.common.orm.entity.Auditable;

@Intercepts({ @Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }) })
public class CdeerAuditableHandlerInterceptor implements Interceptor {
	private AuditableSetter auditableSetter;

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		Object[] args = invocation.getArgs();
		if (args.length == 2) {
			if ((args[0] instanceof MappedStatement) && (args[1] instanceof Auditable)) {
				MappedStatement ms = (MappedStatement) args[0];
				Auditable audit = (Auditable) args[1];
				SqlCommandType sqlCommandType = ms.getSqlCommandType();
				if (sqlCommandType == SqlCommandType.INSERT) {
					this.auditableSetter.forInsert(audit);
				} else if (sqlCommandType == SqlCommandType.UPDATE) {
					this.auditableSetter.forUpdate(audit);
				}
			}
		}

		return invocation.proceed();
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}

	public void setAuditableSetter(AuditableSetter auditableSetter) {
		this.auditableSetter = auditableSetter;
	}

}