package com.cdeercloud.common.orm.mybatis.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;

public class TableModel {
	private String tableName;
	private String dataSourceName;
	private Class<?> cls;
	private ColumnModel pk;
	private List<TransientField> transientFields = new ArrayList<TransientField>();
	private List<ColumnModel> cols = new ArrayList<ColumnModel>();
	private CdeerGenerationType strategy;
	private String generator;

	/**
	 * 自定义的构造方法。
	 * 
	 * @param tableName String
	 * @param cls Class<?>
	 */
	public TableModel(String tableName, Class<?> cls) {
		super();
		this.tableName = tableName;
		this.cls = cls;
	}

	/**
	 * 获取tableName。
	 * 
	 * @return String
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * 设置tableName。
	 * 
	 * @param tableName String
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * 获取cls。
	 * 
	 * @return Class<?>
	 */
	public Class<?> getCls() {
		return cls;
	}

	/**
	 * 设置cls。
	 * 
	 * @param cls Class<?>
	 */
	public void setCls(Class<?> cls) {
		this.cls = cls;
	}

	/**
	 * 获取cols。
	 * 
	 * @return List<ColumnModel>
	 */
	public List<ColumnModel> getCols() {
		return cols;
	}

	/**
	 * 增加cols。
	 * 
	 * @param col ColumnModel
	 */
	public void addColumn(ColumnModel col) {
		if (col.isPk()) {
			if (this.pk != null) {
				throw new CdeerException("实体类不允许对多个字段设置@MdpId注解。");
			}
			this.pk = col;
			this.cols.add(0, col);
		} else {
			this.cols.add(col);
		}
	}

	/**
	 * 获取pks。
	 * 
	 * @return List<ColumnModel>
	 */
	public ColumnModel getPk() {
		if (this.pk == null) {
			throw new CdeerException(cls.getName() + "类未指定主键字段：@MdpId。");
		}
		return this.pk;
	}

	/**
	 * 获取AllCols。
	 * 
	 * @return List<ColumnModel>
	 */
	public List<ColumnModel> getAllCols() {
		return Collections.unmodifiableList(this.cols);
	}

	public CdeerGenerationType getStrategy() {
		return strategy;
	}

	public void setStrategy(CdeerGenerationType strategy) {
		this.strategy = strategy;
	}

	public String getGenerator() {
		return generator;
	}

	public void setGenerator(String generator) {
		this.generator = generator;
	}

	public List<TransientField> getTransientFields() {
		return transientFields;
	}

	public void addTransienField(TransientField transientField) {
		this.transientFields.add(transientField);
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
}
