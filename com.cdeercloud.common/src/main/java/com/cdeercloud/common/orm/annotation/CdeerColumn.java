package com.cdeercloud.common.orm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.ibatis.type.JdbcType;

/**
 * 字段
 * @author looyii
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CdeerColumn {
	public String name() default "";
	public JdbcType jdbcType() default JdbcType.UNDEFINED;
}
