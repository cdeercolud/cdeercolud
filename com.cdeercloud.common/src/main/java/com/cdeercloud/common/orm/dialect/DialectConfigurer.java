package com.cdeercloud.common.orm.dialect;

public class DialectConfigurer {
	private final IDialect dialect;

	public DialectConfigurer(IDialect dialect) {
		this.dialect = dialect;
	}

	public IDialect getDialect() {
		return dialect;
	}
}
