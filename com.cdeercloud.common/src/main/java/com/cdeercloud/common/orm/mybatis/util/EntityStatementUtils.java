package com.cdeercloud.common.orm.mybatis.util;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;

import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;

public class EntityStatementUtils {
	final static Logger logger = LoggerFactory.getLogger(EntityStatementUtils.class);

	public static void injectionEntityStatement(BeanFactory beanFactory, Object bean, Field f) {
		EntityStatement anno = f.getAnnotation(EntityStatement.class);
		if (anno != null) {
			Class<?> type = f.getType();
			// TODO 可以支持更多方式
			if (type == IEntityStatement.class) {
				Type cls = f.getGenericType();
				if (cls instanceof ParameterizedType) {
					ParameterizedType genericSuperclass = (ParameterizedType) cls;
					Class<?> clsx = (Class<?>) genericSuperclass.getActualTypeArguments()[0];
					String name = clsx.getSimpleName().toLowerCase() + "_dao";
					Object daoBean = beanFactory.getBean(name);
					f.setAccessible(true);
					try {
						f.set(bean, daoBean);
					} catch (IllegalArgumentException e) {
						logger.error("非法参数", e);
					} catch (IllegalAccessException e) {
						logger.error("非法访问", e);
					}
				}
			}
		}
	}
}
