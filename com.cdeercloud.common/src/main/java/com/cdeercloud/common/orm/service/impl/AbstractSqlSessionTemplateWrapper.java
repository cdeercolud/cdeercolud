package com.cdeercloud.common.orm.service.impl;

import java.util.List;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.orm.Constants;
import com.cdeercloud.common.orm.vo.RowBoundsExtension;

abstract class AbstractSqlSessionTemplateWrapper {
	private SqlSessionTemplate sqlSessionTemplate;

	public AbstractSqlSessionTemplateWrapper(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	protected abstract String getStatementId(String key);

	protected int createByStatement(String key, Object object) {
		String statement = this.getStatementId(key);
		return this.getSqlSessionTemplate().insert(statement, object);
	}

	protected int updateByStatement(String key, Object object) {
		String statement = this.getStatementId(key);
		return this.getSqlSessionTemplate().update(statement, object);
	}

	protected int deleteByStatement(String key, Object object) {
		String statement = this.getStatementId(key);
		return this.getSqlSessionTemplate().delete(statement, object);
	}

	protected <T> List<T> findByStatement(String key, Object parameter) {
		return this.findByStatement(key, parameter, -1, -1);
	}

	protected <T> List<T> findByStatement(String key, Object paramenter, int pageIndex, int pageSize) {
		String statement = this.getStatementId(key);
		if ((pageIndex == -1) && (pageSize == -1)) {
			return this.getSqlSessionTemplate().selectList(statement, paramenter);
		} else {
			RowBounds rb = this.getRowBounds(pageIndex, pageSize);
			return this.getSqlSessionTemplate().selectList(key, paramenter, rb);
		}
	}

	protected <T> T findFirstByStatement(String key, Object parameter) {
		List<T> list = this.findByStatement(key, parameter);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}

	protected <T> List<T> listByStatement(String key) {
		String statement = this.getStatementId(key);
		return this.getSqlSessionTemplate().selectList(statement);
	}

	protected <T> T countByStatement(String key, Object paramenter) {
		String sqlId = this.getStatementId(key);
		return this.getSqlSessionTemplate().selectOne(sqlId, paramenter);
	}

	protected int batchCreateByStatement(final String key, final List<?> list, final int frequency) {
		final String statementId = this.getStatementId(key);
		return new AbstractBatchExecutor() {
			@Override
			public int doExecute(SqlSession session) {
				return this.batchCreate(session, statementId, list, frequency);
			}
		}.run();
	}

	protected int batchUpdateByStatement(final String key, final List<?> list, final int frequency) {
		final String statementId = this.getStatementId(key);
		return new AbstractBatchExecutor() {
			@Override
			public int doExecute(SqlSession session) {
				return this.batchUpdate(session, statementId, list, frequency);
			}
		}.run();
	}

	protected <T> PagedResult<T> pagedQueryByStatement(String key, Object parameter, int pageIndex, int pageSize) {
		RowBounds rb = this.getRowBounds(pageIndex, pageSize);
		String statement = this.getStatementId(key);
		List<T> list = this.getSqlSessionTemplate().selectList(statement, parameter, rb);
		int total = countExt(statement, parameter);
		PagedResult<T> pr = new PagedResult<T>(list, total, pageIndex, pageSize);
		return pr;
	}

	protected int countExt(String key, Object parameter) {
		String statement = this.getStatementId(key);
		String statementCount = statement + "_count";
		Configuration config = this.getSqlSessionTemplate().getConfiguration();
		int total = 0;
		if (config.hasStatement(statementCount)) {
			Object ret = this.getSqlSessionTemplate().selectOne(statementCount, parameter);
			if (ret instanceof Number) {
				Number number = (Number) ret;
				total = number.intValue();
			} else {
				total = -1;
			}
		} else {
			RowBoundsExtension rbExt = new RowBoundsExtension(1, 10);
			rbExt.set(Constants.IS_SMART_COUNT_STATEMENT, true);
			List<Object> result = this.getSqlSessionTemplate().selectList(statement, parameter, rbExt);
			if ((result != null) && (result.size() == 1)) {
				Object object = result.get(0);
				if (object instanceof Integer) {
					total = (Integer) object;
				}
			}
		}
		return total;
	}

	protected RowBounds getRowBounds(int pageIndex, int pageSize) {
		if (pageSize == -1) {
			return RowBounds.DEFAULT;
		}
		if (pageIndex <= 0) {
			pageIndex = 1;
		}
		int offset = (pageIndex - 1) * pageSize;
		return new RowBounds(offset, pageSize);
	}

	protected SqlSessionTemplate getSqlSessionTemplate() {
		return sqlSessionTemplate;
	}

	abstract class AbstractBatchExecutor {
		public abstract int doExecute(SqlSession session);

		public int run() {
			int count = 0;
			SqlSession session = this.openBatchSession();
			try {
				count = this.doExecute(session);
			} catch (Exception e) {
				throw new CdeerException("批量操作时发生错误!", e);
			} finally {
				if (session != null) {
					session.close();
				}
			}
			return count;
		}

		private SqlSession openBatchSession() {
			SqlSessionFactory factory = AbstractSqlSessionTemplateWrapper.this.getSqlSessionTemplate().getSqlSessionFactory();
			SqlSession session = factory.openSession(ExecutorType.BATCH, false);
			return session;
		}

		protected int batchCreate(SqlSession session, String statementId, List<?> list, int frequency) {
			return this.batch(session, list, frequency, statementId, true);
		}

		protected int batchUpdate(SqlSession session, String statementId, List<?> list, int frequency) {
			return this.batch(session, list, frequency, statementId, false);
		}

		private int batch(SqlSession session, List<?> list, int frequency, String statementId, boolean isInsert) {
			int count = 0;
			for (int i = 0; i < list.size(); i++) {
				if ((i > 0) && ((i % frequency) == 0)) {
					session.flushStatements();
				}
				Object object = list.get(i);
				if (isInsert) {
					count += session.insert(statementId, object);
				} else {
					count += session.update(statementId, object);
				}
			}
			// TODO 优化刷新动作
			session.flushStatements();
			return count;
		}
	}

	protected void checkStatementExist(String statement) {
		Configuration configuration = this.getSqlSessionTemplate().getConfiguration();
		if (!configuration.hasStatement(statement)) {
			throw new IllegalArgumentException("未找到MyBatis声明配置：" + statement);
		}
	}
}