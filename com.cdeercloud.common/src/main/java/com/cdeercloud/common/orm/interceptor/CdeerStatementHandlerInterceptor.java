package com.cdeercloud.common.orm.interceptor;

import java.sql.Connection;
import java.util.Properties;

import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.RowBounds;

import com.cdeercloud.common.orm.Constants;
import com.cdeercloud.common.orm.dialect.DialectConfigurer;
import com.cdeercloud.common.orm.dialect.IDialect;
import com.cdeercloud.common.orm.vo.RowBoundsExtension;
import com.cdeercloud.common.util.ReflectUtils;

@Intercepts({ @Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class, Integer.class }) })
public class CdeerStatementHandlerInterceptor implements Interceptor {
	private DialectConfigurer dialectConfigurer;

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		RoutingStatementHandler statement = (RoutingStatementHandler) invocation.getTarget();
		StatementHandler handler = (StatementHandler) ReflectUtils.getFieldValue(statement, "delegate");
		RowBounds rowBounds = (RowBounds) ReflectUtils.getFieldValue(handler, "rowBounds");
		boolean isCount = false;
		if (rowBounds instanceof RowBoundsExtension) {
			RowBoundsExtension ext = (RowBoundsExtension) rowBounds;
			isCount = (Boolean) ext.get(Constants.IS_SMART_COUNT_STATEMENT);
		}
		if (isCount) {
			BoundSql boundSql = statement.getBoundSql();
			String sql = "select count(1) as " + Constants.SMART_AUTO_COUNT_ALIAS + " from (" + boundSql.getSql() + ") tc";
			ReflectUtils.setFieldValue(boundSql, "sql", sql);
		} else if ((rowBounds.getLimit() > 0) && (rowBounds.getLimit() < RowBounds.NO_ROW_LIMIT)) {
			BoundSql boundSql = statement.getBoundSql();
			String sql = boundSql.getSql();
			IDialect dialect = this.dialectConfigurer.getDialect();
			sql = dialect.getPagedString(sql, rowBounds.getOffset(), rowBounds.getLimit());
			ReflectUtils.setFieldValue(boundSql, "sql", sql);
		}
		return invocation.proceed();
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}

	public void setDialectConfigurer(DialectConfigurer dialectConfigurer) {
		this.dialectConfigurer = dialectConfigurer;
	}
}
