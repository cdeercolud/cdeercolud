package com.cdeercloud.common.orm.mybatis.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;

public class ScanUtils {
	private static final Log log = LogFactory.getLog(ScanUtils.class);

	private static final String ENTITY_LOCATION_PATTERN = "classpath*:com/**/entity/*.class";
	private static final ResourcePatternResolver RESOLVER = new PathMatchingResourcePatternResolver();
	private static final MetadataReaderFactory READER_FACTORY = new SimpleMetadataReaderFactory();

	public static List<Class<?>> scanEntityClasses(Class<?> annoClazz) {

		List<MetadataReader> readers = scanEntity(ENTITY_LOCATION_PATTERN);
		return scanClass(readers, null, annoClazz);
	}

	private static List<MetadataReader> scanEntity(String pattern) {
		List<MetadataReader> list = new ArrayList<MetadataReader>();
		try {
			Resource[] resources = RESOLVER.getResources(pattern);

			for (Resource res : resources) {
				MetadataReader meta = READER_FACTORY.getMetadataReader(res);
				list.add(meta);
			}
		} catch (IOException e) {
			log.error("读取类文件时发生I/O错误。", e);
		}
		return list;
	}

	private static List<Class<?>> scanClass(List<MetadataReader> metas, Class<?> superClazz, Class<?> annoClazz) {
		List<Class<?>> ret = new ArrayList<Class<?>>();
		String annoName = annoClazz.getName();
		for (MetadataReader reader : metas) {
			Set<String> annotationTypes = reader.getAnnotationMetadata().getAnnotationTypes();
			if (!annotationTypes.contains(annoName)) {
				continue;
			}
			Class<?> clazz = null;
			String name = reader.getClassMetadata().getClassName();
			try {
				clazz = Class.forName(name);
			} catch (ClassNotFoundException e) {
				log.error("类加载不成功，找不到类文件。", e);
				continue;
			}
			if (superClazz != null) {
				if (!superClazz.isAssignableFrom(clazz)) {
					continue;
				}
			}
			ret.add(clazz);
		}
		return ret;
	}
}
