package com.cdeercloud.common.orm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.METHOD})
@Retention(value=RetentionPolicy.RUNTIME)
public @interface CdeerGeneratedValue {
	CdeerGenerationType strategy() default CdeerGenerationType.ASSIGN;
    String generator() default "";
}
