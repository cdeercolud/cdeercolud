package com.cdeercloud.common.orm.extensions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.ManagedArray;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.config.ConfigHolder;
import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.cdeercloud.common.orm.interceptor.CdeerAuditableHandlerInterceptor;
import com.cdeercloud.common.orm.mybatis.factory.CdeerSqlSessionFactoryBean;
import com.cdeercloud.common.orm.mybatis.util.ScanUtils;
import com.cdeercloud.common.orm.schema.DaoAnnotationBeanPostProcessor;
import com.cdeercloud.common.orm.service.impl.EntityStatment;

/**
 * 
 * @author looyii
 *
 */
public class OrmBeanFactoryPostProcessor {
	static final Logger logger = LoggerFactory.getLogger(OrmBeanFactoryPostProcessor.class);

	public final static String AUDITABLE_HANDLER_INTERCEPTOR_BEAN_NAME = "framework_orm_cdeerAuditableHandlerInterceptor";

	private final ConfigHolder holder;

	public OrmBeanFactoryPostProcessor(ConfigHolder holder) {
		this.holder = holder;
	}

	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		initBeanPostProcessor(registry);
		String auditableSetterName = getAuditableSetterName();
		if (StringUtils.hasText(auditableSetterName)) {
			initAuditableHandlerInterceptor(registry, auditableSetterName);
		}
		initDefaultDatasource(registry);
		initMultiDatasource(registry);
		initSqlSessionTemplate(registry);
	}

	private void initAuditableHandlerInterceptor(BeanDefinitionRegistry registry, String auditableSetterName) {
		RootBeanDefinition auditDef = new RootBeanDefinition(CdeerAuditableHandlerInterceptor.class);
		auditDef.setScope(BeanDefinition.SCOPE_SINGLETON);
		auditDef.getPropertyValues().addPropertyValue("auditableSetter", new RuntimeBeanReference(auditableSetterName));
		registry.registerBeanDefinition(AUDITABLE_HANDLER_INTERCEPTOR_BEAN_NAME, auditDef);
	}

	private void initDefaultDatasource(BeanDefinitionRegistry registry) {
		RootBeanDefinition def = new RootBeanDefinition(CdeerSqlSessionFactoryBean.class);
		def.setScope(BeanDefinition.SCOPE_SINGLETON);
		String beanName = "framework_orm_sqlSessionFactoryBean";
		MutablePropertyValues fMpv = def.getPropertyValues();
		fMpv.addPropertyValue("resolver", new RuntimeBeanReference("framework_orm_tableMappingResolver"));
		fMpv.addPropertyValue("dataSource", new RuntimeBeanReference("${orm.beans.dataSource}"));
		fMpv.addPropertyValue("configLocation", "${orm.mybatis.configLocation}");
		fMpv.addPropertyValue("mapperLocations", "classpath*:/META-INF/conf/mybatis/**/*-mapper.xml");
		ManagedArray plugins = getMyBatisPlugins();
		fMpv.addPropertyValue("plugins", plugins);

		registry.registerBeanDefinition(beanName, def);

	}

	private ManagedArray getMyBatisPlugins() {
		List<RuntimeBeanReference> list = new ArrayList<RuntimeBeanReference>();
		list.add(new RuntimeBeanReference("framework_orm_cdeerStatementHandlerInterceptor"));
		list.add(new RuntimeBeanReference("framework_orm_cdeerResultSetHandlerInterceptor"));

		String auditableSetterName = getAuditableSetterName();
		if (StringUtils.hasText(auditableSetterName)) {
			list.add(new RuntimeBeanReference(AUDITABLE_HANDLER_INTERCEPTOR_BEAN_NAME));
		}
		String iStr = this.holder.getProperty("orm.mybatis.interceptors");
		if (StringUtils.hasText(iStr)) {
			String[] arr = iStr.split(",");
			for (String id : arr) {
				if (StringUtils.hasText(id)) {
					list.add(new RuntimeBeanReference(id.trim()));
				}
			}
		}
		ManagedArray plugins = new ManagedArray("org.apache.ibatis.plugin.Interceptor", list.size());
		for (RuntimeBeanReference ref : list) {
			plugins.add(ref);
		}
		return plugins;
	}

	private String getAuditableSetterName() {
		Properties mybatisProperties = this.holder.getProperties("orm.mybatis", true);
		if ((mybatisProperties != null) && mybatisProperties.containsKey("auditableSetter")) {
			return mybatisProperties.getProperty("auditableSetter");
		}
		return null;
	}

	private void initSqlSessionTemplate(BeanDefinitionRegistry registry) {
		List<Class<?>> clses = ScanUtils.scanEntityClasses(CdeerEntity.class);
		for (Class<?> clz : clses) {
			RootBeanDefinition def = new RootBeanDefinition(EntityStatment.class);
			def.setScope(BeanDefinition.SCOPE_SINGLETON);
			String templateBeanName = "framework_orm_sqlSessionTemplate";
			CdeerTable st = AnnotationUtils.findAnnotation(clz, CdeerTable.class);
			if (st != null) {
				String ds = st.dataSource();
				if (StringUtils.hasText(ds)) {
					templateBeanName = "framework_core_sqlSessionTemplate_" + ds;
				}
			}
			RuntimeBeanReference sqlSessionTemplateRef = new RuntimeBeanReference(templateBeanName);
			def.getPropertyValues().addPropertyValue("sqlSessionTemplate", sqlSessionTemplateRef);
			def.getPropertyValues().addPropertyValue("entityClass", clz);
			String beanName = clz.getSimpleName().toLowerCase() + "_dao";
			registry.registerBeanDefinition(beanName, def);
		}
	}

	private void initBeanPostProcessor(BeanDefinitionRegistry registry) {
		RootBeanDefinition processor = new RootBeanDefinition(DaoAnnotationBeanPostProcessor.class);
		processor.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		String simpleName = "orm_" + DaoAnnotationBeanPostProcessor.class.getSimpleName();
		registry.registerBeanDefinition(simpleName, processor);
	}

	private void initMultiDatasource(BeanDefinitionRegistry registry) {
		Map<String, Properties> map = holder.getSubProperties("orm.datasources", true);
		for (Map.Entry<String, Properties> ent : map.entrySet()) {
			if ("main".equals(ent.getKey())) {
				continue;
			}
			String dsBeanName = "smart_datasource_" + ent.getKey();
			Properties prop = ent.getValue();
			RootBeanDefinition ds = new RootBeanDefinition(BasicDataSource.class);
			ds.setScope(BeanDefinition.SCOPE_SINGLETON);
			ds.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			ds.setDestroyMethodName("close");
			MutablePropertyValues dsMpv = ds.getPropertyValues();
			dsMpv.addPropertyValue("driverClassName", prop.get("jdbc.driverClassName"));
			dsMpv.addPropertyValue("url", prop.get("jdbc.url"));
			dsMpv.addPropertyValue("username", prop.get("jdbc.username"));
			dsMpv.addPropertyValue("password", prop.get("jdbc.password"));

			fillProp(dsMpv, prop, "validationQuery", "jdbc.validationQuery");
			fillProp(dsMpv, prop, "initialSize", "jdbc.initialSize");
			fillProp(dsMpv, prop, "maxTotal", "jdbc.maxActive");
			fillProp(dsMpv, prop, "minIdle", "jdbc.minIdle");
			fillProp(dsMpv, prop, "maxIdle", "jdbc.maxIdle");

			registry.registerBeanDefinition(dsBeanName, ds);

			RuntimeBeanReference tmr = new RuntimeBeanReference("framework_orm_tableMappingResolver");
			String factoryBeanName = "framework_core_sqlSessionFactoryBean_" + ent.getKey();
			RootBeanDefinition factory = new RootBeanDefinition(CdeerSqlSessionFactoryBean.class);
			factory.setScope(BeanDefinition.SCOPE_SINGLETON);
			factory.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			MutablePropertyValues fMpv = factory.getPropertyValues();
			fMpv.addPropertyValue("resolver", tmr);
			fMpv.addPropertyValue("dataSourceName", ent.getKey());
			fMpv.addPropertyValue("configLocation", "${orm.mybatis.configLocation}");
			fMpv.addPropertyValue("mapperLocations", "classpath*:/META-INF/conf/mybatis/**/*-mapper.xml");
			fMpv.addPropertyValue("plugins", this.getMyBatisPlugins());

			RuntimeBeanReference dsRef = new RuntimeBeanReference(dsBeanName);
			fMpv.addPropertyValue("dataSource", dsRef);
			registry.registerBeanDefinition(factoryBeanName, factory);

			String templateBeanName = "framework_core_sqlSessionTemplate_" + ent.getKey();
			RootBeanDefinition template = new RootBeanDefinition(SqlSessionTemplate.class);
			template.setScope(BeanDefinition.SCOPE_SINGLETON);
			template.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			RuntimeBeanReference factoryRef = new RuntimeBeanReference(factoryBeanName);
			template.getConstructorArgumentValues().addIndexedArgumentValue(0, factoryRef);
			registry.registerBeanDefinition(templateBeanName, template);
		}
	}

	private void fillProp(MutablePropertyValues pvs, Properties prop, String field, String pKey) {
		Object value = prop.get(field);
		if (value == null) {
			value = "${orm.datasources.main." + pKey + "}";
		}
		pvs.addPropertyValue(field, value);
	}
}
