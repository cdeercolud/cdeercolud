package com.cdeercloud.common.orm.mybatis.factory;

import java.util.List;

import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.SqlSessionFactoryBean;

import com.cdeercloud.common.orm.Constants;
import com.cdeercloud.common.orm.mybatis.vo.TableModel;

/**
 * Smart定制SqlSessionFactoryBean，支持自动CRUD;
 * 
 * @author looyii
 *
 */
public class CdeerSqlSessionFactoryBean extends SqlSessionFactoryBean {
	private boolean smartJpaSupport = true;
	private String dataSourceName = Constants.DEFAULT_SMART_DATASOURCE_NAME;
	private TableMappingResolver resolver;

	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		if (smartJpaSupport) {
			Configuration config = this.getObject().getConfiguration();
			List<TableModel> tableModels = this.resolver.getTableModels(dataSourceName);
			AutoStatementBuilder builder = new AutoStatementBuilder(config, tableModels);
			builder.built();
		}
	}

	public void setSmartJpaSupport(boolean smartJpaSupport) {
		this.smartJpaSupport = smartJpaSupport;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public void setResolver(TableMappingResolver resolver) {
		this.resolver = resolver;
	}

}