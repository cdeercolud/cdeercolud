package com.cdeercloud.common.orm.interceptor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.ibatis.executor.resultset.DefaultResultSetHandler;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.RowBounds;

import com.cdeercloud.common.orm.Constants;
import com.cdeercloud.common.orm.vo.RowBoundsExtension;
import com.cdeercloud.common.util.ReflectUtils;

@Intercepts({ @Signature(type = ResultSetHandler.class, method = "handleResultSets", args = { Statement.class }) })
public class CdeerResultSetHandlerInterceptor implements Interceptor {

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		DefaultResultSetHandler resultSet = (DefaultResultSetHandler) invocation.getTarget();
		RowBounds rowBounds = (RowBounds) ReflectUtils.getFieldValue(resultSet, "rowBounds");
		boolean isCount = false;
		if (rowBounds instanceof RowBoundsExtension) {
			RowBoundsExtension ext = (RowBoundsExtension) rowBounds;
			isCount = (Boolean) ext.get(Constants.IS_SMART_COUNT_STATEMENT);
		}
		if (isCount) {
			return gainCountResult(invocation);
		} else if ((rowBounds.getLimit() > 0) && (rowBounds.getLimit() < RowBounds.NO_ROW_LIMIT)) {
			ReflectUtils.setFieldValue(resultSet, "rowBounds", new RowBounds());
		}
		return invocation.proceed();
	}

	private List<Integer> gainCountResult(Invocation invocation) throws SQLException {
		int count = 0;
		List<Integer> list = new ArrayList<Integer>();
		Object[] args = invocation.getArgs();
		if (args.length == 1) {
			if (args[0] instanceof Statement) {
				Statement stmt = (Statement) args[0];
				ResultSet rs = stmt.getResultSet();
				while (rs == null) {
					if (stmt.getMoreResults()) {
						rs = stmt.getResultSet();
					} else {
						if (stmt.getUpdateCount() == -1) {
							break;
						}
					}
				}
				if ((rs != null) && rs.next()) {
					count = rs.getInt(Constants.SMART_AUTO_COUNT_ALIAS);
				}
			}
		}
		list.add(count);
		return list;
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}

}
