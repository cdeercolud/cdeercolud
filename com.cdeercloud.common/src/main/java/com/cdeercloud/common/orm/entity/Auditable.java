package com.cdeercloud.common.orm.entity;

import java.util.Date;

/**
 * 公共字段
 * 
 * @author looyii
 *
 */
public interface Auditable {
	String getStatus();

	void setStatus(String status);

	Date getCreateTime();

	void setCreateTime(Date date);
}
