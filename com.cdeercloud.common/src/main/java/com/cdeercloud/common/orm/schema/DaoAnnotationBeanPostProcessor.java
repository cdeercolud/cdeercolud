package com.cdeercloud.common.orm.schema;

import java.lang.reflect.Field;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.ReflectionUtils;

import com.cdeercloud.common.orm.mybatis.util.EntityStatementUtils;
import com.cdeercloud.common.orm.service.IEntityStatement;

public class DaoAnnotationBeanPostProcessor implements BeanFactoryAware, BeanPostProcessor {
	private BeanFactory beanFactory;
	private ReflectionUtils.FieldFilter fieldFilter = new ReflectionUtils.FieldFilter() {
		@Override
		public boolean matches(Field field) {
			return field.getType() == IEntityStatement.class;
		}
	};

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		Package pkg = bean.getClass().getPackage();
		if (pkg != null) {
			String name = pkg.getName();
			if (!name.matches("com[.].*?[.]service[.]impl")) {
				return bean;
			}
		}
		ReflectionUtils.doWithFields(bean.getClass(), new ReflectionUtils.FieldCallback() {
			@Override
			public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
				BeanFactory beanFactory2 = DaoAnnotationBeanPostProcessor.this.beanFactory;
				EntityStatementUtils.injectionEntityStatement(beanFactory2, bean, field);
			}
		}, fieldFilter);
		return bean;
	}

}