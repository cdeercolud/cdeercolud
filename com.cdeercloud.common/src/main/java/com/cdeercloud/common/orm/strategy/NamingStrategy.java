package com.cdeercloud.common.orm.strategy;

import com.cdeercloud.common.orm.strategy.impl.CdeerDefaultNamingStrategy;

/**
 * 命名策略。
 * 
 * @author looyii
 *
 */
public interface NamingStrategy {
	public static final NamingStrategy DEFAULT = new CdeerDefaultNamingStrategy();

	public String classToTableName(String className);

	public String propertyToColumnName(String propertyName);

	public String foreignKeyColumnName(String propertyName);

}
