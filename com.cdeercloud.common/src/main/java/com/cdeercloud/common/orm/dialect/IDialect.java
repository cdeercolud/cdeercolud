package com.cdeercloud.common.orm.dialect;

public interface IDialect {
	public String getPagedString(String sql, int offset, int length);

	public boolean supportsPaged();
}
