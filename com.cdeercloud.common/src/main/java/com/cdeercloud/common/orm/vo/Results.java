package com.cdeercloud.common.orm.vo;

import java.util.ArrayList;
import java.util.List;

public class Results {
	private List<String> properties = new ArrayList<String>();

	public static Results include(String property, String... propertyN) {
		Results rcs = new Results();
		rcs.properties.add(property);
		for (String c : propertyN) {
			rcs.properties.add(c);
		}
		return rcs;
	}

	public List<String> getProperties() {
		return properties;
	}
}
