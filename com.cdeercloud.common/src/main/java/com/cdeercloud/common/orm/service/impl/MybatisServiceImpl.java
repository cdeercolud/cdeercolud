package com.cdeercloud.common.orm.service.impl;

import java.lang.reflect.Field;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.util.ReflectionUtils;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.core.util.ContextUtils;
import com.cdeercloud.common.core.vo.PagedResult;
import com.cdeercloud.common.orm.Constants;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.mybatis.util.EntityStatementUtils;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.IMybatisService;
import com.cdeercloud.common.orm.vo.RowBoundsExtension;

/**
 * Smart 服务层基类。
 * 
 * @author looyii
 *
 */
public class MybatisServiceImpl extends ApplicationObjectSupport implements IMybatisService, InitializingBean {
	private SqlSessionTemplate sqlSessionTemplate;

	/*
	 * 补偿措施，在BeanPostProcessor未处理的情况下，通过afterPropertiesSet方法注入EntityStatement
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			EntityStatement anno = field.getAnnotation(EntityStatement.class);
			if (anno != null) {
				field.setAccessible(true);
				Object object = ReflectionUtils.getField(field, this);
				if (object == null) {
					EntityStatementUtils.injectionEntityStatement(this.getApplicationContext(), this, field);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected <T> IEntityStatement<T> getEntityStatement(Class<T> clazz) {
		IEntityStatement<T> bean = (IEntityStatement<T>) ContextUtils.getBean(clazz.getSimpleName().toLowerCase() + "_dao");
		return bean;
	}

	// API(create)----------------------------------------------
	protected int createByStatement(String statement, Object object) {
		return this.getSqlSessionTemplate().insert(statement, object);
	}

	protected int updateByStatement(String statement, Object object) {
		return this.getSqlSessionTemplate().update(statement, object);
	}

	protected int deleteByStatement(String statement, Object object) {
		return this.getSqlSessionTemplate().delete(statement, object);
	}

	protected <T> List<T> findByStatement(String key, Object parameter) {
		return this.findByStatement(key, parameter, -1, -1);
	}

	protected <T> List<T> findByStatement(String statement, Object paramenter, int pageIndex, int pageSize) {
		if ((pageIndex == -1) && (pageSize == -1)) {
			return this.getSqlSessionTemplate().selectList(statement, paramenter);
		} else {
			RowBounds rb = this.getRowBounds(pageIndex, pageSize);
			return this.getSqlSessionTemplate().selectList(statement, paramenter, rb);
		}
	}

	protected <T> T findFirstByStatement(String key, Object parameter) {
		List<T> list = this.findByStatement(key, parameter);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}

	protected <T> List<T> listByStatement(String statement) {
		return this.getSqlSessionTemplate().selectList(statement);
	}

	protected <T> T countByStatement(String statement, Object paramenter) {
		return this.getSqlSessionTemplate().selectOne(statement, paramenter);
	}

	protected int batchCreateByStatement(final String statement, final List<?> list) {
		return new AbstractBatchExecutor() {
			@Override
			public int doExecute(SqlSession session) {
				// TODO 使用全局配置默认值
				return this.batchCreate(session, statement, list, 1000);
			}
		}.run();
	}

	protected int batchCreateByStatement(final String statement, final List<?> list, final int frequency) {
		return new AbstractBatchExecutor() {
			@Override
			public int doExecute(SqlSession session) {
				return this.batchCreate(session, statement, list, frequency);
			}
		}.run();
	}

	protected int batchUpdateByStatement(String statement, final List<?> list, final int frequency) {
		return new AbstractBatchExecutor() {
			@Override
			public int doExecute(SqlSession session) {
				return this.batchUpdate(session, statement, list, frequency);
			}
		}.run();
	}

	protected int batchUpdateByStatement(String statement, final List<?> list) {
		// TODO 使用全局配置默认值
		return new AbstractBatchExecutor() {
			@Override
			public int doExecute(SqlSession session) {
				return this.batchUpdate(session, statement, list, 1000);
			}
		}.run();
	}

	protected <T> PagedResult<T> pagedQueryByStatement(String statement, Object parameter, int pageIndex, int pageSize) {
		RowBounds rb = this.getRowBounds(pageIndex, pageSize);
		List<T> list = this.getSqlSessionTemplate().selectList(statement, parameter, rb);
		int total = countExt(statement, parameter);
		PagedResult<T> pr = new PagedResult<T>(list, total, pageIndex, pageSize);
		return pr;
	}

	protected int countExt(String statement, Object parameter) {
		String statementCount = statement + "_count";
		Configuration config = this.getSqlSessionTemplate().getConfiguration();
		int total = 0;
		if (config.hasStatement(statementCount)) {
			Object ret = this.getSqlSessionTemplate().selectOne(statementCount, parameter);
			if (ret instanceof Number) {
				Number number = (Number) ret;
				total = number.intValue();
			} else {
				total = -1;
			}
		} else {
			RowBoundsExtension rbExt = new RowBoundsExtension(1, 10);
			rbExt.set(Constants.IS_SMART_COUNT_STATEMENT, true);
			List<Object> result = this.getSqlSessionTemplate().selectList(statement, parameter, rbExt);
			if ((result != null) && (result.size() == 1)) {
				Object object = result.get(0);
				if (object instanceof Integer) {
					total = (Integer) object;
				}
			}
		}
		return total;
	}

	// utils-----------------------------------------------------
	protected RowBounds getRowBounds(int pageIndex, int pageSize) {
		if (pageSize == -1) {
			return RowBounds.DEFAULT;
		}
		if (pageIndex <= 0) {
			pageIndex = 1;
		}
		int offset = (pageIndex - 1) * pageSize;
		return new RowBounds(offset, pageSize);
	}

	protected String getStatementId(String key) {
		return this.getStatementId(key, null);
	}

	protected String getStatementId(String key, Class<?> clazz) {
		if (clazz == null) {
			clazz = this.getClass();
		}
		String statement = null;
		if ((key != null) && (key.indexOf(".") > -1)) {
			statement = key;
		} else {
			statement = clazz.getName() + "." + key;
		}
		this.checkStatementExist(statement);
		return statement;
	}

	private void checkStatementExist(String statement) {
		Configuration configuration = this.getSqlSessionTemplate().getConfiguration();
		if (!configuration.hasStatement(statement)) {
			throw new IllegalArgumentException("未找到MyBatis声明配置：" + statement);
		}
	}

	// setter---------------------------------------------------
	protected SqlSessionTemplate getSqlSessionTemplate() {
		return this.sqlSessionTemplate;
	}

	protected SqlSessionTemplate getSqlSessionTemplate(String datasource) {
		return (SqlSessionTemplate) ContextUtils.getBean("framework_core_sqlSessionTemplate_" + datasource);
	}

	@Resource(name = "framework_orm_sqlSessionTemplate")
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	abstract class AbstractBatchExecutor {
		public abstract int doExecute(SqlSession session);

		public int run() {
			int count = 0;
			SqlSession session = this.openBatchSession();
			try {
				count = this.doExecute(session);
			} catch (Exception e) {
				throw new CdeerException("批量操作时发生错误!", e);
			} finally {
				if (session != null) {
					session.close();
				}
			}
			return count;
		}

		private SqlSession openBatchSession() {
			SqlSessionFactory factory = MybatisServiceImpl.this.getSqlSessionTemplate().getSqlSessionFactory();
			SqlSession session = factory.openSession(ExecutorType.BATCH, false);
			return session;
		}

		protected int batchCreate(SqlSession session, String statementId, List<?> list, int frequency) {
			return this.batch(session, list, frequency, statementId, true);
		}

		protected int batchUpdate(SqlSession session, String statementId, List<?> list, int frequency) {
			return this.batch(session, list, frequency, statementId, false);
		}

		private int batch(SqlSession session, List<?> list, int frequency, String statementId, boolean isInsert) {
			int count = 0;
			for (int i = 0; i < list.size(); i++) {
				if ((i > 0) && ((i % frequency) == 0)) {
					session.flushStatements();
				}
				Object object = list.get(i);
				if (isInsert) {
					count += session.insert(statementId, object);
				} else {
					count += session.update(statementId, object);
				}
			}
			// TODO 优化刷新动作
			session.flushStatements();
			return count;
		}
	}
}
