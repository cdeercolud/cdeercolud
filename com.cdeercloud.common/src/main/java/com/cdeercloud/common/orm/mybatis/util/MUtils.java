package com.cdeercloud.common.orm.mybatis.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cdeercloud.common.orm.mybatis.vo.ColumnModel;
import com.cdeercloud.common.orm.mybatis.vo.TableModel;

public class MUtils {
	private static Map<String, TableModel> mapping = new HashMap<String, TableModel>();
	private static Map<String, Map<String, String>> cModels;

	public static void init(List<TableModel> tables) {
		Map<String, TableModel> m = new HashMap<String, TableModel>();
		for (TableModel model : tables) {
			m.put(model.getCls().getName(), model);
		}
		mapping = Collections.unmodifiableMap(m);
	}

	public static TableModel getTableModel(String className) {
		return mapping.get(className);
	}

	public static Map<String, String> getPCMapping(String className) {
		if (cModels == null) {
			Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();
			for (Map.Entry<String, TableModel> ent : mapping.entrySet()) {
				TableModel table = ent.getValue();
				List<ColumnModel> cols = table.getAllCols();
				HashMap<String, String> value = new HashMap<String, String>();
				for (ColumnModel cm : cols) {
					value.put(cm.getProperty(), cm.getColumn());
				}
				map.put(ent.getKey(), Collections.unmodifiableMap(value));
			}
			cModels = Collections.unmodifiableMap(map);
		}
		return cModels.get(className);
	}
}
