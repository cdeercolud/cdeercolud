package com.cdeercloud.common.orm.dialect;

/**
 * 针对MySQL数据库的方言。
 * 
 * @author looyii
 *
 */
public class MySQLDialect implements IDialect {
	/**
	 * 获得分页语句。
	 * 
	 * @param sql String
	 * @param offset int
	 * @param length int
	 * 
	 * @return String
	 */
	@Override
	public String getPagedString(String sql, int offset, int length) {
		sql = sql.trim();
		String forUpdateClause = null;
		boolean isForUpdate = false;
		// TODO 此处判断过于简单，需要改为正则表达式进行处理。
		final int forUpdateIndex = sql.toLowerCase().lastIndexOf("for update");
		if (forUpdateIndex > -1) {
			forUpdateClause = sql.substring(forUpdateIndex);
			sql = sql.substring(0, forUpdateIndex - 1);
			isForUpdate = true;
		}
		StringBuffer pagingSelect = new StringBuffer(sql.length() + 100);
		pagingSelect.append(sql);
		pagingSelect.append(" limit ").append(offset).append(" ,").append(length);
		if (isForUpdate) {
			pagingSelect.append(" ");
			pagingSelect.append(forUpdateClause);
		}
		return pagingSelect.toString();
	}

	/**
	 * 是否支持分页。
	 * 
	 * @return boolean
	 */
	@Override
	public boolean supportsPaged() {
		return true;
	}

}