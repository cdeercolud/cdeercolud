package com.cdeercloud.common.orm.interceptor;

import com.cdeercloud.common.orm.entity.Auditable;

public interface AuditableSetter {
	public void forInsert(Auditable audit);

	public void forUpdate(Auditable audit);
}
