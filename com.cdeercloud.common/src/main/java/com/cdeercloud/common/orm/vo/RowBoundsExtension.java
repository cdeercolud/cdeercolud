package com.cdeercloud.common.orm.vo;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

/**
 * 扩展的分页值对象 。
 * 
 * @author looyii
 *
 */
public class RowBoundsExtension extends RowBounds {
	private Map<String, Object> additions = new HashMap<String, Object>();

	public RowBoundsExtension() {
		super();
	}

	public RowBoundsExtension(int offset, int limit) {
		super(offset, limit);
	}

	public Object get(String key) {
		return this.additions.get(key);
	}

	public void set(String key, Object value) {
		this.additions.put(key, value);
	}
}
