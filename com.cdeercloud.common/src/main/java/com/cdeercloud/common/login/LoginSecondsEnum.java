package com.cdeercloud.common.login;

/**
 * token 保留时长
 * 
 * @author looyii
 *
 */
public enum LoginSecondsEnum {

	// APP登录token 30天有效
	APP_SECONDS(30 * 24 * 60 * 60),
	// web登录token 30分钟有效
	WEB_SECONDS((30 * 60));

	private int key;

	private LoginSecondsEnum(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}
}
