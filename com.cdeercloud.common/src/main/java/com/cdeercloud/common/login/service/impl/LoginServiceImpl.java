package com.cdeercloud.common.login.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.login.service.ILoginService;
import com.cdeercloud.common.login.vo.Profile;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.security.pwd.CdeerHashEncoder;

@Service
public class LoginServiceImpl extends MybatisServiceImpl implements ILoginService {

	@Resource
	private CdeerHashEncoder encoder;
	private static final String USER_ID = "id";
	private static final String MOBILE = "mobile";
	private static final String PASSWORD = "password";

	@Override
	public Profile findProfile(String loginId, String password) {
		Map map = this.findFirstByStatement("getLoginProfile", loginId);
		if ((map == null) || (map.size() <= 0)) {
			throw new CdeerException("用户不存在！");
		}

		Profile profile = new Profile();
		profile.setId(map.get(USER_ID).toString());
		String encode = this.encoder.encode(password, profile.getId());
		if (!map.get(PASSWORD).equals(encode)) {
			throw new CdeerException("用户密码错误！");
		}
		profile.setMoblie(map.get(MOBILE).toString());
		profile.setLoginId(loginId);
		return profile;
	}

}
