package com.cdeercloud.common.login.service;

import com.cdeercloud.common.login.vo.Profile;

/**
 * 登录接口实现类
 * 
 * @author looyii
 *
 */
public interface ILoginService {
	/**
	 * 通过用户ID登录
	 * 
	 * @param loginId
	 * @param password
	 * @return
	 */
	public Profile findProfile(String loginId, String password);
}
