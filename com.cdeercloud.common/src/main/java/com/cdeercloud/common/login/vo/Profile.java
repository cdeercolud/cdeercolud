package com.cdeercloud.common.login.vo;

import java.io.Serializable;

/**
 * 登录需要保存到session的数据
 *
 * @author looyii
 */
public class Profile implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7293358859038036402L;
	private String id;
	private String moblie;
	private String loginId;
	private String platform;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMoblie() {
		return moblie;
	}

	public void setMoblie(String moblie) {
		this.moblie = moblie;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

}
