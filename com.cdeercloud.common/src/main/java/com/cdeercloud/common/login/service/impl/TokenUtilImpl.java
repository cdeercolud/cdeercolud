package com.cdeercloud.common.login.service.impl;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.cdeercloud.common.util.HttpServletUtil;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.login.vo.Profile;
import com.cdeercloud.common.util.JWTUtil;
import com.cdeercloud.common.util.JsonUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class TokenUtilImpl implements ITokenUtil {

	private static final String TOKEN = "token";

	@Value("${security.request.header}")
	private String authHeaderName;

	@Override
	public String getToken() {
		HttpServletRequest request = WebUtils.toHttp(HttpServletUtil.getRequest());
		if (request == null) {
			return null;
		}
		String token = request.getHeader(this.authHeaderName);
		if (!StringUtils.hasText(token)) {
			token = request.getParameter(this.authHeaderName);
		}
		if (!StringUtils.hasText(token)) {
			token = request.getHeader(TOKEN.toLowerCase());
		}
		if (!StringUtils.hasText(token)) {
			token = request.getParameter(this.authHeaderName);
		}
		if (!StringUtils.hasText(token)) {
			Cookie cookie[] = request.getCookies();
			if ((cookie != null) && (cookie.length > 0)) {
				for (Cookie c : cookie) {
					if (TOKEN.equals(c.getName().toLowerCase())) {
						token = c.getValue();
						break;
					}
				}
			}
		}
		return token;
	}

	@Override
	public Profile getProfile() {
		Profile profile = JsonUtils.fromJson(JWTUtil.getProfile(this.getToken()), Profile.class);
		return profile;
	}

	@Override
	public String getUserId() {
		return this.getProfile().getId();
	}


}
