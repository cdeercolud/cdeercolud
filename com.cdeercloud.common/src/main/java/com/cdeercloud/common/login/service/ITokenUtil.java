package com.cdeercloud.common.login.service;

import javax.servlet.ServletRequest;

import com.cdeercloud.common.login.vo.Profile;

public interface ITokenUtil {

	/**
	 * 获取token
	 * @return
	 */
	public String getToken();

	/**
	 * 获取用户
	 * @return
	 */
	public Profile getProfile();

	/**
	 * 获取用户id
	 * @return
	 */
	public String getUserId();
}
