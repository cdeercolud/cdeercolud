package com.cdeercloud.common.login;

/**
 * 系统登录验证类型
 * 
 * @author looyii
 *
 */
public enum SecurityLoginTypeEnum {

	//默认类型
	DEFAULT("default", "默认类型"),
	//默认类型
	CRYPTER("crypter", "默认类型"),
	//默认类型
	BASIC_HTTP("basicHttp", "默认类型");

	private String code;
	private String desc;

	private SecurityLoginTypeEnum(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
