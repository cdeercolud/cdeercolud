package com.cdeercloud.common.group.service;

import com.cdeercloud.common.entities.base.entity.Group;

public interface IGroupService {

	/**
	 * 创建群组
	 * @param group
	 * @return
	 */
	public boolean createGroup(Group group);

	/**
	 * 查询群组
	 * 
	 * @param groupCode
	 * @return
	 */
	public Group getGroupByGroupCode(String groupCode);

	/**
	 * 更新群组
	 * 
	 * @param group
	 * @return
	 */
	public boolean updateGroup(Group group);
}
