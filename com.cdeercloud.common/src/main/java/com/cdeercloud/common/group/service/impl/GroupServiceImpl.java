package com.cdeercloud.common.group.service.impl;

import com.cdeercloud.common.constant.CommonConstant;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.entities.base.entity.Group;
import com.cdeercloud.common.entities.base.entity.GroupExpand;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.group.constant.GroupConstant;
import com.cdeercloud.common.group.service.IGroupService;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.orm.vo.ParamMap;
import com.cdeercloud.common.util.CodeGenerateUtil;
import com.cdeercloud.common.util.DateUtil;
import com.cdeercloud.common.util.PinyinUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class GroupServiceImpl extends MybatisServiceImpl implements IGroupService {

	@EntityStatement
	private IEntityStatement<Group> groupDao;

	@EntityStatement
	private IEntityStatement<GroupExpand> groupExpandDao;

	@Override
	public boolean createGroup(Group group) {

		String code = CodeGenerateUtil.generateCode(GroupConstant.CODE_NUMBER);
		group.setGroupCode(code);
		group.setPinyin(PinyinUtil.chineseToPinyin(group.getName()));
		group.setPrefix(StringUtils.substring(group.getPinyin(), 0, 1));
		group.setCreateTime(DateUtil.getNow());
		group.setModifyTime(DateUtil.getNow());
		group.setLevelPath(group.getParentCode() == null ? "" + code : group.getParentCode() + code);
		group.setStatus(CommonStatusEnum.NORMAL.getValue());
		this.groupDao.create(group);
		GroupExpand groupExpand = group.getGroupExpand();
		if (group.getGroupExpand() == null) {
			groupExpand = new GroupExpand();
			groupExpand.setGroupCode(code);
		} else {
			groupExpand.setGroupCode(code);
		}
		this.groupExpandDao.create(groupExpand);
		return true;

	}

	@Override
	public Group getGroupByGroupCode(String groupCode) {

		ParamMap paramMap = new ParamMap();
		paramMap.ne("status", CommonStatusEnum.DELETE_D.getValue());
		paramMap.add("groupCode", groupCode);
		Group group = this.groupDao.findFirstBy(paramMap);
		if (group == null) {
			throw new CdeerException(CommonConstant.DATA_NOT_AVAILABLE);
		}
		group.setGroupExpand(this.groupExpandDao.findFirstBy("groupCode", group.getGroupCode()));
		return group;

	}

	@Override
	public boolean updateGroup(Group group) {

		if ((group == null) || (group.getGroupCode() == null)) {
			throw new CdeerException(CommonConstant.PARAM_IS_NOT_NULL);
		}

		// 验证群组是否存在
		boolean checkExist = this.groupDao.checkExist("groupCode", group.getGroupCode());
		if (!checkExist) {
			throw new CdeerException(CommonConstant.DATA_NOT_AVAILABLE);
		}

		if (CommonStatusEnum.NORMAL.getValue().equals(group.getStatus())) {
			group.setModifyTime(DateUtil.getNow());
			this.groupDao.update(group);
			if (group.getGroupExpand() != null) {
				this.groupExpandDao.update(group.getGroupExpand());
			}
		} else {
			group.setModifyTime(DateUtil.getNow());
			this.groupDao.update(group, false);
		}
		return true;

	}

}
