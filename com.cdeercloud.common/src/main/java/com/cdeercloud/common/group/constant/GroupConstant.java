package com.cdeercloud.common.group.constant;

/**
 *
 * @Description:
 * @Auther: looyii
 * @Date: 2018/7/18 23:36
 */
public class GroupConstant {

    /**
     * 群组编号位数
     */
    public static final int CODE_NUMBER = 8;

}
