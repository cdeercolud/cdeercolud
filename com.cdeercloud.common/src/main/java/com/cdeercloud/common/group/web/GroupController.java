package com.cdeercloud.common.group.web;

import com.cdeercloud.common.entities.base.entity.Group;
import com.cdeercloud.common.group.service.IGroupService;
import com.cdeercloud.common.web.CdeerController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "校群、班级、群组", tags = {"群组_公共 GroupController"})
@RestController("group")
public class GroupController extends CdeerController {

	@Autowired
	private IGroupService groupService;

	@PostMapping("/create")
    @ApiOperation(value = "创建群组", notes = "根据Group实体对象创建")
	public boolean createGroup(@ApiParam(required = true, value = "群组参数") @RequestBody Group group) {

		return this.groupService.createGroup(group);

	}

	@PostMapping("/update")
    @ApiOperation(value = "修改群组", notes = "根据Group实体对象修改")
	public boolean updateGroup(@ApiParam(required = true, value = "群组参数") @RequestBody Group group) {

		return this.groupService.updateGroup(group);

	}

	@GetMapping("/getGroupByCode/{groupCode}")
	@ApiOperation(value = "群组详情", notes = "根据群组编号查看详情")
	public Group getGroupByCode(@ApiParam(required = true, value = "群组编号") @PathVariable String groupCode) {

	    return this.groupService.getGroupByGroupCode(groupCode);

    }
}
