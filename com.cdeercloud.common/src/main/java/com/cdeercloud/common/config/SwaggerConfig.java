package com.cdeercloud.common.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//注解开启 swagger2 功能
@Configuration
@EnableSwagger2
@Profile({ "dev" })
public class SwaggerConfig extends WebMvcConfigurerAdapter {
	/**
	 * 这个地方要重新注入一下资源文件，不然不会注入资源的，也没有注入requestHandlerMappping,相当于xml配置的
	 * <!--swagger资源配置-->
	 * <mvc:resources location="classpath:/META-INF/resources/" mapping="swagger-ui.html"/>
	 * <mvc:resources location="classpath:/META-INF/resources/webjars/" mapping="/webjars/**"/>
	 * 不知道为什么，这也是spring boot的一个缺点（菜鸟觉得的）
	 * 
	 * @param registry
	 */
	// @Override
	// public void addResourceHandlers(ResourceHandlerRegistry registry) {
	// registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
	// registry.addResourceHandler("/webjars*").addResourceLocations("classpath:/META-INF/resources/webjars/");
	// }

	/**
	 * 通过 createRestApi函数来构建一个DocketBean
	 * 函数名,可以随意命名,喜欢什么命名就什么命名
	 */
	@Bean
	public Docket createReportApi() {

		Set<String> contentTypes = new HashSet<>();
		contentTypes.add("application/json");
		return new Docket(DocumentationType.SWAGGER_2).groupName("cdeercolud").useDefaultResponseMessages(true).consumes(contentTypes).produces(contentTypes).apiInfo(apiInfo())// 调用apiInfo方法,创建一个ApiInfo实例,里面是展示在文档页面信息内容
				.select()
				// 控制暴露出去的路径下的实例
				// 如果某个接口不想暴露,可以使用以下注解
				// @ApiIgnore 这样,该接口就不会暴露在 swagger2 的页面下
				.apis(RequestHandlerSelectors.basePackage("com.cdeercloud")).paths(PathSelectors.any()).build();
	}

	/**
	 * 构建 api文档的详细信息函数
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				// 页面标题
				.title("cdeercloud服务 API")
				// 创建人
				// 版本号
				.version("1.0")
				// 描述
				.description("cdeercloud api接口").build();
	}
}
