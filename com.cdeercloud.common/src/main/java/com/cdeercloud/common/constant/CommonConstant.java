package com.cdeercloud.common.constant;

/**
 *
 * 公共常量
 * @Auther: looyii
 * @Date: 2018/7/19 00:34
 * @Description:
 */
public class CommonConstant {

    /**
     * 数据不存在！
     */
    public static final String DATA_NOT_AVAILABLE = "数据不存在！";


    //----- 请求相关 ------

    /**
     * 请求数据为空！
     */
    public static final String PARAM_IS_NOT_NULL = "请求数据为空！";

}
