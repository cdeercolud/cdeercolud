package com.cdeercloud.common.mq.consumer;

public interface CdeerConsumer {
	public void consume();

	public void shutdown();
}
