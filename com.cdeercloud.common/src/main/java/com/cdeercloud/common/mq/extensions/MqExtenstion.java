package com.cdeercloud.common.mq.extensions;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.util.StringUtils;

import com.cdeercloud.common.core.config.ConfigHolder;
import com.cdeercloud.common.core.definition.BeanDefinitionInitializer;
import com.cdeercloud.common.mq.MqException;
import com.cdeercloud.common.mq.consumer.impl.OCPTConsumer;
import com.cdeercloud.common.mq.producer.CdeerProducer;

public class MqExtenstion implements BeanDefinitionInitializer {

	@Override
	public void init(BeanDefinitionRegistry registry, Properties properties) {
		ConfigHolder holder = new ConfigHolder(properties);
		Map<String, Properties> map = holder.getSubProperties("mq.producer", true);
		Properties defaultProducerConfig = holder.getProperties("mq.config.producer", true);
		for (Map.Entry<String, Properties> ent : map.entrySet()) {
			RootBeanDefinition def = new RootBeanDefinition(CdeerProducer.class);
			def.setScope(BeanDefinition.SCOPE_SINGLETON);
			def.setDestroyMethodName("closeByContainer");
			def.setLazyInit(true);
			Properties config = ent.getValue();
			this.copyDefault(config, defaultProducerConfig, "acks");
			this.copyDefault(config, defaultProducerConfig, "retries");
			this.copyDefault(config, defaultProducerConfig, "batch.size");
			this.copyDefault(config, defaultProducerConfig, "linger.ms");
			this.copyDefault(config, defaultProducerConfig, "buffer.memory");

			def.getConstructorArgumentValues().addIndexedArgumentValue(0, config);
			String beanName = "framework_mq_" + ent.getKey() + "Producer";
			registry.registerBeanDefinition(beanName, def);
		}
		Properties defaultConsumerConfig = holder.getProperties("mq.config.consumer", true);
		Map<String, Properties> mc = holder.getSubProperties("mq.consumer", true);
		for (Map.Entry<String, Properties> ent : mc.entrySet()) {
			Properties p = ent.getValue();
			Object mode = this.getWithDefault(p, defaultConsumerConfig, "mode");

			Properties config = new Properties();
			if ("ocpt".equals(mode)) {
				RootBeanDefinition def = new RootBeanDefinition(OCPTConsumer.class);
				MutablePropertyValues propertyValues = def.getPropertyValues();

				propertyValues.add("autoActive", this.getWithDefault(p, defaultConsumerConfig, "autoActive"));
				propertyValues.add("nThreads", this.getWithDefault(p, defaultConsumerConfig, "nThreads"));
				propertyValues.add("handler", new RuntimeBeanReference(p.getProperty("handler")));
				propertyValues.add("pollTimeout", this.getWithDefault(p, defaultConsumerConfig, "pollTimeout"));
				String str = (String) p.get("topics");
				String[] split = str.split(",");
				propertyValues.add("topics", Arrays.asList(split));

				this.copyDefault(config, defaultConsumerConfig, "enable.auto.commit");
				this.copyDefault(config, defaultConsumerConfig, "auto.commit.interval.ms");
				this.copyDefault(config, defaultConsumerConfig, "session.timeout.ms");

				this.copyMustProperty(config, p, "bootstrap.servers");
				this.copyMustProperty(config, p, "group.id");
				this.copyMustProperty(config, p, "key.deserializer");
				this.copyMustProperty(config, p, "value.deserializer");

				propertyValues.add("config", config);
				def.setDestroyMethodName("shutdown");
				registry.registerBeanDefinition("framework_mq_" + ent.getKey() + "Consumer", def);
			} else if ("dcp".equals(mode)) {
				throw new MqException("暂未实现：" + mode);
			} else {
				throw new MqException("不支持消息消费模式：" + mode);
			}
		}
	}

	private void copyMustProperty(Properties config, Properties p, String key) {
		String value = p.getProperty(key);
		if (!StringUtils.hasText(value)) {
			throw new MqException("消费者必须配置key:" + key);
		}
		config.put(key, value);
	}

	private void copyDefault(Properties config, Properties defaultConfig, String key) {
		if (config.get(key) == null) {
			config.put(key, defaultConfig.get(key));
		}
	}

	private Object getWithDefault(Properties config, Properties defaultConfig, String key) {
		if (config.get(key) != null) {
			return config.get(key);
		} else {
			return defaultConfig.get(key);
		}

	}

}
