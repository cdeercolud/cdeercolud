package com.cdeercloud.common.mq.serialization;

import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;

import com.cdeercloud.common.util.SerializeUtils;

public class EwpSerializeObject implements Serializer<Object> {
	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {

	}

	@Override
	public byte[] serialize(String topic, Object data) {
		return SerializeUtils.serialize(data);
	}

	@Override
	public void close() {

	}

}