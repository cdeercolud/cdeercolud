package com.cdeercloud.common.mq.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecords;

/**
 * 
 * @author looyii
 *
 * @param <K>
 * @param <V>
 */
public interface ConsumerHandler<K, V> {
	public void doHandle(ConsumerRecords<K, V> records);
}
