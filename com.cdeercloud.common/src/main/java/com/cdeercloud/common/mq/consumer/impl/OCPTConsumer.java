package com.cdeercloud.common.mq.consumer.impl;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.cdeercloud.common.mq.consumer.CdeerConsumer;
import com.cdeercloud.common.mq.consumer.ConsumerHandler;

/**
 * One Consumer Per Thread 消费模式。
 * 
 * @author looyii
 *
 * @param <K>
 * @param <V>
 */
public class OCPTConsumer<K, V> implements CdeerConsumer, ApplicationListener<ContextRefreshedEvent> {

	private ConsumerHandler<K, V> handler;
	private int nThreads;
	private int pollTimeout;
	private List<String> topics;
	private Properties config;
	private boolean hasInit;
	private boolean autoActive;
	private ExecutorService executor;

	static final Logger logger = LoggerFactory.getLogger(OCPTConsumer.class);

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (this.autoActive) {
			this.consume();
		}
	}

	@Override
	public void consume() {
		if (this.hasInit) {
			logger.warn("消费者已初始，忽略重复初始化尝试。");
			return;
		}
		this.hasInit = true;
		executor = Executors.newFixedThreadPool(nThreads);
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Consumer<K, V> consumer = new KafkaConsumer<>(config);
				consumer.subscribe(topics);
				try {
					while (true) {
						ConsumerRecords<K, V> crs = consumer.poll(pollTimeout);
						handler.doHandle(crs);
					}
				} catch (Exception e) {
					logger.error("", e);
				} finally {
					consumer.close();
				}
			}
		};
		for (int i = 0; i < nThreads; i++) {
			executor.submit(runnable);
		}
	}

	@Override
	public void shutdown() {
		if ((this.executor != null) && !this.executor.isShutdown()) {
			executor.shutdown();
		}
	}

	public void setHandler(ConsumerHandler<K, V> handler) {
		this.handler = handler;
	}

	public void setnThreads(int nThreads) {
		this.nThreads = nThreads;
	}

	public void setPollTimeout(int pollTimeout) {
		this.pollTimeout = pollTimeout;
	}

	public void setTopics(List<String> topics) {
		this.topics = topics;
	}

	public void setConfig(Properties config) {
		this.config = config;
	}

	public void setAutoActive(boolean autoActive) {
		this.autoActive = autoActive;
	}

}
