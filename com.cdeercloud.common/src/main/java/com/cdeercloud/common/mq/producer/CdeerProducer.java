package com.cdeercloud.common.mq.producer;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.serialization.Serializer;

/**
 * 生产
 * 
 * @author looyii
 *
 * @param <K>
 * @param <V>
 */
public class CdeerProducer<K, V> extends KafkaProducer<K, V> {

	public CdeerProducer(Map<String, Object> configs) {
		super(configs);
	}

	public CdeerProducer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
		super(configs, keySerializer, valueSerializer);
	}

	public CdeerProducer(Properties properties) {
		super(properties);
	}

	public CdeerProducer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
		super(properties, keySerializer, valueSerializer);
	}

	@Override
	public void close() {
		throw new RuntimeException("不允许手动调用关闭操作。");
	}

	public void closeByContainer() {
		super.close();
	}

	@Override
	public void close(long timeout, TimeUnit timeUnit) {
		throw new RuntimeException("不允许手动调用关闭操作。");
	}

	public void closeByContainer(long timeout, TimeUnit timeUnit) {
		super.close(timeout, timeUnit);
	}

}
