package com.cdeercloud.common.mq.serialization;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;

import com.cdeercloud.common.util.SerializeUtils;

public class EwpDeserializeObject implements Deserializer<Object> {

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {

	}

	@Override
	public Object deserialize(String topic, byte[] data) {
		return SerializeUtils.deserialize(data);
	}

	@Override
	public void close() {

	}

}
