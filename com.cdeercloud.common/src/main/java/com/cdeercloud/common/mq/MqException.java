package com.cdeercloud.common.mq;

import com.cdeercloud.common.core.exception.CdeerException;

public class MqException extends CdeerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6660839339757841696L;

	/**
	 * 默认的构造方法。
	 */
	public MqException() {
	}

	/**
	 * 自定义的构造方法。
	 * 
	 * @param cause Throwable
	 */
	public MqException(Throwable cause) {
		super(cause);
	}

	/**
	 * 自定义的构造方法。
	 * 
	 * @param message String
	 */
	public MqException(String message) {
		super(message);
	}

	/**
	 * 自定义的构造方法。
	 * 
	 * @param message String
	 * @param cause Throwable
	 */
	public MqException(String message, Throwable cause) {
		super(message, cause);
	}
}
