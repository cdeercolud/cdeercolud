package com.cdeercloud.common.web;

import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.util.HttpServletUtil;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * Cdeer Controller基类。
 * 
 * @author looyii
 *
 */
public class CdeerController {
	static final Logger logger = LoggerFactory.getLogger(CdeerController.class);
	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private final static ObjectMapper OBJECT_MAPPER_IGNORE = new ObjectMapper();

	static {
		OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		OBJECT_MAPPER_IGNORE.setSerializationInclusion(Include.NON_NULL);
		OBJECT_MAPPER_IGNORE.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	protected int pageIndex() {
		HttpServletRequest request = HttpServletUtil.getRequest();
		String str = request.getParameter("pageIndex");
		if ((str == null) && !StringUtils.hasText(str)) {
			return 1;
		}
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException e) {
			logger.info("pageIndex 参数转换出现错误!", e);
		}
		return 1;
	}

	protected int pageSize() {
		HttpServletRequest request = HttpServletUtil.getRequest();
		String str = request.getParameter("pageSize");
		if ((str == null) && !StringUtils.hasText(str)) {
			return 10;
		}
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException e) {
			logger.info("pageSize 参数转换出现错误!", e);
		}
		return 10;
	}

	protected String toJson(Object object) {
		return this.toJson(object, false);
	}

	protected String toJson(Object object, boolean ignoreNull) {
		try {
			if (ignoreNull) {
				return OBJECT_MAPPER_IGNORE.writeValueAsString(object);
			} else {
				return OBJECT_MAPPER.writeValueAsString(object);
			}
		} catch (Exception e) {
			throw new CdeerException("JSON数据转换时出现错误:" + object.getClass().getName(), e);
		}
	}

	protected <T> T parseJson(String content, Class<T> type) {
		try {
			return OBJECT_MAPPER.readValue(content, type);
		} catch (Exception e) {
			throw new CdeerException("解析JSON字符串时发生错误。", e);
		}
	}

	@SuppressWarnings("unchecked")
	protected List<Map<String, Object>> parseJsonList(String content) {
		try {
			return OBJECT_MAPPER.readValue(content, List.class);
		} catch (Exception e) {
			throw new CdeerException("解析JSON字符串时发生错误。", e);
		}
	}

	protected <T> List<T> parseJsonList(String content, Class<T> clazz) {
		try {
			TypeFactory typeFactory = OBJECT_MAPPER.getTypeFactory();
			CollectionType collectionType = typeFactory.constructCollectionType(List.class, clazz);
			return OBJECT_MAPPER.readValue(content, collectionType);
		} catch (Exception e) {
			throw new CdeerException("解析JSON字符串时发生错误。", e);
		}
	}

	protected ObjectMapper getObjectMapper() {
		return OBJECT_MAPPER;
	}

	/**
	 * 文件下载
	 * 
	 * @param response
	 * @param filePath
	 */
	protected void downloadFile(HttpServletResponse response, String filePath) {
		String[] arr = filePath.split("/");
		this.downloadFile(response, filePath, arr[arr.length - 1]);
	}

	/**
	 * 文件下载
	 * 
	 * @param response
	 * @param downloadName
	 * @throws IOException
	 */
	protected void downloadFile(HttpServletResponse response, InputStream in, String downloadName) throws IOException {
		try {
			response.setContentType("multipart/form-data");
			response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(downloadName, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new CdeerException("下载文件名编码时出现错误.", e);
		}

		byte[] buf = new byte[1024];
		int len = -1;
		ServletOutputStream out = response.getOutputStream();
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		out.flush();

	}

	/**
	 * 文件下载，可以下载放在META-INF/downloads目录下的文件
	 * 
	 * @param response
	 * @param filePath
	 * @param downloadName
	 */
	protected void downloadFile(HttpServletResponse response, String filePath, String downloadName) {
		try {
			response.setContentType("multipart/form-data");
			response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(downloadName, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new CdeerException("下载文件名编码时出现错误.", e);
		}
		InputStream in = null;
		try {
			ClassLoader loader = this.getClass().getClassLoader();
			String path = "META-INF/downloads/" + filePath;
			in = loader.getResourceAsStream(path);
			this.downloadFile(response, in, downloadName);
		} catch (IOException e) {
			throw new CdeerException("read temp error....", e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				logger.info("关闭输入流时出现错误。", e);
			}
		}
	}
}
