package com.cdeercloud.common.sahara.boot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.util.StringUtils;

public class CdeerApplication implements EmbeddedServletContainerCustomizer {
	@Value("${boot.application.port}")
	private int port;
	@Value("${boot.application.sessionTimeout}")
	private int sessionTimeout;
	@Value("${boot.application.contextPath}")
	private String contextPath;
	@Value("${boot.application.displayName}")
	private String displayName;

	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		container.setPort(this.port);
		container.setSessionTimeout(this.sessionTimeout);
		if (StringUtils.hasText(this.contextPath)) {
			container.setContextPath(this.contextPath);
		}
		if (StringUtils.hasText(this.displayName)) {
			container.setDisplayName(this.displayName);
		}
	}

}
