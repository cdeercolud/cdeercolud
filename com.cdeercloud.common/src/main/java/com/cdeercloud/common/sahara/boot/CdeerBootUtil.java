package com.cdeercloud.common.sahara.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import com.cdeercloud.common.core.definition.CdeerBeanDefinitionRegistryPostProcessor;

public abstract class CdeerBootUtil {
	public static void addInitializers(SpringApplication application, String[] args) {
		application.addInitializers(new ApplicationContextInitializer<ConfigurableApplicationContext>() {
			@Override
			public void initialize(ConfigurableApplicationContext applicationContext) {
				SimpleCommandLinePropertySource ps = new SimpleCommandLinePropertySource(args);

				CdeerBeanDefinitionRegistryPostProcessor processor = new CdeerBeanDefinitionRegistryPostProcessor(applicationContext, ps);
				applicationContext.addBeanFactoryPostProcessor(processor);
			}
		});
	}
}
