package com.cdeercloud.app.entities.message.entity;

import com.cdeercloud.common.orm.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "应用消息评论表", description = "应用消息评论表")
@CdeerEntity
@CdeerTable(name = "t_message_reply")
@JsonInclude(value = Include.NON_NULL)
public class MessageReply implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6741686414538253998L;
	@ApiModelProperty(value = "主键id")
	private String id;

    @ApiModelProperty(value = "* 应用消息id Message.id", required = true)
	private String msgId;

    @ApiModelProperty(value = "* 评论内容", required = true)
	private String content;

    @ApiModelProperty(value = "* 评论用户id User.id", required = true)
	private String userId;

    @ApiModelProperty(value = "回复对象ID")
	private String replyUserId;

    @ApiModelProperty(value = "@用户")
	private String notifys;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    @ApiModelProperty(value = "用户名")
    private String name;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "回复对象名称")
    private String replyUserName;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.AUTO)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getReplyUserId() {
		return replyUserId;
	}

	public void setReplyUserId(String replyUserId) {
		this.replyUserId = replyUserId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNotifys() {
		return notifys;
	}

	public void setNotifys(String notifys) {
		this.notifys = notifys;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

    @CdeerTransient
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @CdeerTransient
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @CdeerTransient
    public String getReplyUserName() {
        return replyUserName;
    }

    public void setReplyUserName(String replyUserName) {
        this.replyUserName = replyUserName;
    }
}
