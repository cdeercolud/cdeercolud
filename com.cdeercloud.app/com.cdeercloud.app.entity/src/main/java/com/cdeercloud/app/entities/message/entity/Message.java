package com.cdeercloud.app.entities.message.entity;

import com.cdeercloud.common.orm.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "应用消息表", description = "应用消息表")
@CdeerEntity
@CdeerTable(name = "t_message")
@JsonInclude(value = Include.NON_NULL)
public class Message implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3925396399999348321L;

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "群消息序号， 唯一，相当于主键")
	private Integer serialId;

	@ApiModelProperty(value = "* 消息类型", required = true)
	private String type;

	@ApiModelProperty(value = "新建人")
	private String userId;

	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "* 内容", required = true)
	private String content;

	@ApiModelProperty(value = "* 群组id", required = true)
	private String groupCode;

	@ApiModelProperty(value = "是否可以评论")
	private String isComment;

	@ApiModelProperty(value = "父消息")
	private String parentId;

	@ApiModelProperty(value = "是否公开（1：公开，0：私有）")
	private String isPrivate;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "子消息数量")
	private Integer childCount;

	@ApiModelProperty(value = "点赞数")
	private Integer praiseCount;

	@ApiModelProperty(value = "已阅数")
	private Integer readCount;

	@ApiModelProperty(value = "分享数")
	private Integer shareCount;

	@ApiModelProperty(value = "评价人数")
	private Integer reviewCount;

	@ApiModelProperty(value = "收藏次数")
	private Integer collectCount;

    @ApiModelProperty(value = "消息回复数")
    private Integer replyCount;

	@ApiModelProperty(value = "是否显示（1：显示，0：不显示）【页面上不显示子消息】")
	private String isShow;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

    @ApiModelProperty(value = "消息扩展表")
	private MessageExpand messageExpand;

    @ApiModelProperty(value = "消息媒体表")
	private List<MessageMedia> medias;

    @ApiModelProperty(value = "用户相关表")
	private List<MessagePerson> persons;

    @ApiModelProperty(value = "用户点赞")
    private List<MessagePraise> praises;

    @ApiModelProperty(value = "消息回复")
    private List<MessageReply> replys;

    @ApiModelProperty(value = "子消息")
	private List<Message> childs;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getSerialId() {
		return serialId;
	}

	public void setSerialId(Integer serialId) {
		this.serialId = serialId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getIsComment() {
		return isComment;
	}

	public void setIsComment(String isComment) {
		this.isComment = isComment;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(String isPrivate) {
		this.isPrivate = isPrivate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public Integer getPraiseCount() {
		return praiseCount;
	}

	public void setPraiseCount(Integer praiseCount) {
		this.praiseCount = praiseCount;
	}

	public Integer getReadCount() {
		return readCount;
	}

	public void setReadCount(Integer readCount) {
		this.readCount = readCount;
	}

	public Integer getShareCount() {
		return shareCount;
	}

	public void setShareCount(Integer shareCount) {
		this.shareCount = shareCount;
	}

	public Integer getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(Integer reviewCount) {
		this.reviewCount = reviewCount;
	}

    public Integer getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(Integer collectCount) {
        this.collectCount = collectCount;
    }

    public Integer getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(Integer replyCount) {
        this.replyCount = replyCount;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public MessageExpand getMessageExpand() {
        return messageExpand;
    }

    public void setMessageExpand(MessageExpand messageExpand) {
        this.messageExpand = messageExpand;
    }

    public List<MessageMedia> getMedias() {
        return medias;
    }

    public void setMedias(List<MessageMedia> medias) {
        this.medias = medias;
    }

    public List<MessagePerson> getPersons() {
        return persons;
    }

    public void setPersons(List<MessagePerson> persons) {
        this.persons = persons;
    }

    public List<MessagePraise> getPraises() {
        return praises;
    }

    public List<MessageReply> getReplys() {
        return replys;
    }

    public void setReplys(List<MessageReply> replys) {
        this.replys = replys;
    }

    public void setPraises(List<MessagePraise> praises) {
        this.praises = praises;
    }

    public List<Message> getChilds() {
        return childs;
    }

    public void setChilds(List<Message> childs) {
        this.childs = childs;
    }
}
