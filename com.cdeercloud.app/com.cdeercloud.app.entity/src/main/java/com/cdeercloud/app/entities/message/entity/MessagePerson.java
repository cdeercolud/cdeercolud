package com.cdeercloud.app.entities.message.entity;

import com.cdeercloud.common.orm.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "应用消息用户相关表", description = "应用消息用户相关表")
@CdeerEntity
@CdeerTable(name = "t_message_person")
@JsonInclude(value = Include.NON_NULL)
public class MessagePerson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1757112644387537837L;

	@ApiModelProperty(value = "主键id")
	private String id;

    @ApiModelProperty(value = "* 应用消息id Message.id", required = true)
	private String msgId;

    @ApiModelProperty(value = "* 1：相关人 TO、 2：责任人 、3：创建人 CREATE", required = true)
	private String type;

    @ApiModelProperty(value = "* 用户id/群组id User.id/Group.groupCode", required = true)
	private String relationId;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

	@ApiModelProperty(value = "* 用户类型（u:用户，g：群组）", required = true)
	private String personType;

	@ApiModelProperty(value = "用户/群组头像 查询参数")
	private String avatar;

    @ApiModelProperty(value = "用户/群组名称 查询参数")
	private String name;

    @ApiModelProperty(value = "用户/群组拼音 查询参数")
	private String pinyin;

    @ApiModelProperty(value = "用户/群组拼音第一个单词 查询参数")
	private String prefix;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRelationId() {
		return relationId;
	}

	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	@CdeerTransient
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@CdeerTransient
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@CdeerTransient
	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	@CdeerTransient
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
