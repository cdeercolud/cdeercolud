package com.cdeercloud.app.entities.message.entity;

import java.io.Serializable;
import java.util.Date;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "应用消息收藏表", description = "应用消息收藏表")
@CdeerEntity
@CdeerTable(name = "t_message_collect")
@JsonInclude(value = Include.NON_NULL)
public class MessageCollect implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5688548869740105201L;

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "* 应用消息id Message.id", required = true)
	private String msgId;

	@ApiModelProperty(value = "* 用户id User.id", required = true)
	private String userId;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.AUTO)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

}
