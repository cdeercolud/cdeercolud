package com.cdeercloud.app.entities.message.entity;

import java.io.Serializable;

import com.cdeercloud.common.orm.annotation.CdeerEntity;
import com.cdeercloud.common.orm.annotation.CdeerGeneratedValue;
import com.cdeercloud.common.orm.annotation.CdeerGenerationType;
import com.cdeercloud.common.orm.annotation.CdeerId;
import com.cdeercloud.common.orm.annotation.CdeerTable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * 
 * @author looyii
 *
 */
@ApiModel(value = "应用消息扩展表", description = "应用消息扩展表")
@CdeerEntity
@CdeerTable(name = "t_message_expand")
@JsonInclude(value = Include.NON_NULL)
public class MessageExpand implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7150294839372887844L;

	@ApiModelProperty(value = "主键id")
	private String id;

    @ApiModelProperty(value = "* 应用消息id Message.id", required = true)
	private String msgId;

    @ApiModelProperty(value = "扩展字段",required =  false)
	private String msgJson;

    @ApiModelProperty(value = "客户端ID",required = false)
	private String clientId;
	//
    @ApiModelProperty(value = "消息来源（1：web、2：IOS、3：Android）")
	private String clientSource;
	//
    @ApiModelProperty(value = "@用户")
	private String notifys;

	@CdeerId
	@CdeerGeneratedValue(strategy = CdeerGenerationType.UUID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMsgJson() {
		return msgJson;
	}

	public void setMsgJson(String msgJson) {
		this.msgJson = msgJson;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSource() {
		return clientSource;
	}

	public void setClientSource(String clientSource) {
		this.clientSource = clientSource;
	}

	public String getNotifys() {
		return notifys;
	}

	public void setNotifys(String notifys) {
		this.notifys = notifys;
	}

}
