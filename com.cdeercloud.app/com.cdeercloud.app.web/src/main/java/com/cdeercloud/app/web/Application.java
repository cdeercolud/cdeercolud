package com.cdeercloud.app.web;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import com.cdeercloud.common.core.logger.LogbackUtils;
import com.cdeercloud.common.sahara.boot.CdeerApplication;
import com.cdeercloud.common.sahara.boot.CdeerBootUtil;

/**
 * Web 启动类。
 * 
 * @author looyii
 *
 */
@SpringBootApplication
@ImportResource("classpath*:META-INF/conf/spring/*.xml")
public class Application extends CdeerApplication {

	public static void main(String[] args) {
		System.out.println(args);
		LogbackUtils.initLogbackPropertyDefiner(args);
		SpringApplication application = new SpringApplication(new Object[] { Application.class });
		CdeerBootUtil.addInitializers(application, args);
		application.run(args);
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		// 文件最大
		factory.setMaxFileSize("500MB"); // KB,MB
		// 设置总上传数据总大小
		factory.setMaxRequestSize("500MB");
		return factory.createMultipartConfig();
	}
}
