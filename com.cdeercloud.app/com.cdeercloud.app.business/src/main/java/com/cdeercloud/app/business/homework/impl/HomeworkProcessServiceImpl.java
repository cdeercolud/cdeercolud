package com.cdeercloud.app.business.homework.impl;

import com.cdeercloud.app.base.message.dto.MessageQueryParam;
import com.cdeercloud.app.base.message.service.IMessageProcessService;
import com.cdeercloud.app.base.message.service.impl.MessageProcessServiceImpl;
import com.cdeercloud.app.entities.message.entity.Message;
import com.cdeercloud.common.enums.CommonStatusEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 01:00
 * @Description:
 */
@Service("homeworkProcessService")
public class HomeworkProcessServiceImpl extends MessageProcessServiceImpl implements IMessageProcessService {

    @Override
    public boolean create(Message message) {

        //TODO 新建作业业务表数据
        super.create(message);
        return true;
    }

    @Override
    public List<Message> query(MessageQueryParam queryParam, String fromUserId) {
        List<Message> messages = super.query(queryParam, fromUserId);
        //TODO 处理查询记录
        return messages;
    }

    @Override
    public boolean update(Message message) {
        if (CommonStatusEnum.DELETE_D.getValue().equals(message.getStatus())) {
            return super.update(message);
        } else if (CommonStatusEnum.CENCEL.getValue().equals(message.getStatus())) {
            //撤销作业
            //TODO 已有交作业记录不准撤销验证等
            return super.update(message);
        }
        return super.update(message);
    }

}
