package com.cdeercloud.app.business.shara.service.impl;

import com.cdeercloud.app.base.message.dto.MessageQueryParam;
import com.cdeercloud.app.base.message.service.IMessageProcessService;
import com.cdeercloud.app.base.message.service.impl.MessageProcessServiceImpl;
import com.cdeercloud.app.entities.message.entity.Message;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("shareProcessService")
public class ShareProcessServiceImpl extends MessageProcessServiceImpl implements IMessageProcessService {

	@Override
	public boolean create(Message message) {
		super.create(message);
		return true;
	}

    @Override
    public List<Message> query(MessageQueryParam queryParam, String fromUserId) {
        return super.query(queryParam, fromUserId);
    }

    @Override
    public boolean update(Message message) {
        return super.update(message);
    }

}
