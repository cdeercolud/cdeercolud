package com.cdeercloud.app.base.message.service.impl;

import com.cdeercloud.app.base.message.dto.MessageQueryParam;
import com.cdeercloud.app.base.message.service.IMessageHanlderService;
import com.cdeercloud.app.base.message.service.IMessageProcessService;
import com.cdeercloud.app.common.enums.mq.MqTopicEnum;
import com.cdeercloud.app.common.util.MessageProcessBeanUtil;
import com.cdeercloud.app.entities.message.entity.Message;
import com.cdeercloud.common.constant.CommonConstant;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.mq.producer.CdeerProducer;
import com.cdeercloud.common.util.JsonUtils;
import com.cdeercloud.common.util.SpringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MessageHanlderServiceImpl implements IMessageHanlderService {

    private static final Logger logger = LoggerFactory.getLogger(IMessageHanlderService.class);

	@Resource(name = "framework_mq_messageProcessTopicProducer")
	private CdeerProducer<String, Object> producer;

	@Autowired
	private ITokenUtil tokenUtil;

	@Resource(name = "messageProcessService")
	private IMessageProcessService messageProcessService;

	@Override
    public boolean create(Message message) {
		this.messageValidata(message);
        logger.info("userId【{}】 -> createMessage start 【{}】", this.tokenUtil.getUserId(), JsonUtils.toJson(message));
		ProducerRecord<String, Object> record = new ProducerRecord<String, Object>(MqTopicEnum.MESSAGE_TOPIC.getTopic(), message);
        this.producer.send(record, callback);
		return true;
	}

	private Callback callback = new Callback() {
		@Override
		public void onCompletion(RecordMetadata metadata, Exception exception) {
			if (exception != null) {
				exception.printStackTrace();
                throw new CdeerException("操作失败！");
			}
		}
	};

	/**
	 * 验证数据是否正确
	 * 
	 * @param message
	 */
	private void messageValidata(Message message) {
        this.messageProcessService = SpringUtil.getBean(MessageProcessBeanUtil.getMessageProcessBeanName(message.getType()), IMessageProcessService.class);
        this.messageProcessService.messageValidata(message);
	}

	@Override
    public List<Message> query(MessageQueryParam queryParam) {

		if ((queryParam == null) || StringUtils.isEmpty(queryParam.getGroupCode())) {
            throw new CdeerException(CommonConstant.PARAM_IS_NOT_NULL);
        }

        List<Message> messages = null;
        String fromUserId = this.tokenUtil.getUserId();

        if (StringUtils.isEmpty(queryParam.getType())) {
            // 首页查询
            messages = this.messageProcessService.query(queryParam, fromUserId);
            return this.dealMessages(messages);
        } else {
            // 单个应用查询
            //返回对应的Bean
            this.messageProcessService = SpringUtil.getBean(MessageProcessBeanUtil.getMessageProcessBeanName(queryParam.getType()), IMessageProcessService.class);
            return this.messageProcessService.query(queryParam, fromUserId);
        }

    }

    @Override
    public boolean update(Message message) {

        if (message == null || StringUtils.isEmpty(message.getId())) {
            throw new CdeerException(CommonConstant.PARAM_IS_NOT_NULL);
        }

        logger.info("userId【{}】 -> updateMessage start 【{}】", this.tokenUtil.getUserId(), JsonUtils.toJson(message));
        ProducerRecord<String, Object> record = new ProducerRecord<String, Object>(MqTopicEnum.MESSAGE_TOPIC.getTopic(), message);
        this.producer.send(record, callback);
        return true;

    }

    /**
     * 处理首页相关消息
     *
     * @param messages
     * @return
     */
    private List<Message> dealMessages(List<Message> messages) {
        if (CollectionUtils.isEmpty(messages)) {
            return null;
        }

        Map<String, List<Message>> messageMaps = new HashMap<String, List<Message>>();
        for (Message message : messages) {
            if (messageMaps.containsKey(message.getType())) {
                messageMaps.get(message.getType()).add(message);
            } else {
                List<Message> msgs = new ArrayList<>();
                msgs.add(message);
                messageMaps.put(message.getType(), msgs);
            }
        }

        messages = new ArrayList<Message>();
        for (String type : messageMaps.keySet()) {
            this.messageProcessService = SpringUtil.getBean(MessageProcessBeanUtil.getMessageProcessBeanName(type), IMessageProcessService.class);
            messages.addAll(this.messageProcessService.dealMessages(messageMaps.get(type)));
        }

        //对消息进行排序
        messages.sort((Message m1, Message m2) -> m1.getSerialId().compareTo(m2.getSerialId()));
        return messages;
    }

}
