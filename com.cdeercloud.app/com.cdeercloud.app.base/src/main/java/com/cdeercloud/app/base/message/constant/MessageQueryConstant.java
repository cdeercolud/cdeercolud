package com.cdeercloud.app.base.message.constant;

/**
 * @author looyii
 */
public class MessageQueryConstant {

    /**
     * 创建人消息列表
     */
    public static final String QUERY_PARAM_CREATE = "CREATE";

    /**
     * 相关人消息列表
     */
    public static final String QUERY_PARAM_TO = "TO";

}
