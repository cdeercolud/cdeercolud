package com.cdeercloud.app.base.collect.service.impl;

import com.cdeercloud.app.base.collect.service.ICollectService;
import com.cdeercloud.app.entities.message.entity.MessageCollect;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.orm.vo.ParamMap;
import com.cdeercloud.common.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 21:14
 * @Description:
 */
@Service
public class CollectServiceImpl extends MybatisServiceImpl implements ICollectService {

    private IEntityStatement<MessageCollect> messageCollectDao;

    @Autowired
    private ITokenUtil tokenUtil;

    @Override
    public List<MessageCollect> listCollect() {
        ParamMap param = new ParamMap();
        param.add("userId", this.tokenUtil.getUserId());
        param.add("status", CommonStatusEnum.NORMAL.getValue());
        return this.messageCollectDao.findBy(param);
    }

    @Override
    public boolean saveCollect(String msgId) {

        boolean add = true;
        String userId = this.tokenUtil.getUserId();
        Date now = DateUtil.getNow();
        ParamMap param = new ParamMap();
        param.add("userId", userId);
        param.add("msgId", msgId);
        MessageCollect collect = this.messageCollectDao.findFirstBy(param);
        if (collect == null) {
            collect = new MessageCollect();
            collect.setMsgId(msgId);
            collect.setUserId(userId);
            collect.setStatus(CommonStatusEnum.NORMAL.getValue());
            collect.setCreateTime(now);
            collect.setModifyTime(now);
            this.messageCollectDao.create(collect);
        } else if (CommonStatusEnum.NORMAL.getValue().equals(collect.getStatus())) {
            add = false;
            collect.setStatus(CommonStatusEnum.DELETE_D.getValue());
            this.messageCollectDao.update(collect);
        } else {
            collect.setStatus(CommonStatusEnum.NORMAL.getValue());
            this.messageCollectDao.update(collect);
        }

        // 更新消息点赞数量
        param = new ParamMap();
        if (add) {
            param.add("addPraise", 1);
        } else {
            param.add("addPraise", -1);
        }
        param.add("msgId", msgId);
        this.findByStatement("updateMessagePraiseCount", msgId);
        // TODO 消息推送
        return true;

    }

}