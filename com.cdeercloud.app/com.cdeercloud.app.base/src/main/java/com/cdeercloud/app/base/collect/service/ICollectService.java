package com.cdeercloud.app.base.collect.service;

import com.cdeercloud.app.entities.message.entity.MessageCollect;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 21:14
 * @Description:
 */
public interface ICollectService {

    /**
     * 我的收藏
     *
     * @return
     */
    public List<MessageCollect> listCollect();

    /**
     * 添加收藏
     *
     * @param msgId
     * @return
     */
    public boolean saveCollect(String msgId);

}
