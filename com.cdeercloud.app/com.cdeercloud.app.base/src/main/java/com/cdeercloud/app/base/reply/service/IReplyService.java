package com.cdeercloud.app.base.reply.service;

import com.cdeercloud.app.entities.message.entity.MessageReply;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 17:29
 * @Description:
 */
public interface IReplyService {

    public List<MessageReply> listReply(List<String> msgIds);
}
