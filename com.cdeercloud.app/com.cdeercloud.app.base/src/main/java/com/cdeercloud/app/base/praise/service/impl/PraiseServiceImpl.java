package com.cdeercloud.app.base.praise.service.impl;

import com.cdeercloud.app.base.praise.service.IPraiseService;
import com.cdeercloud.app.entities.message.entity.MessagePraise;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.orm.vo.ParamMap;
import com.cdeercloud.common.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 14:14
 * @Description:
 */
@Service
public class PraiseServiceImpl extends MybatisServiceImpl implements IPraiseService {

    @EntityStatement
    private IEntityStatement<MessagePraise> messagePraiseDao;

    @Autowired
    private ITokenUtil tokenUtil;

    @Override
    public List<MessagePraise> listPraise(List<String> msgIds) {
        Map param = new HashMap<>();
        param.put("msgIds", msgIds);
        param.put("status", CommonStatusEnum.NORMAL.getValue());
        return this.findByStatement("listPraise", param);
    }

    @Override
    public boolean savePraise(String msgId) {

        boolean add = true;
        String userId = this.tokenUtil.getUserId();
        Date now = DateUtil.getNow();
        ParamMap param = new ParamMap();
        param.add("userId", userId);
        param.add("msgId", msgId);
        MessagePraise praise = this.messagePraiseDao.findFirstBy(param);
        if (praise == null) {
            praise = new MessagePraise();
            praise.setMsgId(msgId);
            praise.setUserId(userId);
            praise.setStatus(CommonStatusEnum.NORMAL.getValue());
            praise.setCreateTime(now);
            praise.setModifyTime(now);
            this.messagePraiseDao.create(praise);
        } else if (CommonStatusEnum.NORMAL.getValue().equals(praise.getStatus())) {
            add = false;
            praise.setStatus(CommonStatusEnum.DELETE_D.getValue());
            this.messagePraiseDao.update(praise);
        } else {
            praise.setStatus(CommonStatusEnum.NORMAL.getValue());
            this.messagePraiseDao.update(praise);
        }

        // 更新消息点赞数量
        param = new ParamMap();
        if (add) {
            param.add("addPraise", 1);
        } else {
            param.add("addPraise", -1);
        }
        param.add("msgId", msgId);
        this.findByStatement("updateMessagePraiseCount", msgId);
        // TODO 消息推送
        return true;
    }

}
