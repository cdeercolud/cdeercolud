package com.cdeercloud.app.base.person.service;

import com.cdeercloud.app.entities.message.entity.MessagePerson;

import java.util.List;

public interface IPersonService {

	/**
	 * 创建相关用户
	 * 
	 * @param persons
	 */
	public void createPersons(List<MessagePerson> persons);

	public List<MessagePerson> listPerson(List<String> msgIds);

}
