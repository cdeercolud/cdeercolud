package com.cdeercloud.app.base.message.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "消息列表查询")
public class MessageQueryParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6670616458339118261L;

	@ApiModelProperty(value = "* 查询类型 (MessageQueryConstant类 GROUP TYPE TO CREATE PARENTID)")
	private String queryType;

	@ApiModelProperty(value = "用户id User.id", required = false)
	private String userId;

	@ApiModelProperty(value = "父消息id Message.id", required = false)
	private String parentId;

	@ApiModelProperty(value = "群号", required = false)
	private String groupCode;

	@ApiModelProperty(value = "应用类型", required = false)
	private String type;

	@ApiModelProperty(value = "群消息序号（每个群的消息的序号）", required = false)
	private String serialId;

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSerialId() {
		return serialId;
	}

	public void setSerialId(String serialId) {
		this.serialId = serialId;
	}

}
