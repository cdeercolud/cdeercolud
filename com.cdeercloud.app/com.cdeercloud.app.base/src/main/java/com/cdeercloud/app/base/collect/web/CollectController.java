package com.cdeercloud.app.base.collect.web;

import com.cdeercloud.app.base.collect.service.ICollectService;
import com.cdeercloud.app.entities.message.entity.MessageCollect;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 13:55
 * @Description:
 */
@Api(value = "消息收藏", tags = {"消息收藏 CollectController"})
@RestController
@RequestMapping("collect")
public class CollectController {

    @Autowired
    private ICollectService collectService;

    @GetMapping("/save/{msgId}")
    @ApiOperation(value = "添加收藏", notes = "添加收藏")
    public boolean saveCollect(@ApiParam(value = "消息id", required = true) @PathVariable("msgId") String msgId) {
        return this.collectService.saveCollect(msgId);
    }

    @GetMapping("/list")
    @ApiOperation(value = "收藏列表", notes = "收藏列表")
    public List<MessageCollect> listCollect() {
        return this.collectService.listCollect();
    }

}
