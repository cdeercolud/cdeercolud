package com.cdeercloud.app.base.praise.web;

import com.cdeercloud.app.base.praise.service.IPraiseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 13:55
 * @Description:
 */
@Api(value = "消息点赞", tags = {"消息点赞 PraiseController"})
@RestController
@RequestMapping("praise")
public class PraiseController {

    @Autowired
    private IPraiseService praiseService;

    @GetMapping("/save/{msgId}")
    @ApiOperation(value = "点赞列表", notes = "消息点赞用户列表")
    public boolean savePraise(@ApiParam(value = "消息id", required = true) @PathVariable("msgId") String msgId) {
        return this.praiseService.savePraise(msgId);
    }

}
