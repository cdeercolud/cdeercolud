package com.cdeercloud.app.base.message.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cdeercloud.app.base.message.dto.MessageQueryParam;
import com.cdeercloud.app.entities.message.entity.Message;

/**
 *
 * 应用消息接口
 * @author looyii
 * @date
 */
public interface IMessageHanlderService {

    /**
     * 创建应用消息
     * @param message
     * @return
     */
	public boolean create(Message message);

	/**
	 * 查询展示页面消息
	 * 
	 * @param queryParam
	 * @return
	 */
	public List<Message> query(MessageQueryParam queryParam);

	/**
	 * 修改应用消息
	 * @param message
	 * @return
	 */
    public boolean update(Message message);
}
