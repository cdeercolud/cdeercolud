package com.cdeercloud.app.base.message.web;

import com.cdeercloud.app.base.message.dto.MessageQueryParam;
import com.cdeercloud.app.base.message.service.IMessageHanlderService;
import com.cdeercloud.app.entities.message.entity.Message;
import com.cdeercloud.common.web.CdeerController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * 
 * @author looyii
 *
 */
@Api(value = "应用消息", tags = {"应用消息 MessageController"})
@RestController
@RequestMapping("message")
public class MessageController extends CdeerController {

	@Autowired
	private IMessageHanlderService messageService;

	@PostMapping("/create")
    @ApiOperation(value = "创建应用消息", notes = "根据Message实体对象创建")
	public boolean create(@ApiParam(required = true, value = "Message 实体参数") @RequestBody(required = true) Message message) {
		return this.messageService.create(message);
	}

	@GetMapping("/query/{groupCode}")
    @ApiOperation(value = "查询应用消息列表", notes = "根据MessageQueryParam实体参数条件")
	public List<Message> query(@ApiParam(required = true, value = "MessageQueryParam 实体参数") MessageQueryParam queryParam) {
		return this.messageService.query(queryParam);
	}

	@PostMapping("/update")
	@ApiOperation(value = "修改应用消息", notes = "根据Message实体对象修改")
	public boolean update(@ApiParam(required = true, value = "Message 实体参数") @RequestBody(required = true) Message message) {
		return this.messageService.update(message);
	}

}
