package com.cdeercloud.app.base.praise.service;

import com.cdeercloud.app.entities.message.entity.MessagePraise;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 14:13
 * @Description:
 */
public interface IPraiseService {

    public List<MessagePraise> listPraise(List<String> msgIds);

    /**
     * 添加点赞
     *
     * @param msgId
     * @return
     */
    public boolean savePraise(String msgId);

}
