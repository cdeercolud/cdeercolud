package com.cdeercloud.app.base.media.service.impl;

import com.cdeercloud.app.base.media.service.IMediaService;
import com.cdeercloud.app.entities.message.entity.MessageMedia;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.orm.vo.ParamMap;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MediaServiceImpl extends MybatisServiceImpl implements IMediaService {

	@EntityStatement
	private IEntityStatement<MessageMedia> messageMediaDao;

	@Override
	public void createMedias(List<MessageMedia> medias) {
		this.messageMediaDao.batchCreate(medias);
	}

	@Override
    public List<MessageMedia> listMedia(List<String> msgIds) {
        ParamMap paramMap = new ParamMap();
        paramMap.add("msgId", msgIds);
        paramMap.add("status", CommonStatusEnum.NORMAL.getValue());
		return this.messageMediaDao.findBy("msgId", msgIds);
	}

}
