package com.cdeercloud.app.base.user.web;

import com.cdeercloud.app.base.user.service.IUserService;
import com.cdeercloud.common.entities.user.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 13:55
 * @Description:
 */
@Api(value = "用户", tags = {"用户 UserController"})
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("/list/{groupCode}")
    @ApiOperation(value = "收藏列表", notes = "收藏列表")
    public List<User> listUserByGroupCode(@ApiParam(value = "用户编号", required = true) @PathVariable("groupCode") String groupCode) {
        return this.userService.listUserByGroupCode(groupCode);
    }

}
