package com.cdeercloud.app.base.readed.service;

import com.cdeercloud.app.entities.message.entity.Message;
import com.cdeercloud.app.entities.message.entity.MessageReaded;

import java.util.List;

public interface IReadedService {

    /**
     * 创建消息已阅
     *
     * @param messages
     */
    public void createReadeds(List<Message> messages);

    /**
     * 查询已阅用户列表
     *
     * @param msgId
     * @return
     */
    public List<MessageReaded> listReaded(String msgId);

}
