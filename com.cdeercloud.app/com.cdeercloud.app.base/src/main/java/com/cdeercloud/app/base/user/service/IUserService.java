package com.cdeercloud.app.base.user.service;

import com.cdeercloud.common.entities.user.entity.User;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 22:55
 * @Description:
 */
public interface IUserService {

    /**
     * 查询群组下的用户列表
     *
     * @param groupCode
     * @return
     */
    List<User> listUserByGroupCode(String groupCode);

}
