package com.cdeercloud.app.base.media.service;

import com.cdeercloud.app.entities.message.entity.MessageMedia;

import java.util.List;

public interface IMediaService {

	/**
	 * 创建消息附件
	 * 
	 * @param medias
	 */
	public void createMedias(List<MessageMedia> medias);

	/**
	 * 消息附件
	 * 
	 * @param msgIds
	 * @return
	 */
	public List<MessageMedia> listMedia(List<String> msgIds);

}
