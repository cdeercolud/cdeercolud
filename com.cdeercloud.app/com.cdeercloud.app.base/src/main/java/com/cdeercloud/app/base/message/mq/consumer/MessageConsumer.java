package com.cdeercloud.app.base.message.mq.consumer;

import com.cdeercloud.app.base.message.constant.MessageConstant;
import com.cdeercloud.app.base.message.service.IMessageProcessService;
import com.cdeercloud.app.common.util.MessageProcessBeanUtil;
import com.cdeercloud.app.entities.message.entity.Message;
import com.cdeercloud.common.cache.CacheService;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.mq.consumer.ConsumerHandler;
import com.cdeercloud.common.util.JsonUtils;
import com.cdeercloud.common.util.SpringUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("messageConsumer")
public class MessageConsumer implements ConsumerHandler<String, Message> {

	private static final Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

	@Autowired
	private CacheService cacheService;

	@Autowired
	private ITokenUtil tokenUtil;

	@Override
	public void doHandle(ConsumerRecords<String, Message> records) {

		for (ConsumerRecord<String, Message> record : records) {
			String userId = this.tokenUtil.getUserId();
			Message message = record.value();
			try {

				IMessageProcessService messageProcessService = SpringUtil.getBean(MessageProcessBeanUtil.getMessageProcessBeanName(message.getType()), IMessageProcessService.class);

				if (message.getId() == null) {
					String key = String.format(MessageConstant.REDIS_MESSAGE_SERIAL_ID, message.getGroupCode());
					String serialId = this.cacheService.get(key);
					if (serialId != null) {
						message.setSerialId(Integer.valueOf(serialId) + 1);
					} else {
						message.setSerialId(messageProcessService.getMaxSerialId() + 1);
					}
					this.cacheService.set(key, String.valueOf(message.getSerialId()));
					message.setUserId(this.tokenUtil.getUserId());
					messageProcessService.create(message);
					logger.info("【Message】userId【{}】 -> createMessage sucess :  appType【{}】 clientId【{}】", userId, message.getType(), JsonUtils.toJson(message));
				} else {

					messageProcessService.update(message);
					logger.info("【Message】userId【{}】 -> updateMessage sucess :  appType【{}】 clientId【{}】", userId, message.getType(), JsonUtils.toJson(message));
				}

			} catch (Exception e) {
				logger.error("【Message】userId【{}】 -> message handle fail : MessageData【{}】", userId, JsonUtils.toJson(message), e);
			}
		}
	}

}
