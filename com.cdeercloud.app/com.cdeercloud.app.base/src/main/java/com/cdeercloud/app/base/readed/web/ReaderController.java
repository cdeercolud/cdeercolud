package com.cdeercloud.app.base.readed.web;

import com.cdeercloud.app.base.readed.service.IReadedService;
import com.cdeercloud.app.entities.message.entity.MessageReaded;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 05:02
 * @Description:
 */
@Api(value = "消息已阅", tags = {"消息已阅 ReaderController"})
@RestController
@RequestMapping("readed")
public class ReaderController {

    @Autowired
    private IReadedService readedService;

    @GetMapping("/list/{msgId}")
    @ApiOperation(value = "已阅列表", notes = "消息已阅用户列表")
    public List<MessageReaded> listReaded(@ApiParam(value = "消息id", required = true) @PathVariable("msgId") String msgId) {
        return this.readedService.listReaded(msgId);
    }

}
