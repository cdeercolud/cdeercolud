package com.cdeercloud.app.base.message.service;

import com.cdeercloud.app.base.message.dto.MessageQueryParam;
import com.cdeercloud.app.entities.message.entity.Message;

import java.util.List;

public interface IMessageProcessService {

	/**
	 * 保存消息
	 * 
	 * @param message
	 * @return
	 */
    public boolean create(Message message);

	/**
	 * 验证数据是否正确
	 * 
	 * @param message
	 */
	public void messageValidata(Message message);

	/**
	 * 获取消息最大序号
	 * 
	 * @return
	 */
	public int getMaxSerialId();

	/**
	 * 查询页面上显示的消息， 有父子消息只显示父消息
	 * 
	 * @param queryParam
     * @param fromUserId
     * @return
	 */
    public List<Message> query(MessageQueryParam queryParam, String fromUserId);

	/**
	 * 处理消息
     *
	 * @param messages
	 * @return
	 */
    List<Message> dealMessages(List<Message> messages);

    /**
     * 修改消息
     *
     * @param message
     * @return
     */
    public boolean update(Message message);

}