package com.cdeercloud.app.base.readed.service.impl;

import com.cdeercloud.app.base.message.service.IMessageProcessService;
import com.cdeercloud.app.base.readed.service.IReadedService;
import com.cdeercloud.app.entities.message.entity.Message;
import com.cdeercloud.app.entities.message.entity.MessageReaded;
import com.cdeercloud.common.constant.CommonConstant;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.orm.vo.ParamMap;
import com.cdeercloud.common.util.DateUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class ReadedServiceImpl extends MybatisServiceImpl implements IReadedService {

	@EntityStatement
	private IEntityStatement<MessageReaded> readedDao;

    @EntityStatement
    private IEntityStatement<Message> messageDao;

    @EntityStatement
    private IMessageProcessService messageProcessService;

    @EntityStatement
    private ITokenUtil tokenUtil;

	@Override
    public void createReadeds(List<Message> messages) {

        Date now = DateUtil.getNow();
        String userId = this.tokenUtil.getUserId();
        List<String> addMsgIds = new ArrayList<String>();
        //已阅的消息不再重复添加已阅记录
        List<String> readedMsgIds = this.hasReadeds(this.getMsgIds(messages), userId);
        MessageReaded readed = null;
        List<MessageReaded> readeds = new ArrayList<MessageReaded>();
        for (Message message : messages) {
            if (!CollectionUtils.isEmpty(readedMsgIds)) {
                for (String msgId : readedMsgIds) {
                    if (msgId.equals(message.getId())) {
                        break;
                    }
                }
            }
            readed = new MessageReaded();
            readed.setUserId(userId);
            readed.setMsgId(message.getId());
            readed.setStatus(CommonStatusEnum.NORMAL.getValue());
            readed.setCreateTime(now);
            readed.setModifyTime(now);
            readeds.add(readed);
            addMsgIds.add(message.getId());
        }

        if (!CollectionUtils.isEmpty(addMsgIds)) {
            this.readedDao.batchCreate(readeds);
            // 更新消息已阅数量
            this.findByStatement("updateMessageReadedCount", addMsgIds);
        }

    }

    /**
     * 得到消息 ids
     *
     * @param messages
     * @return
     */
    private List<String> getMsgIds(List<Message> messages) {

        List<String> msgIds = new ArrayList<String>();
        for (Message message : messages) {
            msgIds.add(message.getId());
        }
        return msgIds;

    }

    @Override
    public List<MessageReaded> listReaded(String msgId) {

        Message message = this.messageDao.find(msgId);
        if (message == null) {
            throw new CdeerException(CommonConstant.DATA_NOT_AVAILABLE);
        }

        Map map = new HashMap();
        map.put("msgId", msgId);
        map.put("groupCode", message.getGroupCode());
        return this.findByStatement("listReaded", map);

    }

    /**
     * 已阅消息
     *
     * @param msgIds
     * @param userId
     * @return
     */
    private List<String> hasReadeds(List<String> msgIds, String userId) {

        ParamMap param = new ParamMap();
        param.add("msgId", msgIds);
        param.add("userId", userId);
        param.ne("status", CommonStatusEnum.DELETE_D.getValue());
        List<MessageReaded> readeds = this.readedDao.findBy(param);
        if (CollectionUtils.isEmpty(readeds)) {
            return null;
        }
        List<String> hasMsgIds = new ArrayList<String>();
        for (MessageReaded readed : readeds) {
            hasMsgIds.add(readed.getMsgId());
        }
        return hasMsgIds;

    }

}
