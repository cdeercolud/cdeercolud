package com.cdeercloud.app.base.person.service.impl;

import com.cdeercloud.app.base.person.service.IPersonService;
import com.cdeercloud.app.entities.message.entity.MessagePerson;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonServiceImpl extends MybatisServiceImpl implements IPersonService {

	@EntityStatement
	private IEntityStatement<MessagePerson> messagePersonDao;

	@Override
	public void createPersons(List<MessagePerson> persons) {
		this.messagePersonDao.batchCreate(persons);
	}

	@Override
	public List<MessagePerson> listPerson(List<String> msgIds) {
		if (CollectionUtils.isEmpty(msgIds)) {
			return null;
		}
		Map map = new HashMap<>();
		map.put("msgIds", msgIds);
		return this.findByStatement("listPerson", map);
	}

}
