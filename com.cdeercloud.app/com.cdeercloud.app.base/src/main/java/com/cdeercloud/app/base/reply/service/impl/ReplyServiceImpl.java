package com.cdeercloud.app.base.reply.service.impl;

import com.cdeercloud.app.base.reply.service.IReplyService;
import com.cdeercloud.app.entities.message.entity.MessageReply;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 17:32
 * @Description:
 */
@Service
public class ReplyServiceImpl extends MybatisServiceImpl implements IReplyService {

    @Override
    public List<MessageReply> listReply(List<String> msgIds) {
        return this.findByStatement("listReply", msgIds);
    }
}
