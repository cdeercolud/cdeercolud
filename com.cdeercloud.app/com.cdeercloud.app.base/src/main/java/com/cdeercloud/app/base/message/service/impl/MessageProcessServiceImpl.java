package com.cdeercloud.app.base.message.service.impl;

import com.cdeercloud.app.base.media.service.IMediaService;
import com.cdeercloud.app.base.message.constant.MessageQueryConstant;
import com.cdeercloud.app.base.message.dto.MessageQueryParam;
import com.cdeercloud.app.base.message.service.IMessageProcessService;
import com.cdeercloud.app.base.person.service.IPersonService;
import com.cdeercloud.app.base.praise.service.IPraiseService;
import com.cdeercloud.app.base.readed.service.IReadedService;
import com.cdeercloud.app.base.reply.service.IReplyService;
import com.cdeercloud.app.common.enums.message.MessageEnum;
import com.cdeercloud.app.common.enums.mq.MqTopicEnum;
import com.cdeercloud.app.entities.message.entity.*;
import com.cdeercloud.common.constant.CommonConstant;
import com.cdeercloud.common.core.exception.CdeerException;
import com.cdeercloud.common.enums.CommonStatusEnum;
import com.cdeercloud.common.enums.PersonTypeEnum;
import com.cdeercloud.common.login.service.ITokenUtil;
import com.cdeercloud.common.mq.producer.CdeerProducer;
import com.cdeercloud.common.orm.EntityStatement;
import com.cdeercloud.common.orm.service.IEntityStatement;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;
import com.cdeercloud.common.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("messageProcessService")
public class MessageProcessServiceImpl extends MybatisServiceImpl implements IMessageProcessService {

    @EntityStatement
    private IEntityStatement<Message> messageDao;

    @EntityStatement
    private IEntityStatement<MessageExpand> messageExpandDao;

    @Resource(name = "framework_mq_readedProcessTopicProducer")
    private CdeerProducer<String, Object> producer;

    @Autowired
    private IPersonService personService;

    @Autowired
    private IMediaService mediaService;

    @Autowired
    private IReadedService readedService;

    @Autowired
    private IPraiseService praiseService;

    @Autowired
    private IReplyService replyService;

    @Autowired
    private ITokenUtil tokenUtil;

    @Override
    public boolean create(Message message) {

        this.setMessageDefaultValue(message);
        this.messageDao.create(message);
        this.createExpand(message);
        this.createMedias(message);
        this.createPersons(message);

        List<Message> messages = new ArrayList<Message>();
        messages.add(message);
        ProducerRecord<String, Object> record = new ProducerRecord<String, Object>(MqTopicEnum.READED_TOPIC.getTopic(), messages);
        this.producer.send(record);
        return true;

    }

    /**
     * 设置消息的默认参数
     *
     * @param message
     */
    private void setMessageDefaultValue(Message message) {
        Date now = DateUtil.getNow();
        message.setCreateTime(now);
        message.setModifyTime(now);
        message.setStatus(CommonStatusEnum.NORMAL.getValue());
        if (message.getIsComment() == null) {
            message.setIsComment(MessageEnum.IS_COMMENT_Y.getValue());
        } else {
            message.setIsComment(MessageEnum.IS_COMMENT_N.getValue());
        }

        if (message.getIsPrivate() == null) {
            message.setIsPrivate(MessageEnum.IS_PRIVATE_Y.getValue());
        } else {
            message.setIsPrivate(MessageEnum.IS_PRIVATE_N.getValue());
        }

        if (message.getParentId() == null) {
            message.setIsShow(MessageEnum.IS_SHOW_Y.getValue());
        } else {
            message.setIsShow(MessageEnum.IS_SHOW_N.getValue());
        }

        message.setReadCount(1);
        message.setChildCount(0);
        message.setPraiseCount(0);
        message.setShareCount(0);
        message.setReviewCount(0);
        message.setCollectCount(0);
        message.setReplyCount(0);

    }

    /**
     * 消息扩展
     *
     * @param message
     */
    private void createExpand(Message message) {

        if (message.getMessageExpand() == null) {
            message.setMessageExpand(new MessageExpand());
        }
        message.getMessageExpand().setMsgId(message.getId());
        if (message.getMessageExpand().getClientSource() == null) {
            message.getMessageExpand().setClientSource(MessageEnum.CLIENT_SOURCE_WEB.getValue());
        }
        this.messageExpandDao.create(message.getMessageExpand());

    }

    /**
     * 消息相关人
     *
     * @param message
     */
    private void createPersons(Message message) {

        if (CollectionUtils.isEmpty(message.getPersons())) {
            message.setPersons(new ArrayList<MessagePerson>());
        }
        MessagePerson person = new MessagePerson();
        person.setMsgId(message.getId());
        person.setType(PersonTypeEnum.CREATE.getValue());
        person.setRelationId(message.getUserId());
        person.setStatus(CommonStatusEnum.NORMAL.getValue());
        person.setCreateTime(message.getCreateTime());
        person.setModifyTime(message.getModifyTime());
        person.setPersonType(PersonTypeEnum.USER.getValue());
        message.getPersons().add(person);
        this.personService.createPersons(message.getPersons());

    }

    /**
     * 消息附件
     *
     * @param message
     */
    private void createMedias(Message message) {
        if (CollectionUtils.isEmpty(message.getMedias())) {
            return;
        }

        for (MessageMedia media : message.getMedias()) {
            media.setMsgId(message.getId());
            media.setCreateTime(message.getCreateTime());
            media.setModifyTime(message.getModifyTime());
        }

        this.mediaService.createMedias(message.getMedias());
    }

    /**
     * 消息验证
     */
    @Override
    public void messageValidata(Message message) {
        if ((message == null) || StringUtils.isEmpty(message.getGroupCode()) || StringUtils.isEmpty(message.getType()) || StringUtils.isEmpty(message.getContent())) {
            throw new CdeerException(CommonConstant.PARAM_IS_NOT_NULL);
        }
    }

    @Override
    public int getMaxSerialId() {
        return this.messageDao.count();
    }

    @Override
    public List<Message> query(MessageQueryParam queryParam, String fromUserId) {

        List<Message> messages = null;
        if (MessageQueryConstant.QUERY_PARAM_CREATE.equals(queryParam.getQueryType())) {
            // TODO 创建人消息列表
        } else if (MessageQueryConstant.QUERY_PARAM_TO.equals(queryParam.getQueryType())) {
            // TODO 相关人消息列表
        } else {
            messages = this.findByStatement("listMessage", queryParam);
        }

        if (CollectionUtils.isEmpty(messages)) {
            return null;
        }

        List<String> msgIds = this.getMsgIds(messages);
        this.getMessageExpand(messages, msgIds);
        this.listMessagePerson(messages, msgIds);
        this.listMessageMedia(messages, msgIds);
        this.listMessagePraise(messages, msgIds);
        this.listMessageReply(messages, msgIds);

        ProducerRecord<String, Object> record = new ProducerRecord<String, Object>(MqTopicEnum.READED_TOPIC.getTopic(), messages);
        this.producer.send(record);

        return messages;

    }

    /**
     * 查询消息扩展表数据
     *
     * @param messages
     */
    private void getMessageExpand(List<Message> messages, List<String> msgIds) {

        List<MessageExpand> messageExpands = this.messageExpandDao.findBy("msgId", msgIds);
        for (Message message : messages) {
            for (MessageExpand messageExpand : messageExpands) {
                if (message.getId().equals(messageExpand.getMsgId())) {
                    message.setMessageExpand(messageExpand);
                    continue;
                }
            }
        }

    }

    /**
     * 得到消息 ids
     *
     * @param messages
     * @return
     */
    private List<String> getMsgIds(List<Message> messages) {

        List<String> msgIds = new ArrayList<String>();
        for (Message message : messages) {
            msgIds.add(message.getId());
        }
        return msgIds;

    }

    /**
     * 相关用户
     *
     * @param messages
     */
    private void listMessagePerson(List<Message> messages, List<String> msgIds) {

        List<MessagePerson> persons = this.personService.listPerson(msgIds);
        for (Message message : messages) {
            message.setPersons(new ArrayList<>());
            for (MessagePerson person : persons) {
                if (message.getId().equals(person.getMsgId())) {
                    message.getPersons().add(person);
                }
            }
        }

    }

    /**
     * 消息附件
     *
     * @param messages
     */
    private void listMessageMedia(List<Message> messages, List<String> msgIds) {

        List<MessageMedia> medias = this.mediaService.listMedia(msgIds);
        if (CollectionUtils.isEmpty(medias)) {
            return;
        }

        for (Message message : messages) {
            message.setMedias(new ArrayList<>());
            for (MessageMedia media : medias) {
                if (message.getId().equals(media.getMsgId())) {
                    message.getMedias().add(media);
                }
            }
        }

    }

    /**
     * 消息点赞
     *
     * @param messages
     */
    private void listMessagePraise(List<Message> messages, List<String> msgIds) {

        List<MessagePraise> praises = this.praiseService.listPraise(msgIds);
        if (CollectionUtils.isEmpty(praises)) {
            return;
        }

        for (Message message : messages) {
            message.setMedias(new ArrayList<>());
            for (MessagePraise praise : praises) {
                if (message.getId().equals(praise.getMsgId())) {
                    message.getPraises().add(praise);
                }
            }
        }

    }

    /**
     * 消息回复
     *
     * @param messages
     */
    private void listMessageReply(List<Message> messages, List<String> msgIds) {

        List<MessageReply> replys = this.replyService.listReply(msgIds);
        if (CollectionUtils.isEmpty(replys)) {
            return;
        }

        for (Message message : messages) {
            message.setMedias(new ArrayList<>());
            for (MessageReply reply : replys) {
                if (message.getId().equals(reply.getMsgId())) {
                    message.getReplys().add(reply);
                }
            }
        }

    }

    @Override
    public List<Message> dealMessages(List<Message> messages) {
        return messages;
    }

    @Override
    public boolean update(Message message) {
        this.messageDao.update(message, false);
        return true;
    }



}

