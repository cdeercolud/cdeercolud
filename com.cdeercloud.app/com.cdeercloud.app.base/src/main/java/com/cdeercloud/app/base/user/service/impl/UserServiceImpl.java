package com.cdeercloud.app.base.user.service.impl;

import com.cdeercloud.app.base.user.service.IUserService;
import com.cdeercloud.common.entities.user.entity.User;
import com.cdeercloud.common.orm.service.impl.MybatisServiceImpl;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 23:29
 * @Description:
 */
public class UserServiceImpl extends MybatisServiceImpl implements IUserService {

    @Override
    public List<User> listUserByGroupCode(String groupCode) {
        return this.findByStatement("listUserByGroupCode", groupCode);
    }

}
