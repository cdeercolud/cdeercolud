package com.cdeercloud.app.base.message.mq.consumer;

import com.cdeercloud.app.base.readed.service.IReadedService;
import com.cdeercloud.app.entities.message.entity.Message;
import com.cdeercloud.common.mq.consumer.ConsumerHandler;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Auther: looyii
 * @Date: 2018/7/21 17:00
 * @Description:
 */
@Component("readedConsumer")
public class ReadedConsumer implements ConsumerHandler<String, List<Message>> {

    private static final Logger logger = LoggerFactory.getLogger(ReadedConsumer.class);

    @Autowired
    private IReadedService readedService;

    @Override
    public void doHandle(ConsumerRecords<String, List<Message>> records) {

        for (ConsumerRecord<String, List<Message>> record : records) {

            List<Message> messages = record.value();

            this.readedService.createReadeds(messages);

        }
    }

}