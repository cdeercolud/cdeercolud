package com.cdeercloud.app.common.enums.message;

public enum MessageEnum {

	// 可以评论
	IS_COMMENT_Y("1"),
	// 不可以评论
	IS_COMMENT_N("0"),
	// 公开
	IS_PRIVATE_Y("1"),
	// 私有
	IS_PRIVATE_N("2"),
	// 消息来源WEB
	CLIENT_SOURCE_WEB("1"),
	// 消息来源IOS
	CLIENT_SOURCE_IOS("2"),
	// 消息来源ANDROID
	CLIENT_SOURCE_ANDROID("3"),
	// 页面显示
	IS_SHOW_Y("1"),
	// 页面不显示
	IS_SHOW_N("N");

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private MessageEnum(String value) {
		this.value = value;
	}

}
