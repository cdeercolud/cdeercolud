package com.cdeercloud.app.common.util;

import com.cdeercloud.common.enums.AppEnum;

/**
 * 获取bean
 * 
 * @author looyii
 *
 */
public class MessageProcessBeanUtil {

	public static String getMessageProcessBeanName(String type) {
		String messageBeanName = "messageProcessService";
		AppEnum appTypes[] = AppEnum.values();
		for (AppEnum appType : appTypes) {
			if (appType.getAppType().equals(type)) {
				messageBeanName = appType.getAppBean() + "ProcessService";
			}
		}
		return messageBeanName;
	}

	public static void main(String[] args) {
		System.out.println(getMessageProcessBeanName("1001"));
	}
}
