package com.cdeercloud.app.common.enums.mq;

/**
 * TOPIC
 */
public enum MqTopicEnum {

	//消息TOPIC
	MESSAGE_TOPIC("message"),
	//已阅topic
	READED_TOPIC("readed");

	private String topic;

	private MqTopicEnum(String topic) {
		this.topic = topic;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
